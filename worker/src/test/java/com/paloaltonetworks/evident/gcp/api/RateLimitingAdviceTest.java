package com.paloaltonetworks.evident.gcp.api;

import com.paloaltonetworks.evident.gcp.models.request.TenantAwareRequest;
import org.aspectj.lang.JoinPoint;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

public class RateLimitingAdviceTest {
    @InjectMocks
    RateLimitingAdvice advice;
    @Mock
    private GCPRateLimitingService rateLimitingService;
    @Mock
    private JoinPoint jp;
    @Mock
    private TenantAwareRequest request;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void beforeExecSuccess() throws Throwable {
        given(jp.getArgs()).willReturn(new Object[]{request});
        given(request.getApiType()).willReturn(GCPApiType.DEFAULT);
        given(rateLimitingService.getRateLimitLock(any(), any(), eq(GCPApiType.DEFAULT))).willReturn(true);

        // when
        advice.beforeExec(jp);

        // then:
        // no exception is pass
    }

    @Test
    public void beforeExecFailure() throws Throwable {
        given(jp.getArgs()).willReturn(new Object[]{"1", request});
        given(request.getApiType()).willReturn(GCPApiType.DEFAULT);
        given(rateLimitingService.getRateLimitLock(any(), any(), eq(GCPApiType.DEFAULT))).willReturn(false);

        // when
        try {
            advice.beforeExec(jp);
            Assertions.fail("Expected exception was not thrown");
        } catch (RuntimeException throwable) {
            // then:
            // Throwing RuntimException is pass
        }
    }

    @Test
    public void beforeExecInvalidRequest() throws Throwable {
        given(jp.getArgs()).willReturn(new Object[]{"1"});
        given(request.getApiType()).willReturn(GCPApiType.DEFAULT);
        given(rateLimitingService.getRateLimitLock(any(), any(), eq(GCPApiType.DEFAULT))).willReturn(true);

        // when
        try {
            advice.beforeExec(jp);
            Assertions.fail("Expected exception was not thrown");
        } catch (IllegalArgumentException throwable) {
            // then:
            // Throwing RuntimException is pass
        }
    }

}