package com.paloaltonetworks.evident.gcp.token;

import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.esp.model.ESPApiResponse;
import com.paloaltonetworks.evident.exceptions.GCPTokenException;
import com.paloaltonetworks.evident.utils.JsonUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class DefaultGCPTokenServiceTest {

    @Mock
    private ESPClient espClient;

    private GCPTokenService tokenService;
    private ESPApiResponse apiResponse;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        tokenService = new DefaultGCPTokenService(espClient);
        apiResponse = JsonUtils.decodeValue("{\n" +
                "   \"data\": {\n" +
                "     \"id\": \"1005\",\n" +
                "     \"type\": \"external_account_google\",\n" +
                "     \"attributes\": {\n" +
                "       \"service_account_id\": \"service-account-3-990\",\n" +
                "       \"email\": \"google3@example.com\",\n" +
                "       \"validation_errors\": null,\n" +
                "       \"created_at\": \"2018-06-27T01:51:51.326Z\",\n" +
                "       \"updated_at\": \"2018-06-27T01:51:51.326Z\",\n" +
                "       \"secret\": \"{\\\"type\\\":\\\"service_account\\\",\\\"project_id\\\":\\\"project-3-3\\\",\\\"private_key_id\\\":\\\"a528d5f457ce572fc569d58d115d0b98be12f1f7\\\",\\\"private_key\\\":\\\"-----BEGIN RSA PRIVATE KEY-----\\\\\\\\n-----END RSA PRIVATE KEY-----\\\\n\\\",\\\"client_email\\\":\\\"gcp-account-3@iam.gserviceaccount.example.com\\\",\\\"client_id\\\":\\\"266758035263760488369\\\",\\\"auth_uri\\\":\\\"https://accounts.google.com/o/oauth2/auth\\\",\\\"token_uri\\\":\\\"https://accounts.google.com/o/oauth2/token\\\",\\\"auth_provider_x509_cert_url\\\":\\\"https://www.googleapis.com/oauth2/v1/certs\\\",\\\"client_x509_cert_url\\\":\\\"https://www.googleapis.com/robot/v1/metadata/x509/gcp-pc-fe-test-account%40aperture-gmail-integration.iam.gserviceaccount.com\\\"}\"\n" +
                "     },\n" +
                "     \"relationships\": {\n" +
                "       \"external_account\": {\n" +
                "         \"links\": {\n" +
                "           \"related\": \"http://test.host/api/v2/external_accounts/1006.json\"\n" +
                "         }\n" +
                "       }\n" +
                "     }\n" +
                "   }\n" +
                " }", ESPApiResponse.class);
    }

    // get from local cache
    // pkcs8 invalid
    @Test
    public void getAuthTokenWithBadJson() throws Exception {
        when(espClient.gcpAccount(anyString())).thenReturn(apiResponse);
        try {
            tokenService.getOauthToken("1234");
            Assertions.fail("Expected exception was not thrown");
        } catch (GCPTokenException e) {
            // expected exception
        }
    }
}