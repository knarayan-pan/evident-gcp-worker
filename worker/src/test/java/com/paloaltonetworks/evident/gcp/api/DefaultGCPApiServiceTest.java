package com.paloaltonetworks.evident.gcp.api;

import org.junit.jupiter.api.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertTrue;

class DefaultGCPApiServiceTest {
    @Test
    void annotationTest() {
        Stream.of(DefaultGCPApiService.class.getDeclaredMethods())
                .filter(method -> Modifier.isPublic(method.getModifiers()))
                .forEach(method -> {
                    assertAnnotation(RateLimited.class, method);
                    assertAnnotation(Monitored.class, method);
                });
    }

    private void assertAnnotation(Class<? extends Annotation> annotation, Method method) {
        assertTrue(
                method.isAnnotationPresent(annotation),
                String.format("Public method %s should be annotated with @%s", method.getName(),
                        annotation.getSimpleName()));
    }
}