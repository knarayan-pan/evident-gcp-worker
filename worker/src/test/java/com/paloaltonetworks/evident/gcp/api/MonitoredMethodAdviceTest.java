package com.paloaltonetworks.evident.gcp.api;


import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.paloaltonetworks.evident.exceptions.GCPApiException;
import com.paloaltonetworks.evident.gcp.models.request.TenantAwareRequest;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

class MonitoredMethodAdviceTest {
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private ProceedingJoinPoint jp;
    @Mock
    private TenantAwareRequest request;
    @Mock
    private MetricRegistry metricRegistry;
    @Mock
    private GoogleJsonResponseException googleJsonResponseException;

    @InjectMocks
    private MonitoredMethodAdvice advice;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void aroundExecTest() throws Throwable {
        given(request.getExternalAccountId()).willReturn("123");
        given(request.getTenant()).willReturn("test-tenant");
        given(jp.getArgs()).willReturn(new Object[]{request});
        given(jp.getSignature().getName()).willReturn("testMethod");
        given(metricRegistry.timer(anyString())).willReturn(new Timer());

        advice.aroundExec(jp);
    }

    @Test
    public void aroundGCPApiExceptionErrorTest() throws Throwable {
        given(request.getExternalAccountId()).willReturn("123");
        given(request.getTenant()).willReturn("test-tenant");
        given(jp.getArgs()).willReturn(new Object[]{request});
        given(jp.getSignature().getName()).willReturn("testMethod");
        given(metricRegistry.timer(anyString())).willReturn(new Timer());

        given(jp.proceed())
                .willThrow(new GCPApiException(504, "testMessage"))
                .willThrow(new RuntimeException())
                .willThrow(new Exception())
                .willThrow(googleJsonResponseException)
                .willThrow(new IOException());

        Assertions.assertThrows(GCPApiException.class, () -> advice.aroundExec(jp));

        Assertions.assertThrows(RuntimeException.class, () -> advice.aroundExec(jp));

        Assertions.assertThrows(RuntimeException.class, () -> advice.aroundExec(jp));

        Assertions.assertThrows(GCPApiException.class, () -> advice.aroundExec(jp));

        Assertions.assertThrows(GCPApiException.class, () -> advice.aroundExec(jp));
    }


}