package com.paloaltonetworks.evident.gcp.api;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import java.util.Optional;

public class ResourceTypeTest {
    @Test
    public void resourceRequiresUpdate() throws Exception {
        assertTrue(ResourceType.GCE_INSTANCE.resourceRequiresUpdate("v1.compute.instances.setMetadata"));
        assertFalse(ResourceType.GCE_INSTANCE.resourceRequiresUpdate("v1.compute.instances.unknown"));
    }

    @Test
    public void resourceRequiresDelete() throws Exception {
        assertTrue(ResourceType.GCE_INSTANCE.resourceRequiresDelete("v1.compute.instances.delete"));
        assertFalse(ResourceType.GCE_INSTANCE.resourceRequiresDelete("v1.compute.instances.unknown"));
    }

    @Test
    public void getResourceIdLabelKey() throws Exception {
        assertEquals(Optional.empty(), ResourceType.GCE_INSTANCE.getResourceIdLabelKey());
    }

    @Test
    public void getZoneLabelKey() throws Exception {
        assertEquals(Optional.of("zone"), ResourceType.GCE_INSTANCE.getZoneLabelKey());
    }

    @Test
    public void getResourceTypeFilterString() throws Exception {
        assertEquals("resource.type=(project OR gce_instance OR " +
                        "gcs_bucket OR cloudsql_database OR dns_managed_zone OR " +
                        "gce_firewall_rule OR gce_network OR gce_subnetwork)",
                ResourceType.getResourceTypeFilterString());
    }

    @Test
    public void fromResourceTypeString() throws Exception {
        assertEquals(Optional.of(ResourceType.GCE_INSTANCE),
                ResourceType.fromResourceTypeString("gce_instance"));
        assertEquals(Optional.empty(),
                ResourceType.fromResourceTypeString("some-random"));
    }

}
