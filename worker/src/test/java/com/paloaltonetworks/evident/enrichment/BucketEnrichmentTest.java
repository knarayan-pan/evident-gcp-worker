package com.paloaltonetworks.evident.enrichment;

import com.google.api.services.storage.model.Bucket;
import com.google.api.services.storage.model.Buckets;
import com.google.api.services.storage.model.Policy;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.metadata.BucketMetadata;
import com.paloaltonetworks.evident.enrichment.request.ResourceEnrichmentRequest;
import com.paloaltonetworks.evident.exceptions.GCPApiException;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.models.request.FetchBucketsRequest;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.gcp.models.request.SignatureRequest;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import com.paloaltonetworks.evident.utils.JsonUtils;
import com.paloaltonetworks.evident.utils.ReactorUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BucketEnrichmentTest {
    @Mock
    private GCPApiService gcpApiService;
    @Mock
    private QueueService queueService;
    @Mock
    private ResourceService resourceService;
    @Mock
    private RedisClient redisClient;

    @InjectMocks
    private BucketEnrichment bucketEnrichment;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void enrichRetryableException() throws Exception {
        when(gcpApiService.getBucket(any(ResourceRefreshRequest.class))).thenThrow(new GCPApiException(400, "abc"));
        Assertions.assertThrows(GCPApiException.class, () ->
                bucketEnrichment.enrich(ResourceEnrichmentRequest.builder().projectId("test-proj").build()));
    }

    @Test
    public void enrichNoRetryException() throws Exception {
        when(gcpApiService.getBucket(any(ResourceRefreshRequest.class))).thenThrow(new GCPApiException(404, "abc"));
        bucketEnrichment.enrich(ResourceEnrichmentRequest.builder().projectId("test-proj").build());
    }

    @Test
    public void enrichWithNoBucket() throws Exception {
        // no response
        when(gcpApiService.getBucket(any(ResourceRefreshRequest.class))).thenReturn(null);
        Mono<Resource> enrich = bucketEnrichment.enrich(ResourceEnrichmentRequest.builder().projectId("test-proj").build());
        assertNotNull(enrich);
        verify(gcpApiService, times(0)).getBucketPolicy(any(ResourceRefreshRequest.class));
    }

    @Test
    public void enrichList() throws Exception {
        when(gcpApiService.getBuckets(any(FetchBucketsRequest.class)))
                .thenReturn(new Buckets().setNextPageToken("next-page")
                        .setItems(Arrays.asList(new Bucket().setId("123"), new Bucket().setId("456"))));
        when(gcpApiService.getBucketPolicy(any(ResourceRefreshRequest.class))).thenReturn(new Policy().setResourceId("1234"));
        Flux<Resource> resourceFlux = bucketEnrichment.enrichList(ResourceEnrichmentRequest.builder()
                .tenant("tenant")
                .externalAccountId("extId")
                .resourceType("buckets")
                .projectId("test-proj")
                .build());
        verify(queueService, times(1)).queueResourceRefresh(any(ResourceRefreshRequest.class));
        verify(gcpApiService, times(1)).getBuckets(any(FetchBucketsRequest.class));
        verify(gcpApiService, times(2)).getBucketPolicy(any(ResourceRefreshRequest.class));
        resourceFlux.subscribeOn(Schedulers.immediate()).subscribe(ReactorUtils.subscriber(resource -> {
            assertNotNull(resource);
            assertTrue(Arrays.asList("test-proj/123", "test-proj/456").contains(resource.getResourceId()));
            assertTrue(resource.getResourceMetadata(BucketMetadata.class).getPolicy().getResourceId().equalsIgnoreCase("1234"));
        }));

    }

    @Test
    public void applyForCollection() throws Exception {
        when(resourceService.findResourcesByType(any(String.class), any(String.class), anyString())).thenReturn(Stream.of(Resource.builder()
                .resourceId("123")
                .resourceKey(ResourceKey.builder()
                        .resourceIdHash(1234)
                        .resourceType("bucket")
                        .externalAccountId("1234")
                        .build())
                .resourceMetadata(JsonUtils.encode(BucketMetadata.builder().build()))
                .build(), Resource.builder()
                .resourceId("456")
                .resourceMetadata(JsonUtils.encode(BucketMetadata.builder().build()))
                .resourceKey(ResourceKey.builder()
                        .resourceIdHash(1234)
                        .resourceType("bucket")
                        .externalAccountId("1234")
                        .build())
                .build()));
        when(resourceService.upsert(anyString(), anyString(), any())).thenReturn(true);
        when(gcpApiService.getBuckets(any(FetchBucketsRequest.class)))
                .thenReturn(new Buckets().setNextPageToken("next-page")
                        .setItems(Arrays.asList(new Bucket().setId("123"), new Bucket().setId("456"))));
        when(gcpApiService.getBucketPolicy(any(ResourceRefreshRequest.class))).thenReturn(new Policy().setResourceId("1234"));
        bucketEnrichment.applyForCollection(ResourceEnrichmentRequest.builder()
                .tenant("tenant")
                .externalAccountId("extId")
                .projectId("test-proj")
                .resourceType("buckets")
                .reconcile(true)
                .build());
        verify(queueService, times(2)).queueSignatureForResourceWithDelay(any(SignatureRequest.class), anyInt());
        verify(queueService, times(1)).queueResourceRefresh(any(ResourceRefreshRequest.class));
        verify(redisClient, times(2)).setKey(anyString(), eq(Boolean.TRUE.toString()), anyInt());
    }

}