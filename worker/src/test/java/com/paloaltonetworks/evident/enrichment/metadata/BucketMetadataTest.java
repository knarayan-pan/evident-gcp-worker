package com.paloaltonetworks.evident.enrichment.metadata;

import com.paloaltonetworks.evident.utils.JsonUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;


public class BucketMetadataTest {

    @Test
    public void deserialize() throws Exception {
        String json = "{\n" +
                "  \"bucketAccessControl\": [\n" +
                "    {\n" +
                "      \"bucket\": \"quarantiebucket\",\n" +
                "      \"entity\": \"project-owners-561866596667\",\n" +
                "      \"etag\": \"CAs=\",\n" +
                "      \"id\": \"quarantiebucket/project-owners-561866596667\",\n" +
                "      \"kind\": \"storage#bucketAccessControl\",\n" +
                "      \"projectTeam\": {\n" +
                "        \"projectNumber\": \"561866596667\",\n" +
                "        \"team\": \"owners\"\n" +
                "      },\n" +
                "      \"role\": \"OWNER\",\n" +
                "      \"selfLink\": \"https://www.googleapis.com/storage/v1/b/quarantiebucket/acl/project-owners-561866596667\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"bucket\": \"quarantiebucket\",\n" +
                "      \"entity\": \"project-editors-561866596667\",\n" +
                "      \"etag\": \"CAs=\",\n" +
                "      \"id\": \"quarantiebucket/project-editors-561866596667\",\n" +
                "      \"kind\": \"storage#bucketAccessControl\",\n" +
                "      \"projectTeam\": {\n" +
                "        \"projectNumber\": \"561866596667\",\n" +
                "        \"team\": \"editors\"\n" +
                "      },\n" +
                "      \"role\": \"OWNER\",\n" +
                "      \"selfLink\": \"https://www.googleapis.com/storage/v1/b/quarantiebucket/acl/project-editors-561866596667\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"bucket\": \"quarantiebucket\",\n" +
                "      \"entity\": \"project-viewers-561866596667\",\n" +
                "      \"etag\": \"CAs=\",\n" +
                "      \"id\": \"quarantiebucket/project-viewers-561866596667\",\n" +
                "      \"kind\": \"storage#bucketAccessControl\",\n" +
                "      \"projectTeam\": {\n" +
                "        \"projectNumber\": \"561866596667\",\n" +
                "        \"team\": \"viewers\"\n" +
                "      },\n" +
                "      \"role\": \"READER\",\n" +
                "      \"selfLink\": \"https://www.googleapis.com/storage/v1/b/quarantiebucket/acl/project-viewers-561866596667\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"policy\": {\n" +
                "    \"bindings\": [\n" +
                "      {\n" +
                "        \"members\": [\n" +
                "          \"projectEditor:digital-rhythm-188121\",\n" +
                "          \"projectOwner:digital-rhythm-188121\"\n" +
                "        ],\n" +
                "        \"role\": \"roles/storage.legacyBucketOwner\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"members\": [\n" +
                "          \"projectViewer:digital-rhythm-188121\"\n" +
                "        ],\n" +
                "        \"role\": \"roles/storage.legacyBucketReader\"\n" +
                "      }\n" +
                "    ],\n" +
                "    \"etag\": \"CAs=\",\n" +
                "    \"kind\": \"storage#policy\",\n" +
                "    \"resourceId\": \"projects/_/buckets/quarantiebucket\"\n" +
                "  }\n" +
                "}";
        BucketMetadata bucketMetadata = JsonUtils.decodeValue(json, BucketMetadata.class);
        assertNotNull(bucketMetadata);

    }
}