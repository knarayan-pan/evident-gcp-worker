package com.paloaltonetworks.evident.enrichment;

import com.google.api.services.dns.model.DnsKeySpec;
import com.google.api.services.dns.model.ManagedZone;
import com.google.api.services.dns.model.ManagedZoneDnsSecConfig;
import com.google.api.services.dns.model.ManagedZonesListResponse;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.metadata.DnsManagedZoneMetadata;
import com.paloaltonetworks.evident.enrichment.request.ResourceEnrichmentRequest;
import com.paloaltonetworks.evident.exceptions.GCPApiException;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.models.request.ManagedZoneRequest;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.test.StepVerifier;

import java.math.BigInteger;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class DNSManagedZoneEnrichmentTest {
    @Mock
    private GCPApiService gcpApiService;
    @Mock
    private QueueService queueService;
    @Mock
    private ResourceService resourceService;
    @Mock
    private RedisClient redisClient;
    private ManagedZone managedZone;
    private ResourceEnrichmentRequest resourceEnrichmentRequest;
    @InjectMocks
    private DnsManagedZoneEnrichment dnsManagedZoneEnrichment;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        resourceEnrichmentRequest =
                ResourceEnrichmentRequest.builder()
                        .externalAccountId("1")
                        .tenant("1")
                        .projectId("project-1")
                        .build();
        managedZone = new ManagedZone()
                .setId(BigInteger.ONE).setDescription("test").setDnsName("a.b.com").setName("sample1")
                .setDnssecConfig(new ManagedZoneDnsSecConfig().setState("on")
                        .setDefaultKeySpecs(Collections.singletonList(new DnsKeySpec().setAlgorithm("abc")
                                .setKeyLength(123L).setKeyType("rsa"))));
    }

    @Test
    public void enrichException() throws Exception {
        when(gcpApiService.getManagedZoneDetails(any(ManagedZoneRequest.class))).thenThrow(new GCPApiException(403, "forbidden"));
        StepVerifier.create(dnsManagedZoneEnrichment.enrich(resourceEnrichmentRequest))
                .verifyComplete();
    }

    @Test
    public void enrichNotFoundException() throws Exception {
        when(gcpApiService.getManagedZoneDetails(any(ManagedZoneRequest.class))).thenThrow(new GCPApiException(404, "Notfound"));
        StepVerifier.create(dnsManagedZoneEnrichment.enrich(resourceEnrichmentRequest))
                .expectComplete();
    }

    @Test
    public void enrichSingleResource() throws Exception {
        when(gcpApiService.getManagedZoneDetails(any(ManagedZoneRequest.class))).thenReturn(managedZone);
        StepVerifier.create(dnsManagedZoneEnrichment.enrich(resourceEnrichmentRequest))
                .assertNext(resource -> {
                    assertNotNull(resource.getResourceMetadata());
                    assertEquals(resource.getResourceId(), "project-1/sample1");
                    assertTrue(resource.getResourceMetadata(DnsManagedZoneMetadata.class).getDefaultKeySpecsList().size() > 0);
                })
                .expectComplete()
                .verify();
    }

    @Test
    public void enrichList() throws Exception {
        ManagedZonesListResponse managedZonesListResponse = new ManagedZonesListResponse().setManagedZones(Collections.singletonList(managedZone));
        when(gcpApiService.getManagedZones(any(ManagedZoneRequest.class))).thenReturn(managedZonesListResponse);
        StepVerifier.create(dnsManagedZoneEnrichment.enrichList(resourceEnrichmentRequest))
                .assertNext(resource -> {
                    assertNotNull(resource.getResourceMetadata());
                    assertEquals(resource.getResourceId(), "project-1/sample1");
                    assertTrue(resource.getResourceMetadata(DnsManagedZoneMetadata.class).getDefaultKeySpecsList().size() > 0);
                })
                .expectComplete()
                .verify();
    }
}
