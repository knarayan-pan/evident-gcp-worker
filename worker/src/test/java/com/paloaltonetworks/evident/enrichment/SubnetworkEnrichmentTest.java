package com.paloaltonetworks.evident.enrichment;

import com.google.api.services.compute.model.Subnetwork;
import com.google.api.services.compute.model.SubnetworkAggregatedList;
import com.google.api.services.compute.model.SubnetworksScopedList;
import com.google.common.collect.ImmutableMap;
import com.paloaltonetworks.evident.enrichment.metadata.SubNetworkMetadata;
import com.paloaltonetworks.evident.enrichment.request.ResourceEnrichmentRequest;
import com.paloaltonetworks.evident.exceptions.GCPApiException;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.service.QueueService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import reactor.test.StepVerifier;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

class SubnetworkEnrichmentTest {
    private static final ResourceEnrichmentRequest RESOURCE_ENRICHMENT_REQUEST =
            ResourceEnrichmentRequest.builder().externalAccountId("1").tenant("1").projectId("project-1").build();
    @Mock private GCPApiService gcpApiService;
    @Mock private QueueService queueService;
    @InjectMocks private SubnetworkEnrichment enrichment;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void enrichRetryableException() {
        when(gcpApiService.getSubNetwork(any(ResourceRefreshRequest.class)))
                .thenThrow(new GCPApiException(1, "test exception"));
        try {
            StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST)).verifyError();
            Assertions.fail("Expected exception was not thrown");
        } catch (GCPApiException e) {
            // this is expected exception
        }
    }

    @Test
    void enrichNonRetryableException() {
        when(gcpApiService.getSubNetwork(any(ResourceRefreshRequest.class)))
                .thenThrow(new GCPApiException(404, "test exception"));
        StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST)).verifyComplete();
    }

    @Test
    void enrichList() {
        SubnetworkAggregatedList arggregatedList = Mockito.mock(SubnetworkAggregatedList.class);

        given(gcpApiService.getSubNetwork(any(ResourceRefreshRequest.class)))
                .willReturn(listOf(metadataOf("2000-01-01T000"),
                        metadataOf("2001-01-01T000")).get(0));
        when(arggregatedList.getNextPageToken()).thenReturn("/mock/next");
        when(arggregatedList.getItems()).thenReturn(
                ImmutableMap.of("zone-1", aggregateListOf(metadataOf("2000-01-01T000")),
                        "zone-2", aggregateListOf(metadataOf("2001-01-01T000"))));
        when(gcpApiService.getSubNetworks(any())).thenReturn(arggregatedList);
        StepVerifier.create(
                enrichment.enrichList(RESOURCE_ENRICHMENT_REQUEST))
                .assertNext(resource ->
                        assertNotNull(resource.getResourceMetadata(SubNetworkMetadata.class)))
                .assertNext(resource ->
                        assertNotNull(resource.getResourceMetadata(SubNetworkMetadata.class)))
                .expectComplete()
                .verify();
    }

    @Test
    void enrichTest() {
        // no subnet exists, should ignore and complete
        StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST)).verifyComplete();

        given(gcpApiService.getSubNetwork(any(ResourceRefreshRequest.class)))
                .willReturn(listOf(metadataOf("2000-01-01T000")).get(0));

        // returns a valid data
        StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST))
                .assertNext(resource ->
                        assertEquals("test-resource",
                                resource.getResourceMetadata(SubNetworkMetadata.class).getResourceName()))
                .verifyComplete();
    }

    @Test
    void enrichDateParseTest() {
        // val enUnderTest = new VMEnrichment(resourceService, redisClient, queueService, gcpApiService);
        StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST)).verifyComplete();

        //stub to return data
        given(gcpApiService.getSubNetwork(any(ResourceRefreshRequest.class)))
                .willReturn(listOf(metadataOf("BAD-FORMAT")).get(0));

        // returns a valid data
        StepVerifier.create(
                enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST))
                .assertNext(resource ->
                        assertEquals("BAD-FORMAT",
                                resource.getResourceMetadata(SubNetworkMetadata.class).getCreationTimeStamp())
                )
                .verifyComplete();
    }

    private SubNetworkMetadata metadataOf(String createTimestamp) {
        return SubNetworkMetadata.builder()
                .id(BigInteger.valueOf(1L))
                .resourceName("test-resource")
                .selfLink("/subnet/link")
                .region("test-region")
                .creationTimeStamp(createTimestamp)
                .projectId("project-1")
                .build();
    }

    private SubnetworksScopedList aggregateListOf(SubNetworkMetadata... m) {
        return new SubnetworksScopedList().setSubnetworks(listOf(m));
    }

    private List<Subnetwork> listOf(SubNetworkMetadata... metadata) {
        return Stream.of(metadata).map(mdata -> {
                    final Subnetwork inst = new Subnetwork();
                    inst.setId(mdata.getId());
                    inst.setCreationTimestamp(mdata.getCreationTimeStamp());
                    inst.setName(mdata.getResourceName());
                    inst.setSelfLink(mdata.getSelfLink());
                    inst.setRegion(mdata.getRegion());
                    return inst;
                }
        ).collect(Collectors.toList());
    }
}
