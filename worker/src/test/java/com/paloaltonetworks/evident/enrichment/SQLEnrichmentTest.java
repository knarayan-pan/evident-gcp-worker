package com.paloaltonetworks.evident.enrichment;


import com.google.api.services.sqladmin.model.AclEntry;
import com.google.api.services.sqladmin.model.DatabaseInstance;
import com.google.api.services.sqladmin.model.InstancesListResponse;
import com.google.api.services.sqladmin.model.IpConfiguration;
import com.google.api.services.sqladmin.model.Settings;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.metadata.SQLInstanceMetadata;
import com.paloaltonetworks.evident.enrichment.request.ResourceEnrichmentRequest;
import com.paloaltonetworks.evident.exceptions.GCPApiException;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.models.request.FetchSQLInstancesRequest;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.test.StepVerifier;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.BDDMockito.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.when;

@Slf4j
public class SQLEnrichmentTest {
    private static final ResourceEnrichmentRequest RESOURCE_ENRICHMENT_REQUEST =
            ResourceEnrichmentRequest.builder()
                    .externalAccountId("1")
                    .projectId("test-proj")
                    .tenant("1")
                    .build();
    @Mock
    private GCPApiService gcpApiService;
    @Mock
    private QueueService queueService;
    @Mock
    private ResourceService resourceService;

    @Mock
    private RedisClient redisClient;

    @Mock
    private InstancesListResponse instancesListResponse;

    @InjectMocks
    private SQLEnrichment enrichment;


    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void enrichRetryableException() throws Exception {
        try {
            when(gcpApiService.getSQLInstance(any(ResourceRefreshRequest.class)))
                    .thenThrow(new GCPApiException(1, "test exception"));
            StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST))
                    .verifyError();
            Assertions.fail("Expected exception was not thrown");
        } catch (GCPApiException e) {
            //this is expected
        }
    }

    @Test
    public void enrichNonRetryableException() throws Exception {
        when(gcpApiService.getSQLInstance(any(ResourceRefreshRequest.class)))
                .thenThrow(new GCPApiException(404, "test exception"));
        StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST))
                .verifyComplete();
    }

    @Test
    public void enrichList() throws Exception {
        stubSQLList();
        stubGetSQLInstances();

        StepVerifier.create(
                enrichment.enrichList(RESOURCE_ENRICHMENT_REQUEST))
                .assertNext(resource ->
                        assertNotNull(resource.getResourceMetadata(SQLInstanceMetadata.class)))
                .assertNext(resource ->
                        assertNotNull(resource.getResourceMetadata(SQLInstanceMetadata.class)))
                .expectComplete()
                .verify();
    }

    @Test
    public void enrichTest() throws Exception {
        // no instance exists, should ignore and complete
        StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST))
                .verifyComplete();

        //stub to return data
        stubGetComputeInstance(
                listOf(
                        metadataOf("name1", "ver1", "connection", Lists.newArrayList(),
                                false, false, "Self")));

        // returns a valid data
        StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST))
                .assertNext(resource ->
                        assertEquals("name1",
                                resource.getResourceMetadata(SQLInstanceMetadata.class).getResourceName()))
                .verifyComplete();

    }

    private void stubGetComputeInstance(List<DatabaseInstance> instanceList) {
        given(gcpApiService.getSQLInstance(any(ResourceRefreshRequest.class)))
                .willReturn(instanceList.get(0));
    }

    private void stubGetSQLInstances() {
        when(gcpApiService.getSQLInstances(any(FetchSQLInstancesRequest.class)))
                .thenReturn(instancesListResponse);
    }

    private void stubSQLList() {
        when(instancesListResponse.getNextPageToken()).thenReturn("/mock/next");
        when(instancesListResponse.getItems()).thenReturn(
                listOf(
                        metadataOf("name1", "ver1", "connection", Lists.newArrayList(),
                        false, false, "Self"),
                        metadataOf("name2", "ver1", "connection", Lists.newArrayList(),
                                true, true, "Self")
                        )
                );

    }

    private SQLInstanceMetadata metadataOf(String name, String databaseVersion, String connectionName, List<Map<String, Object>> authorizedNetworks, boolean requireSSL, boolean autobackupEnabled, String selfLink) {
        return SQLInstanceMetadata.builder()
                .resourceName(name)
                .databaseVersion(databaseVersion)
                .connectionName(connectionName)
                .authorizedNetworks(authorizedNetworks)
                .requireSSL(requireSSL)
                .autobackupEnabled(autobackupEnabled)
                .selfLink(selfLink)
                .zone("zone-1")
                .region("region")
                .build();
    }


    private List<DatabaseInstance> listOf(SQLInstanceMetadata... instanceMetadata) {
        return Stream.of(instanceMetadata).map(mdata -> {
                    final DatabaseInstance inst = new DatabaseInstance();
                    inst.setConnectionName(mdata.getConnectionName());
            val aclEntry = new AclEntry().setValue("0.0.0.0/0").setName("test-acl");
            inst.setSettings(new Settings().setIpConfiguration(new IpConfiguration().setAuthorizedNetworks(Lists.newArrayList(aclEntry))));
            inst.setName(mdata.getResourceName())
                    .setRegion(mdata.getRegion())
                    .setSelfLink(mdata.getSelfLink())
                    .setGceZone(mdata.getZone())
                    .setDatabaseVersion(mdata.getDatabaseVersion());
                    return inst;
                }
        ).collect(Collectors.toList());
    }
}
