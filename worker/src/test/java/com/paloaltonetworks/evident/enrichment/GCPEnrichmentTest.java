package com.paloaltonetworks.evident.enrichment;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.api.services.compute.model.NetworkList;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.request.ProjectEnrichmentRequest;
import com.paloaltonetworks.evident.exceptions.RedisClientException;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.gcp.models.request.SignatureRequest;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.Optional;

public class GCPEnrichmentTest {

    @Mock
    private GCPApiService gcpApiService;
    @Mock
    private ResourceService resourceService;
    @Mock
    private RedisClient redisClient;
    @Mock
    private QueueService queueService;
    private ProjectEnrichment enrichment;
    private String tenant = "enrich-test";
    private String externalAccountId = "enrich-test-id";
    private Resource resource;
    private ProjectEnrichmentRequest request;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(gcpApiService.getProjectNetworks(any())).thenReturn(new NetworkList().setItems(Collections.emptyList()));
        enrichment = new ProjectEnrichment(resourceService, redisClient, queueService, gcpApiService);
        ResourceKey key = new ResourceKey();
        key.setExternalAccountId(externalAccountId);
        key.setResourceIdHash(-1956384286);
        key.setResourceType(ResourceType.GCP_PROJECT.getResourceType());
        resource = Resource.builder()
                .resourceKey(key)
                .cloudAppType(CloudAppType.GCP)
                .tenant(tenant)
                .resourceId("12345")
                .region("global")
                .resourceMetadata("{\"orgId\":\"1234\", \"networks\":[], \"resource_name\":\"project1\", \"project_id\":\"12345\", \"version\":\"v1.0\"}")
                .build();
        request = ProjectEnrichmentRequest.builder()
                .tenant(tenant)
                .externalAccountId(externalAccountId)
                .projectName("project1")
                .resourceId("12345")
                .orgId("1234")
                .build();
    }

    @Test
    public void applyWhenResourceIsSimilarToDb() throws Exception {
        // return same resource as what has been enriched
        when(resourceService.findOne(eq(tenant), any(ResourceKey.class))).thenReturn(Optional.of(resource));
        enrichment.apply(request);
        // ensure we dont hit redis
        verify(redisClient, times(0)).getResourceLock(eq(externalAccountId), any());
        // ensure we dont hit db
        verify(resourceService, times(0)).upsert(eq(tenant), eq(externalAccountId), any());
        // ensure we dont queue signature task
        verify(queueService, times(0)).queueSignatureForResource(any(SignatureRequest.class));
    }

    @Test
    public void applyWhenResourceIsNotFound() throws Exception {
        // no resource found
        when(resourceService.findOne(eq(tenant), any(ResourceKey.class))).thenReturn(Optional.empty());
        // achieved lock
        when(redisClient.getResourceLock(eq(externalAccountId), any())).thenReturn(true);
        // upsert failure
        when(resourceService.upsert(eq(tenant), eq(externalAccountId), any())).thenReturn(true);
        enrichment.apply(request);
        // ensure we dont hit redis
        verify(redisClient, times(1)).getResourceLock(eq(externalAccountId), any());
        // ensure we dont hit db
        verify(resourceService, times(1)).upsert(eq(tenant), eq(externalAccountId), any());
        // ensure we dont queue signature task
        verify(queueService, times(1)).queueSignatureForResource(any(SignatureRequest.class));
    }

    @Test
    public void applyWheResourceIsFoundAndIsDifferentButRedisFailure() throws Exception {
        // make a change to resource to mimick update
        when(resourceService.findOne(eq(tenant), any(ResourceKey.class))).thenReturn(Optional.ofNullable(resource.toBuilder().region("us-west").build()));
        when(redisClient.getResourceLock(eq(externalAccountId), any())).thenThrow(new RedisClientException("redis down!"));
        try {
            enrichment.apply(request);
            Assertions.fail("Expected exception was not thrown");
        } catch (RuntimeException e) {
            // this is expected exception
        }
        // ensure we dont hit redis
        verify(redisClient, times(1)).getResourceLock(eq(externalAccountId), any());
        // ensure we dont hit db
        verify(resourceService, times(0)).upsert(eq(tenant), eq(externalAccountId), any());
        // ensure we dont queue signature task
        verify(queueService, times(0)).queueSignatureForResource(any(SignatureRequest.class));
    }

    @Test
    public void applyWhenResourceIsFoundAndIsDifferentButCassandraFailure() throws Exception {
        // make a change to resource to mimick update
        when(resourceService.findOne(eq(tenant), any(ResourceKey.class))).thenReturn(Optional.ofNullable(resource.toBuilder().region("us-west").build()));
        // achieved lock
        when(redisClient.getResourceLock(eq(externalAccountId), any())).thenReturn(true);
        // upsert failure
        when(resourceService.upsert(eq(tenant), eq(externalAccountId), any())).thenReturn(false);
        try {
            enrichment.apply(request);
            Assertions.fail("Expected exception was not thrown");
        } catch (RuntimeException e) {
            // this is expected exception
        }
        // ensure we dont hit redis
        verify(redisClient, times(1)).getResourceLock(eq(externalAccountId), any());
        // ensure we dont hit db
        verify(resourceService, times(1)).upsert(eq(tenant), eq(externalAccountId), any());
        // ensure we dont queue signature task
        verify(queueService, times(0)).queueSignatureForResource(any(SignatureRequest.class));
    }

    @Test
    public void applyWhenResourceIsFoundAndIsDifferentAndEverythingWorks() throws Exception {
        // make a change to resource to mimick update
        when(resourceService.findOne(eq(tenant), any(ResourceKey.class))).thenReturn(Optional.ofNullable(resource.toBuilder().region("us-west").build()));
        // achieved lock
        when(redisClient.getResourceLock(eq(externalAccountId), any())).thenReturn(true);
        // upsert failure
        when(resourceService.upsert(eq(tenant), eq(externalAccountId), any())).thenReturn(true);
        enrichment.apply(request);
        // ensure we dont hit redis
        verify(redisClient, times(1)).getResourceLock(eq(externalAccountId), any());
        // ensure we dont hit db
        verify(resourceService, times(1)).upsert(eq(tenant), eq(externalAccountId), any());
        // ensure we dont queue signature task
        verify(queueService, times(1)).queueSignatureForResource(any(SignatureRequest.class));
    }
}