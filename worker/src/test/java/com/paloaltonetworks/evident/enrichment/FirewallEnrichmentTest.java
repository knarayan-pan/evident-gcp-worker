package com.paloaltonetworks.evident.enrichment;

import com.google.api.services.compute.model.Firewall;
import com.google.api.services.compute.model.FirewallList;
import com.google.common.collect.ImmutableList;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.metadata.FirewallMetadata;
import com.paloaltonetworks.evident.enrichment.request.ResourceEnrichmentRequest;
import com.paloaltonetworks.evident.exceptions.GCPApiException;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.gcp.models.request.FetchFirewallsRequest;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.test.StepVerifier;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

public class FirewallEnrichmentTest {
    private static final ResourceEnrichmentRequest RESOURCE_ENRICHMENT_REQUEST =
            ResourceEnrichmentRequest.builder()
                    .resourceType(ResourceType.GCE_FIREWALL.toString())
                    .resourceName("default_rule")
                    .resourceId("default_rule")
                    .externalAccountId("1")
                    .projectId("1")
                    .tenant("1")
                    .build();
    @Mock
    private GCPApiService gcpApiService;
    @Mock
    private QueueService queueService;
    @Mock
    private ResourceService resourceService;

    @Mock
    private RedisClient redisClient;

    @Mock
    private FirewallList firewallList;

    @InjectMocks
    private FirewallEnrichment enrichment;


    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void enrichRetryableException() throws Exception {
        try {
            when(gcpApiService.getFirewall(any(ResourceRefreshRequest.class)))
                    .thenThrow(new GCPApiException(1, "test exception"));
            StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST))
                    .verifyError();
            Assertions.fail("Expected exception was not thrown");
        } catch (GCPApiException e) {
            //expected exception
        }
    }

    @Test
    public void enrichNonRetryableException() throws Exception {
        when(gcpApiService.getFirewall(any(ResourceRefreshRequest.class)))
                .thenThrow(new GCPApiException(404, "test exception"));
        StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST))
                .verifyComplete();
    }

    @Test
    public void enrichList() throws Exception {
        stubFirewallListItems();
        stubGetFirwallList();

        StepVerifier.create(
                enrichment.enrichList(RESOURCE_ENRICHMENT_REQUEST))
                .assertNext(resource ->
                        assertNotNull(resource.getResourceMetadata(FirewallMetadata.class)))
                .assertNext(resource ->
                        assertNotNull(resource.getResourceMetadata(FirewallMetadata.class)))
                .expectComplete()
                .verify();
    }

    @Test
    public void enrichTest() throws Exception {
        // no instance exists, should ignore and complete
        StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST))
                .verifyComplete();

        //stub to return data
        stubGetFirewall(
                listOf(
                        metadataOf("name1", "Self")));

        // returns a valid data
        StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST))
                .assertNext(resource ->
                        assertEquals("name1",
                                resource.getResourceMetadata(FirewallMetadata.class).getResourceName()))
                .verifyComplete();

    }

    private void stubGetFirewall(List<Firewall> firewallList) {
        given(gcpApiService.getFirewall(any(ResourceRefreshRequest.class)))
                .willReturn(firewallList.get(0));
    }

    private void stubGetFirwallList() {
        when(gcpApiService.getFirewalls(any(FetchFirewallsRequest.class)))
                .thenReturn(firewallList);
    }

    private void stubFirewallListItems() {
        when(firewallList.getNextPageToken()).thenReturn("/mock/next");
        when(firewallList.getItems()).thenReturn(
                listOf(
                        metadataOf("name1", "Self1"),
                        metadataOf("name2", "Self2")
                )
        );

    }

    private FirewallMetadata metadataOf(String name, String selfLink) {
        return FirewallMetadata.builder()
                .allowedRules(ImmutableList.of(allowedOf(ImmutableList.of("0.0.0.0/0"), "TCP", ImmutableList.of("10-100"))))
                .deniedRules(ImmutableList.of(allowedOf(ImmutableList.of("0.0.0.0/0"), "TCP", ImmutableList.of("10-100"))))
                .selfLink(selfLink)
                .resourceName(name)
                .build();
    }

    private FirewallMetadata.RulesMetadata allowedOf(List<String> sourceIPRanges, String protocol, List<String> ports) {
        return FirewallMetadata.RulesMetadata.builder()
                .sourceIPRanges(sourceIPRanges)
                .protocol(protocol)
                .ports(ports)
                .build();
    }


    private List<Firewall> listOf(FirewallMetadata... firewallMetadata) {
        return Stream.of(firewallMetadata).map(mdata -> {
                    final Firewall inst = new Firewall();
                    inst.setName(mdata.getResourceName())
                            .setSelfLink(mdata.getSelfLink());
                    return inst;
                }
        ).collect(Collectors.toList());
    }
}