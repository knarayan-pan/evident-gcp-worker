package com.paloaltonetworks.evident.enrichment.metadata;

import com.paloaltonetworks.evident.config.CustomObjectMapper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ProjectMetadataTest {
    @Test
    public void deserialize() throws Exception {
        String json = "{\"orgId\":\"605530761854\",\"tags\":{},\"resource_name\":\"Test Corp Eco Monitoring\"}";
        ProjectMetadata projectMetadata = CustomObjectMapper.decodeValue(json, ProjectMetadata.class);
        assertNotNull(projectMetadata);
        assertTrue(projectMetadata.getResourceName().equals("Test Corp Eco Monitoring"));
    }


}