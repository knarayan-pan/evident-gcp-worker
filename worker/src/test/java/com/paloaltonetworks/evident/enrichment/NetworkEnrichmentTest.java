package com.paloaltonetworks.evident.enrichment;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.google.api.services.compute.model.Network;
import com.google.api.services.compute.model.NetworkList;
import com.google.api.services.compute.model.NetworkPeering;
import com.google.api.services.compute.model.NetworkRoutingConfig;
import com.paloaltonetworks.evident.enrichment.metadata.NetworkMetadata;
import com.paloaltonetworks.evident.enrichment.request.ResourceEnrichmentRequest;
import com.paloaltonetworks.evident.exceptions.GCPApiException;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.service.QueueService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import reactor.test.StepVerifier;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class NetworkEnrichmentTest {
    private static final ResourceEnrichmentRequest RESOURCE_ENRICHMENT_REQUEST =
            ResourceEnrichmentRequest.builder().externalAccountId("1").tenant("1").projectId("project-1").build();
    @Mock
    private GCPApiService gcpApiService;
    @Mock private QueueService queueService;
    @InjectMocks
    private NetworkEnrichment enrichment;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void enrichRetryableException() {
        when(gcpApiService.getNetwork(any(ResourceRefreshRequest.class)))
                .thenThrow(new GCPApiException(1, "test exception"));
        try {
            StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST)).verifyError();
            Assertions.fail("Expected exception was not thrown");
        } catch (GCPApiException e) {
            // this is expected exception
        }
    }

    @Test
    void enrichNonRetryableException() {
        when(gcpApiService.getNetwork(any(ResourceRefreshRequest.class)))
                .thenThrow(new GCPApiException(404, "test exception"));
        StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST)).verifyComplete();
    }

    @Test
    void enrichList() {
        NetworkList networkList = Mockito.mock(NetworkList.class);
        when(networkList.getItems()).thenReturn(testNetworks());
        when(gcpApiService.getNetworks(any(ResourceRefreshRequest.class)))
                .thenReturn(networkList);
        StepVerifier.create(
                enrichment.enrichList(RESOURCE_ENRICHMENT_REQUEST))
                .assertNext(resource ->
                        assertNotNull(resource.getResourceMetadata(NetworkMetadata.class)))
                .assertNext(resource ->
                        assertNotNull(resource.getResourceMetadata(NetworkMetadata.class)))
                .expectComplete()
                .verify();
    }

    @Test
    void enrichTest() {
        // no instance exists, should ignore and complete
        StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST))
                .verifyComplete();
        when(gcpApiService.getNetwork(any(ResourceRefreshRequest.class)))
                .thenReturn(testNetwork("foo"));
        StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST))
                .assertNext(resource ->
                        assertEquals(expectedMetadata("foo"), resource.getResourceMetadata(NetworkMetadata.class)))
                .verifyComplete();
    }

    private List<Network> testNetworks() {
        List<Network> networks = new ArrayList<>();
        networks.add(testNetwork("foo"));
        networks.add(testNetwork("bar"));
        return networks;
    }

    private Network testNetwork(String name) {
        return new Network()
                .setAutoCreateSubnetworks(true)
                .setCreationTimestamp("creationTimestamp")
                .setGatewayIPv4("gatewayIPv4")
                .setId(BigInteger.ONE)
                .setIPv4Range("IPv4Range")
                .setKind("kind")
                .setName(name)
                .setPeerings(Collections.singletonList(new NetworkPeering().setName("peering").setAutoCreateRoutes(true)))
                .setRoutingConfig(new NetworkRoutingConfig().setRoutingMode("routingMode"))
                .setSelfLink("selfLink")
                .setSubnetworks(Collections.singletonList("subnetwork"))
                .setDescription("description");
    }

    private NetworkMetadata expectedMetadata(String name) {
        return NetworkMetadata.builder()
                .autoCreateSubnetworks(true)
                .creationTimestamp("creationTimestamp")
                .gatewayIPv4("gatewayIPv4")
                .iPv4Range("IPv4Range")
                .kind("kind")
                .resourceName(name)
                .peerings(Collections.singletonList(NetworkMetadata.Peering.builder().name("peering").autoCreateRoutes(true).build()))
                .routingConfig(NetworkMetadata.RoutingConfig.builder().routingMode("routingMode").build())
                .selfLink("selfLink")
                .subnetworks(Collections.singletonList("subnetwork"))
                .description("description")
                .projectId("project-1")
                .build();
    }
}
