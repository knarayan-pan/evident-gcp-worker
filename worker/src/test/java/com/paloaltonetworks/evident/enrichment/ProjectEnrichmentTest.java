package com.paloaltonetworks.evident.enrichment;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.google.api.services.cloudresourcemanager.model.AuditConfig;
import com.google.api.services.cloudresourcemanager.model.AuditLogConfig;
import com.google.api.services.cloudresourcemanager.model.Binding;
import com.google.api.services.cloudresourcemanager.model.Policy;
import com.google.api.services.compute.model.Network;
import com.google.api.services.compute.model.NetworkList;
import com.google.common.collect.ImmutableList;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.metadata.IamPolicy;
import com.paloaltonetworks.evident.enrichment.metadata.ProjectMetadata;
import com.paloaltonetworks.evident.enrichment.request.ProjectEnrichmentRequest;
import com.paloaltonetworks.evident.exceptions.GCPApiException;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import com.paloaltonetworks.evident.utils.JsonUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.util.CollectionUtils;
import reactor.core.publisher.Mono;

import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class ProjectEnrichmentTest {
    @Mock
    private GCPApiService gcpApiService;
    @Mock
    private ResourceService resourceService;
    @Mock
    private RedisClient redisClient;
    @Mock
    private QueueService queueService;
    private ProjectEnrichment projectEnrichment;
    private ProjectEnrichmentRequest request;

    private final String externalAccountId = "enrich-test-id";
    private final String tenant = "enrich-test";

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        projectEnrichment = new ProjectEnrichment(resourceService, redisClient, queueService, gcpApiService);
        String tenant = "enrich-test";
        String externalAccountId = "enrich-test-id";
        request = ProjectEnrichmentRequest.builder()
            .tenant(tenant)
            .externalAccountId(externalAccountId)
            .projectName("project1")
            .resourceId("12345")
            .orgId("1234")
            .build();
    }

    @Test
    public void enrichRetryableException() throws Exception {
        when(gcpApiService.getProjectIamPolicy(any(ProjectEnrichmentRequest.class))).thenThrow(new GCPApiException(400, "abc"));
        Assertions.assertThrows(GCPApiException.class, () -> projectEnrichment.enrich(request));
    }

    @Test
    public void enrichNoRetryException() throws Exception {
        when(gcpApiService.getProjectIamPolicy(any(ProjectEnrichmentRequest.class))).thenThrow(new GCPApiException(404, "abc"));
        Mono<Resource> enrich = projectEnrichment.enrich(request);
        Assertions.assertNotNull(enrich);
        enrich.subscribe(resource -> {
                ProjectMetadata projectMetadata =
                    JsonUtils.decodeValue(resource.getResourceMetadata(), ProjectMetadata.class);
                Assertions.assertNull(projectMetadata.getIamPolicy());
            }
        );
    }

    private static Stream<Arguments> policyBindingsProvider() {
        return Stream.of(
            Arguments.of("null bindings, null audits", new Policy()),
            Arguments.of("empty bindings, null audits", new Policy().setBindings(Collections.emptyList())),
            Arguments.of("1 empty binding, null audits",
                new Policy().setBindings(ImmutableList.of(
                    new Binding().setRole("role 1").setMembers(Collections.emptyList())))),
            Arguments.of("1 binding 1 member, null audits",
                new Policy().setBindings(ImmutableList.of(
                    new Binding().setRole("role 1").setMembers(ImmutableList.of("user 1"))))),
            Arguments.of("1 binding 2 members, null audits",
                new Policy().setBindings(ImmutableList.of(
                    new Binding().setRole("role 1").setMembers(ImmutableList.of("user 1", "user 2"))))),
            Arguments.of("2 bindings, null audits",
                new Policy().setBindings(ImmutableList.of(
                    new Binding().setRole("role 1").setMembers(ImmutableList.of("user 1", "user 2")),
                    new Binding().setRole("role 1").setMembers(ImmutableList.of("user 3", "user 4"))
                )))
        );
    }

    private void checkProject(Resource resource) {
        ProjectMetadata projectMetadata =
            JsonUtils.decodeValue(resource.getResourceMetadata(), ProjectMetadata.class);

        Assertions.assertEquals(tenant, resource.getTenant());
        ResourceKey resourceKey = resource.getResourceKey();
        Assertions.assertEquals(externalAccountId, resourceKey.getExternalAccountId());
        Assertions.assertEquals("project", resourceKey.getResourceType());
        Assertions.assertEquals(-1956384286, (int)resourceKey.getResourceIdHash());

        Assertions.assertEquals(resource.getResourceId(), "12345");
        Assertions.assertEquals(resource.getRegion(), "global");
        Assertions.assertEquals("project1", projectMetadata.getResourceName());
    }

    private void checkBindings(Resource resource, Policy policy) {
        ProjectMetadata projectMetadata =
            JsonUtils.decodeValue(resource.getResourceMetadata(), ProjectMetadata.class);
        List<IamPolicy.RoleMembers> roleMembersList = projectMetadata.getIamPolicy().getRoleMembers();
        if (policy.getBindings() == null) {
            Assertions.assertTrue(CollectionUtils.isEmpty(roleMembersList));
        }
        else {
            Assertions.assertEquals(policy.getBindings().size(), roleMembersList.size());
            policy.getBindings().stream().forEach(
                binding ->
                {
                    Assertions.assertTrue(
                        roleMembersList.stream().anyMatch(
                            roleMembers ->
                                Objects.equals(roleMembers.getRole(), binding.getRole()) &&
                                    Objects.equals(roleMembers.getMembers(), binding.getMembers())
                        )
                    );
                }
            );
        }
    }

    @ParameterizedTest(name = "policy #{index} - {0}")
    @MethodSource("policyBindingsProvider")
    public void enrichPolicyWithBindings(String description, Policy policy) throws Exception {
        // no response
        when(gcpApiService.getProjectIamPolicy(any(ProjectEnrichmentRequest.class))).thenReturn(policy);
        when(gcpApiService.getProjectNetworks(any(ProjectEnrichmentRequest.class))).thenReturn(testNetworkList());
        Mono<Resource> enrich = projectEnrichment.enrich(request);
        Assertions.assertNotNull(enrich);

        enrich.subscribe(resource -> {
            checkProject(resource);
            checkBindings(resource, policy);
        });
    }

    private static Stream<Arguments> policyAuditlogsProvider() {
        return Stream.of(
            Arguments.of("null bindings, null audits", new Policy()),
            Arguments.of("null binding, empty auditLogs",
                new Policy().setAuditConfigs(
                    Collections.emptyList())),
            Arguments.of("null binding, 1 auditLog with no logConfigs",
                new Policy().setAuditConfigs(ImmutableList.of(
                    new AuditConfig().setService("service 1")))),
            Arguments.of("null binding, 1 auditLog with service but empty logConfigs",
                new Policy().setAuditConfigs(ImmutableList.of(
                    new AuditConfig().setService("service 1").setAuditLogConfigs(Collections.emptyList())))),
            Arguments.of("null binding, 1 auditLog with service with an empty logConfig",
                new Policy().setAuditConfigs(ImmutableList.of(
                    new AuditConfig().setService("service 1").setAuditLogConfigs(ImmutableList.of(new AuditLogConfig()))))),
            Arguments.of("null binding, 1 auditLog with service with a logConfig of no exempted",
                new Policy().setAuditConfigs(ImmutableList.of(
                    new AuditConfig().setService("service 1").setAuditLogConfigs(
                        ImmutableList.of(new AuditLogConfig().setLogType("type 1")))))),
            Arguments.of("null binding, 1 auditLog with service with a logConfig of empty exempted",
                new Policy().setAuditConfigs(ImmutableList.of(
                    new AuditConfig().setService("service 1").setAuditLogConfigs(
                        ImmutableList.of(new AuditLogConfig().setLogType("type 1").setExemptedMembers(Collections.emptyList())))))),
            Arguments.of("null binding, 1 auditLog with service with a logConfig of non-empty exempted",
                new Policy().setAuditConfigs(ImmutableList.of(
                    new AuditConfig().setService("service 1").setAuditLogConfigs(
                        ImmutableList.of(new AuditLogConfig().setLogType("type 1").setExemptedMembers(
                            ImmutableList.of("member 1", "member 2"))))))),
            Arguments.of("null binding, 2 auditLogs",
                new Policy().setAuditConfigs(ImmutableList.of(
                    new AuditConfig().setService("service 1").setAuditLogConfigs(
                        ImmutableList.of(new AuditLogConfig().setLogType("type 1").setExemptedMembers(
                            ImmutableList.of("member 1", "member 2")),
                            new AuditLogConfig().setLogType("type 2").setExemptedMembers(
                                ImmutableList.of("member 3", "member 4")))),
                    new AuditConfig().setService("service 2").setAuditLogConfigs(
                        ImmutableList.of(new AuditLogConfig().setLogType("type 3").setExemptedMembers(
                            ImmutableList.of("member 11", "member 12")),
                            new AuditLogConfig().setLogType("type 4").setExemptedMembers(
                                ImmutableList.of("member 3", "member 4")))))))
        );
    }

    private void checkAuditConfigs(Resource resource, Policy policy) {
        ProjectMetadata projectMetadata =
            JsonUtils.decodeValue(resource.getResourceMetadata(), ProjectMetadata.class);
        List<IamPolicy.AuditConfig> auditConfigsMetadata = projectMetadata.getIamPolicy().getAuditConfigs();
        if (policy.getAuditConfigs() == null) {
            Assertions.assertTrue(CollectionUtils.isEmpty(auditConfigsMetadata));
        }
        else {
            Assertions.assertEquals(policy.getAuditConfigs().size(), auditConfigsMetadata.size());
            policy.getAuditConfigs().stream().forEach(
                auditConfig ->
                {
                    List<IamPolicy.AuditConfig> auditConfigMetadatas = auditConfigsMetadata.stream()
                        .filter(auditConfigMetadata ->
                            Objects.equals(auditConfig.getService(), auditConfigMetadata.getService()))
                        .collect(Collectors.toList());
                    Assertions.assertEquals(1, auditConfigMetadatas.size());
                    List<IamPolicy.AuditLogConfig> logConfigMetadatas = auditConfigMetadatas.get(0).getAuditLogConfigs();
                    if (CollectionUtils.isEmpty(auditConfig.getAuditLogConfigs())) {
                        Assertions.assertTrue(CollectionUtils.isEmpty(logConfigMetadatas));
                    }
                    else {
                        auditConfig.getAuditLogConfigs().stream().forEach(
                            logConfig -> Assertions.assertTrue(
                                logConfigMetadatas.stream().anyMatch(
                                    logConfigMetadata ->
                                        Objects.equals(logConfig.getLogType(), logConfigMetadata.getLogType()) &&
                                            Objects.equals(logConfig.getExemptedMembers(), logConfigMetadata.getExemptedMembers())
                                )
                            )
                        );
                    }
                }
            );
        }
    }

    @ParameterizedTest(name = "policy #{index} - {0}")
    @MethodSource("policyAuditlogsProvider")
    public void enrichPolicyWithAuditConfigs(String description, Policy policy) throws Exception {
        // no response
        when(gcpApiService.getProjectIamPolicy(any(ProjectEnrichmentRequest.class))).thenReturn(policy);
        when(gcpApiService.getProjectNetworks(any(ProjectEnrichmentRequest.class))).thenReturn(testNetworkList());
        Mono<Resource> enrich = projectEnrichment.enrich(request);
        Assertions.assertNotNull(enrich);

        enrich.subscribe(resource -> checkAuditConfigs(resource, policy));
    }

    @Test
    public void enrichPolicyWithBothBindingsAndAuditConfigs() {
        Policy policy = new Policy()
                .setBindings(ImmutableList.of(
                        new Binding().setRole("role 1").setMembers(ImmutableList.of("user 1", "user 2")),
                        new Binding().setRole("role 1").setMembers(ImmutableList.of("user 3", "user 4"))))
                .setAuditConfigs(ImmutableList.of(
                        new AuditConfig().setService("service 1").setAuditLogConfigs(
                                ImmutableList.of(new AuditLogConfig().setLogType("type 1").setExemptedMembers(
                                        ImmutableList.of("member 1", "member 2"))))));
        // no response
        when(gcpApiService.getProjectIamPolicy(any(ProjectEnrichmentRequest.class))).thenReturn(policy);
        when(gcpApiService.getProjectNetworks(any(ProjectEnrichmentRequest.class))).thenReturn(testNetworkList());
        Mono<Resource> enrich = projectEnrichment.enrich(request);
        Assertions.assertNotNull(enrich);

        enrich.subscribe(resource -> checkAuditConfigs(resource, policy));
    }

    @Test
    public void testNetworkListInProject() {
        Policy policy = new Policy()
                .setBindings(ImmutableList.of(
                        new Binding().setRole("role 1").setMembers(ImmutableList.of("user 1", "user 2")),
                        new Binding().setRole("role 1").setMembers(ImmutableList.of("user 3", "user 4"))))
                .setAuditConfigs(ImmutableList.of(
                        new AuditConfig().setService("service 1").setAuditLogConfigs(
                                ImmutableList.of(new AuditLogConfig().setLogType("type 1").setExemptedMembers(
                                        ImmutableList.of("member 1", "member 2"))))));
        ProjectMetadata.Network expectedNetwork = ProjectMetadata.Network.builder()
                .name("foo")
                .id(BigInteger.ONE)
                .creationTimestamp("timestamp")
                .subnetworks(Collections.singletonList("subnetwork"))
                .build();
        when(gcpApiService.getProjectIamPolicy(any(ProjectEnrichmentRequest.class))).thenReturn(policy);
        when(gcpApiService.getProjectNetworks(any(ProjectEnrichmentRequest.class))).thenReturn(testNetworkList());
        Mono<Resource> enrich = projectEnrichment.enrich(request);
        Assertions.assertEquals(
                Collections.singletonList(expectedNetwork),
                enrich.block().getResourceMetadata(ProjectMetadata.class).getNetworks()
        );
    }

    private NetworkList testNetworkList() {
        Network testNetwork = new Network()
                .setName("foo")
                .setId(BigInteger.ONE)
                .setCreationTimestamp("timestamp")
                .setSubnetworks(Collections.singletonList("subnetwork"));
        return new NetworkList()
                .setItems(Collections.singletonList(testNetwork));
    }
}