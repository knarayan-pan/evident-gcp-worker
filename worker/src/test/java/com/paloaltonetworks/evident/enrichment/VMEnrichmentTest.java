package com.paloaltonetworks.evident.enrichment;


import com.google.api.services.compute.model.Instance;
import com.google.api.services.compute.model.InstanceAggregatedList;
import com.google.api.services.compute.model.InstancesScopedList;
import com.google.common.collect.ImmutableMap;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.metadata.InstanceMetadata;
import com.paloaltonetworks.evident.enrichment.request.ResourceEnrichmentRequest;
import com.paloaltonetworks.evident.exceptions.GCPApiException;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.models.request.FetchComputeInstancesRequest;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.test.StepVerifier;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.BDDMockito.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.when;

@Slf4j
public class VMEnrichmentTest {
    private static final ResourceEnrichmentRequest RESOURCE_ENRICHMENT_REQUEST =
            ResourceEnrichmentRequest.builder()
                    .externalAccountId("1")
                    .tenant("1")
                    .projectId("project-1")
                    .build();
    @Mock
    private GCPApiService gcpApiService;
    @Mock
    private QueueService queueService;
    @Mock
    private ResourceService resourceService;

    @Mock
    private RedisClient redisClient;

    @Mock
    private InstanceAggregatedList arggregatedList;

    @InjectMocks
    private VMEnrichment enrichment;


    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void enrichRetryableException() throws Exception {
        when(gcpApiService.getComputeInstance(any(ResourceRefreshRequest.class)))
                .thenThrow(new GCPApiException(1, "test exception"));
        try {
            StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST))
                    .verifyError();
            Assertions.fail("Expected exception was not thrown");
        } catch (GCPApiException e) {
            // this is expected exception
        }
    }

    @Test
    public void enrichNonRetryableException() throws Exception {
        when(gcpApiService.getComputeInstance(any(ResourceRefreshRequest.class)))
                .thenThrow(new GCPApiException(404, "test exception"));
        StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST))
                .verifyComplete();
    }

    @Test
    public void enrichList() throws Exception {
        stubAggregatedList();
        stubGetComputeInstances();

        StepVerifier.create(
                enrichment.enrichList(RESOURCE_ENRICHMENT_REQUEST))
                .assertNext(resource ->
                        assertNotNull(resource.getResourceMetadata(InstanceMetadata.class)))
                .assertNext(resource ->
                        assertNotNull(resource.getResourceMetadata(InstanceMetadata.class)))
                .expectComplete()
                .verify();
    }

    @Test
    public void enrichTest() throws Exception {
        // no instance exists, should ignore and complete
        StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST))
                .verifyComplete();

        //stub to return data
        stubGetComputeInstance(
                listOf(
                        metadataOf("cputest-test", "RUNNING",
                                "/someurl/testName1", "", "link1",
                                true, "2000-01-01T000")));

        // returns a valid data
        StepVerifier.create(enrichment.enrich(RESOURCE_ENRICHMENT_REQUEST))
                .assertNext(resource ->
                        assertEquals("/someurl/testName1",
                                resource.getResourceMetadata(InstanceMetadata.class).getName()))
                .verifyComplete();

    }

    @Test
    public void enrichDateParseTest() throws Exception {

        // no instance exists, should ignore and complete
        val enUnderTest = new VMEnrichment(resourceService, redisClient, queueService, gcpApiService);
        StepVerifier.create(
                enUnderTest.enrich(RESOURCE_ENRICHMENT_REQUEST))
                .verifyComplete();

        //stub to return data
        stubGetComputeInstance(listOf(metadataOf("cpu", "RUNNING", "/someurl/testName1", "", "link1", true, "BAD-FORMAT")));

        // returns a valid data
        StepVerifier.create(
                enUnderTest.enrich(RESOURCE_ENRICHMENT_REQUEST))
                .assertNext(resource ->
                        assertEquals("BAD-FORMAT",
                                resource.getResourceMetadata(InstanceMetadata.class).getCreateTimestamp())
                )
                .verifyComplete();

    }

    private void stubGetComputeInstance(List<Instance> instanceList) {
        given(gcpApiService.getComputeInstance(any(ResourceRefreshRequest.class)))
                .willReturn(instanceList.get(0));
    }

    private void stubGetComputeInstances() {
        when(gcpApiService.getComputeInstances(any(FetchComputeInstancesRequest.class)))
                .thenReturn(arggregatedList);
    }

    private void stubAggregatedList() {
        when(arggregatedList.getNextPageToken()).thenReturn("/mock/next");
        when(arggregatedList.getItems()).thenReturn(
                ImmutableMap.of(
                        "zone-1", instancesScopedListOf(metadataOf("cpu1", "RUNNING1", "/someurl/testName1", "", "link1", true, "2000-01-01T000")),
                        "zone-2", instancesScopedListOf(metadataOf("cpu2", "RUNNING2", "/someurl/testName2", "", "link1", true, "2000-01-01T000"))));
    }

    private InstanceMetadata metadataOf(String cpuPlatform, String status, String name, String description,
                                        String selfLink, boolean canIpForward, String createTimestamp) {
        return InstanceMetadata.builder()
                .id(BigInteger.valueOf(1L))
                .cpuPlatform(cpuPlatform)
                .status(status)
                .name(name)
                .description(description)
                .selfLink(selfLink)
                .canIpForward(canIpForward)
                .createTimestamp(createTimestamp)
                .machineType("testMType")
                .status("RUNNING")
                .zone("zone-1")
                .projectId("project-1")
                .build();
    }

    private InstancesScopedList instancesScopedListOf(InstanceMetadata... instanceMetadata) {
        return new InstancesScopedList().setInstances(listOf(instanceMetadata));
    }

    private List<Instance> listOf(InstanceMetadata... instanceMetadata) {
        return Stream.of(instanceMetadata).map(mdata -> {
                    final Instance inst = new Instance();
                    inst.setId(mdata.getId());
                    inst.setCreationTimestamp(mdata.getCreateTimestamp());
                    inst.setName(mdata.getName());
                    inst.setCanIpForward(mdata.isCanIpForward());
                    inst.setCpuPlatform(mdata.getCpuPlatform());
                    inst.setDescription(mdata.getDescription());
                    inst.setSelfLink(mdata.getSelfLink());
                    inst.setStatus(mdata.getStatus());
                    inst.setZone(mdata.getZone());
                    inst.setMachineType(mdata.getMachineType());
                    inst.setStatus(mdata.getStatus());
                    return inst;
                }
        ).collect(Collectors.toList());
    }
}
