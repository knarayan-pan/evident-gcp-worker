package com.paloaltonetworks.evident;

import com.paloaltonetworks.evident.signatures.AlertStatus;
import com.paloaltonetworks.evident.utils.JsonUtils;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import reactor.util.function.Tuple2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Map;


/**
 * Test Utility methods
 */
public class TestUtil {
    public static void assertStatusAndMessage(Tuple2<AlertStatus, String> statusAndMessage,
                                              AlertStatus status, String message) {
        assertEquals(status, statusAndMessage.getT1());
        assertEquals(message, statusAndMessage.getT2());
    }

    public static void assertMetadata(JSONObject metadata1, JSONObject metadata2, String message) {
        Assertions.assertEquals(JsonUtils.decodeValue(metadata1.toString(), Map.class),
            JsonUtils.decodeValue(metadata2.toString(), Map.class), message);
    }
}
