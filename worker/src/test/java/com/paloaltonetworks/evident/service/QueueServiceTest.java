package com.paloaltonetworks.evident.service;

import com.paloaltonetworks.aperture.ApertureSqsAsyncClient;
import com.paloaltonetworks.evident.config.PropertyHandler;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class QueueServiceTest {

    @Mock
    ApertureSqsAsyncClient apertureSqsAsyncClient;

    @Mock
    PropertyHandler propertyHandler;

    private QueueService queueService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        queueService = new QueueService(apertureSqsAsyncClient);
    }

    @AfterEach
    public void tearDown() {
        queueService = null;
    }

    @Test
    public void acquireLocalMsgLock() {
        assertTrue(queueService.acquireLocalMsgLock("msg1"));
        assertFalse(queueService.acquireLocalMsgLock("msg1"));
    }

    @Test
    public void clearLock() {
        assertTrue(queueService.acquireLocalMsgLock("msg1"));
        queueService.clearLock("msg1");
        assertTrue(queueService.acquireLocalMsgLock("msg1"));
    }

}