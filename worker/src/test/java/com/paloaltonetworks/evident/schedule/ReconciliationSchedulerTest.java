package com.paloaltonetworks.evident.schedule;

import com.paloaltonetworks.aperture.consul.ConsulService;
import com.paloaltonetworks.evident.config.PropertyHandler;
import com.paloaltonetworks.evident.exceptions.RedisClientException;
import com.paloaltonetworks.evident.redis.DefaultRedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class ReconciliationSchedulerTest {
    @Mock
    private ConsulService consulService;
    @Mock
    private PropertyHandler propertyHandler;
    @Mock
    private DefaultRedisClient redisClient;
    @Mock
    private QueueService queueService;

    private ReconciliationScheduler reconciliationScheduler;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);


        when(propertyHandler.getTenant()).thenReturn("test-env");
        when(propertyHandler.getExternalId()).thenReturn("test-ext-id");
        when(consulService.getIntForTenant(eq("test-env"), eq(ReconciliationScheduler.RECONCILIATION_INTERVAL_IN_HOURS_KEY),
                eq(ReconciliationScheduler.DEFAULT_RECONCILIATION_INTERVAL_IN_HOURS))).thenReturn(2);
        when(redisClient.acquireLock(anyString(), anyInt())).thenThrow(RedisClientException.class);
        reconciliationScheduler = new ReconciliationScheduler(consulService, Executors.newScheduledThreadPool(1),
                propertyHandler, redisClient, queueService);

    }

    @Test
    public void getSchedulerInterval() throws Exception {
        // ensure the interval is converted to seconds
        assertEquals(reconciliationScheduler.getSchedulerInterval(), 7200);
    }

    @Test
    public void schedule() throws Exception {
        reconciliationScheduler.schedule();
    }

}