package com.paloaltonetworks.evident.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ResourceUtilTest {
    @Test
    public void extractGCEInstanceName() throws Exception {
       assertEquals("test-name1",
               ResourceUtil.extractGCEInstanceName("/project/1/zones/2/name/test-name1"));
        assertEquals("test-name2",
                ResourceUtil.extractGCEInstanceName("test-name2"));
    }

    @Test
    public void extractResourceName() throws Exception {
        assertEquals("test-name1",
                ResourceUtil.extractResourceName("/project/1/zones/2/name/test-name1"));
        assertEquals("test-name2",
                ResourceUtil.extractResourceName("test-name2"));
    }

    @Test
    public void extractSQLInstanceName() throws Exception {
        assertEquals("test-sql1",
                ResourceUtil.extractSQLInstanceName("project:test-sql1"));
        assertEquals("test-sql2",
                ResourceUtil.extractSQLInstanceName("test-sql2"));
    }

    @Test
    public void extractZone() throws Exception {
        assertEquals("test-region-1a",
                ResourceUtil.extractZone("/project/1/zones/2/test-Region-1a"));
        assertEquals("test-zone2",
                ResourceUtil.extractZone("test-zone2"));
        assertEquals("global",
                ResourceUtil.extractZone(null));
    }

    @Test
    public void extractRegionFromZone() throws Exception {
        assertEquals("test-zone1",
                ResourceUtil.extractRegionFromZone("/project/1/zones/2/test-Zone1-a"));
        assertEquals("test",
                ResourceUtil.extractRegionFromZone("TEST-zone2"));
        assertEquals("global",
                ResourceUtil.extractRegionFromZone(null));
    }

    @Test
    public void makeResourceIdTest1() throws Exception {
        assertEquals("prj1/test-name1",
                ResourceUtil.makeResourceId("prj1",
                        "/project/1/zones/2/name/test-name1"));
    }
}