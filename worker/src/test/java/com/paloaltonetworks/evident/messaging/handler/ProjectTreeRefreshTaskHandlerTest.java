package com.paloaltonetworks.evident.messaging.handler;

import com.paloaltonetworks.aperture.consul.ConsulService;
import com.paloaltonetworks.evident.config.GCPConfig;
import com.paloaltonetworks.evident.enrichment.NoOpEnrichment;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.messaging.task.ProjectRefreshTaskMessage;
import com.paloaltonetworks.evident.service.QueueService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ProjectTreeRefreshTaskHandlerTest {
    @Mock
    private QueueService queueService;
    private ProjectTreeRefreshTaskHandler handler;
    @Mock
    private GCPConfig config;
    @Mock
    private ConsulService consulService;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        // default it to no-op
        handler = new ProjectTreeRefreshTaskHandler(queueService, config, consulService);
    }

    @Test
    public void execute() throws Exception {
        doNothing().when(queueService).queueResourceRefresh(any(ResourceRefreshRequest.class));
        when(config.getEnrichmentClass(anyString())).thenReturn(new NoOpEnrichment());
        when(consulService.getValueForTenant(anyString(), anyString(), anyString())).thenReturn("");
        handler.execute(ProjectRefreshTaskMessage.builder()
                .resourceId("rid")
                .externalId("ectId")
                .tenantName("tenant")
                .resourceType("unknown-type")
                .build());
        verify(queueService, times(ResourceType.values().length - 1)).queueResourceRefresh(any(ResourceRefreshRequest.class));
    }

}