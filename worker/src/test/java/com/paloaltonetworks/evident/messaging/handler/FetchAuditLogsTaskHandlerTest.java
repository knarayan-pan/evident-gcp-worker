package com.paloaltonetworks.evident.messaging.handler;

import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.models.AuditLogPage;
import com.paloaltonetworks.evident.gcp.models.request.GcpActivityLogRequest;
import com.paloaltonetworks.evident.messaging.task.AuditLogMessage;
import com.paloaltonetworks.evident.service.GCPAuditLogService;
import com.paloaltonetworks.evident.service.QueueService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class FetchAuditLogsTaskHandlerTest {

    @Mock
    private GCPAuditLogService auditLogService;
    @Mock
    private GCPApiService gcpApiService;
    @Mock
    private QueueService queueService;
    private FetchAuditLogsTaskHandler handler;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        handler = new FetchAuditLogsTaskHandler(auditLogService, gcpApiService, queueService);
    }

    @Test
    public void emptyResponseFromCloud() throws Exception {
        when(gcpApiService.getAuditLogs(any(GcpActivityLogRequest.class))).thenReturn(new AuditLogPage());
        handler.execute(AuditLogMessage.builder()
                .tenantName("tenant")
                .externalId("extId")
                .projectIds(ImmutableSet.<String>builder().build())
                .fromDate(new Date())
                .toDate(new Date())
                .build());
        verify(auditLogService, times(0)).processAuditLogPage(any(GcpActivityLogRequest.class),
                any());
    }
}