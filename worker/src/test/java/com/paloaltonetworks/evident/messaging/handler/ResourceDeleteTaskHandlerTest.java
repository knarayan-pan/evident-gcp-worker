package com.paloaltonetworks.evident.messaging.handler;

import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.config.GCPConfig;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.metadata.BucketMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.esp.model.AlertRequest;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.messaging.task.ResourceDeleteTaskMessage;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.signatures.GCPSignature;
import com.paloaltonetworks.evident.signatures.ProjectSignature;
import com.paloaltonetworks.evident.utils.JsonUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.Collections;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ResourceDeleteTaskHandlerTest {

    @Mock
    private
    ResourceService resourceService;

    @Mock
    private
    ESPClient espClient;

    @Mock
    private GCPConfig config;

    @Mock
    private GCPSignature signature;

    @Mock
    private RedisClient redisClient;

    @InjectMocks
    private
    ResourceDeleteTaskHandler deleteTaskHandler;
    private String tenant = "tenant";
    private String externalAccountId = "externalAccountId";

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void execute() throws Exception {
        when(config.getSignatureClass(anyString(), anyString())).thenReturn(ImmutableSet.of(signature));
        when(resourceService.findOne(anyString(), any(ResourceKey.class))).thenReturn(Optional.of(Resource.builder()
                .cloudAppType(CloudAppType.GCP)
                .resourceId("1")
                .resourceMetadata("{}")
                .resourceKey(ResourceKey.builder()
                        .externalAccountId(externalAccountId)
                        .resourceIdHash(1)

                        .resourceType(ResourceType.GCE_FIREWALL.toString())
                        .build())
                .build()));
        when(espClient.suppressAlert(eq(externalAccountId), any(AlertRequest.class))).thenThrow(new IOException());
        ResourceDeleteTaskMessage resourceDeleteTaskMessage = ResourceDeleteTaskMessage.builder()
                .externalId(externalAccountId)
                .tenantName(tenant)
                .resourceIds(Collections.singleton("resourceId"))
                .resourceType("instance")
                .build();
        try {
            deleteTaskHandler.execute(resourceDeleteTaskMessage);
            Assertions.fail("Expected exception was not thrown");
        } catch (RuntimeException e) {
            // this is expected exception
        }
    }

    @Test
    public void executeSuccess() throws Exception {
        when(config.getSignatureClass(eq("instance"), any())).thenReturn(ImmutableSet.of(new ProjectSignature(espClient, null)));
        when(resourceService.findOne(eq(tenant), any(ResourceKey.class))).thenReturn(Optional.ofNullable(Resource.builder()
                .resourceId("resourceId")
                .resourceKey(ResourceKey.builder()
                        .externalAccountId(externalAccountId)
                        .resourceIdHash(123)
                        .resourceType("instance")
                        .build())
                .resourceMetadata(JsonUtils.encode(BucketMetadata.builder().projectId("test-proj").build()))
                .region("us")
                .build()));
        when(espClient.suppressAlert(eq(externalAccountId), any(AlertRequest.class))).thenReturn(null);
        ResourceDeleteTaskMessage resourceDeleteTaskMessage = ResourceDeleteTaskMessage.builder()
                .externalId(externalAccountId)
                .tenantName(tenant)
                .resourceIds(Collections.singleton("resourceId"))
                .resourceType("instance")
                .build();
        deleteTaskHandler.execute(resourceDeleteTaskMessage);
        verify(resourceService, times(1)).deleteResource(eq(tenant), any(ResourceKey.class));

    }
}