package com.paloaltonetworks.evident.messaging.handler;

import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.config.GCPConfig;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.metadata.BucketMetadata;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.messaging.task.SignatureTaskMessage;
import com.paloaltonetworks.evident.service.QueueService;
import com.paloaltonetworks.evident.signatures.GCPSignature;
import com.paloaltonetworks.evident.signatures.ProjectSignature;
import com.paloaltonetworks.evident.utils.JsonUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SignatureHandlerTest {

    @Mock
    private GCPConfig gcpConfig;
    @Mock
    private ResourceService resourceService;
    @Mock
    private ProjectSignature projectSignature;

    @Mock
    private QueueService queueService;

    @InjectMocks
    private SignatureHandler signatureHandler;
    private String tenant = "tenant";
    private String externalId = "externalId";

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void executeWhenResourceIsNotFound() throws Exception {
        when(resourceService.findOne(eq(tenant), any(ResourceKey.class))).thenReturn(Optional.empty());
        assertThrows(RuntimeException.class, () -> signatureHandler.execute(SignatureTaskMessage.builder()
                .projectId("project1")
                .resourceId("resource1")
                .resourceType("instance")
                .externalId(externalId)
                .tenantName(tenant)
                .build()));
        verify(gcpConfig, times(0)).getSignatureClass(anyString(), any());
    }

    @Test
    public void executeWhenResourceIsFoundButNoSignature() throws Exception {
        when(resourceService.findOne(eq(tenant), any(ResourceKey.class))).thenReturn(Optional.of(Resource.builder()
                .tenant(tenant)
                .resourceKey(ResourceKey.builder()
                        .externalAccountId(externalId)
                        .resourceType("instance")
                        .resourceIdHash(1234)
                        .build())
                .resourceMetadata(JsonUtils.encode(BucketMetadata.builder().build()))
                .build()));
        signatureHandler.execute(SignatureTaskMessage.builder()
                .projectId("project1")
                .resourceId("resource1")
                .resourceType("instance")
                .externalId(externalId)
                .tenantName(tenant)
                .build());
        verify(gcpConfig, times(1)).getSignatureClass(anyString(), any());
    }

    @Test
    public void executeWithResourceAndSignatureFound() throws Exception {
        Resource resource = Resource.builder()
                .tenant(tenant)
                .resourceKey(ResourceKey.builder()
                        .externalAccountId(externalId)
                        .resourceType("instance")
                        .resourceIdHash(1234)
                        .build())
                .resourceMetadata(JsonUtils.encode(BucketMetadata.builder().build()))
                .build();
        when(resourceService.findOne(eq(tenant), any(ResourceKey.class))).thenReturn(Optional.of(resource));
        when(gcpConfig.getSignatureClass(eq("instance"), any())).thenReturn(ImmutableSet.<GCPSignature>builder()
                .add(projectSignature)
                .build());
        signatureHandler.execute(SignatureTaskMessage.builder()
                .projectId("project1")
                .resourceId("resource1")
                .resourceType("instance")
                .externalId(externalId)
                .tenantName(tenant)
                .build());
        verify(gcpConfig, times(1)).getSignatureClass(anyString(), any());
        verify(projectSignature, times(1)).check(eq(resource));
    }

    @Test
    public void executeSigSpecificRescan() throws Exception {
        when(gcpConfig.getSignatureResourceTypes(anyString())).thenReturn(Collections.singleton(ResourceType.GCS_STORAGE));
        signatureHandler.execute(SignatureTaskMessage.builder()
                .tenantName("2921")
                .externalId("1234")
                .signatureId("GCP:GCS-001")
                .build());
        verify(gcpConfig, times(1)).getSignatureResourceTypes(eq("GCP:GCS-001"));
    }
}