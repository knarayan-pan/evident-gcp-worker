package com.paloaltonetworks.evident.messaging.task;

import com.paloaltonetworks.evident.utils.JsonUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class StartTaskMessageTest {

    @Test
    public void serialization() {
        StartTaskMessage abc = StartTaskMessage.builder()
                .externalId("123")
                .tenantName("abc")
                .build();
        assertNotNull(abc);
        String encode = JsonUtils.encode(abc);
        assertNotNull(encode);
    }

    @Test
    public void deserialization() throws Exception {
        TaskMessage taskMessage = JsonUtils.decodeValue("{\n" +
                "  \"task_type\": \"start_task\",\n" +
                "  \"tenant\": \"abc\",\n" +
                "  \"external_id\": \"123\",\n" +
                "  \"current_attempt_count\": 0,\n" +
                "  \"max_attempt_count\": 5,\n" +
                "  \"attempts_with_runtime_errors_count\": 0,\n" +
                "  \"max_attempts_with_runtime_errors_count\": 2,\n" +
                "  \"task_id\": \"feacef36-e439-4b9f-9b2f-902cb6101239\",\n" +
                "  \"parent_task_id\": null\n" +
                "}", TaskMessage.class);
        assertTrue(taskMessage instanceof StartTaskMessage);
    }
}