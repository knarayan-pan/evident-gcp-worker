package com.paloaltonetworks.evident.messaging.handler;

import com.paloaltonetworks.evident.config.GCPConfig;
import com.paloaltonetworks.evident.enrichment.NoOpEnrichment;
import com.paloaltonetworks.evident.enrichment.request.ResourceEnrichmentRequest;
import com.paloaltonetworks.evident.messaging.task.ResourceRefreshTaskMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ResourceRefreshTaskHandlerTest {
    @Mock
    private GCPConfig gcpConfig;
    @Mock
    private NoOpEnrichment noOpEnrichment;
    @InjectMocks
    private ResourceRefreshTaskHandler resourceRefreshTaskHandler;
    private String tenant = "tenant";
    private String externalAccountId = "extId";

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(gcpConfig.getEnrichmentClass(eq("instance"))).thenReturn(noOpEnrichment);
    }

    @Test
    public void executeSpecificResource() throws Exception {
        ResourceRefreshTaskMessage singleResourceMessage = ResourceRefreshTaskMessage.builder()
                .externalId(externalAccountId)
                .tenantName(tenant)
                .resourceId("resourceId")
                .resourceType("instance")
                .projectId("project1")
                .organizationId("1234")
                .build();

        resourceRefreshTaskHandler.execute(singleResourceMessage);
        verify(noOpEnrichment, times(1)).apply(any(ResourceEnrichmentRequest.class));
        verify(noOpEnrichment, times(0)).applyForCollection(any(ResourceEnrichmentRequest.class));
    }

    @Test
    public void executeResourcePage() throws Exception {
        ResourceRefreshTaskMessage pageOfSameResourceType = ResourceRefreshTaskMessage.builder()
                .externalId(externalAccountId)
                .tenantName(tenant)
                .resourceType("instance")
                .projectId("project1")
                .pageToken(null)
                .pageSize(50)
                .organizationId("1234")
                .build();
        resourceRefreshTaskHandler.execute(pageOfSameResourceType);
        verify(noOpEnrichment, times(0)).apply(any(ResourceEnrichmentRequest.class));
        verify(noOpEnrichment, times(1)).applyForCollection(any(ResourceEnrichmentRequest.class));
    }
}