package com.paloaltonetworks.evident.messaging.handler;

import com.google.api.services.cloudresourcemanager.model.ListProjectsResponse;
import com.google.common.collect.ImmutableMap;
import com.paloaltonetworks.evident.config.ResourceWorkerQueues;
import com.paloaltonetworks.evident.data_access.models.AccountStatus;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.data_access.service.AccountStatusService;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.metadata.ProjectMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.esp.model.ESPApiData;
import com.paloaltonetworks.evident.esp.model.ESPApiResponse;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.gcp.models.request.FetchOrganizationIdRequest;
import com.paloaltonetworks.evident.gcp.models.request.FetchProjectsRequest;
import com.paloaltonetworks.evident.gcp.models.request.GcpActivityLogRequest;
import com.paloaltonetworks.evident.messaging.task.StartTaskMessage;
import com.paloaltonetworks.evident.service.QueueService;
import com.paloaltonetworks.evident.utils.JsonUtils;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Date;
import java.util.Optional;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class StartTaskHandlerTest {
    @Mock
    private QueueService queueService;
    @Mock
    private ESPClient espClient;
    @Mock
    private GCPApiService apiService;
    @Mock
    private ResourceService resourceService;
    @Mock
    private AccountStatusService accountStatusService;
    private StartTaskHandler handler;
    private String tenant = "tenant";
    private String externalId = "extId";

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        handler = new StartTaskHandler(queueService, espClient, apiService, resourceService, accountStatusService);
        doNothing().when(accountStatusService).upsert(eq(tenant), eq(externalId), any(AccountStatus.class));
        when(espClient.gcpAccount(eq(externalId))).thenReturn(ESPApiResponse.builder()
                .data(ESPApiData.builder()
                        .attributes(ImmutableMap.<String, Object>builder()
                                .put(ESPClient.DOMAIN, "domain")
                                .build())
                        .build())
                .build());
        val request = FetchOrganizationIdRequest.builder()
                .tenant(tenant)
                .externalAccountId(externalId)
                .build();
        when(apiService.organizationId(eq(request), anyString())).thenReturn(Optional.of("orgId"));
    }

    @Test
    public void onboarding() throws Exception {
        // check onboarding
        when(accountStatusService.getStatus(eq(tenant), eq(externalId))).thenReturn(Optional.empty());
        val request = FetchProjectsRequest.builder()
                .tenant(tenant)
                .externalAccountId(externalId)
                .build();
        when(apiService.getProjects(eq(request), any()))
                .thenReturn(new ListProjectsResponse().setNextPageToken(null).setProjects(null));
        handler.execute(StartTaskMessage.builder()
                .tenantName(tenant)
                .externalId(externalId)
                .build());
        verify(accountStatusService, times(3)).upsert(eq(tenant), eq(externalId), any());

    }

    @Test
    public void iterativeScan() throws Exception {
        when(accountStatusService.getStatus(eq(tenant), eq(externalId))).thenReturn(Optional.of(AccountStatus.builder()
                .onboardingDate(new Date())
                .build()));
        when(queueService.getNumMessages(eq(externalId), eq(ResourceWorkerQueues.AUDIT_LOG.getQueueName()))).thenReturn(0L);
        when(queueService.getNumMessages(eq(externalId), eq(ResourceWorkerQueues.FETCH_RESOURCES.getQueueName()))).thenReturn(0L);

        when(resourceService.findResourcesByType(eq(tenant), eq(externalId), eq(ResourceType.GCP_PROJECT.getResourceType())))
                .thenReturn(Stream.of(Resource.builder()
                        .resourceId("proj1")
                        .resourceKey(ResourceKey.builder()
                                .resourceType(ResourceType.GCP_PROJECT.getResourceType())
                                .resourceIdHash(12345)
                                .externalAccountId(externalId)
                                .build())
                        .resourceMetadata(JsonUtils.encode(ProjectMetadata.builder().build()))
                        .tenant(tenant)
                        .region("global")
                        .build()));
        handler.execute(StartTaskMessage.builder()
                .tenantName(tenant)
                .externalId(externalId)
                .build());
        verify(queueService, times(1)).queueAuditLogTask(any(GcpActivityLogRequest.class));
        verify(accountStatusService, times(1)).updateLastScanEventTime(eq(tenant), eq(externalId), any());
    }

}