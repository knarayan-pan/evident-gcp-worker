package com.paloaltonetworks.evident.messaging.task;

import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.utils.JsonUtils;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AuditLogMessageTest {

    @Test
    public void deserialize() throws Exception {
        String json = "{\"tenant\":\"krish\",\"external_id\":\"1234\",\"from\":\"2018-08-04T02:38:26.102+0000\",\"to\":\"2018-08-04T02:40:43.923+0000\",\"pageToken\":null,\"projectIds\":[\"digital-rhythm-188121\",\"gcpbackendnishantadmin\",\"aperture-project\",\"preflightcheck-207010\",\"even-equinox-188119\",\"aperture-3rd-party-monitor-stg\",\"cellular-smoke-187319\",\"wired-compass-161616\",\"aperture-gmail-integration\",\"maxime-test-project\",\"test-proj-211520\",\"test-corp-eco-monitoring\"],\"retriedTask\":false,\"current_attempt_count\":0,\"max_attempt_count\":5,\"attempts_with_runtime_errors_count\":0,\"max_attempts_with_runtime_errors_count\":2,\"task_type\":\"fetch_audit_log\"}";
        AuditLogMessage auditLogMessage = JsonUtils.decodeValue(json, AuditLogMessage.class);
        assertNotNull(auditLogMessage);
    }

    @Test
    public void serialize() throws Exception {
        AuditLogMessage ms = AuditLogMessage.builder()
                .externalId("1234")
                .tenantName("tenant")
                .fromDate(new Date())
                .toDate(new Date())
                .projectIds(ImmutableSet.<String>builder().add("p1").build())
                .pageToken(null)
                .build();
        String encode = JsonUtils.encode(ms);
        assertNotNull(encode);
    }

    @Test
    public void serializeStart() throws Exception {
        StartTaskMessage build = StartTaskMessage.builder()
                .tenantName("tenant")
                .externalId("extr")
                .build();
        String encode = JsonUtils.encode(build);
        assertNotNull(encode);
    }
}