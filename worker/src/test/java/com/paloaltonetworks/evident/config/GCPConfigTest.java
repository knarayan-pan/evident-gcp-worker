package com.paloaltonetworks.evident.config;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableList;
import com.paloaltonetworks.aperture.consul.ConsulService;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.GCPEnrichment;
import com.paloaltonetworks.evident.enrichment.ProjectEnrichment;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import com.paloaltonetworks.evident.signatures.GCPSignature;
import com.paloaltonetworks.evident.signatures.ProjectSignature;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

public class GCPConfigTest {

    @Mock
    private GCPApiService gcpApiService;
    @Mock
    private ResourceService resourceService;
    @Mock
    private ESPClient espClient;
    @Mock
    private RedisClient redisClient;
    @Mock
    private QueueService queueService;
    @Mock
    private MetricRegistry metricRegistry;
    @Mock
    private ConsulService consulService;

    private List<GCPEnrichment> enrichmentList = ImmutableList.<GCPEnrichment>builder()
            .add(new ProjectEnrichment(resourceService, redisClient, queueService, gcpApiService))
            .build();
    private List<GCPSignature> signatureList = ImmutableList.<GCPSignature>builder()
            .add(new ProjectSignature(espClient, metricRegistry))
            .build();
    private GCPConfig gcpConfig;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        gcpConfig = new GCPConfig(enrichmentList, signatureList, consulService);
    }

}