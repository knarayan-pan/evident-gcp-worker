package com.paloaltonetworks.evident.integration;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import com.google.api.services.cloudresourcemanager.CloudResourceManager;
import com.google.api.services.cloudresourcemanager.model.ListProjectsResponse;
import com.google.api.services.compute.Compute;
import com.google.api.services.dns.model.ManagedZone;
import com.google.api.services.dns.model.ManagedZonesListResponse;
import com.google.api.services.storage.model.Bucket;
import com.google.api.services.storage.model.Buckets;
import com.google.api.services.storage.model.Policy;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.aperture.consul.PropertyCipher;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.enrichment.SubnetworkEnrichment;
import com.paloaltonetworks.evident.enrichment.request.ProjectEnrichmentRequest;
import com.paloaltonetworks.evident.enrichment.request.ResourceEnrichmentRequest;
import com.paloaltonetworks.evident.esp.NoOpESPClient;
import com.paloaltonetworks.evident.gcp.api.APIConfig;
import com.paloaltonetworks.evident.gcp.api.DefaultGCPApiService;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.api.GCPRateLimitingService;
import com.paloaltonetworks.evident.gcp.models.AuditLog;
import com.paloaltonetworks.evident.gcp.models.AuditLogPage;
import com.paloaltonetworks.evident.gcp.models.request.FetchBucketsRequest;
import com.paloaltonetworks.evident.gcp.models.request.FetchComputeInstancesRequest;
import com.paloaltonetworks.evident.gcp.models.request.FetchFirewallsRequest;
import com.paloaltonetworks.evident.gcp.models.request.FetchOrganizationIdRequest;
import com.paloaltonetworks.evident.gcp.models.request.FetchProjectsRequest;
import com.paloaltonetworks.evident.gcp.models.request.FetchSQLInstancesRequest;
import com.paloaltonetworks.evident.gcp.models.request.GcpActivityLogRequest;
import com.paloaltonetworks.evident.gcp.models.request.ManagedZoneRequest;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.gcp.token.DefaultGCPTokenService;
import com.paloaltonetworks.evident.gcp.token.GCPTokenService;
import com.paloaltonetworks.evident.service.QueueService;
import com.paloaltonetworks.evident.signatures.NET001FlowLogsEnabled;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.collections.CollectionUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Flux;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Optional;

@Disabled
@Slf4j
class GCPApiIntegrationTest {
    @Mock private GCPRateLimitingService gcpRateLimitingService;
    @Mock private QueueService queueService;
    @InjectMocks private SubnetworkEnrichment subnetworkEnrichment;
    @InjectMocks private DefaultGCPApiService gcpApiService;
    @Mock private GCPApiService mockApiService;

    private final String externalAccountId = "1234";
    private final String tenant = "test";
    private CloudResourceManager crmSpy;
    private Compute computeSpy;
    APIConfig apiConfig = new APIConfig();

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        // set up proxy cache proxy for redis calls
        Cache<String, String> tokenCache = CacheBuilder.<String, String>newBuilder().build();
        // stub out ratelimit and redis api calls
        when(gcpRateLimitingService.getRateLimitLock(anyString(), anyString(), any())).thenReturn(true);
        // fetch and cache token for this account for this integration test
        GCPTokenService gcpTokenService = new DefaultGCPTokenService(new NoOpESPClient());

        val initzer = apiConfig.gcpHttpInitializer();
        gcpApiService = new DefaultGCPApiService(gcpTokenService,
                apiConfig.cloudResourceManager(initzer),
                apiConfig.storage(initzer),
                apiConfig.compute(initzer),
                apiConfig.sqlAdmin(initzer),
                apiConfig.dnsClient(initzer), apiConfig.httpClientCache(gcpTokenService));
        String oauthToken = gcpTokenService.getOauthToken(externalAccountId);
        tokenCache.put(externalAccountId, PropertyCipher.cipher(oauthToken));
    }

    @AfterEach
    void tearDown() {
        gcpApiService = null;
    }

    @InjectMocks private NET001FlowLogsEnabled checker;

    @Test
    void getSubNetworks() {
        when(mockApiService.getSubNetworks(any()))
                .thenAnswer(i -> gcpApiService.getSubNetworks(i.getArgument(0)));

        Flux<Resource> test = subnetworkEnrichment.enrichList(ResourceEnrichmentRequest.builder()
                .projectId("evidentio-ev-dev")
                .externalAccountId(externalAccountId)
                .tenant(tenant)
                .resourceType("gce_subnetwork")
                .build());

        test.subscribe(r -> {
            log.debug(checker.alertStatusAndMessage(r).toString());
        });
        log.info("{}", test);
        assertNotNull(test);
    }

    @Test
    public void getProjectList() throws Exception {
        ListProjectsResponse res = gcpApiService.getProjects(FetchProjectsRequest.builder()
                        .tenant(tenant)
                        .externalAccountId(externalAccountId)
                        .build(),
                null);
        assertNotNull(res);
        assertTrue(CollectionUtils.isNotEmpty(res.getProjects()));
        assertTrue(res.getProjects().size() > 0);
    }

    @Test
    public void getLogs() throws Exception {
        GcpActivityLogRequest request;
        String pageToken = null;
        AuditLogPage auditLogResponse;
        do {
            auditLogResponse = gcpApiService.getAuditLogs(GcpActivityLogRequest.builder()
                    .tenant(tenant)
                    .externalAccountId(externalAccountId)
                    .projectIds(ImmutableSet.<String>builder()
                            .add("test-proj-211520")
                            .add("even-equinox-188119")
                            .build())
                    .fromDate(Date.from(Instant.now().minus(10, ChronoUnit.DAYS)))
                    .toDate(Date.from(Instant.now()))
                    .pageToken(pageToken)
                    .build());
            assertNotNull(auditLogResponse);
            pageToken = auditLogResponse.getNextPageToken();
            assertTrue(CollectionUtils.isNotEmpty(auditLogResponse.getEntries()));
            for (AuditLog entry : auditLogResponse.getEntries()) {
                assertNotNull(entry.getProtoPayload());
            }
            System.out.println(String.format("Fetched %s events in this call", auditLogResponse.getEntries().size()));
        } while (auditLogResponse.hasNextPage());
    }

    @Test
    public void getBuckets() throws Exception {
        Buckets test = gcpApiService.getBuckets(FetchBucketsRequest.builder()
                .projectId("even-equinox-188119")
                .externalAccountId(externalAccountId)
                .tenant(tenant)
                .build());
        assertNotNull(test);
    }

    @Test
    public void getComputeInstances() throws Exception {
        val test = gcpApiService.getComputeInstances(FetchComputeInstancesRequest.builder()
                .projectId("even-equinox-188119")
                .externalAccountId(externalAccountId)
                .tenant(tenant)
                .build());
        log.info("{}", test);
        assertNotNull(test);
    }

    @Test
    public void getComputeInstance() throws Exception {
        val test = gcpApiService.getComputeInstance(ResourceRefreshRequest.builder()
                .projectId("even-equinox-188119")
                .externalAccountId(externalAccountId)
                .zone("us-east1-b")
                .resourceName("test-instance-1")
                .tenant(tenant)
                .build());
        log.info("{}", test);

        assertNotNull(test);
    }

    @Test
    public void getSQLInstances() throws Exception {
        val test = gcpApiService.getSQLInstances(FetchSQLInstancesRequest.builder()
                .projectId("even-equinox-188119")
                .externalAccountId(externalAccountId)
                .tenant(tenant)
                .build());
        log.info("{}", test);
        assertNotNull(test);
    }

    @Test
    public void getSQLInstance() throws Exception {
        val test = gcpApiService.getSQLInstance(ResourceRefreshRequest.builder()
                .projectId("even-equinox-188119")
                .externalAccountId(externalAccountId)
                .resourceId("vimal-test-mysql")
                .resourceName("vimal-test-mysql")
                .tenant(tenant)
                .build());
        log.info("{}", test);
        assertNotNull(test);
    }

    @Test
    public void getFirewalls() throws Exception {
        val test = gcpApiService.getFirewalls(FetchFirewallsRequest.builder()
                .projectId("even-equinox-188119")
                .externalAccountId(externalAccountId)
                .tenant(tenant)
                .build());
        log.info("{}", test);
        assertNotNull(test);
    }

    @Test
    public void getFirewall() throws Exception {
        val test = gcpApiService.getFirewall(ResourceRefreshRequest.builder()
                .projectId("even-equinox-188119")
                .externalAccountId(externalAccountId)
                .resourceId("6830471202412187877")
                .resourceName("default-allow-icmp")
                .tenant(tenant)
                .build());
        log.info("{}", test);
        assertNotNull(test);
    }

    @Test
    public void getBucket() throws Exception {
        Bucket bucket = gcpApiService.getBucket(ResourceRefreshRequest.builder()
                .resourceId("evident-bucket")
                .resourceType("bucket")
                .tenant(tenant)
                .externalAccountId(externalAccountId)
                .build());
        assertNotNull(bucket);
    }

    @Test
    public void getIamPolicy() throws Exception {
        Policy policy = gcpApiService.getBucketPolicy(ResourceRefreshRequest.builder()
                .resourceId("ap-quarantine-do-not-touch-080518-h0u36p")
                .resourceType("bucket")
                .tenant(tenant)
                .externalAccountId(externalAccountId)
                .build());
        assertNotNull(policy);
    }

    @Test
    public void getOrganizationId() throws Exception {
        Optional<String> idOptional = gcpApiService.organizationId(FetchOrganizationIdRequest.builder()
                .tenant(tenant)
                .externalAccountId(externalAccountId)
                .build(), "cirrotester.com");
        assertTrue(idOptional.isPresent());
        assertEquals(idOptional.get(), "605530761854");
    }

    @Test
    public void getManagedZones() throws Exception {
        ManagedZonesListResponse managedZones = gcpApiService.getManagedZones(ManagedZoneRequest.builder()
                .projectId("even-equinox-188119")
                .externalAccountId(externalAccountId)
                .tenant(tenant)
                .build());
        assertNotNull(managedZones);
        assertTrue(managedZones.getManagedZones().size() > 0);
    }

    @Test
    public void getManagedZoneDetails() throws Exception {
        ManagedZone sample2 = gcpApiService.getManagedZoneDetails(ManagedZoneRequest.builder()
                .projectId("even-equinox-188119")
                .externalAccountId(externalAccountId)
                .tenant(tenant)
                .managedZone("sample2")
                .build());
        assertNotNull(sample2);
        assertNotNull(sample2.getDnssecConfig());
    }

    @Test
    public void getProjectIamPolicy() throws Exception {
        com.google.api.services.cloudresourcemanager.model.Policy policy =
            gcpApiService.getProjectIamPolicy(ProjectEnrichmentRequest.builder()
                .resourceId("even-equinox-188119")
                .externalAccountId(externalAccountId)
                .projectName("even-equinox")
                .tenant(tenant)
                .build());
        assertNotNull(policy);
    }
}
