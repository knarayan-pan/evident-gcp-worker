package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.enrichment.metadata.SubNetworkMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.utils.JsonUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.paloaltonetworks.evident.TestUtil.assertStatusAndMessage;
import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCE_SUBNETWORK;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class NET002PrivateIpAccessEnabledTest {

    @Mock
    private ESPClient espClient;
    @Mock private MetricRegistry metricRegistry;
    @InjectMocks
    private NET002PrivateIpAccessEnabled checker;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void signatureId() {
        assertEquals("GCP:NET-002", checker.signatureId().getValue());
    }

    @Test
    void resourceTypes() {
        assertEquals(ImmutableSet.of(GCE_SUBNETWORK), checker.resourceTypes());
    }

    @Test
    void alertStatusAndMessageFail() {
        Resource resource = resourceOf(false);
        assertStatusAndMessage(checker.alertStatusAndMessage(resource), AlertStatus.FAIL,
                "PrivateIpGoogleAccess doesn't exists or is not set to true for subnet selfLink.");
        assertNotNull(checker.metadata(resource));
    }

    @Test
    void alertStatusAndMessagePass() {
        Resource resource = resourceOf(true);
        assertStatusAndMessage(checker.alertStatusAndMessage(resource), AlertStatus.PASS,
                "PrivateIpGoogleAccess exists and is set to true for subnet selfLink.");
        assertNotNull(checker.metadata(resource));
    }

    private Resource resourceOf(boolean privateIp) {
        return Resource.builder()
                .tenant("tenant")
                .resourceKey(ResourceKey.builder()
                        .externalAccountId("1234")
                        .resourceIdHash(123)
                        .resourceType("instance")
                        .build())
                .resourceId("id1")
                .region("us")
                .cloudAppType(CloudAppType.GCP)
                .resourceMetadata(JsonUtils.encode(SubNetworkMetadata.builder()
                        .privateIpGoogleAddress(privateIp)
                        .region("testRegion")
                        .selfLink("selfLink")
                        .projectId("test-project")
                        .resourceName("testResourceName")
                        .build()))
                .build();
    }
}
