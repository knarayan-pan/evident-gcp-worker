package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.enrichment.metadata.DefaultKeySpecs;
import com.paloaltonetworks.evident.enrichment.metadata.DnsManagedZoneMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.utils.JsonUtils;
import com.paloaltonetworks.evident.utils.ResourceUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static com.paloaltonetworks.evident.TestUtil.assertStatusAndMessage;
import static com.paloaltonetworks.evident.gcp.api.ResourceType.DNS_MANAGED_ZONE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DNS002KeySigningCheckTest {

    @Mock
    private ESPClient espClient;
    @Mock
    private MetricRegistry metricRegistry;
    @InjectMocks
    private DNS002KeySigningCheck dns002KeySigningCheck;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void signatureId() throws Exception {
        assertEquals("GCP:DNS-002", dns002KeySigningCheck.signatureId().getValue());
    }

    @Test
    public void resourceTypes() throws Exception {
        assertEquals(ImmutableSet.of(DNS_MANAGED_ZONE), dns002KeySigningCheck.resourceTypes());
    }

    @Test
    public void alertStatusAndMessageFail() throws Exception {
        Resource resource = resourceOf("rsasha1");
        assertStatusAndMessage(dns002KeySigningCheck.alertStatusAndMessage(resource), AlertStatus.FAIL,
                "RSASHA1 is in use for key signing in managed zone sample1 in project project1");
        assertNotNull(dns002KeySigningCheck.metadata(resource));
    }

    @Test
    public void alertStatusAndMessagePass() throws Exception {
        Resource resource = resourceOf("rsasha256");
        assertStatusAndMessage(dns002KeySigningCheck.alertStatusAndMessage(resource), AlertStatus.PASS,
                "RSASHA1 is not in use for key signing in managed zone sample1 in project project1");
        assertNotNull(dns002KeySigningCheck.metadata(resource));
    }

    private Resource resourceOf(String signingAlgo) {
        return Resource.builder()
                .tenant("tenant")
                .resourceKey(ResourceKey.builder()
                        .externalAccountId("1234")
                        .resourceIdHash(123)
                        .resourceType(ResourceType.DNS_MANAGED_ZONE.getResourceType())
                        .build())
                .region(ResourceUtil.REGION_GLOBAL)
                .cloudAppType(CloudAppType.GCP)
                .resourceMetadata(JsonUtils.encode(DnsManagedZoneMetadata.builder()
                        .id("someID")
                        .dnsSecConfigState("on")
                        .projectId("project1")
                        .dnsName("a.b.com")
                        .resourceName("sample1")
                        .defaultKeySpecs(Collections.singletonList(DefaultKeySpecs.builder()
                                .algorithm(signingAlgo)
                                .keyType("keySigning")
                                .build()))
                        .build()))
                .build();
    }
}