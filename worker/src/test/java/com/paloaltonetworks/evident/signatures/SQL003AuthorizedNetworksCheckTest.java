package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.enrichment.metadata.SQLInstanceMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.utils.JsonUtils;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.paloaltonetworks.evident.TestUtil.assertStatusAndMessage;
import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCE_SQL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class SQL003AuthorizedNetworksCheckTest {
    @Mock
    private ESPClient espClient;
    @Mock
    private MetricRegistry metricRegistry;
    @InjectMocks
    private SQL003AuthorizedNetworksCheck checker;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void signatureId() throws Exception {
        assertEquals("GCP:SQL-003", checker.signatureId().getValue());
    }

    @Test
    public void resourceTypes() throws Exception {
        assertEquals(ImmutableSet.of(GCE_SQL), checker.resourceTypes());
    }

    @Test
    public void alertStatusAndMessageFail() throws Exception {
        Resource resource = resourceOf(networksOf("0.0.0.0/0"));
        assertStatusAndMessage(checker.alertStatusAndMessage(resource), AlertStatus.FAIL,
                "Cloud SQL testResourceName in project test-project is exposed publicly.");
        assertNotNull(checker.metadata(resource));
    }

    @Test
    public void alertStatusAndMessagePassNoData() throws Exception {
        Resource resource = resourceOf(Collections.emptyList());
        assertStatusAndMessage(checker.alertStatusAndMessage(resource), AlertStatus.PASS,
                "Cloud SQL testResourceName in project test-project is not exposed publicly.");
        assertNotNull(checker.metadata(resource));
    }

    @Test
    public void alertStatusAndMessagePass() throws Exception {
        Resource resource = resourceOf(networksOf("10.10.0.0/26"));
        assertStatusAndMessage(checker.alertStatusAndMessage(resource), AlertStatus.PASS,
                "Cloud SQL testResourceName in project test-project is not exposed publicly.");
        assertNotNull(checker.metadata(resource));
    }

    private List<Map<String, Object>> networksOf(String cidr) {
        return Lists.newArrayList(
                ImmutableMap.<String, Object>builder()
                        .put("value", cidr)
                        .put("name", "name1")
                        .build()
        );
    }

    private Resource resourceOf(List<Map<String, Object>> authorizedNetworks) {
        return Resource.builder()
                .tenant("tenant")
                .resourceKey(ResourceKey.builder()
                        .externalAccountId("1234")
                        .resourceIdHash(123)
                        .resourceType("instance")
                        .build())
                .resourceId("id1")
                .region("us")
                .cloudAppType(CloudAppType.GCP)
                .resourceMetadata(JsonUtils.encode(SQLInstanceMetadata.builder()
                        .authorizedNetworks(authorizedNetworks)
                        .connectionName("testConnectionName")
                        .databaseVersion("testDBVersion")
                        .region("testRegoin")
                        .selfLink("selfLink")
                        .zone("us-east-1b")
                        .projectId("test-project")
                        .resourceName("testResourceName")
                        .build()))
                .build();
    }

}