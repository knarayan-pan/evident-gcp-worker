package com.paloaltonetworks.evident.signatures;

import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.metadata.BucketMetadata;
import com.paloaltonetworks.evident.utils.JsonUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class GCS001BucketLoggingTurnedOnCheckTest {
    @Mock
    private ResourceService resourceService;

    @InjectMocks
    private GCS001BucketLoggingTurnedOnCheck check;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void alertStatusAndMessage() throws Exception {

        when(resourceService.findResourcesByType(anyString(), anyString(), anyString())).thenReturn(Stream.of(Resource.builder()
                .tenant("tenant")
                .resourceId("monitored_bucket")
                .resourceMetadata(JsonUtils.encode(BucketMetadata.builder().logBucket("monitoring_bucket").build()))
                .resourceKey(ResourceKey.builder()
                        .resourceIdHash(1234)
                        .resourceType("bucket")
                        .externalAccountId("234")
                        .build())

                .build()));
        // expect fail here
        Resource testBucket1 = Resource.builder()
                .tenant("tenant")
                .resourceId("random_bucket_with_no_logging")
                .resourceMetadata(JsonUtils.encode(BucketMetadata.builder().build()))
                .resourceKey(ResourceKey.builder()
                        .resourceIdHash(1234)
                        .resourceType("bucket")
                        .externalAccountId("234")
                        .build())
                .build();
        // expect pass here
        Resource testBucket2 = Resource.builder()
                .tenant("tenant")
                .resourceId("bucket_has_logging")
                .resourceMetadata(JsonUtils.encode(BucketMetadata.builder().logBucket("monitoring_bucket").build()))
                .resourceKey(ResourceKey.builder()
                        .resourceIdHash(1234)
                        .resourceType("bucket")
                        .externalAccountId("234")
                        .build())
                .build();
        // expect pass here as this is a target bucker for logs from another place
        Resource testBucket3 = Resource.builder()
                .tenant("tenant")
                .resourceId("monitoring_bucket")
                .resourceMetadata(JsonUtils.encode(BucketMetadata.builder().build()))
                .resourceKey(ResourceKey.builder()
                        .resourceIdHash(1234)
                        .resourceType("bucket")
                        .externalAccountId("234")
                        .build())
                .build();

        assertTrue(check.alertStatusAndMessage(testBucket1).getT1().equals(AlertStatus.FAIL));
        assertTrue(check.alertStatusAndMessage(testBucket2).getT1().equals(AlertStatus.PASS));
    }

}