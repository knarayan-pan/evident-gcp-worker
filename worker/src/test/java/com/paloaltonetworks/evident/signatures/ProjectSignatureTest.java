package com.paloaltonetworks.evident.signatures;

import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Mono;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class ProjectSignatureTest {
    @Mock
    private ESPClient espClient;
    @InjectMocks
    private
    ProjectSignature signature;
    private Resource resource;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        resource = Resource.builder()
                .resourceKey(ResourceKey.builder()
                        .externalAccountId("ext")
                        .resourceIdHash(1234)
                        .resourceType("instance")
                        .build())
                .tenant("tenant")
                .region("global")
                .resourceId("resourceId")
                .resourceMetadata("{\n" +
                        "  \"version\": \"0.1a.0\",\n" +
                        "  \"type\": \"infrastructure\",\n" +
                        "  \"payload\": {\n" +
                        "    \"action\": \"create\",\n" +
                        "    \"cs_uid\": \"1234\",\n" +
                        "    \"cs_user\": \"jahkkobi\",\n" +
                        "    \"cloudapp_type\": \"gcpworker\"\n" +
                        "  }\n" +
                        "}")
                .cloudAppType(CloudAppType.GCP)
                .dateUpdated(new Date())
                .build();
    }

    @Test
    public void signatureId() throws Exception {
        assertEquals(SignatureIdentifier.GCP_PRO_001, signature.signatureId());
    }

    @Test
    public void resourceTypes() throws Exception {
        assertTrue(signature.resourceTypes().contains(ResourceType.GCP_PROJECT));
    }

    @Test
    public void metadata() throws Exception {
        Mono<JSONObject> metadata = signature.metadata(resource);
        assertNotNull(metadata);
        metadata.doOnNext(Assertions::assertNotNull);
    }

    @Test
    public void alertStatusAndMessage() throws Exception {
        assertEquals(signature.alertStatusAndMessage(resource).getT1(), AlertStatus.PASS);
    }

}