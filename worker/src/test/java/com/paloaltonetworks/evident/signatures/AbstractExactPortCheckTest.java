package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.enrichment.metadata.FirewallMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.utils.JsonUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static com.paloaltonetworks.evident.TestUtil.assertStatusAndMessage;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
abstract class AbstractExactPortCheckTest {
    @Mock
    ESPClient espClient;
    @Mock
    MetricRegistry metricRegistry;
    AbstractExactPortCheck checker;

    abstract List<Arguments> arguments();
    abstract Set<ResourceType> expectedResourceTypes();
    abstract String expectedSignatureId();
    abstract AbstractExactPortCheck checker();

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        checker = checker();
    }

    @ParameterizedTest
    @DisplayName("Verify expected status and message")
    @MethodSource("arguments")
    void alertStatusAndMessage(Resource resource, AlertStatus status, String message) {
        assertStatusAndMessage(checker.alertStatusAndMessage(resource), status, message);
        checker.metadata(resource);
    }

    @Test
    @DisplayName("Verify signature id")
    void signatureIdTest() throws Exception {
        assertEquals(expectedSignatureId(), checker.signatureId().getValue());
    }

    @Test
    @DisplayName("Verify resource types")
    void resourceTypesTest() throws Exception {
        assertEquals(expectedResourceTypes(), checker.resourceTypes());
    }

    /////////////// helper methods
    static FirewallMetadata.RulesMetadata rulesOf(ArrayList<String> sourceIPRanges, String protocol, ArrayList<String> ports) {
        return FirewallMetadata.RulesMetadata.builder()
                .sourceIPRanges(sourceIPRanges)
                .protocol(protocol)
                .ports(ports)
                .build();
    }

    static Resource resourceOf(List<FirewallMetadata.RulesMetadata> allowedRules) {
        return Resource.builder()
                .tenant("tenant")
                .resourceKey(ResourceKey.builder()
                        .externalAccountId("1234")
                        .resourceIdHash(123)
                        .resourceType("instance")
                        .build())
                .region("us")
                .cloudAppType(CloudAppType.GCP)
                .resourceMetadata(JsonUtils.encode(FirewallMetadata.builder()
                        .allowedRules(allowedRules)
                        .direction("INGRESS")
                        .creationTimeStamp(new Date().toString())
                        .network("network1")
                        .priority(6555)
                        .id(BigInteger.valueOf(1L))
                        .resourceName("test-rule")
                        .selfLink("selfLink")
                        .projectId("test-project")
                        .build()))
                .build();
    }
}