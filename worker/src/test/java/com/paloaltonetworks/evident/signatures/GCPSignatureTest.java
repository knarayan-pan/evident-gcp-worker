package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.enrichment.metadata.ProjectMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.esp.model.AlertRequest;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.utils.JsonUtils;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.reflections.Reflections;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Modifier;
import java.util.Date;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GCPSignatureTest {
    @Mock
    private
    ESPClient espClient;
    @Mock
    private MetricRegistry metricRegistry;
    private testSignature testSignature;
    private Resource resource;

    class testSignature extends GCPSignature {

        testSignature(ESPClient espClient, MetricRegistry metricRegistry) {
            super(espClient, metricRegistry);
        }

        @Override
        public SignatureIdentifier signatureId() {
            return SignatureIdentifier.GCP_PRO_001;
        }

        @Override
        public Set<ResourceType> resourceTypes() {
            return ImmutableSet.<ResourceType>builder()
                    .add(ResourceType.GCE_INSTANCE)
                    .build();
        }

        @Override
        public Mono<JSONObject> metadata(Resource resource) {
            // for testing empty case
            return Mono.just(new JSONObject(resource.getResourceMetadata()));
        }

        @Override
        public Tuple2<AlertStatus, String> alertStatusAndMessage(Resource resource) {
            return passTuple("test signature passed");
        }
    }

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        testSignature = new testSignature(espClient, metricRegistry);
        resource = Resource.builder()
                .resourceKey(ResourceKey.builder()
                        .externalAccountId("ext")
                        .resourceIdHash(1234)
                        .resourceType("instance")
                        .build())
                .tenant("tenant")
                .region("global")
                .resourceId("resourceId")
                .resourceMetadata("{\n" +
                        "  \"version\": \"0.1a.0\",\n" +
                        "  \"type\": \"infrastructure\",\n" +
                        "  \"project_id\": \"test-project\",\n" +
                        "  \"payload\": {\n" +
                        "    \"action\": \"create\",\n" +
                        "    \"cs_uid\": \"1234\",\n" +
                        "    \"cs_user\": \"jahkkobi\",\n" +
                        "    \"cloudapp_type\": \"gcpworker\"\n" +
                        "  }\n" +
                        "}")
                .cloudAppType(CloudAppType.GCP)
                .dateUpdated(new Date())
                .build();
        when(metricRegistry.timer(anyString())).thenReturn(new Timer());
    }


    @Test
    public void checkWithNoResource() throws Exception {
        testSignature.check(Resource.builder()
                .resourceKey(ResourceKey.builder()
                        .externalAccountId("extId")
                        .resourceType("bucket")
                        .resourceIdHash(1234)
                        .build())
                .resourceId("1234")
                .region("us")
                .resourceMetadata(JsonUtils.encode(ProjectMetadata.builder().projectId("test-proj").build()))
                .build());
        verify(metricRegistry, times(1)).timer(anyString());
        verify(espClient, times(1)).createAlert(anyString(), any(AlertRequest.class));
    }

    @Test
    public void checkWithResource() throws Exception {
        testSignature.check(resource);
        verify(metricRegistry, times(1)).timer(anyString());
        verify(espClient, times(1)).createAlert(eq("ext"), any(AlertRequest.class));
    }

    @Test
    public void checkWithErrorPostingToEspWeb() throws Exception {
        when(espClient.createAlert(anyString(), any(AlertRequest.class))).thenThrow(new IOException());
        Assertions.assertThrows(RuntimeException.class, () -> testSignature.check(resource));
        verify(metricRegistry, times(1)).timer(anyString());
    }

    @Test
    public void testTags() throws Exception {
        assertNotNull(testSignature.processTags(null));
        assertTrue(testSignature.processTags(null).size() == 0);
        // check that we get an empty list in json
        assertTrue("[]".equalsIgnoreCase(JsonUtils.encode(testSignature.processTags(null))));
        ImmutableMap<String, String> tags = ImmutableMap.of("k1", "v1");
        assertNotNull(testSignature.processTags(tags));
        assertTrue(testSignature.processTags(tags).get(0).get("key").equalsIgnoreCase("k1"));
        assertTrue(testSignature.processTags(tags).get(0).get("value").equalsIgnoreCase("v1"));
    }

    @Test
    void annotationTest() {
        Set<Class> excludedClasses = Sets.newHashSet(ProjectSignature.class);
        new Reflections("com.paloaltonetworks.evident.signatures")
                .getSubTypesOf(GCPSignature.class)
                .stream()
                .filter(klass -> !Modifier.isAbstract(klass.getModifiers()) &&
                        !excludedClasses.contains(klass) &&
                        !klass.getSimpleName().toLowerCase().contains("test"))
                .forEach(klass -> {
                    assertAnnotation(Component.class, klass);
                });
    }

    private void assertAnnotation(Class<? extends Annotation> annotation, Class klass) {
        assertTrue(
                klass.isAnnotationPresent(annotation),
                String.format("Signature class %s should be annotated with @%s", klass.getName(),
                        annotation.getSimpleName()));
    }
}