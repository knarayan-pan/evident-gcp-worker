package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.api.services.cloudresourcemanager.model.AuditConfig;
import com.google.api.services.cloudresourcemanager.model.AuditLogConfig;
import com.google.api.services.cloudresourcemanager.model.Policy;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.enrichment.metadata.IamPolicy;
import com.paloaltonetworks.evident.enrichment.metadata.ProjectMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.utils.JsonUtils;
import com.paloaltonetworks.evident.utils.ResourceUtil;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static com.paloaltonetworks.evident.TestUtil.assertMetadata;
import static com.paloaltonetworks.evident.TestUtil.assertStatusAndMessage;
import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCP_PROJECT;
import static org.junit.Assert.*;

public class IAM001CloudAuditLoggingTest {
    private final static String PASS_MESSAGE = "Project %s has Admin Activity and Data Access enabled for all services and all members.";
    private final static String FAIL_ALL_MESSAGE = "Project %s does not have AuditConfigs configured.";
    private final static String FAIL_PARTIAL_MESSAGE = "Project %s does not have AuditConfigs configured for all logType/services/members.";
    private final static String WARN_MESSAGE = "No iam policy retrieved for project %s.";

    private static final String PROJECT_NAME = "testProjectName";

    @Mock
    private ESPClient espClient;
    @Mock
    private MetricRegistry metricRegistry;
    @InjectMocks
    private IAM001CloudAuditLogging signature;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void signatuerId() { assertEquals("GCP:IAM-001", signature.signatureId().getValue()); }

    @Test
    public void resourceTypes() throws Exception {
        assertEquals(ImmutableSet.of(GCP_PROJECT), signature.resourceTypes());
    }

    @Test
    public void shouldWarnFoNoIamPolicy() throws Exception {
        Resource resource = Resource.builder()
                .tenant("testTenant")
                .resourceKey(ResourceKey.builder()
                        .externalAccountId("1234")
                        .resourceIdHash(123)
                        .resourceType(GCP_PROJECT.getResourceType())
                        .build())
                .region(ResourceUtil.REGION_GLOBAL)
                .cloudAppType(CloudAppType.GCP)
                .resourceMetadata(JsonUtils.encode(ProjectMetadata.builder()
                        .projectId("testProjectId")
                        .resourceName(PROJECT_NAME)
                        .build()
                ))
                .build();
        assertStatusAndMessage(signature.alertStatusAndMessage(resource), AlertStatus.WARN, String.format(WARN_MESSAGE, PROJECT_NAME));
    }

    @Test
    public void shouldFailForNoAuditLogging() throws Exception {
        Resource resource = resourceOf(null);
        assertStatusAndMessage(signature.alertStatusAndMessage(resource), AlertStatus.FAIL, String.format(FAIL_ALL_MESSAGE, PROJECT_NAME));
    }

    @Test
    public void shouldFailForExemptedMember() throws Exception {
        Resource resource = resourceOf(ImmutableList.of(
                auditConfigOf("allServices", ImmutableList.of(
                        new AuditLogConfig().setLogType("ADMIN_READ").setExemptedMembers(Lists.newArrayList("rob@gmail.com")),
                        new AuditLogConfig().setLogType("DATA_READ").setExemptedMembers(Lists.newArrayList()),
                        new AuditLogConfig().setLogType("DATA_WRITE").setExemptedMembers(Lists.newArrayList())
                        )
                )));
        assertStatusAndMessage(signature.alertStatusAndMessage(resource), AlertStatus.FAIL, String.format(FAIL_PARTIAL_MESSAGE, PROJECT_NAME));
    }

    @Test
    public void shouldPassForAllLogsAllSrvsNoExemptions() throws Exception {
        Resource resource = resourceOf(ImmutableList.of(
                auditConfigOf("allServices", ImmutableList.of(
                    new AuditLogConfig().setLogType("ADMIN_READ").setExemptedMembers(Lists.newArrayList()),
                    new AuditLogConfig().setLogType("DATA_READ").setExemptedMembers(Lists.newArrayList()),
                    new AuditLogConfig().setLogType("DATA_WRITE").setExemptedMembers(Lists.newArrayList())
                )
        )));
        assertStatusAndMessage(signature.alertStatusAndMessage(resource), AlertStatus.PASS, String.format(PASS_MESSAGE, PROJECT_NAME));
    }

    @Test
    public void passForAllLogsSvcsNullExemptions() throws Exception {
        Resource resource = resourceOf(ImmutableList.of(
                auditConfigOf("allServices", ImmutableList.of(
                    new AuditLogConfig().setLogType("ADMIN_READ").setExemptedMembers(null),
                    new AuditLogConfig().setLogType("DATA_READ").setExemptedMembers(null),
                    new AuditLogConfig().setLogType("DATA_WRITE").setExemptedMembers(null)
                )
        )));
        assertStatusAndMessage(signature.alertStatusAndMessage(resource), AlertStatus.PASS, String.format(PASS_MESSAGE, PROJECT_NAME));
    }


    @Test
    public void passForAllLogsSvcsExemptionsNotSet() throws Exception {
        Resource resource = resourceOf(ImmutableList.of(
                auditConfigOf("allServices", ImmutableList.of(
                    new AuditLogConfig().setLogType("ADMIN_READ"),
                    new AuditLogConfig().setLogType("DATA_READ"),
                    new AuditLogConfig().setLogType("DATA_WRITE")
                )
        )));
        assertStatusAndMessage(signature.alertStatusAndMessage(resource), AlertStatus.PASS, String.format(PASS_MESSAGE, PROJECT_NAME));
    }


    @Test
    public void shouldPassIgnoringAddtionalServices() throws Exception {
        Resource resource = resourceOf(ImmutableList.of(
                auditConfigOf("allServices", ImmutableList.of(
                        new AuditLogConfig().setLogType("ADMIN_READ").setExemptedMembers(Lists.newArrayList()),
                        new AuditLogConfig().setLogType("DATA_READ").setExemptedMembers(Lists.newArrayList()),
                        new AuditLogConfig().setLogType("DATA_WRITE").setExemptedMembers(Lists.newArrayList())
                        )
                ),
                auditConfigOf("cloudfunctions.googleapis.com", ImmutableList.of(
                        new AuditLogConfig().setLogType("ADMIN_READ").setExemptedMembers(Lists.newArrayList("rob@gmail.com"))
                        )
                )
        ));
        assertStatusAndMessage(signature.alertStatusAndMessage(resource), AlertStatus.PASS, String.format(PASS_MESSAGE, PROJECT_NAME));
    }

    @Test
    public void shouldNotBeFooledByDuplicateLogTypes() throws Exception {
        Resource resource = resourceOf(ImmutableList.of(
                auditConfigOf("allServices", ImmutableList.of(
                        new AuditLogConfig().setLogType("DATA_READ").setExemptedMembers(Lists.newArrayList()),
                        new AuditLogConfig().setLogType("DATA_READ").setExemptedMembers(Lists.newArrayList()),
                        new AuditLogConfig().setLogType("DATA_READ").setExemptedMembers(Lists.newArrayList())
                        )
                ),
                auditConfigOf("cloudfunctions.googleapis.com", ImmutableList.of(
                        new AuditLogConfig().setLogType("ADMIN_READ").setExemptedMembers(Lists.newArrayList("rob@gmail.com"))
                        )
                )
        ));

        assertStatusAndMessage(signature.alertStatusAndMessage(resource), AlertStatus.FAIL, String.format(FAIL_PARTIAL_MESSAGE, PROJECT_NAME));
    }


    @Test
    public void shouldReturnUsefulMetadata() throws Exception {
        Resource resource = resourceOf(ImmutableList.of(
                auditConfigOf("allServices", ImmutableList.of(
                        new AuditLogConfig().setLogType("ADMIN_READ").setExemptedMembers(Lists.newArrayList("rob@gmail.com")),
                        new AuditLogConfig().setLogType("DATA_READ").setExemptedMembers(Lists.newArrayList()),
                        new AuditLogConfig().setLogType("DATA_WRITE").setExemptedMembers(Lists.newArrayList())
                        )
                )));
        JSONObject expectedMetadata = new JSONObject(JsonUtils.encode(
                ImmutableMap.of("projectId", "testProjectId",
                                "name", PROJECT_NAME,
                                "auditConfigs", ImmutableList.of(auditConfigOf("allServices", ImmutableList.of(
                                new AuditLogConfig().setLogType("ADMIN_READ").setExemptedMembers(Lists.newArrayList("rob@gmail.com")),
                                new AuditLogConfig().setLogType("DATA_READ").setExemptedMembers(Lists.newArrayList()),
                                new AuditLogConfig().setLogType("DATA_WRITE").setExemptedMembers(Lists.newArrayList())
                                )
                        )))
        ));

        JSONObject actualMetadata = signature.metadata(resource).block();
        assertNotNull(actualMetadata);
        assertEquals("testProjectId", actualMetadata.get("projectId"));
        assertEquals(PROJECT_NAME, actualMetadata.get("name"));

        assertMetadata(expectedMetadata, actualMetadata, "not sure why this needs a string");
    }

    private Resource resourceOf(List<AuditConfig> auditConfigs) {
        return Resource.builder()
                .tenant("testTenant")
                .resourceKey(ResourceKey.builder()
                        .externalAccountId("1234")
                        .resourceIdHash(123)
                        .resourceType(GCP_PROJECT.getResourceType())
                        .build())
                .region(ResourceUtil.REGION_GLOBAL)
                .cloudAppType(CloudAppType.GCP)
                .resourceMetadata(JsonUtils.encode(ProjectMetadata.builder()
                        .projectId("testProjectId")
                        .resourceName(PROJECT_NAME)
                        .iamPolicy(iamPolicy(auditConfigs))
                        .build()
                ))
                .build();
    }

    private AuditConfig auditConfigOf(String services, List<AuditLogConfig> auditLogConfigs) {
        return new AuditConfig().setService(services).setAuditLogConfigs(auditLogConfigs);
    }

    private IamPolicy iamPolicy(List<AuditConfig> auditConfigs) {
        return new IamPolicy(new Policy()
                .setAuditConfigs(auditConfigs)
        );
    }

}