package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.enrichment.metadata.InstanceMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.utils.JsonUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigInteger;

import static com.paloaltonetworks.evident.TestUtil.assertStatusAndMessage;
import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCE_INSTANCE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GCE001IPForwardingCheckTest {
    @Mock
    private ESPClient espClient;
    @Mock
    private MetricRegistry metricRegistry;
    @InjectMocks
    private
    GCE001IPForwardingCheck checker;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void signatureId() throws Exception {
        assertEquals("GCP:GCE-001", checker.signatureId().getValue());
    }

    @Test
    public void resourceTypes() throws Exception {
        assertEquals(ImmutableSet.of(GCE_INSTANCE), checker.resourceTypes());
    }

    @Test
    public void alertStatusAndMessageFail() throws Exception {
        Resource resource = resourceOf(true);
        assertStatusAndMessage(checker.alertStatusAndMessage(resource), AlertStatus.FAIL,
                "VM Instance test-instance in project test-project has IP Forwarding enabled.");
        assertNotNull(checker.metadata(resource));
    }

    @Test
    public void alertStatusAndMessagePass() throws Exception {
        Resource resource = resourceOf(false);
        assertStatusAndMessage(checker.alertStatusAndMessage(resource), AlertStatus.PASS,
                "VM Instance test-instance in project test-project does not have IP Forwarding enabled.");
        assertNotNull(checker.metadata(resource));
    }

    private Resource resourceOf(boolean canIpForward) {
        return Resource.builder()
                .tenant("tenant")
                .resourceKey(ResourceKey.builder()
                        .externalAccountId("1234")
                        .resourceIdHash(123)
                        .resourceType("instance")
                        .build())
                .region("us")
                .cloudAppType(CloudAppType.GCP)
                .resourceMetadata(JsonUtils.encode(InstanceMetadata.builder()
                        .canIpForward(canIpForward)
                        .cpuPlatform("platform")
                        .createTimestamp("2002-12-12T")
                        .description("Desc")
                        .id(BigInteger.valueOf(1L))
                        .name("test-instance")
                        .selfLink("selfLink")
                        .status("RUNNING")
                        .zone("us-east-1b")
                        .machineType("linux")
                        .status("instance")
                        .projectId("test-project")
                        .build()))
                .build();
    }

}