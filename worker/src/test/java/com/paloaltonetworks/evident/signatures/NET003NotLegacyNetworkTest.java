package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.enrichment.metadata.NetworkMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.utils.JsonUtils;
import com.paloaltonetworks.evident.utils.ResourceUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static com.paloaltonetworks.evident.TestUtil.assertStatusAndMessage;
import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCE_NETWORK;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class NET003NotLegacyNetworkTest {
    @Mock
    private ESPClient espClient;
    @Mock private MetricRegistry metricRegistry;
    @InjectMocks
    private NET003NotLegacyNetwork signature;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void signatureId() {
        assertEquals("GCP:NET-003", signature.signatureId().getValue());
    }

    @Test
    void resourceTypes() {
        assertEquals(ImmutableSet.of(GCE_NETWORK), signature.resourceTypes());
    }

    @Test
    void alertStatusAndMessageFail() {
        Resource resource = testResource((false));
        assertStatusAndMessage(signature.alertStatusAndMessage(resource), AlertStatus.FAIL,
                "Subnetworks do not exist for legacy network selfLink.");
        assertNotNull(signature.metadata(resource));
    }

    @Test
    void alertStatusAndMessagePass() {
        Resource resource = testResource(true);
        assertStatusAndMessage(signature.alertStatusAndMessage(resource), AlertStatus.PASS,
                "Subnetworks exist for network selfLink.");
        assertNotNull(signature.metadata(resource));
    }


    private Resource testResource(boolean hasSubnets) {
        List<String> subnets;
        if (hasSubnets) {
            subnets = Collections.singletonList("subnetwork");
        } else {
            subnets = null;
        }
        return Resource.builder()
                .cloudAppType(CloudAppType.GCP)
                .resourceId("resourceId")
                .tenant("tenant")
                .region(ResourceUtil.REGION_GLOBAL)
                .resourceKey(ResourceKey.builder()
                        .externalAccountId("1")
                        .resourceType(ResourceType.GCE_NETWORK.getResourceType())
                        .resourceIdHash(ResourceKey.computeResourceIdHash("resourceId"))
                        .build())
                .resourceMetadata(JsonUtils.encode(
                        NetworkMetadata.builder()
                                .iPv4Range("IPv4Range")
                                .gatewayIPv4("gatewayIPv4")
                                .selfLink("selfLink")
                                .autoCreateSubnetworks(true)
                                .subnetworks(subnets)
                                .peerings(Collections.singletonList(NetworkMetadata.Peering.builder().autoCreateRoutes(true).build()))
                                .routingConfig(NetworkMetadata.RoutingConfig.builder()
                                        .routingMode("routingMode")
                                        .build()
                                )
                                .kind("kind")
                                .projectId("projectId")
                                .resourceName("resourceName")
                                .creationTimestamp("creationTimestamp")
                                .description("description")
                                .build()
                        )
                )
                .build();
    }
}
