package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.api.services.cloudresourcemanager.model.Binding;
import com.google.api.services.cloudresourcemanager.model.Policy;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.paloaltonetworks.evident.TestUtil;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.enrichment.metadata.IamPolicy;
import com.paloaltonetworks.evident.enrichment.metadata.ProjectMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.utils.JsonUtils;
import com.paloaltonetworks.evident.utils.ResourceUtil;
import lombok.val;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCP_PROJECT;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class IAM005NoServiceAccountUserTest {
    @Mock
    private ESPClient espClient;
    @Mock
    private MetricRegistry metricRegistry;
    @InjectMocks
    private
    IAM005NoServiceAccountUser signature;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void signatureId() throws Exception {
        assertEquals("GCP:IAM-005", signature.signatureId().getValue());
    }

    @Test
    public void resourceTypes() throws Exception {
        assertEquals(ImmutableSet.of(GCP_PROJECT), signature.resourceTypes());
    }

    @Test
    public void nulIamPolicy() {
        ProjectMetadata projMetadata = ProjectMetadata.builder().projectId("testProject-1").build();

        Resource resource = Resource.builder()
                .tenant("tenant")
                .resourceKey(ResourceKey.builder()
                        .externalAccountId("1234")
                        .resourceIdHash(123)
                        .resourceType("project")
                        .build())
                .region("global")
                .cloudAppType(CloudAppType.GCP)
                .resourceMetadata(JsonUtils.encode(projMetadata))
                .build();

        signature.alertsStatusMessageAndMetadata(resource).subscribe(
                alert -> Assertions.fail("Should not emit alert")
        );
    }

    @Test
    public void statusMessageMetadataMapFail() throws Exception {
        Map<String, Triple<AlertStatus, String, JSONObject>> expectedAlerts = Maps.newLinkedHashMap();
        expectedAlerts.put("serviceAccount:me@bigcorp.com", ImmutableTriple.of(AlertStatus.FAIL, "User serviceAccount:me@bigcorp.com has ServiceAccountUser role at the project level.",
                new JSONObject()
                        .put("email","serviceAccount:me@bigcorp.com")
                        .put("roles", Arrays.asList("roles/iam.serviceAccountUser", "roles/compute.networkViewer"))
        ));
        expectedAlerts.put("serviceAccount:you@bigcorp.com", Triple.of(AlertStatus.FAIL, "User serviceAccount:you@bigcorp.com has ServiceAccountUser role at the project level.",
                new JSONObject()
                        .put("email","serviceAccount:you@bigcorp.com")
                        .put("roles", Arrays.asList("roles/iam.serviceAccountUser"))
        ));
        Resource resource = resourceOf(Arrays.asList(
                IamPolicy.RoleMembers.builder()
                        .role("roles/iam.serviceAccountUser")
                        .members(Arrays.asList("serviceAccount:me@bigcorp.com", "serviceAccount:you@bigcorp.com"))
                        .build(),
                IamPolicy.RoleMembers.builder()
                        .role("roles/compute.networkViewer")
                        .members(Arrays.asList("serviceAccount:me@bigcorp.com"))
                        .build()
                )
        );
        testExpectedVsActual(expectedAlerts, resource);
    }

    @Test
    public void statusMessageMetadataMapPass() throws Exception {
        Map<String, Triple<AlertStatus, String, JSONObject>> expectedAlerts = Maps.newLinkedHashMap();
        expectedAlerts.put("user:hey@bigcorp.com", ImmutableTriple.of(AlertStatus.PASS, "User user:hey@bigcorp.com does not have ServiceAccountUser role at the project level.",
                new JSONObject()
                        .put("email","user:hey@bigcorp.com")
                        .put("roles", Arrays.asList("roles/compute.networkViewer"))
        ));
        expectedAlerts.put("serviceAccount:you@bigcorp.com", Triple.of(AlertStatus.PASS, "User serviceAccount:you@bigcorp.com does not have ServiceAccountUser role at the project level.",
                new JSONObject()
                        .put("email","serviceAccount:you@bigcorp.com")
                        .put("roles", Arrays.asList("roles/compute.networkViewer"))
        ));
        Resource resource = resourceOf(Arrays.asList(
                IamPolicy.RoleMembers.builder()
                        .role("roles/compute.networkViewer")
                        .members(Arrays.asList("user:hey@bigcorp.com", "serviceAccount:you@bigcorp.com"))
                        .build()));
        testExpectedVsActual(expectedAlerts, resource);
    }

    @Test
    public void statusMessageMetadataMapPassAndFail() throws Exception {
        Map<String, Triple<AlertStatus, String, JSONObject>> expectedAlerts = Maps.newLinkedHashMap();
        expectedAlerts.put("serviceAccount:me@bigcorp.com", ImmutableTriple.of(AlertStatus.FAIL, "User serviceAccount:me@bigcorp.com has ServiceAccountUser role at the project level.",
                new JSONObject()
                        .put("email","serviceAccount:me@bigcorp.com")
                        .put("roles", Arrays.asList("roles/iam.serviceAccountUser"))
        ));
        expectedAlerts.put("serviceAccount:you@bigcorp.com", Triple.of(AlertStatus.PASS, "User serviceAccount:you@bigcorp.com does not have ServiceAccountUser role at the project level.",
                new JSONObject()
                        .put("email","serviceAccount:you@bigcorp.com")
                        .put("roles", Arrays.asList("roles/compute.networkViewer"))
        ));
        Resource resource = resourceOf(Arrays.asList(
                IamPolicy.RoleMembers.builder()
                        .role("roles/iam.serviceAccountUser")
                        .members(Arrays.asList("serviceAccount:me@bigcorp.com"))
                        .build(),
                IamPolicy.RoleMembers.builder()
                        .role("roles/compute.networkViewer")
                        .members(Arrays.asList("serviceAccount:you@bigcorp.com"))
                        .build()
        ));
        testExpectedVsActual(expectedAlerts, resource);
    }

    private Resource resourceOf(List<IamPolicy.RoleMembers> roleMembers) {
        return Resource.builder()
                .tenant("tenant")
                .resourceKey(ResourceKey.builder()
                        .externalAccountId("1234")
                        .resourceIdHash(123)
                        .resourceType(GCP_PROJECT.getResourceType())
                        .build())
                .region(ResourceUtil.REGION_GLOBAL)
                .cloudAppType(CloudAppType.GCP)
                .resourceMetadata(JsonUtils.encode(ProjectMetadata.builder()
                        .projectId("testProjectId")
                        .resourceName("testProject123")
                        .iamPolicy(iamPolicy(roleMembers))
                        .build()
                ))
                .build();
    }

    private IamPolicy iamPolicy(List<IamPolicy.RoleMembers> roleMembers) {
        List<Binding> bindings = new ArrayList<>();
        roleMembers.forEach(roleMember -> bindings.add(new Binding()
                .setRole(roleMember.getRole())
                .setMembers(roleMember.getMembers())));
        return new IamPolicy(new Policy().setBindings(bindings));
    }

    private void testExpectedVsActual(Map<String, Triple<AlertStatus, String, JSONObject>> expectedAlerts, Resource resource) {
        val alerts = signature.alertsStatusMessageAndMetadata(resource).block();
        Assertions.assertEquals(alerts.size(), expectedAlerts.size());
        alerts.entrySet().stream()
                .forEach(
                        entry -> {
                            Assertions.assertEquals(expectedAlerts.get(entry.getKey()).getLeft(), entry.getValue().getLeft());
                            Assertions.assertEquals(expectedAlerts.get(entry.getKey()).getMiddle(), entry.getValue().getMiddle());
                            TestUtil.assertMetadata(expectedAlerts.get(entry.getKey()).getRight(),
                                    entry.getValue().getRight(), "metadata should match for " + entry.getKey());
                        }
                );
    }
}