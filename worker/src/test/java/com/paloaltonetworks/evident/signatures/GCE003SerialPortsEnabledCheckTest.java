package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.enrichment.metadata.InstanceMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.utils.JsonUtils;
import org.assertj.core.util.Maps;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigInteger;
import java.util.Map;

import static com.paloaltonetworks.evident.TestUtil.assertStatusAndMessage;
import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCE_INSTANCE;
import static com.paloaltonetworks.evident.signatures.AlertStatus.FAIL;
import static com.paloaltonetworks.evident.signatures.AlertStatus.PASS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GCE003SerialPortsEnabledCheckTest {
    @Mock
    private ESPClient espClient;
    @Mock
    private MetricRegistry metricRegistry;
    @InjectMocks
    private GCE003SerialPortsEnabledCheck checker;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void signatureId() throws Exception {
        assertEquals("GCP:GCE-003", checker.signatureId().getValue());
    }

    @Test
    public void resourceTypes() throws Exception {
        assertEquals(ImmutableSet.of(GCE_INSTANCE), checker.resourceTypes());
    }

    @Test
    public void alertStatusAndMessageFail() throws Exception {
        Resource resource = resourceOf(mapOf("serial-port-enable", "true"));
        assertStatusAndMessage(checker.alertStatusAndMessage(resource), FAIL,
                "VM Instance test-instance in project test-project has connecting to serial ports enabled.");
        assertNotNull(checker.metadata(resource));
    }

    @Test
    public void alertStatusAndMessagePassForNoMetadata() throws Exception {
        Resource resource = resourceOf(null);
        assertStatusAndMessage(checker.alertStatusAndMessage(resource),
                PASS,
                "VM Instance test-instance in project test-project does not have connecting to serial ports enabled.");
        assertNotNull(checker.metadata(resource));
    }

    @Test
    public void alertStatusAndMessagePassForMissingBlockSshKeyAttribute() throws Exception {
        Resource resource = resourceOf(mapOf("some-random-key", "some-random-value"));
        assertStatusAndMessage(checker.alertStatusAndMessage(resource), PASS,
                "VM Instance test-instance in project test-project does not have connecting to serial ports enabled.");
        assertNotNull(checker.metadata(resource));
    }

    @Test
    public void alertStatusAndMessagePassForFalseBlockSshKeyAttribute() throws Exception {
        Resource resource = resourceOf(mapOf("serial-port-enable", "false"));
        assertStatusAndMessage(checker.alertStatusAndMessage(resource), PASS,
                "VM Instance test-instance in project test-project does not have connecting to serial ports enabled.");
        assertNotNull(checker.metadata(resource));
    }

    private Resource resourceOf(Map<String, String> metadata) {
        return Resource.builder()
                .tenant("tenant")
                .resourceKey(ResourceKey.builder()
                        .externalAccountId("1234")
                        .resourceIdHash(123)
                        .resourceType("instance")
                        .build())
                .region("us")
                .cloudAppType(CloudAppType.GCP)
                .resourceMetadata(JsonUtils.encode(InstanceMetadata.builder()
                        .canIpForward(true)
                        .cpuPlatform("platform")
                        .createTimestamp("2002-12-12T")
                        .description("Desc")
                        .id(BigInteger.valueOf(1L))
                        .name("test-instance")
                        .selfLink("selfLink")
                        .status("instance")
                        .zone("us-east-1b")
                        .machineType("linux")
                        .projectId("test-project")
                        .metadata(metadata)
                        .build()))
                .build();
    }

    private Map<String, String> mapOf(String key, String value) {
        return Maps.newHashMap(key, value);
    }

}