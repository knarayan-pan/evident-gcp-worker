package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.TestUtil;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.enrichment.metadata.IamPolicy;
import com.paloaltonetworks.evident.enrichment.metadata.ProjectMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.utils.JsonUtils;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class IAM004NotBothKmsAdminAndUserTest {
    private static final String PASS_MESSAGE = "User %s has Cloud KMS admin role or Cloud KMS non-admin role, but not both.";
    private static final String FAIL_MESSAGE = "User %s has both Cloud KMS admin and non-admin roles.";
    private static final String INFO_MESSAGE = "User %s has no cloud KMS role.";

    @Mock
    private ESPClient espClient;
    @Mock
    private MetricRegistry metricRegistry;
    @InjectMocks
    private IAM004NotBothKmsAdminAndUser signature;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void signatureId() throws Exception {
        Assertions.assertEquals("GCP:IAM-004", signature.signatureId().getValue());
    }

    @Test
    public void resourceTypes() throws Exception {
        Assertions.assertEquals(ImmutableSet.of(ResourceType.GCP_PROJECT), signature.resourceTypes());
    }

    @Test
    public void nulIamPolicy() {
        ProjectMetadata projMetadata = ProjectMetadata.builder().projectId("testProject-1").build();

        Resource resource = Resource.builder()
            .tenant("tenant")
            .resourceKey(ResourceKey.builder()
                .externalAccountId("1234")
                .resourceIdHash(123)
                .resourceType("project")
                .build())
            .region("global")
            .cloudAppType(CloudAppType.GCP)
            .resourceMetadata(JsonUtils.encode(projMetadata))
            .build();

        signature.alertsStatusMessageAndMetadata(resource).subscribe(
            alert -> Assertions.fail("Should not emit alert")
        );
    }

    private static Stream<Arguments> resourcesProvider() {
        return Stream.of(
            Arguments.of("null roleMembers",
                null, Collections.emptyMap()),
            Arguments.of("empty roleMembers",
                Collections.emptyList(),
                Collections.emptyMap()),
            Arguments.of("1 roleMembers without user account",
                ImmutableList.of(
                    IamPolicy.RoleMembers.builder()
                        .role("role1")
                        .members(ImmutableList.of("randomAccount:a@b.com"))
                        .build()
                ),
                Collections.emptyMap()),
            Arguments.of("1 roleMembers with non-kms role, 1 user account",
                ImmutableList.of(
                    IamPolicy.RoleMembers.builder()
                        .role("role1")
                        .members(ImmutableList.of("user:a@b.com"))
                        .build()
                ),
                Collections.singletonMap("testProject-1/user:a@b.com",
                    ImmutableTriple.of(AlertStatus.INFO,String.format(INFO_MESSAGE, "user:a@b.com"),
                        new JSONObject(JsonUtils.encode(
                            ImmutableMap.of("userEmail", "user:a@b.com", "kmsRoles", Collections.emptyList())
                        ))))),
            Arguments.of("1 roleMembers with kms role (roles/cloudkms.admin), 1 user account",
                ImmutableList.of(
                    IamPolicy.RoleMembers.builder()
                        .role("roles/cloudkms.admin")
                        .members(ImmutableList.of("random:x@y.com", "user:a@b.com"))
                        .build()
                ),
                Collections.singletonMap("testProject-1/user:a@b.com",
                    ImmutableTriple.of(AlertStatus.PASS,String.format(PASS_MESSAGE, "user:a@b.com"),
                        new JSONObject(JsonUtils.encode(
                            ImmutableMap.of("userEmail", "user:a@b.com", "kmsRoles", ImmutableList.of("roles/cloudkms.admin"))
                        ))))),
            Arguments.of("1 roleMembers with kms role (roles/cloudkms.cryptoKeyDecrypter), 1 user account",
                ImmutableList.of(
                    IamPolicy.RoleMembers.builder()
                        .role("roles/cloudkms.cryptoKeyDecrypter")
                        .members(ImmutableList.of("random:x@y.com", "user:a@b.com"))
                        .build()
                ),
                Collections.singletonMap("testProject-1/user:a@b.com",
                    ImmutableTriple.of(AlertStatus.PASS,String.format(PASS_MESSAGE, "user:a@b.com"),
                        new JSONObject(JsonUtils.encode(
                            ImmutableMap.of("userEmail", "user:a@b.com", "kmsRoles", ImmutableList.of("roles/cloudkms.cryptoKeyDecrypter"))
                        ))))),
            Arguments.of("1 roleMembers with kms role (roles/cloudkms.cryptoKeyEncrypter), 1 user account",
                ImmutableList.of(
                    IamPolicy.RoleMembers.builder()
                        .role("roles/cloudkms.cryptoKeyEncrypter")
                        .members(ImmutableList.of("random:x@y.com", "user:a@b.com"))
                        .build()
                ),
                Collections.singletonMap("testProject-1/user:a@b.com",
                    ImmutableTriple.of(AlertStatus.PASS,String.format(PASS_MESSAGE, "user:a@b.com"),
                        new JSONObject(JsonUtils.encode(
                            ImmutableMap.of("userEmail", "user:a@b.com", "kmsRoles", ImmutableList.of("roles/cloudkms.cryptoKeyEncrypter"))
                        ))))),
            Arguments.of("1 roleMembers with kms role (roles/cloudkms.cryptoKeyEncrypterDecrypter), 1 user account",
                ImmutableList.of(
                    IamPolicy.RoleMembers.builder()
                        .role("roles/cloudkms.cryptoKeyEncrypterDecrypter")
                        .members(ImmutableList.of("random:x@y.com", "user:a@b.com"))
                        .build()
                ),
                Collections.singletonMap("testProject-1/user:a@b.com",
                    ImmutableTriple.of(AlertStatus.PASS,String.format(PASS_MESSAGE, "user:a@b.com"),
                        new JSONObject(JsonUtils.encode(
                            ImmutableMap.of("userEmail", "user:a@b.com", "kmsRoles", ImmutableList.of("roles/cloudkms.cryptoKeyEncrypterDecrypter"))
                        ))))),
            Arguments.of("multiple roleMembers with kms admin and user roles, multiple user account",
                ImmutableList.of(
                    IamPolicy.RoleMembers.builder()
                        .role("roles/cloudkms.admin")
                        .members(ImmutableList.of("random:x@y.com", "user:a@b.com", "user:c@d.com"))
                        .build(),
                    IamPolicy.RoleMembers.builder()
                        .role("roles/cloudkms.cryptoKeyEncrypterDecrypter")
                        .members(ImmutableList.of("random:x@y.com", "user:c2@d2.com"))
                        .build(),
                    IamPolicy.RoleMembers.builder()
                        .role("roles/cloudkms.cryptoKeyEncrypter")
                        .members(ImmutableList.of("random:x3@y3.com", "user:a@b.com"))
                        .build(),
                    IamPolicy.RoleMembers.builder()
                        .role("roles/admin")
                        .members(ImmutableList.of("random:x3@y3.com", "user:e@f.com"))
                        .build()
                ),
                ImmutableMap.of(
                    "testProject-1/user:a@b.com",
                    ImmutableTriple.of(AlertStatus.FAIL,String.format(FAIL_MESSAGE, "user:a@b.com"),
                        new JSONObject(JsonUtils.encode(
                            ImmutableMap.of("userEmail", "user:a@b.com", "kmsRoles", ImmutableList.of("roles/cloudkms.admin", "roles/cloudkms.cryptoKeyEncrypter"))
                        ))),
                    "testProject-1/user:c@d.com",
                    ImmutableTriple.of(AlertStatus.PASS,String.format(PASS_MESSAGE, "user:c@d.com"),
                        new JSONObject(JsonUtils.encode(
                            ImmutableMap.of("userEmail", "user:c@d.com", "kmsRoles", ImmutableList.of("roles/cloudkms.admin"))
                        ))),
                    "testProject-1/user:c2@d2.com",
                    ImmutableTriple.of(AlertStatus.PASS,String.format(PASS_MESSAGE, "user:c2@d2.com"),
                        new JSONObject(JsonUtils.encode(
                            ImmutableMap.of("userEmail", "user:c2@d2.com", "kmsRoles", ImmutableList.of("roles/cloudkms.cryptoKeyEncrypterDecrypter"))
                        ))),
                    "testProject-1/user:e@f.com",
                    ImmutableTriple.of(AlertStatus.INFO,String.format(INFO_MESSAGE, "user:e@f.com"),
                        new JSONObject(JsonUtils.encode(
                            ImmutableMap.of("userEmail", "user:e@f.com", "kmsRoles", Collections.emptyList())
                        )))
                )
            )
        );
    }

    @ParameterizedTest(name = "resource #{index} - {0}")
    @MethodSource("resourcesProvider")
    public void checkVariousResource(String description, List<IamPolicy.RoleMembers> roleMembersList,
                                     Map<String, Triple<AlertStatus, String, JSONObject>> expectedAlerts) {
        ProjectMetadata projMetadata = ProjectMetadata.builder()
            .projectId("testProject-1")
            .iamPolicy(IamPolicy.builder()
                .roleMembers(roleMembersList)
                .build())
            .build();

        Resource resource = Resource.builder()
            .tenant("tenant")
            .resourceKey(ResourceKey.builder()
                .externalAccountId("1234")
                .resourceIdHash(123)
                .resourceType("project")
                .build())
            .region("global")
            .cloudAppType(CloudAppType.GCP)
            .resourceMetadata(JsonUtils.encode(projMetadata))
            .build();

        signature.alertsStatusMessageAndMetadata(resource).subscribe(
            alerts ->  {
                Assertions.assertEquals(alerts.size(), expectedAlerts.size());
                alerts.entrySet().stream()
                    .forEach(
                        entry -> {
                            Assertions.assertEquals(expectedAlerts.get(entry.getKey()).getLeft(), entry.getValue().getLeft());
                            Assertions.assertEquals(expectedAlerts.get(entry.getKey()).getMiddle(), entry.getValue().getMiddle());
                            TestUtil.assertMetadata(expectedAlerts.get(entry.getKey()).getRight(),
                                entry.getValue().getRight(), "metadata should match for " + entry.getKey());
                        }
                    );
            }
        );
    }
}
