package com.paloaltonetworks.evident.signatures;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import org.assertj.core.util.Lists;
import org.junit.jupiter.params.provider.Arguments;
import org.mockito.InjectMocks;

import java.util.List;

import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCE_FIREWALL;

class FW010TCPPort3306CheckTest extends AbstractExactPortCheckTest {
    @InjectMocks
    private FW010TCPPort3306Check checker;

    protected List<Arguments> arguments() {
        return ImmutableList.of(
                Arguments.of(resourceOf(ImmutableList.of(
                        rulesOf(Lists.newArrayList("10.0.0.0/0"), "UDP", Lists.newArrayList("3306")),
                        rulesOf(Lists.newArrayList("0.0.0.0/0"), "TCP", Lists.newArrayList("0-10000")))),
                        AlertStatus.FAIL,
                        "Firewall rule test-rule for project test-project has TCP port 3306 exposed globally."),
                Arguments.of(resourceOf(Lists.newArrayList(rulesOf(Lists.newArrayList("0.0.0.0/0"), "TCP", Lists.newArrayList("3306")))),
                        AlertStatus.FAIL, "Firewall rule test-rule for project test-project has TCP port 3306 exposed globally."),
                Arguments.of(resourceOf(Lists.newArrayList(rulesOf(Lists.newArrayList("0.0.0.0/0"), "ALL", Lists.newArrayList()))),
                        AlertStatus.FAIL,
                        "Firewall rule test-rule for project test-project has TCP port 3306 exposed globally."),
                Arguments.of(resourceOf(Lists.newArrayList(rulesOf(Lists.newArrayList("0.0.0.0/0"), "TCP", Lists.newArrayList("21")))),
                        AlertStatus.PASS,
                        "Firewall rule test-rule for project test-project does not have TCP port 3306 exposed globally."),
                Arguments.of(resourceOf(Lists.newArrayList(rulesOf(Lists.newArrayList("0.0.0.0/0"), "TCP", null))),
                        AlertStatus.FAIL,
                        "Firewall rule test-rule for project test-project has TCP port 3306 exposed globally."),
                Arguments.of(resourceOf(Lists.newArrayList(rulesOf(Lists.newArrayList("0.0.0.0/0"), "icmp", null),
                        rulesOf(Lists.newArrayList("0.0.0.0/0"), "tcp", null),
                        rulesOf(Lists.newArrayList("0.0.0.0/0"), "udp", Lists.newArrayList("1-600")))),
                        AlertStatus.FAIL,
                        "Firewall rule test-rule for project test-project has TCP port 3306 exposed globally."));
    }

    @Override
    AbstractExactPortCheck checker() {
        return checker;
    }

    @Override
    String expectedSignatureId() {
        return "GCP:FW-010";
    }

    ImmutableSet<ResourceType> expectedResourceTypes() {
        return ImmutableSet.of(GCE_FIREWALL);
    }
}