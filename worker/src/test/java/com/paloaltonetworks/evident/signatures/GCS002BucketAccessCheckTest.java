package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.enrichment.metadata.BucketAcl;
import com.paloaltonetworks.evident.enrichment.metadata.BucketMetadata;
import com.paloaltonetworks.evident.enrichment.metadata.BucketPolicy;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.utils.JsonUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class GCS002BucketAccessCheckTest {

    @Mock
    private ESPClient espClient;
    @Mock
    private MetricRegistry metricRegistry;
    @InjectMocks
    private GCS002BucketAccessCheck checker;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCheck() throws Exception {
        Resource resource = Resource.builder()
                .tenant("tenant")
                .resourceKey(ResourceKey.builder()
                        .externalAccountId("1234")
                        .resourceIdHash(123)
                        .resourceType("bucket")
                        .build())
                .region("us")
                .cloudAppType(CloudAppType.GCP)
                .resourceMetadata(JsonUtils.encode(BucketMetadata.builder()
                        .bucketAccessControl(Collections.singletonList(BucketAcl.builder().entity("allUsers").build()))
                        .policy(BucketPolicy.builder()
                                .resourceId("resource1").build())
                        .build()))
                .build();
        assertTrue(checker.alertStatusAndMessage(resource).getT1().equals(AlertStatus.FAIL));

    }
}