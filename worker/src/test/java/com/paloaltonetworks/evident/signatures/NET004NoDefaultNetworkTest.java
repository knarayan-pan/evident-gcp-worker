package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.enrichment.metadata.NetworkMetadata;
import com.paloaltonetworks.evident.enrichment.metadata.ProjectMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.utils.JsonUtils;
import com.paloaltonetworks.evident.utils.ResourceUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigInteger;
import java.util.Collections;
import java.util.List;

import static com.paloaltonetworks.evident.TestUtil.assertStatusAndMessage;
import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCP_PROJECT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class NET004NoDefaultNetworkTest {
    @Mock
    private ESPClient espClient;
    @Mock private MetricRegistry metricRegistry;
    @InjectMocks
    private NET004NoDefaultNetwork signature;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void signatureId() {
        assertEquals("GCP:NET-004", signature.signatureId().getValue());
    }

    @Test
    void resourceTypes() {
        assertEquals(ImmutableSet.of(GCP_PROJECT), signature.resourceTypes());
    }

    @Test
    void alertStatusAndMessageFail() {
        Resource resource = testResource((true));
        assertStatusAndMessage(signature.alertStatusAndMessage(resource), AlertStatus.FAIL,
                "Found default network on project foo.");
        assertNotNull(signature.metadata(resource));
    }

    @Test
    void alertStatusAndMessagePass() {
        Resource resource = testResource(false);
        assertStatusAndMessage(signature.alertStatusAndMessage(resource), AlertStatus.PASS,
                "Default network does not exists on project foo.");
        assertNotNull(signature.metadata(resource));
    }

    private Resource testResource(boolean hasDefaultNetwork) {
        return Resource.builder()
                .cloudAppType(CloudAppType.GCP)
                .resourceId("resourceId")
                .tenant("tenant")
                .region(ResourceUtil.REGION_GLOBAL)
                .resourceKey(ResourceKey.builder()
                        .externalAccountId("1")
                        .resourceType(ResourceType.GCE_NETWORK.getResourceType())
                        .resourceIdHash(ResourceKey.computeResourceIdHash("resourceId"))
                        .build())
                .resourceMetadata(JsonUtils.encode(
                        ProjectMetadata.builder()
                                .projectId("foo")
                                .networks(Collections.singletonList(
                                        ProjectMetadata.Network.builder()
                                                .name(hasDefaultNetwork ? "default" : "foo")
                                                .id(BigInteger.ONE)
                                                .creationTimestamp("timestamp")
                                                .subnetworks(Collections.singletonList("subnetwork"))
                                                .build()
                                        )
                                )
                                .build()
                        )
                )
                .build();
    }
}
