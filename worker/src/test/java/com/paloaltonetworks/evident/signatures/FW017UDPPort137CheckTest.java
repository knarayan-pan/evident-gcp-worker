package com.paloaltonetworks.evident.signatures;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import org.assertj.core.util.Lists;
import org.junit.jupiter.params.provider.Arguments;
import org.mockito.InjectMocks;

import java.util.List;

import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCE_FIREWALL;

class FW017UDPPort137CheckTest extends AbstractExactPortCheckTest {
    @InjectMocks
    private FW017UDPPort137Check checker;

    protected List<Arguments> arguments() {
        return ImmutableList.of(
                Arguments.of(resourceOf(ImmutableList.of(
                        rulesOf(Lists.newArrayList("10.0.0.0/0"), "TCP", Lists.newArrayList("137")),
                        rulesOf(Lists.newArrayList("0.0.0.0/0"), "UDP", Lists.newArrayList("0-500")))),
                        AlertStatus.FAIL,
                        "Firewall rule test-rule for project test-project has UDP port 137 exposed globally."),
                Arguments.of(resourceOf(Lists.newArrayList(rulesOf(Lists.newArrayList("0.0.0.0/0"), "UDP", Lists.newArrayList("137")))),
                        AlertStatus.FAIL, "Firewall rule test-rule for project test-project has UDP port 137 exposed globally."),
                Arguments.of(resourceOf(Lists.newArrayList(rulesOf(Lists.newArrayList("0.0.0.0/0"), "ALL", Lists.newArrayList()))),
                        AlertStatus.FAIL,
                        "Firewall rule test-rule for project test-project has UDP port 137 exposed globally."),
                Arguments.of(resourceOf(Lists.newArrayList(rulesOf(Lists.newArrayList("0.0.0.0/0"), "UDP", Lists.newArrayList("21")))),
                        AlertStatus.PASS,
                        "Firewall rule test-rule for project test-project does not have UDP port 137 exposed globally."));
    }

    @Override
    AbstractExactPortCheck checker() {
        return checker;
    }

    @Override
    String expectedSignatureId() {
        return "GCP:FW-017";
    }

    ImmutableSet<ResourceType> expectedResourceTypes() {
        return ImmutableSet.of(GCE_FIREWALL);
    }
}