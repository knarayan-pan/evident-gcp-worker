package com.paloaltonetworks.evident.signatures;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class SignatureIdentifierTest {

    @Test
    void testDuplicateSigIds() {
        assertEquals(Arrays.stream(SignatureIdentifier.values()).map(SignatureIdentifier::getValue)
                .collect(Collectors.toSet()).size(), SignatureIdentifier.values().length);
    }
}