package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.api.services.cloudresourcemanager.model.Binding;
import com.google.api.services.cloudresourcemanager.model.Policy;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.enrichment.metadata.IamPolicy;
import com.paloaltonetworks.evident.enrichment.metadata.ProjectMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.utils.JsonUtils;
import com.paloaltonetworks.evident.utils.ResourceUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.paloaltonetworks.evident.TestUtil.assertStatusAndMessage;
import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCP_PROJECT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class IAM002CorpLoginCredsTest {
    private final static String PASS_MESSAGE = "Project %s does not have any Gmail accounts.";
    private final static String FAIL_MESSAGE = "Project %s has one or more Gmail accounts.";
    private final static String WARN_MESSAGE = "No iam policy retrieved for project %s";

    private static final String PROJECT_NAME = "testProjectName";

    @Mock
    private ESPClient espClient;
    @Mock
    private MetricRegistry metricRegistry;
    @InjectMocks
    private IAM002CorpLoginCreds iam002CorpLoginCreds;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void signatureId() {
        assertEquals("GCP:IAM-002", iam002CorpLoginCreds.signatureId().getValue());
    }

    @Test
    public void resourceTypes() throws Exception {
        assertEquals(ImmutableSet.of(GCP_PROJECT), iam002CorpLoginCreds.resourceTypes());
    }

    @Test
    public void alertStatusWarnOnNullPolicy() throws Exception {
        Resource resource = Resource.builder()
                .tenant("tenant")
                .resourceKey(ResourceKey.builder()
                        .externalAccountId("1234")
                        .resourceIdHash(123)
                        .resourceType(GCP_PROJECT.getResourceType())
                        .build())
                .region(ResourceUtil.REGION_GLOBAL)
                .cloudAppType(CloudAppType.GCP)
                .resourceMetadata(JsonUtils.encode(ProjectMetadata.builder()
                        .projectId("testProjectId")
                        .resourceName(PROJECT_NAME)
                        .build()
                ))
                .build();
        assertStatusAndMessage(iam002CorpLoginCreds.alertStatusAndMessage(resource), AlertStatus.WARN,
                String.format(WARN_MESSAGE, PROJECT_NAME));
        assertNotNull(iam002CorpLoginCreds.metadata(resource));
    }

    @Test
    public void alertStatusAndMessagePass() throws Exception {
        Resource resource = resourceOf(Arrays.asList(IamPolicy.RoleMembers.builder()
                .role("testRole")
                .members(Arrays.asList("user:me@bigcorp.com", "user:you@bigcorp.com"))
                .build()));
        assertStatusAndMessage(iam002CorpLoginCreds.alertStatusAndMessage(resource), AlertStatus.PASS,
                String.format(PASS_MESSAGE, PROJECT_NAME));
        assertNotNull(iam002CorpLoginCreds.metadata(resource));
    }

    @Test
    public void alertStatusAndMessageFail() throws Exception {
        Resource resource = resourceOf(Arrays.asList(
                IamPolicy.RoleMembers.builder()
                    .role("testRole")
                    .members(Arrays.asList("user:me@bigcorp.com", "user:you@gmail.com"))
                    .build())
        );
        assertStatusAndMessage(iam002CorpLoginCreds.alertStatusAndMessage(resource), AlertStatus.FAIL,
                String.format(FAIL_MESSAGE, PROJECT_NAME));
        assertNotNull(iam002CorpLoginCreds.metadata(resource));
    }

    @Test
    public void shouldPassWithMultipleRoles() throws Exception {
        Resource resource = resourceOf(Arrays.asList(
                IamPolicy.RoleMembers.builder()
                        .role("testReaderRole")
                        .members(Arrays.asList("user:me@bigcorp.com", "user:you@bigcorp.com"))
                        .build(),
                IamPolicy.RoleMembers.builder()
                        .role("testOwnerRole")
                        .members(Arrays.asList("user:someoneelse@bigcorp.com", "user:another@bigcorp.com"))
                        .build()
                )
        );

        assertStatusAndMessage(iam002CorpLoginCreds.alertStatusAndMessage(resource), AlertStatus.PASS,
                String.format(PASS_MESSAGE, PROJECT_NAME));
        assertNotNull(iam002CorpLoginCreds.metadata(resource));
    }


    @Test
    public void shouldFindFailuresInMultipleRoles() throws Exception {
        Resource resource = resourceOf(Arrays.asList(
                IamPolicy.RoleMembers.builder()
                        .role("testReaderRole")
                        .members(Arrays.asList("user:me@bigcorp.com", "user:you@gmail.com"))
                        .build(),
                IamPolicy.RoleMembers.builder()
                        .role("testOwnerRole")
                        .members(Arrays.asList("user:someoneelse@bigcorp.com", "user:another@gmail.com"))
                        .build()
                )
        );

        assertStatusAndMessage(iam002CorpLoginCreds.alertStatusAndMessage(resource), AlertStatus.FAIL,
                String.format(FAIL_MESSAGE, PROJECT_NAME));
        assertNotNull(iam002CorpLoginCreds.metadata(resource));
    }


    private Resource resourceOf(List<IamPolicy.RoleMembers> roleMembers) {
        return Resource.builder()
                .tenant("tenant")
                .resourceKey(ResourceKey.builder()
                        .externalAccountId("1234")
                        .resourceIdHash(123)
                        .resourceType(GCP_PROJECT.getResourceType())
                        .build())
                .region(ResourceUtil.REGION_GLOBAL)
                .cloudAppType(CloudAppType.GCP)
                .resourceMetadata(JsonUtils.encode(ProjectMetadata.builder()
                        .projectId("testProjectId")
                        .resourceName(PROJECT_NAME)
                        .iamPolicy(iamPolicy(roleMembers))
                        .build()
                ))
                .build();
    }

    private IamPolicy iamPolicy(List<IamPolicy.RoleMembers> roleMembers) {
        List<Binding> bindings = new ArrayList<>();
        roleMembers.forEach(roleMember -> bindings.add(new Binding()
            .setRole(roleMember.getRole())
            .setMembers(roleMember.getMembers())));
        return new IamPolicy(new Policy().setBindings(bindings));
    }
}