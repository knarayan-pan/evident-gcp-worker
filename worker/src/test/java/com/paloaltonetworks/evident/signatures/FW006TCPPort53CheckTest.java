package com.paloaltonetworks.evident.signatures;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import org.assertj.core.util.Lists;
import org.junit.jupiter.params.provider.Arguments;
import org.mockito.InjectMocks;

import java.util.List;

import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCE_FIREWALL;

class FW006TCPPort53CheckTest extends AbstractExactPortCheckTest {
    @InjectMocks
    private FW006TCPPort53Check checker;

    protected List<Arguments> arguments() {
        return ImmutableList.of(
                Arguments.of(resourceOf(ImmutableList.of(
                        rulesOf(Lists.newArrayList("10.0.0.0/0"), "UDP", Lists.newArrayList("53")),
                        rulesOf(Lists.newArrayList("0.0.0.0/0"), "TCP", Lists.newArrayList("0-100")))),
                        AlertStatus.FAIL,
                        "Firewall rule test-rule for project test-project has TCP port 53 exposed globally."),
                Arguments.of(resourceOf(Lists.newArrayList(rulesOf(Lists.newArrayList("0.0.0.0/0"), "TCP", Lists.newArrayList("53")))),
                        AlertStatus.FAIL, "Firewall rule test-rule for project test-project has TCP port 53 exposed globally."),
                Arguments.of(resourceOf(Lists.newArrayList(rulesOf(Lists.newArrayList("0.0.0.0/0"), "ALL", Lists.newArrayList()))),
                        AlertStatus.FAIL,
                        "Firewall rule test-rule for project test-project has TCP port 53 exposed globally."),
                Arguments.of(resourceOf(Lists.newArrayList(rulesOf(Lists.newArrayList("0.0.0.0/0"), "TCP", Lists.newArrayList("21")))),
                        AlertStatus.PASS,
                        "Firewall rule test-rule for project test-project does not have TCP port 53 exposed globally."));
    }

    @Override
    AbstractExactPortCheck checker() {
        return checker;
    }

    @Override
    String expectedSignatureId() {
        return "GCP:FW-006";
    }

    ImmutableSet<ResourceType> expectedResourceTypes() {
        return ImmutableSet.of(GCE_FIREWALL);
    }
}