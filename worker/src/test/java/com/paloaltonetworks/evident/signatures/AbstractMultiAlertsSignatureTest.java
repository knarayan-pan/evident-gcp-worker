package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.enrichment.metadata.BucketMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.esp.model.ESPApiResponse;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.utils.JsonUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AbstractMultiAlertsSignatureTest {

    @Mock
    private MetricRegistry metricRegistry;

    @Mock
    private ESPClient espClient;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

    }

    class TestMultiAlertSig extends AbstractMultiAlertsSignature {

        TestMultiAlertSig(ESPClient espClient, MetricRegistry metricRegistry) {
            super(espClient, metricRegistry);
        }

        @Override
        protected Mono<Map<String, Triple<AlertStatus, String, JSONObject>>> alertsStatusMessageAndMetadata(Resource resource) {
            return Mono.just(ImmutableMap.of("alert1", Triple.of(AlertStatus.PASS, "something", new JSONObject()),
                    "alert2", Triple.of(AlertStatus.FAIL, "somethingelse", new JSONObject())));
        }

        @Override
        public SignatureIdentifier signatureId() {
            return SignatureIdentifier.GCP_DNS_002;
        }

        @Override
        public Set<ResourceType> resourceTypes() {
            return ImmutableSet.of(ResourceType.GCS_STORAGE);
        }
    }

    @Test
    void testAtomicCountersForMultiAlerts() throws IOException {
        when(espClient.createAlert(anyString(), any())).thenReturn(ESPApiResponse.builder().build());
        when(metricRegistry.timer(any())).thenReturn(new Timer());
        TestMultiAlertSig testMultiAlertSig = new TestMultiAlertSig(espClient, metricRegistry);
        Resource bucket = Resource.builder()
                .resourceId("123")
                .resourceKey(ResourceKey.builder()
                        .resourceIdHash(1234)
                        .resourceType("bucket")
                        .externalAccountId("1234")
                        .build())
                .region("us-east-1")
                .resourceMetadata(JsonUtils.encode(BucketMetadata.builder()
                        .location("us-east")
                        .build()))
                .build();
        testMultiAlertSig.check(bucket);
        // verify that the alert_count parameter is added in the tags
        verify(metricRegistry, times(1)).timer(contains("alert_count"));
    }
}