package com.paloaltonetworks.evident.signatures;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.TestUtil;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.enrichment.metadata.IamPolicy;
import com.paloaltonetworks.evident.enrichment.metadata.ProjectMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.utils.JsonUtils;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class IAM003ServiceAccountNoAdminTest {
    private static final String PASS_MESSAGE = "Service account %s does not have any admin privileges.";
    private static final String FAIL_MESSAGE = "Service account %s has admin privileges.";

    @Mock
    private ESPClient espClient;
    @Mock
    private MetricRegistry metricRegistry;
    @InjectMocks
    private IAM003ServiceAccountNoAdmin signature;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void signatureId() throws Exception {
        assertEquals("GCP:IAM-003", signature.signatureId().getValue());
    }

    @Test
    public void resourceTypes() throws Exception {
        assertEquals(ImmutableSet.of(ResourceType.GCP_PROJECT), signature.resourceTypes());
    }

    @Test
    public void nulIamPolicy() {
        ProjectMetadata projMetadata = ProjectMetadata.builder().projectId("testProject-1").build();

        Resource resource = Resource.builder()
            .tenant("tenant")
            .resourceKey(ResourceKey.builder()
                .externalAccountId("1234")
                .resourceIdHash(123)
                .resourceType("project")
                .build())
            .region("global")
            .cloudAppType(CloudAppType.GCP)
            .resourceMetadata(JsonUtils.encode(projMetadata))
            .build();

        signature.alertsStatusMessageAndMetadata(resource).subscribe(
            alert -> Assertions.fail("Should not emit alert")
        );
    }

    private static Stream<Arguments> resourcesProvider() {
        return Stream.of(
            Arguments.of("null roleMembers",
                null, Collections.emptyMap()),
            Arguments.of("empty roleMembers",
                Collections.emptyList(),
                Collections.emptyMap()),
            Arguments.of("1 roleMembers without service account",
                ImmutableList.of(
                    IamPolicy.RoleMembers.builder()
                        .role("role1")
                        .members(ImmutableList.of("userAccount:a@b.com"))
                        .build()
                ),
                Collections.emptyMap()),
            Arguments.of("1 roleMembers with non-admin role, 1 service account",
                ImmutableList.of(
                    IamPolicy.RoleMembers.builder()
                        .role("role1")
                        .members(ImmutableList.of("serviceAccount:a@b.com"))
                        .build()
                ),
                Collections.singletonMap("testProject-1/serviceAccount:a@b.com",
                    ImmutableTriple.of(AlertStatus.PASS,String.format(PASS_MESSAGE, "serviceAccount:a@b.com"),
                        new JSONObject(JsonUtils.encode(
                            ImmutableMap.of("serviceAccountEmail", "serviceAccount:a@b.com", "adminLevelRoles", Collections.emptyList())
                        ))))),
            Arguments.of("1 roleMembers with admin role (roleAdmin), 1 service account",
                ImmutableList.of(
                    IamPolicy.RoleMembers.builder()
                        .role("roleAdmin")
                        .members(ImmutableList.of("user:x@y.com", "serviceAccount:a@b.com"))
                        .build()
                ),
                Collections.singletonMap("testProject-1/serviceAccount:a@b.com",
                    ImmutableTriple.of(AlertStatus.FAIL,String.format(FAIL_MESSAGE, "serviceAccount:a@b.com"),
                        new JSONObject(JsonUtils.encode(
                            ImmutableMap.of("serviceAccountEmail", "serviceAccount:a@b.com", "adminLevelRoles", ImmutableList.of("roleAdmin"))
                        ))))),
            Arguments.of("1 roleMembers with admin role (a/b/roles/editor), 1 service account",
                ImmutableList.of(
                    IamPolicy.RoleMembers.builder()
                        .role("a/b/roles/editor")
                        .members(ImmutableList.of("user:x@y.com", "serviceAccount:a@b.com"))
                        .build()
                ),
                Collections.singletonMap("testProject-1/serviceAccount:a@b.com",
                    ImmutableTriple.of(AlertStatus.FAIL,String.format(FAIL_MESSAGE, "serviceAccount:a@b.com"),
                        new JSONObject(JsonUtils.encode(
                            ImmutableMap.of("serviceAccountEmail", "serviceAccount:a@b.com", "adminLevelRoles", ImmutableList.of("a/b/roles/editor"))
                        ))))),
            Arguments.of("1 roleMembers with admin role (a/b/roles/owner), 1 service account",
                ImmutableList.of(
                    IamPolicy.RoleMembers.builder()
                        .role("a/b/roles/owner")
                        .members(ImmutableList.of("user:x@y.com", "serviceAccount:a@b.com"))
                        .build()
                ),
                Collections.singletonMap("testProject-1/serviceAccount:a@b.com",
                    ImmutableTriple.of(AlertStatus.FAIL,String.format(FAIL_MESSAGE, "serviceAccount:a@b.com"),
                        new JSONObject(JsonUtils.encode(
                            ImmutableMap.of("serviceAccountEmail", "serviceAccount:a@b.com", "adminLevelRoles", ImmutableList.of("a/b/roles/owner"))
                        ))))),
            Arguments.of("multiple roleMembers with admin role (a/b/roles/owner), multiple service account",
                ImmutableList.of(
                    IamPolicy.RoleMembers.builder()
                        .role("a/b/roles/owner")
                        .members(ImmutableList.of("user:x@y.com", "serviceAccount:a@b.com", "serviceAccount:c@d.com"))
                        .build(),
                    IamPolicy.RoleMembers.builder()
                        .role("roles/editor")
                        .members(ImmutableList.of("user:x@y.com", "serviceAccount:c2@d2.com"))
                        .build(),
                    IamPolicy.RoleMembers.builder()
                        .role("a/b/roleadmin")
                        .members(ImmutableList.of("user:x3@y3.com", "serviceAccount:a@b.com"))
                        .build()
                ),
                ImmutableMap.of(
                    "testProject-1/serviceAccount:a@b.com",
                    ImmutableTriple.of(AlertStatus.FAIL,String.format(FAIL_MESSAGE, "serviceAccount:a@b.com"),
                        new JSONObject(JsonUtils.encode(
                            ImmutableMap.of("serviceAccountEmail", "serviceAccount:a@b.com", "adminLevelRoles", ImmutableList.of("a/b/roles/owner", "a/b/roleadmin"))
                        ))),
                    "testProject-1/serviceAccount:c@d.com",
                    ImmutableTriple.of(AlertStatus.FAIL,String.format(FAIL_MESSAGE, "serviceAccount:c@d.com"),
                        new JSONObject(JsonUtils.encode(
                            ImmutableMap.of("serviceAccountEmail", "serviceAccount:c@d.com", "adminLevelRoles", ImmutableList.of("a/b/roles/owner"))
                        ))),
                "testProject-1/serviceAccount:c2@d2.com",
                ImmutableTriple.of(AlertStatus.FAIL,String.format(FAIL_MESSAGE, "serviceAccount:c2@d2.com"),
                    new JSONObject(JsonUtils.encode(
                        ImmutableMap.of("serviceAccountEmail", "serviceAccount:c2@d2.com", "adminLevelRoles", ImmutableList.of("roles/editor"))
                    ))))
                )
        );
    }

    @ParameterizedTest(name = "resource #{index} - {0}")
    @MethodSource("resourcesProvider")
    public void checkVariousResource(String description, List<IamPolicy.RoleMembers> roleMembersList,
                              Map<String, Triple<AlertStatus, String, JSONObject>> expectedAlerts) {
        ProjectMetadata projMetadata = ProjectMetadata.builder()
            .projectId("testProject-1")
            .iamPolicy(IamPolicy.builder()
                .roleMembers(roleMembersList)
                .build())
            .build();

        Resource resource = Resource.builder()
            .tenant("tenant")
            .resourceKey(ResourceKey.builder()
                .externalAccountId("1234")
                .resourceIdHash(123)
                .resourceType("project")
                .build())
            .region("global")
            .cloudAppType(CloudAppType.GCP)
            .resourceMetadata(JsonUtils.encode(projMetadata))
            .build();

        signature.alertsStatusMessageAndMetadata(resource).subscribe(
            alerts ->  {
                Assertions.assertEquals(alerts.size(), expectedAlerts.size());
                alerts.entrySet().stream()
                    .forEach(
                        entry -> {
                            Assertions.assertEquals(expectedAlerts.get(entry.getKey()).getLeft(), entry.getValue().getLeft());
                            Assertions.assertEquals(expectedAlerts.get(entry.getKey()).getMiddle(), entry.getValue().getMiddle());
                            TestUtil.assertMetadata(expectedAlerts.get(entry.getKey()).getRight(),
                                entry.getValue().getRight(), "metadata should match for " + entry.getKey());
                        }
                    );
            }
        );
    }
}
