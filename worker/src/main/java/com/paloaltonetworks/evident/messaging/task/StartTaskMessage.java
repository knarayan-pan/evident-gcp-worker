package com.paloaltonetworks.evident.messaging.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;
import org.springframework.lang.Nullable;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@JsonTypeName(StartTaskMessage.TASK_TYPE)
public class StartTaskMessage extends BaseTaskMessage {
    public static final String TASK_TYPE = "start_task";
    private static final String RECONCILE = "reconcile";

    @JsonProperty(RECONCILE)
    private final Boolean reconcile;

    @Builder
    @JsonCreator
    public StartTaskMessage(@NonNull @JsonProperty(TENANT_NAME_FIELD) String tenantName,
                            @NonNull @JsonProperty(EXTERNAL_ID) String externalId,
                            @Nullable @JsonProperty(RECONCILE) Boolean reconcile) {
        super(tenantName, externalId);
        this.reconcile = reconcile;
    }

    @Override
    public String getTaskType() {
        return TASK_TYPE;
    }

}
