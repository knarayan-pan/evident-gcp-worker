package com.paloaltonetworks.evident.messaging.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;

import javax.annotation.Nonnull;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@JsonTypeName(ProjectTreeDeleteTaskMessage.TASK_TYPE)
public class ProjectTreeDeleteTaskMessage extends BaseTaskMessage {
    public static final String TASK_TYPE = "project_delete_message";
    private static final String PROJECT_ID = "project_id";
    private static final String RESOURCE_TYPE = "resource_type";
    private static final String ORGANIZATION_ID = "organization_id";

    @JsonProperty(PROJECT_ID)
    private final String projectId;
    @JsonProperty(RESOURCE_TYPE)
    private final String resourceType;
    @JsonProperty(ORGANIZATION_ID)
    private final String organizationId;

    @Builder
    @JsonCreator
    public ProjectTreeDeleteTaskMessage(@NonNull @JsonProperty(TENANT_NAME_FIELD) String tenantName,
                                        @NonNull @JsonProperty(EXTERNAL_ID) String externalId,
                                        @Nonnull @JsonProperty(PROJECT_ID) String projectId,
                                        @Nonnull @JsonProperty(RESOURCE_TYPE) String resourceType,
                                        @JsonProperty(ORGANIZATION_ID) String organizationId) {
        super(tenantName, externalId);
        this.projectId = projectId;
        this.resourceType = resourceType;
        this.organizationId = organizationId;
    }


    @Override
    public String getTaskType() {
        return TASK_TYPE;
    }
}
