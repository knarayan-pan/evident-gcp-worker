package com.paloaltonetworks.evident.messaging.queue;

import com.amazonaws.AbortedException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.CreateQueueResult;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.QueueDeletedRecentlyException;
import com.amazonaws.services.sqs.model.QueueDoesNotExistException;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.codahale.metrics.MetricRegistry;
import com.google.common.base.Joiner;
import com.google.common.base.Stopwatch;
import com.paloaltonetworks.aperture.ApertureSqsAsyncClient;
import com.paloaltonetworks.aperture.MetricConstants;
import com.paloaltonetworks.aperture.Tags;
import com.paloaltonetworks.evident.config.PropertyHandler;
import com.paloaltonetworks.evident.exceptions.FatalTaskErrorException;
import com.paloaltonetworks.evident.exceptions.GCPApiException;
import com.paloaltonetworks.evident.exceptions.GCPTokenException;
import com.paloaltonetworks.evident.exceptions.RetryOperationException;
import com.paloaltonetworks.evident.gcp.api.NonRetryableGCPException;
import com.paloaltonetworks.evident.messaging.MessageContext;
import com.paloaltonetworks.evident.messaging.handler.TaskHandler;
import com.paloaltonetworks.evident.messaging.task.TaskMessage;
import com.paloaltonetworks.evident.service.QueueService;
import com.paloaltonetworks.evident.sqs_client.MonitoredSqsAsyncClient;
import com.paloaltonetworks.evident.utils.JsonUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.scheduler.Schedulers;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public abstract class MessagePoller {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static long SQS_RECEIVE_WAIT_TIMEOUT = TimeUnit.SECONDS.toSeconds(20);
    private static long SQS_VISIBILITY_TIMEOUT = TimeUnit.HOURS.toSeconds(2);
    private static long SQS_MESSAGE_RETENTION_PERIOD = TimeUnit.DAYS.toSeconds(14);
    private static final String MESSAGES_CONSUMED_TIMER = "timer.messages_consumed";
    private static final String EXCEPTIONS_METER = "meter.exceptions";
    private final ApertureSqsAsyncClient sqsClient;
    private final MetricRegistry metricRegistry;
    private final PropertyHandler propertyHandler;
    private final QueueService queueService;
    private List<QueueSubscriber> subscribers;
    private Map<String, TaskHandler> taskHandlerMap;

    @Autowired
    MessagePoller(@Qualifier(MonitoredSqsAsyncClient.NAME) ApertureSqsAsyncClient sqsClient,
                  MetricRegistry metricRegistry,
                  Map<String, TaskHandler> taskHandlerMap,
                  PropertyHandler propertyHandler, QueueService queueService) {
        this.sqsClient = sqsClient;
        this.taskHandlerMap = taskHandlerMap;
        this.metricRegistry = metricRegistry;
        this.propertyHandler = propertyHandler;
        this.queueService = queueService;
        subscribers = new ArrayList<>();
    }

    public void start(int numberOfThreads) throws InterruptedException {
        String queueName = formattedQueueName();
        String dlQueueName = getDLQueueName();
        LOGGER.debug("Creating queue: {} and dl queue: {}", queueName, dlQueueName);
        CreateQueueResult result = sqsClient.createQueue(dlQueueName, new CreateQueueRequest());
        GetQueueAttributesResult queueAttributes = sqsClient.getQueueAttributes(
                new GetQueueAttributesRequest(result.getQueueUrl())
                        .withAttributeNames("QueueArn"));
        sqsClient.createQueue(queueName, new CreateQueueRequest()
                .addAttributesEntry("RedrivePolicy", new JSONObject().put("maxReceiveCount", 5)
                        .put("deadLetterTargetArn", queueAttributes.getAttributes().get("QueueArn")).toString())
                .addAttributesEntry("ReceiveMessageWaitTimeSeconds",
                        Long.toString(SQS_RECEIVE_WAIT_TIMEOUT))
                .addAttributesEntry("VisibilityTimeout", Long.toString(SQS_VISIBILITY_TIMEOUT))
                .addAttributesEntry("MessageRetentionPeriod",
                        Long.toString(SQS_MESSAGE_RETENTION_PERIOD)));
        // Start polling
        LOGGER.debug("Setting up subscribers for queue: {}", queueName);
        for (int i = 0; i < numberOfThreads; i++) {
            subscribeAsync();
            TimeUnit.MILLISECONDS.sleep(500);
        }
    }

    private String formattedQueueName() {
        return formattedQueueName(null);
    }

    private void subscribeAsync() {
        Flux<MessageContext> flux = Flux.create((Consumer<FluxSink<MessageContext>>) sink ->
                sink.onRequest(
                        value -> {
                            sink.next(getMessage());
                        }))
                .retry()
                .filter(this::removeEmptyMessages)
                .subscribeOn(Schedulers.newSingle(getQueueName()));
        QueueSubscriber subscriber = new QueueSubscriber(context -> processMessage(context, Stopwatch.createStarted()));
        flux.subscribe(subscriber);
        subscribers.add(subscriber);
    }

    public void stop() {
        final List<QueueSubscriber> closedSubscriber = new ArrayList<>();
        subscribers.stream().filter(Objects::nonNull).forEach(subscriber -> {
            if (!subscriber.isDisposed()) {
                subscriber.dispose();
                closedSubscriber.add(subscriber);
            }
        });
        LOGGER.info("Closed {} subscribers for queue: {}", closedSubscriber.size(), getQueueName());
    }

    private void cleanUpQueues() {
        deleteQueueSilently(propertyHandler.getSqsQueuePrefix() + formattedQueueName());
        deleteQueueSilently(propertyHandler.getSqsQueuePrefix() + getDLQueueName());
    }

    private void deleteQueueSilently(String queueName) {
        try {
            sqsClient.deleteQueue(queueName);
        } catch (QueueDoesNotExistException | QueueDeletedRecentlyException e) {
            LOGGER.warn("Queue {} does not exist so will not delete.", queueName);
        } catch (AmazonServiceException ase) {
            LOGGER.error("Error in deleting queue: {}. Details: Error msg: {}, Status code: {}, Error code: {}, " +
                            "Error type: {}, Request ID: {}",
                    queueName, ase.getMessage(), ase.getStatusCode(), ase.getErrorCode(), ase.getErrorType(), ase.getRequestId());
            LOGGER.error("Please clean up queue {} manually", queueName);
        } catch (AbortedException ae) {
            if (!ae.isRetryable()) {
                throw new RuntimeException(ae);
            }
            LOGGER.error("Caught AbortedException!!", ae);
        }
    }


    private void processMessage(MessageContext messageContext, Stopwatch stopwatch) {

        Message message = messageContext.getMessage()
                .orElseThrow(() -> new IllegalArgumentException("Message should have been present."));
        // this is only a partial de-dup fix - this only tests true duplicate sqs messages and not application level duplicate messages.
        if (!Boolean.TRUE.equals(queueService.acquireLocalMsgLock(message.getMessageId()))) {
            LOGGER.warn("Detected duplicate sqs message being processed. Ignoring this message with id: {}", message.getMessageId());
            return;
        }

        TaskMessage taskMessage = messageContext.getTaskMessage()
                .orElseThrow(() -> new IllegalArgumentException("Task message should have been present."));
        String queueName = messageContext.getQueueName();
        Tags.TagsBuilder tags = Tags.builder()
                .tag("queue", queueName)
                .tag("type", taskMessage.getTaskType());
        String tenantName = taskMessage.getTenantName();
        if (StringUtils.isNotEmpty(taskMessage.getExternalAccountId())) {
            tags.tag("externalAccountId", taskMessage.getExternalAccountId());
        }
        try {
            taskMessage.incrementAttemptsCount();
            handleMessage(taskMessage);
            sqsClient.deleteMessage(queueName, message);
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.SUCCCESS_TAG);
        } catch (RetryOperationException e) {
            LOGGER.error("Failed to process message: {} and will try it again in {} {}", message, e.getDuration(),
                    e.getTimeUnit());
            LOGGER.error("Exception: ", e);
            String receiptHandle = message.getReceiptHandle();
            sqsClient.extendMessageVisibilityTimeout(queueName, receiptHandle, e.getDuration(), e.getTimeUnit())
                    .whenComplete((changeMessageVisibilityResult, throwable) -> {
                        if (throwable != null) {
                            LOGGER.error("Failed to extend message timeout.", throwable);
                        }
                    });
            Tags exceptionTags = tags.build().toBuilder()
                    .tag("class", e.getClass().getSimpleName())
                    .build();
            metricRegistry.meter(exceptionTags.toMetricName(EXCEPTIONS_METER))
                    .mark();
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.FAILURE_TAG).build();
        } catch (FatalTaskErrorException e) {
            LOGGER.error("Error occurred while processing message: {} "
                    + "\nMessage will be deleted because retrying will not cause it to be successful, "
                    + "so it will not show up later.", message);
            LOGGER.error("Exception: ", e);
            sqsClient.deleteMessage(queueName, message);
            Tags exceptionTags = tags.build().toBuilder()
                    .tag("class", e.getClass().getSimpleName())
                    .build();
            metricRegistry.meter(exceptionTags.toMetricName(EXCEPTIONS_METER))
                    .mark();
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.FAILURE_TAG).build();
        } catch (GCPTokenException e) {
            LOGGER.error("Error refreshing token. Task will be retried with delay if possible");
            Tags.TagsBuilder exceptionTags = tags.build().toBuilder()
                    .tag("class", e.getClass().getSimpleName());
            taskMessage.incrementAttemptsWithRunTimeErrorCount();
            if(taskMessage.canBeRetried()) {
                sqsClient.sendMessageAsync(queueName, new SendMessageRequest()
                        .withMessageBody(JsonUtils.encode(taskMessage))
                        .withDelaySeconds(5 * taskMessage.currentAttempt()));
                LOGGER.debug("Re-queuing token refresh task");
            } else {
                LOGGER.warn("Token refresh task cannot be retried as this is at failure limit");
            }
            sqsClient.deleteMessage(queueName, message);
            metricRegistry.meter(exceptionTags.build().toMetricName(EXCEPTIONS_METER))
                    .mark();
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.FAILURE_TAG).build();
        } catch (NonRetryableGCPException e) {
            LOGGER.warn("Received non-retryable exception - {}. Skipping task", e.getMessage());
            sqsClient.deleteMessage(queueName, message);
            Tags exceptionTags = tags.build().toBuilder()
                    .tag("class", e.getClass().getSimpleName())
                    .build();
            metricRegistry.meter(exceptionTags.toMetricName(EXCEPTIONS_METER))
                    .mark();
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.FAILURE_TAG).build();
        } catch (GCPApiException e) {
            LOGGER.warn("Received api error response from GCP - {}", e.getStatus());
            Tags.TagsBuilder exceptionTags = tags.build().toBuilder()
                    .tag("class", e.getClass().getSimpleName());
            if(e.retryableException()) {
                taskMessage.incrementAttemptsWithRunTimeErrorCount();
                if(taskMessage.canBeRetried()) {
                    sqsClient.sendMessageAsync(queueName, new SendMessageRequest()
                            .withMessageBody(JsonUtils.encode(taskMessage)));
                    LOGGER.debug("Re-queuing task that threw api exception");
                } else {
                    LOGGER.warn("Task with type: {} cannot be retried as it is at retry limit", taskMessage.getTaskType());
                    exceptionTags.tag("at_failure_limit", "true");
                }
            }
            sqsClient.deleteMessage(queueName, message);
            metricRegistry.meter(exceptionTags.build().toMetricName(EXCEPTIONS_METER))
                    .mark();
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.FAILURE_TAG).build();
        } catch (Exception e) {
            LOGGER.error("Error occurred while processing event. Will set the runtime error counts and requeue if possible", e);
            Tags.TagsBuilder exceptionTags = tags.build().toBuilder()
                    .tag("class", e.getClass().getSimpleName());
            taskMessage.incrementAttemptsWithRunTimeErrorCount();
            // check if the task can be retried - then queue the message.
            if (taskMessage.canBeRetried()) {
                LOGGER.info("Message: {} is not at failure limit. Will retry on queue: {}", message, queueName);
                sqsClient.sendMessageAsync(queueName, new SendMessageRequest().withMessageBody(JsonUtils.encode(taskMessage)));
                exceptionTags.tag("at_failure_limit", "false");
            } else {
                // log metric to say this will not be retried.
                LOGGER.info("Message: {} is at failure limit. Will not be retried", message);
                exceptionTags.tag("at_failure_limit", "true");
            }
            sqsClient.deleteMessage(queueName, message);

            metricRegistry.meter(exceptionTags.build().toMetricName(EXCEPTIONS_METER))
                    .mark();
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.FAILURE_TAG).build();
        } finally {
            long elapsed = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
            metricRegistry.timer(tags.build().toMetricName(MESSAGES_CONSUMED_TIMER))
                    .update(elapsed, TimeUnit.MILLISECONDS);
            queueService.clearLock(message.getMessageId());
        }
    }

    private void handleMessage(TaskMessage taskMessage) {
        // loaded from JsonUtils mapper interface..
        TaskHandler taskHandler = taskHandlerMap.get(taskMessage.getTaskType());
        taskHandler.execute(taskMessage);
    }


    /**
     * Return the queue name to read.
     *
     * @return queue name
     */
    public abstract String getQueueName();

    /**
     * Return the dl queue name for this queue
     *
     * @return dl queue name
     */
    private String getDLQueueName() {
        String SQS_DL_QUEUE_FORMATTER = "%s_dl_queue";
        return String.format(SQS_DL_QUEUE_FORMATTER, formattedQueueName());
    }

    /**
     * Returns the number of threads allocated for this queue
     *
     * @return the thread count
     */
    public abstract int getThreadCount();

    private MessageContext getMessage() {
        String queueName = formattedQueueName();
        Optional<Message> message = getMessage(queueName);
        MessageContext context;
        if (!message.isPresent()) {
            // Look for work on another queue
            context = lookForWork();
        } else {
            context = MessageContext.builder()
                    .queueName(queueName)
                    .message(message.get())
                    .build();
        }
        return context;
    }

    private Optional<Message> getMessage(String queueName) {
        return sqsClient.getMessage(false, queueName,
                new ReceiveMessageRequest()
                        .withWaitTimeSeconds(5)
                        .withVisibilityTimeout((int) TimeUnit.MINUTES.toSeconds(30))
                        .withMaxNumberOfMessages(1));
    }

    private MessageContext lookForWork() {
        MessageContext.MessageContextBuilder builder = MessageContext.builder();
        for (String queueName : getBackupQueues()) {
            if (sqsClient.getNumMessages(formattedQueueName(queueName)) > 0) {
                Optional<Message> message = getMessage(queueName);
                if (message.isPresent()) {
                    LOGGER.info("Stealing work from queue: {}", queueName);
                    return builder.queueName(queueName)
                            .message(message.get())
                            .build();
                }
            }
        }
        return builder.build();
    }

    private String formattedQueueName(String queueName) {
        return Joiner.on("_").join(propertyHandler.getExternalId(), StringUtils.isEmpty(queueName) ? getQueueName() : queueName);
    }

    private List<String> getBackupQueues() {
        return Collections.emptyList();
    }

    private boolean removeEmptyMessages(MessageContext context) {
        if (!context.getMessage().isPresent()) {
            LOGGER.debug("Message was not present. Ignoring.");
            return false;
        }
        if (!context.getTaskMessage().isPresent()) {
            LOGGER.debug("Task message was not present. Ignoring.");
            return false;
        }
        return true;
    }


    private class QueueSubscriber extends BaseSubscriber<MessageContext> {
        private final Consumer<MessageContext> consumer;

        QueueSubscriber(Consumer<MessageContext> consumer) {
            this.consumer = consumer;
        }

        @Override
        public void hookOnSubscribe(Subscription subscription) {
            request(1);
        }

        @Override
        public void hookOnNext(MessageContext context) {
            consumer.accept(context);
            request(1);
        }

        @Override
        public void hookOnError(Throwable t) {
            LOGGER.error("Exception while subscribing to queue.", t);
        }
    }
}
