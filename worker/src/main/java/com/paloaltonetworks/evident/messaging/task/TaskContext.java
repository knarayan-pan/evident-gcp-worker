package com.paloaltonetworks.evident.messaging.task;

import lombok.Builder;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.UUID;

@Data
public class TaskContext {
    private final String taskId;
    private final String parentTaskId;


    @Builder
    public TaskContext(String parentTaskId) {
        this.taskId = UUID.randomUUID().toString();
        this.parentTaskId = StringUtils.isEmpty(parentTaskId) ? null : parentTaskId;
    }
}
