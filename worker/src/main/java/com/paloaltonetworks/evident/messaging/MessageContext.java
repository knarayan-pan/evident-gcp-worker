package com.paloaltonetworks.evident.messaging;

import com.amazonaws.services.sqs.model.Message;
import com.paloaltonetworks.evident.messaging.task.TaskMessage;
import com.paloaltonetworks.evident.utils.JsonUtils;
import lombok.Builder;
import lombok.Data;

import java.util.Optional;

@Data
public class MessageContext {
    private final String queueName;
    private final Message message;

    @Builder
    public MessageContext(String queueName, Message message) {
        this.queueName = queueName;
        this.message = message;
    }

    public Optional<Message> getMessage() {
        return Optional.ofNullable(message);
    }

    public Optional<TaskMessage> getTaskMessage() {
        TaskMessage taskMessage = Optional.ofNullable(message)
                .map(val -> JsonUtils.decodeValue(message.getBody(), TaskMessage.class))
                .orElse(null);
        return Optional.ofNullable(taskMessage);
    }
}
