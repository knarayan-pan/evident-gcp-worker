package com.paloaltonetworks.evident.messaging.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;

import javax.annotation.Nonnull;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@JsonTypeName(ResourceRefreshTaskMessage.TASK_TYPE)
public class ResourceRefreshTaskMessage extends BaseTaskMessage {
    public static final String TASK_TYPE = "resource_refresh_message";
    private static final String RESOURCE_ID = "resource_id";
    private static final String RESOURCE_NAME = "resource_name";
    private static final String PROJECT_ID = "project_id";
    private static final String ZONE = "zone";
    private static final String RESOURCE_TYPE = "resource_type";
    private static final String ORGANIZATION_ID = "organization_id";
    private static final String PAGE_SIZE = "page_size";
    private static final String PAGE_TOKEN = "page_token";
    private static final String RECONCILE = "reconcile";


    @JsonProperty(RESOURCE_ID)
    private final String resourceId;
    @JsonProperty(RESOURCE_NAME)
    private String resourceName;
    @JsonProperty(PROJECT_ID)
    private final String projectId;
    @JsonProperty(RESOURCE_TYPE)
    private final String resourceType;
    @JsonProperty(ORGANIZATION_ID)
    private final String organizationId;
    @JsonProperty(ZONE)
    private String zone;
    @JsonProperty(PAGE_SIZE)
    private final Integer pageSize;
    @JsonProperty(PAGE_TOKEN)
    private final String pageToken;
    @JsonProperty(RECONCILE)
    private final Boolean reconcile;


    @Builder
    @JsonCreator
    public ResourceRefreshTaskMessage(@NonNull @JsonProperty(TENANT_NAME_FIELD) String tenantName,
                                      @NonNull @JsonProperty(EXTERNAL_ID) String externalId,
                                      @JsonProperty(RESOURCE_ID) String resourceId,
                                      @JsonProperty(RESOURCE_NAME) String resourceName,
                                      @Nonnull @JsonProperty(PROJECT_ID) String projectId,
                                      @Nonnull @JsonProperty(RESOURCE_TYPE) String resourceType,
                                      @JsonProperty(ORGANIZATION_ID) String organizationId,
                                      @JsonProperty(ZONE) String zone,
                                      @JsonProperty(PAGE_SIZE) Integer pageSize,
                                      @JsonProperty(PAGE_TOKEN) String pageToken,
                                      @JsonProperty(RECONCILE) Boolean reconcile) {
        super(tenantName, externalId);
        this.resourceId = resourceId;
        this.projectId = projectId;
        this.resourceType = resourceType;
        this.organizationId = organizationId;
        this.zone = zone;
        this.resourceName = resourceName;
        this.pageSize = pageSize;
        this.pageToken = pageToken;
        this.reconcile = reconcile;
    }


    @Override
    public String getTaskType() {
        return TASK_TYPE;
    }

    public String getZone() {
        return zone;
    }

    public String getResourceName() {
        return resourceName;
    }
}
