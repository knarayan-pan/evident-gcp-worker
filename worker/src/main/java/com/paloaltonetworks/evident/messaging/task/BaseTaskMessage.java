package com.paloaltonetworks.evident.messaging.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public abstract class BaseTaskMessage implements TaskMessage {
    protected static final String TENANT_NAME_FIELD = "tenant";
    protected static final String EXTERNAL_ID = "external_id";
    protected static final Integer CURRENT_ATTEMPTS_COUNT_INIT_VALUE = 0;
    protected static final Integer MAXIMUM_ATTEMPTS_COUNT_DEFAULT = 5;
    protected static final Integer ATTEMPTS_WITH_RUNTIME_ERRORS_COUNT_INIT_VALUE = 0;
    protected static final Integer MAXIMUM_ATTEMPTS_WITH_RUNTIME_ERRORS_COUNT_DEFAULT = 5;

    protected static final String CURRENT_ATTEMPT_COUNT = "current_attempt_count";
    protected static final String MAX_ATTEMPTS_COUNT = "max_attempt_count";
    protected static final String ATTEMPTS_WITH_RUNTIME_ERROR_COUNT = "attempts_with_runtime_errors_count";
    protected static final String MAX_ATTEMPTS_WITH_RUNTIME_ERROR_COUNT = "max_attempts_with_runtime_errors_count";

    @JsonProperty(CURRENT_ATTEMPT_COUNT)
    Integer currentAttemptsCount = CURRENT_ATTEMPTS_COUNT_INIT_VALUE;

    @JsonProperty(MAX_ATTEMPTS_COUNT)
    Integer maxAttemptsCount = MAXIMUM_ATTEMPTS_COUNT_DEFAULT;

    @JsonProperty(ATTEMPTS_WITH_RUNTIME_ERROR_COUNT)
    Integer attemptsWithRuntimeErrorsCount = ATTEMPTS_WITH_RUNTIME_ERRORS_COUNT_INIT_VALUE;

    @JsonProperty(MAX_ATTEMPTS_WITH_RUNTIME_ERROR_COUNT)
    Integer maxAttemptsWithRuntimeErrorsCount = MAXIMUM_ATTEMPTS_WITH_RUNTIME_ERRORS_COUNT_DEFAULT;

    @JsonProperty(TENANT_NAME_FIELD)
    private String tenantName;

    @JsonProperty(EXTERNAL_ID)
    private String externalAccountId;
    // todo - fix task id and parent task id
//    @JsonProperty("task_id")
//    private final String taskId;
//    @JsonProperty("parent_task_id")
//    private final String parentTaskId;

    // ugly hack - but for some reason lombok @Data does not allow this. Need to check on lombok version.
    public BaseTaskMessage() {

    }

    @JsonCreator
    public BaseTaskMessage(@JsonProperty(TENANT_NAME_FIELD) String tenantName, @JsonProperty(EXTERNAL_ID) String externalId) {
        this.tenantName = tenantName;
        this.externalAccountId = externalId;
    }

    @Override
    public void incrementAttemptsCount() {
        currentAttemptsCount++;
    }

    @Override
    public int currentAttempt() {
        return currentAttemptsCount;
    }

    @Override
    public void incrementAttemptsWithRunTimeErrorCount() {
        attemptsWithRuntimeErrorsCount++;
    }

    @Override
    public boolean runtimeErrorsExceeded() {
        return attemptsWithRuntimeErrorsCount >= maxAttemptsWithRuntimeErrorsCount;
    }

    @Override
    public boolean attemptsCountExceeded() {
        return currentAttemptsCount >= maxAttemptsCount;
    }

    @Override
    public boolean lastAttempt(){
        return (currentAttemptsCount >= (maxAttemptsCount-1)) ||
                (attemptsWithRuntimeErrorsCount >= (maxAttemptsWithRuntimeErrorsCount-1));
    }

    @Override
    public boolean isRetriedTask() {
        return currentAttemptsCount > 1;
    }

    @Override
    public boolean canBeRetried(){
        return (!runtimeErrorsExceeded()) && (!attemptsCountExceeded());
    }

}
