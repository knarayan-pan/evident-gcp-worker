package com.paloaltonetworks.evident.messaging.handler;

import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.paloaltonetworks.evident.data_access.models.ResourceMetadata;
import com.paloaltonetworks.evident.data_access.service.MonitoredResourceService;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.gcp.models.request.DeleteResourcesRequest;
import com.paloaltonetworks.evident.messaging.task.ResourceDedupTaskMessage;
import com.paloaltonetworks.evident.redis.MonitoredRedisClient;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component(ResourceDedupTaskMessage.TASK_TYPE)
public class ResourceDedupTaskHandler implements TaskHandler<ResourceDedupTaskMessage> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final QueueService queueService;
    private final ResourceService resourceService;
    private final RedisClient redisClient;

    @Autowired
    public ResourceDedupTaskHandler(QueueService queueService,
                                    @Qualifier(MonitoredResourceService.NAME) ResourceService resourceService,
                                    @Qualifier(MonitoredRedisClient.NAME) RedisClient redisClient) {
        this.queueService = queueService;
        this.resourceService = resourceService;
        this.redisClient = redisClient;
    }

    @Override
    public void execute(ResourceDedupTaskMessage taskMessage) {
        // get all resources of this type from db for this account.
        // check if key exists in redis in that path.
        // if it does - then delete key in redis and do nothing.
        // if it does not - then delete key in redis and queue delete resource task
        Set<String> resourcesToDelete = new HashSet<>();
        resourceService.findResourcesByType(taskMessage.getTenantName(), taskMessage.getExternalAccountId(), taskMessage.getResourceType())
                .filter(Predicates.notNull())
                // filter out resources only for this project
                .filter(resource -> {
                    ResourceMetadata resourceMetadata = resource.getResourceMetadata(ResourceMetadata.class);
                    return resourceMetadata != null && taskMessage.getProjectId().equalsIgnoreCase(resourceMetadata.getProjectId());
                })
                .forEach(resource -> redisClient.getKey(resource.getResourceKey().redisKeyString()).map(value -> {
                    LOGGER.debug("Reconciliation Dedup phase: Resource: {} exists both in redis and db.", resource.getResourceId());
                    return value;
                }).orElseGet(() -> {
                    resourcesToDelete.add(resource.getResourceId());
                    return null;
                }));

        LOGGER.info("Reconciliation dedup phase: Queuing tasks for {} resources of type {} to end alerts for project: {}, accountId: {}",
                resourcesToDelete.size(),
                taskMessage.getResourceType(),
                taskMessage.getProjectId(), taskMessage.getExternalAccountId());
        Iterable<List<String>> partition = Iterables.partition(resourcesToDelete, 20);
        for (List<String> set : partition) {
            // queue resource delete task
            queueService.queueBatchedResourceDelete(DeleteResourcesRequest.builder()
                    .resourceIds(new HashSet<>(set))
                    .resourceType(taskMessage.getResourceType())
                    .externalAccountId(taskMessage.getExternalAccountId())
                    .tenant(taskMessage.getTenantName())
                    .projectId(taskMessage.getProjectId())
                    .organizationId(taskMessage.getOrganizationId())
                    .build());

        }

    }
}
