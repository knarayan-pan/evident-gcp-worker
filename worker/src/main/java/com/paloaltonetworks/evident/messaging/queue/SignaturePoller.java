package com.paloaltonetworks.evident.messaging.queue;

import com.codahale.metrics.MetricRegistry;
import com.paloaltonetworks.aperture.ApertureSqsAsyncClient;
import com.paloaltonetworks.evident.config.PropertyHandler;
import com.paloaltonetworks.evident.config.ResourceWorkerQueues;
import com.paloaltonetworks.evident.messaging.handler.TaskHandler;
import com.paloaltonetworks.evident.service.QueueService;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class SignaturePoller extends MessagePoller {
    private final int signatureThreadCount;

    protected SignaturePoller(ApertureSqsAsyncClient sqsClient, Map<String, TaskHandler> taskHandlerMap, MetricRegistry metricRegistry, PropertyHandler propertyHandler, QueueService queueService, int signatureThreadCount) {
        super(sqsClient, metricRegistry, taskHandlerMap, propertyHandler, queueService);
        this.signatureThreadCount = signatureThreadCount;
    }

    @Override
    public String getQueueName() {
        return ResourceWorkerQueues.SIGNATURE.getQueueName();
    }

    @Override
    public int getThreadCount() {
        return signatureThreadCount;
    }
}
