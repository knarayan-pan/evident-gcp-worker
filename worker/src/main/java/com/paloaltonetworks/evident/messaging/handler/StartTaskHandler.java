package com.paloaltonetworks.evident.messaging.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.api.services.cloudresourcemanager.model.ListProjectsResponse;
import com.google.api.services.cloudresourcemanager.model.Project;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.AccountMetadata;
import com.paloaltonetworks.evident.data_access.models.AccountStatus;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ScanStatus;
import com.paloaltonetworks.evident.data_access.service.AccountStatusService;
import com.paloaltonetworks.evident.data_access.service.MonitoredAccountStatusService;
import com.paloaltonetworks.evident.data_access.service.MonitoredResourceService;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.esp.MonitoredESPClient;
import com.paloaltonetworks.evident.esp.model.ESPApiResponse;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.gcp.models.request.FetchOrganizationIdRequest;
import com.paloaltonetworks.evident.gcp.models.request.FetchProjectsRequest;
import com.paloaltonetworks.evident.gcp.models.request.GcpActivityLogRequest;
import com.paloaltonetworks.evident.gcp.models.request.ProjectTreeRefreshRequest;
import com.paloaltonetworks.evident.messaging.task.StartTaskMessage;
import com.paloaltonetworks.evident.service.QueueService;
import com.paloaltonetworks.evident.utils.JsonUtils;
import lombok.SneakyThrows;
import lombok.val;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component(StartTaskMessage.TASK_TYPE)
public class StartTaskHandler implements TaskHandler<StartTaskMessage> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final QueueService queueService;
    private final ESPClient espClient;
    private final GCPApiService gcpApiService;
    private final ResourceService resourceService;
    private final AccountStatusService accountStatusService;

    // todo change NoOpESPClient qualifer once ESP delivers the api
    @Autowired
    public StartTaskHandler(QueueService queueService, @Qualifier(MonitoredESPClient.NAME) ESPClient espClient,
                            GCPApiService gcpApiService,
                            @Qualifier(MonitoredResourceService.NAME) ResourceService resourceService,
                            @Qualifier(MonitoredAccountStatusService.NAME) AccountStatusService accountStatusService) {
        this.queueService = queueService;
        this.espClient = espClient;
        this.gcpApiService = gcpApiService;
        this.resourceService = resourceService;
        this.accountStatusService = accountStatusService;
    }

    @SneakyThrows(value = {IOException.class})
    @Override
    public void execute(StartTaskMessage startTaskMessage) {
        // check if the account has been onboarded. If not - the queue tasks to onboard different resource types and also queue events task
        // if already onboarded - then check for the next event interval
        AccountStatus accountStatus = accountStatusService.getStatus(startTaskMessage.getTenantName(), startTaskMessage.getExternalAccountId())
                .orElseGet(() -> {
                    AccountStatus onboardingState = AccountStatus.builder()
                            .dateCreated(new Date())
                            .externalAccountId(startTaskMessage.getExternalAccountId())
                            .scanStatus(ScanStatus.not_started)
                            .cloudAppType(CloudAppType.GCP)
                            .build();
                    upsertStatus(startTaskMessage.getTenantName(), startTaskMessage.getExternalAccountId(), onboardingState);
                    return onboardingState;
                });


        if (accountStatus.getOnboardingDate() == null || Boolean.TRUE.equals(startTaskMessage.getReconcile())) {
            onboardAccount(accountStatus, startTaskMessage);
        } else {
            // the iterative scan will ensure that the org id is available
            iterativeScan(accountStatus, startTaskMessage);
        }
    }

    @VisibleForTesting
    void iterativeScan(AccountStatus accountStatus, StartTaskMessage taskMessage) {
        Date lastEventScanTime = accountStatus.getLastEventScanTime() == null ? accountStatus.getOnboardingDate() :
                accountStatus.getLastEventScanTime();
        Date currentDate = new Date();
        boolean isFirst = true;
        Stream<Resource> projects = resourceService.findResourcesByType(taskMessage.getTenantName(), taskMessage.getExternalAccountId(), ResourceType.GCP_PROJECT.getResourceType());
        Set<String> projectIds = projects
                .map(resource -> resource != null && resource.getResourceId() != null ? resource.getResourceId() : null)
                .filter(Predicates.notNull())
                .collect(Collectors.toSet());

        if (CollectionUtils.isEmpty(projectIds)) {
            LOGGER.error("No projects found for this account: {}. Possibly an error situation where onboarding did not update all projects.");
            return;
        }
        // really awkward flow here - google does not handle partial failures well here if one of the project is
        // removed/changed permissions on - the entire batch fails!
        for (String projectId : projectIds) {
            queueService.queueAuditLogTask(GcpActivityLogRequest.builder()
                    .tenant(taskMessage.getTenantName())
                    .externalAccountId(taskMessage.getExternalAccountId())
                    .projectIds(ImmutableSet.of(projectId))
                    // need to only add organization id in one request
                    .organizationId(isFirst ? accountStatus.getAccountMetadata().map(AccountMetadata::getOrgId).orElse(null) : null)
                    .fromDate(lastEventScanTime)
                    .toDate(currentDate)
                    .build());
            isFirst = false;
        }

        // update timestamp
        accountStatusService.updateLastScanEventTime(taskMessage.getTenantName(), taskMessage.getExternalAccountId(), currentDate);
    }

    @VisibleForTesting
    void onboardAccount(AccountStatus accountStatus, StartTaskMessage taskMessage) throws IOException {
        if (!Boolean.TRUE.equals(taskMessage.getReconcile())) {
            // fetch and set org id
            accountStatus = fetchAndSetOrgId(accountStatus, taskMessage);
        }
        String orgId = accountStatus.getAccountMetadata().map(new Function<AccountMetadata, String>() {
            @Nullable
            @Override
            public String apply(@Nullable AccountMetadata input) {
                return input == null ? null : input.getOrgId();
            }
        }).orElse(null);
        // get list of project currently available
        String token = null;
        ListProjectsResponse projects;
        do {
            projects = gcpApiService.getProjects(FetchProjectsRequest.builder()
                    .externalAccountId(taskMessage.getExternalAccountId())
                    .tenant(taskMessage.getTenantName())
                    .build(), token);
            token = projects.getNextPageToken();
            if (CollectionUtils.isNotEmpty(projects.getProjects())) {
                // queue tasks for gcp project level resource for onboarding tasks
                projects.getProjects().stream().filter((Predicate<Project>) input -> input != null && StringUtils.equalsIgnoreCase("ACTIVE", input.getLifecycleState()))
                        .forEach(project -> queueService.queueProjectRefresh(ProjectTreeRefreshRequest.builder()
                                .tenant(taskMessage.getTenantName())
                                .resourceName(project.getName())
                                .organizationId(orgId)
                                .externalAccountId(taskMessage.getExternalAccountId())
                                .resourceId(project.getProjectId())
                                .resourceType(ResourceType.GCP_PROJECT.getResourceType())
                                .reconcile(taskMessage.getReconcile())
                                .build()));

            }

        } while (StringUtils.isNotEmpty(projects.getNextPageToken()));
        if(!Boolean.TRUE.equals(taskMessage.getReconcile())) {
            // update status to scanning
            upsertStatus(taskMessage.getTenantName(), taskMessage.getExternalAccountId(), accountStatus.toBuilder()
                    .scanStatus(ScanStatus.scanning)
                    .onboardingDate(new Date())
                    .lastEventScanTime(new Date())
                    .build());
        }
    }

    @VisibleForTesting
    AccountStatus fetchAndSetOrgId(AccountStatus accountStatus, StartTaskMessage taskMessage) throws IOException {
        ESPApiResponse espApiResponse = espClient.gcpAccount(taskMessage.getExternalAccountId());
        val domain = Objects.toString(espApiResponse.getData().getAttributes().get(ESPClient.DOMAIN),null);
        if (StringUtils.isEmpty(domain)) {
            LOGGER.error("No admin email found for gcp account with ID: {}. Cannot process any onboarding tasks",
                    taskMessage.getExternalAccountId());
            return accountStatus;
        }

        return gcpApiService.organizationId(FetchOrganizationIdRequest.builder()
                        .externalAccountId(taskMessage.getExternalAccountId())
                        .tenant(taskMessage.getTenantName())
                .build(), domain).map(orgId -> {
            accountStatus.setMetadata(JsonUtils.encode(AccountMetadata.builder().orgId(orgId).build()));
            upsertStatus(taskMessage.getTenantName(), taskMessage.getExternalAccountId(), accountStatus);
            return accountStatus;
        }).orElse(accountStatus);
    }


    private void upsertStatus(String tenant, String externalAccountId, AccountStatus status) {
        try {
            accountStatusService.upsert(tenant, externalAccountId, status);
        } catch (JsonProcessingException e) {
            LOGGER.error("Error updating account status for account: {}", externalAccountId);
            throw new RuntimeException("Error updating account status for account: " + externalAccountId);
        }
    }
}
