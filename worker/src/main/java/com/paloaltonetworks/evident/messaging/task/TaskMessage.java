package com.paloaltonetworks.evident.messaging.task;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import static com.paloaltonetworks.evident.messaging.task.TaskMessage.TASK_TYPE_FIELD;

/**
 * Task message interface.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = TASK_TYPE_FIELD,
        visible = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public interface TaskMessage {
    String TASK_TYPE_FIELD = "task_type";

    @JsonProperty(TASK_TYPE_FIELD)
    String getTaskType();

    String getTenantName();

    String getExternalAccountId();

    void incrementAttemptsCount();

    void incrementAttemptsWithRunTimeErrorCount();

    boolean runtimeErrorsExceeded();

    boolean attemptsCountExceeded();

    boolean lastAttempt();

    boolean isRetriedTask();

    boolean canBeRetried();

    int currentAttempt();
}
