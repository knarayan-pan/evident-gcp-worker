package com.paloaltonetworks.evident.messaging.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.Set;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@JsonTypeName(AuditLogMessage.TASK_TYPE)
public class AuditLogMessage extends BaseTaskMessage {
    public static final String TASK_TYPE = "fetch_audit_log";

    private static final String FROM_DATE = "from";
    private static final String TO_DATE = "to";
    private static final String PAGE_TOKEN = "pageToken";
    private static final String PROJECT_IDS = "projectIds";
    private static final String ORGANIZATION_ID = "organizationId";

    @JsonProperty(FROM_DATE)
    private final Date fromDate;
    @JsonProperty(TO_DATE)
    private final Date toDate;
    @JsonProperty(PAGE_TOKEN)
    private final String pageToken;
    @JsonProperty(PROJECT_IDS)
    private final Set<String> projectIds;
    @JsonProperty(ORGANIZATION_ID)
    private final String organizationId;

    @Builder
    @JsonCreator
    public AuditLogMessage(@NonNull @JsonProperty(TENANT_NAME_FIELD) String tenantName,
                           @NonNull @JsonProperty(EXTERNAL_ID) String externalId,
                           @NonNull @JsonProperty(FROM_DATE) Date fromDate,
                           @NonNull @JsonProperty(TO_DATE) Date toDate,
                           @JsonProperty(PAGE_TOKEN) String pageToken,
                           @Nonnull @JsonProperty(PROJECT_IDS) Set<String> projectIds,
                           @JsonProperty(ORGANIZATION_ID) String organizationId) {
        super(tenantName, externalId);
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.pageToken = pageToken;
        this.projectIds = projectIds;
        this.organizationId = organizationId;
    }

    @Override
    public String getTaskType() {
        return TASK_TYPE;
    }
}
