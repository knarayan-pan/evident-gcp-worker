package com.paloaltonetworks.evident.messaging.handler;

import com.paloaltonetworks.evident.config.GCPConfig;
import com.paloaltonetworks.evident.enrichment.GCPEnrichment;
import com.paloaltonetworks.evident.enrichment.request.ResourceEnrichmentRequest;
import com.paloaltonetworks.evident.messaging.task.ResourceDeleteTaskMessage;
import com.paloaltonetworks.evident.messaging.task.ResourceRefreshTaskMessage;
import com.paloaltonetworks.evident.messaging.task.TaskMessage;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;

@Component(ResourceRefreshTaskMessage.TASK_TYPE)
public class ResourceRefreshTaskHandler implements TaskHandler<ResourceRefreshTaskMessage> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final GCPConfig gcpConfig;


    @Autowired
    public ResourceRefreshTaskHandler(GCPConfig gcpConfig) {
        this.gcpConfig = gcpConfig;
    }

    @Override
    public void execute(ResourceRefreshTaskMessage resourceRefreshTaskMessage) {
        // check if resource id is there - if not get resources from cloud and queue tasks for each resource.
        GCPEnrichment enrichmentClass = gcpConfig.getEnrichmentClass(resourceRefreshTaskMessage.getResourceType());
        ResourceEnrichmentRequest request = ResourceEnrichmentRequest.builder()
                .externalAccountId(resourceRefreshTaskMessage.getExternalAccountId())
                .tenant(resourceRefreshTaskMessage.getTenantName())
                .zone(resourceRefreshTaskMessage.getZone())
                .resourceName(resourceRefreshTaskMessage.getResourceName())
                .orgId(resourceRefreshTaskMessage.getOrganizationId())
                .projectId(resourceRefreshTaskMessage.getProjectId())
                .resourceId(resourceRefreshTaskMessage.getResourceId())
                .resourceType(resourceRefreshTaskMessage.getResourceType())
                .pageSize(resourceRefreshTaskMessage.getPageSize())
                .pageToken(resourceRefreshTaskMessage.getPageToken())
                .retriedTask(resourceRefreshTaskMessage.isRetriedTask())
                .reconcile(resourceRefreshTaskMessage.getReconcile())
                .build();
        if (StringUtils.isEmpty(resourceRefreshTaskMessage.getResourceId())) {
            enrichmentClass.applyForCollection(request);
        } else {
            enrichmentClass.apply(request);
        }
    }
}
