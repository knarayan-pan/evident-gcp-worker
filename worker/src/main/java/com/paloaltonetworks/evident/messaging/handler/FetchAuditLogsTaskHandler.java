package com.paloaltonetworks.evident.messaging.handler;

import com.paloaltonetworks.evident.exceptions.GCPApiException;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.api.NonRetryableGCPException;
import com.paloaltonetworks.evident.gcp.models.AuditLogPage;
import com.paloaltonetworks.evident.gcp.models.request.GcpActivityLogRequest;
import com.paloaltonetworks.evident.messaging.task.AuditLogMessage;
import com.paloaltonetworks.evident.service.GCPAuditLogService;
import com.paloaltonetworks.evident.service.QueueService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;

@Component(AuditLogMessage.TASK_TYPE)
public class FetchAuditLogsTaskHandler implements TaskHandler<AuditLogMessage> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final GCPAuditLogService auditLogService;
    private final GCPApiService apiService;
    private final QueueService queueService;

    @Autowired
    public FetchAuditLogsTaskHandler(GCPAuditLogService auditLogService,
                                     GCPApiService apiService,
                                     QueueService queueService) {
        this.auditLogService = auditLogService;
        this.apiService = apiService;
        this.queueService = queueService;
    }

    @Override
    public void execute(AuditLogMessage auditLogMessage) {

        GcpActivityLogRequest request = GcpActivityLogRequest.builder()
                .tenant(auditLogMessage.getTenantName())
                .externalAccountId(auditLogMessage.getExternalAccountId())
                .fromDate(auditLogMessage.getFromDate())
                .toDate(auditLogMessage.getToDate())
                .pageToken(auditLogMessage.getPageToken())
                .projectIds(auditLogMessage.getProjectIds())
                .organizationId(auditLogMessage.getOrganizationId())
                .build();
        try {
            AuditLogPage auditLogs = apiService.getAuditLogs(request);
            if (StringUtils.isNotEmpty(auditLogs.getNextPageToken())) {
                if (!auditLogMessage.isRetriedTask()) {
                    queueService.queueAuditLogTask(request.toBuilder().pageToken(auditLogs.getNextPageToken()).build());
                }
            }
            if (CollectionUtils.isNotEmpty(auditLogs.getEntries())) {
                // process current page
                auditLogService.processAuditLogPage(request, auditLogs.getEntries());
            }
        } catch (GCPApiException e) {
            if (HttpStatus.FORBIDDEN.value() == e.getStatus() || HttpStatus.NOT_FOUND.value() == e.getStatus()) {
                LOGGER.info("Received status: {} for projectId: {} for accountId: {}. " +
                                "Either project was removed or access to this project was revoked. Cleaning up resources...",
                        e.getStatus(), request.getProjectIds(), request.getExternalAccountId());
                // remove project from stored list and queue tasks to clean up resources for this project per resource type.
                auditLogService.cleanUpProjectAndResources(request);
            } else {
                // for all other error types - we must move on.
                throw new NonRetryableGCPException(e);
            }
        }
    }
}
