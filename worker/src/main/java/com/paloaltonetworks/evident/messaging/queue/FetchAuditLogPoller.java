package com.paloaltonetworks.evident.messaging.queue;

import com.codahale.metrics.MetricRegistry;
import com.paloaltonetworks.aperture.ApertureSqsAsyncClient;
import com.paloaltonetworks.evident.config.PropertyHandler;
import com.paloaltonetworks.evident.config.ResourceWorkerQueues;
import com.paloaltonetworks.evident.messaging.handler.TaskHandler;
import com.paloaltonetworks.evident.service.QueueService;
import com.paloaltonetworks.evident.sqs_client.MonitoredSqsAsyncClient;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class FetchAuditLogPoller extends MessagePoller {
    private final int eventsThreadCount;

    protected FetchAuditLogPoller(@Qualifier(MonitoredSqsAsyncClient.NAME) ApertureSqsAsyncClient sqsClient,
                                  Map<String, TaskHandler> taskHandlerMap,
                                  MetricRegistry metricRegistry, PropertyHandler propertyHandler,
                                  QueueService queueService, int eventsThreadCount) {
        super(sqsClient, metricRegistry, taskHandlerMap, propertyHandler, queueService);
        this.eventsThreadCount = eventsThreadCount;
    }

    @Override
    public String getQueueName() {
        return ResourceWorkerQueues.AUDIT_LOG.getQueueName();
    }

    @Override
    public int getThreadCount() {
        return eventsThreadCount;
    }
}
