package com.paloaltonetworks.evident.messaging.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@JsonTypeName(SignatureTaskMessage.TASK_TYPE)
public class SignatureTaskMessage extends BaseTaskMessage {
    public static final String TASK_TYPE = "signature";
    private static final String RESOURCE_ID = "resource_id";
    private static final String ORGANIZATION_ID = "organization_id";
    private static final String PROJECT_ID = "project_id";
    // use this only if a re-evaluation of a signature is required - e.g. enable flow from Full stack
    private static final String SIGNATURE_ID = "signature_id";
    private static final String RESOURCE_TYPE = "resource_type";

    @JsonProperty(RESOURCE_ID)
    private final String resourceId;
    @JsonProperty(PROJECT_ID)
    private final String projectId;
    @JsonProperty(ORGANIZATION_ID)
    private final String organizationId;
    @JsonProperty(RESOURCE_TYPE)
    private final String resourceType;
    @JsonProperty(SIGNATURE_ID)
    private final String signatureId;

    @Builder
    @JsonCreator
    public SignatureTaskMessage(@Nonnull @JsonProperty(TENANT_NAME_FIELD) String tenantName,
                                @Nonnull @JsonProperty(EXTERNAL_ID) String externalId,
                                @JsonProperty(RESOURCE_ID) String resourceId,
                                @JsonProperty(SIGNATURE_ID) String signatureId,
                                @JsonProperty(PROJECT_ID) String projectId,
                                @JsonProperty(ORGANIZATION_ID) String organizationId,
                                @JsonProperty(RESOURCE_TYPE) String resourceType) {
        super(tenantName, externalId);
        this.resourceId = resourceId;
        this.projectId = projectId;
        this.organizationId = organizationId;
        this.resourceType = resourceType;
        this.signatureId = signatureId;
    }

    @Override
    public String getTaskType() {
        return TASK_TYPE;
    }

    @JsonIgnore
    // check if we need to re-eval all signatures for a particular resource type
    public boolean isResourceTypeTrigger() {
        return StringUtils.isEmpty(resourceId) && StringUtils.isEmpty(signatureId) && StringUtils.isNotEmpty(resourceType);
    }

    @JsonIgnore
    // check if we need to re-eval a specific signature
    public boolean isSignatureSpecificTrigger() {
        return StringUtils.isEmpty(resourceId) && StringUtils.isNotEmpty(signatureId) && StringUtils.isEmpty(resourceType);
    }

    @JsonIgnore
    // check if we need to evaluate a specific resource of a particular type
    public boolean isResourceSpecificTrigger() {
        return StringUtils.isNotEmpty(resourceId) && StringUtils.isEmpty(signatureId) && StringUtils.isNotEmpty(resourceType);
    }
}
