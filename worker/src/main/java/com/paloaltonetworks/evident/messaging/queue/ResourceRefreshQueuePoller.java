package com.paloaltonetworks.evident.messaging.queue;

import com.codahale.metrics.MetricRegistry;
import com.paloaltonetworks.aperture.ApertureSqsAsyncClient;
import com.paloaltonetworks.evident.config.PropertyHandler;
import com.paloaltonetworks.evident.config.ResourceWorkerQueues;
import com.paloaltonetworks.evident.messaging.handler.TaskHandler;
import com.paloaltonetworks.evident.service.QueueService;
import com.paloaltonetworks.evident.sqs_client.MonitoredSqsAsyncClient;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ResourceRefreshQueuePoller extends MessagePoller {
    private final int resourceRefreshThreadCount;

    protected ResourceRefreshQueuePoller(@Qualifier(MonitoredSqsAsyncClient.NAME) ApertureSqsAsyncClient sqsClient,
                                         Map<String, TaskHandler> taskHandlerMap,
                                         MetricRegistry metricRegistry, PropertyHandler propertyHandler,
                                         int resourceRefreshThreadCount, QueueService queueService) {
        super(sqsClient, metricRegistry, taskHandlerMap, propertyHandler, queueService);
        this.resourceRefreshThreadCount = resourceRefreshThreadCount;
    }

    @Override
    public String getQueueName() {
        return ResourceWorkerQueues.FETCH_RESOURCES.getQueueName();
    }

    @Override
    public int getThreadCount() {
        return resourceRefreshThreadCount;
    }
}
