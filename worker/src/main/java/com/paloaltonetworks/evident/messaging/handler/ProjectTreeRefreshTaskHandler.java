package com.paloaltonetworks.evident.messaging.handler;

import com.google.common.collect.Sets;
import com.paloaltonetworks.aperture.consul.ConsulService;
import com.paloaltonetworks.evident.config.GCPConfig;
import com.paloaltonetworks.evident.enrichment.request.ProjectEnrichmentRequest;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.messaging.task.ProjectRefreshTaskMessage;
import com.paloaltonetworks.evident.messaging.task.TaskMessage;
import com.paloaltonetworks.evident.service.QueueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.Set;
import java.util.stream.Stream;

import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCP_PROJECT;

@Component(ProjectRefreshTaskMessage.TASK_TYPE)
public class ProjectTreeRefreshTaskHandler implements TaskHandler<ProjectRefreshTaskMessage> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static final String KEY_DISABLED_ENRICHMENTS = "/v1/worker/gcp/enrichments/DISABLED_LIST";
    private static final String DELIMITER_DISABLED_LIST = "\\,";
    private final QueueService queueService;
    private final ConsulService consulService;
    private final GCPConfig gcpConfig;

    @Autowired
    ProjectTreeRefreshTaskHandler(QueueService queueService, GCPConfig gcpConfig,
                                  ConsulService consulService) {
        this.queueService = queueService;
        this.gcpConfig = gcpConfig;
        this.consulService = consulService;
    }


    @Override
    public void execute(ProjectRefreshTaskMessage refreshTaskMessage) {
        queueTasksForResourceTypes(refreshTaskMessage);
        // apply enrichment and persist if required 
        gcpConfig.getEnrichmentClass(refreshTaskMessage.getResourceType()).<ProjectEnrichmentRequest>apply(ProjectEnrichmentRequest.builder()
                .orgId(refreshTaskMessage.getOrganizationId())
                .resourceId(refreshTaskMessage.getResourceId())
                .projectName(refreshTaskMessage.getResourceName())
                .externalAccountId(refreshTaskMessage.getExternalAccountId())
                .tenant(refreshTaskMessage.getTenantName())
                .reconcile(refreshTaskMessage.getReconcile())
                .build());
    }

    private void queueTasksForResourceTypes(ProjectRefreshTaskMessage refreshTaskMessage) {
        if (refreshTaskMessage.isRetriedTask()) {
            return;
        }

        Set<String> disabledEnrichments = Sets.newHashSet(
                consulService.getValueForTenant(refreshTaskMessage.getTenantName(), KEY_DISABLED_ENRICHMENTS, "")
                        .split(DELIMITER_DISABLED_LIST));
        Stream.of(ResourceType.values())
                .filter(resourceType -> !resourceType.equals(GCP_PROJECT) &&
                        !disabledEnrichments.contains(resourceType.getResourceType()))
                .forEach(resourceType -> queueService.queueResourceRefresh(ResourceRefreshRequest.builder()
                        .externalAccountId(refreshTaskMessage.getExternalAccountId())
                        .tenant(refreshTaskMessage.getTenantName())
                        .projectId(refreshTaskMessage.getResourceId())
                        .resourceType(resourceType.getResourceType())
                        .organizationId(refreshTaskMessage.getOrganizationId())
                        .reconcile(refreshTaskMessage.getReconcile())
                        .build()));
        LOGGER.info("Successfully queued {} resource types for refresh for project: {}", ResourceType.values().length - 1, refreshTaskMessage.getResourceId());
    }
}
