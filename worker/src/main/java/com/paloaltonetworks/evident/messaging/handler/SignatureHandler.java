package com.paloaltonetworks.evident.messaging.handler;

import com.paloaltonetworks.evident.config.GCPConfig;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.data_access.service.MonitoredResourceService;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.gcp.models.request.SignatureRequest;
import com.paloaltonetworks.evident.messaging.task.SignatureTaskMessage;
import com.paloaltonetworks.evident.messaging.task.TaskMessage;
import com.paloaltonetworks.evident.service.QueueService;
import com.paloaltonetworks.evident.signatures.GCPSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

@Component(SignatureTaskMessage.TASK_TYPE)
public class SignatureHandler implements TaskHandler<SignatureTaskMessage> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final GCPConfig gcpConfig;
    private final ResourceService resourceService;
    private final QueueService queueService;

    @Autowired
    public SignatureHandler(GCPConfig gcpConfig, @Qualifier(MonitoredResourceService.NAME) ResourceService resourceService, QueueService queueService) {
        this.gcpConfig = gcpConfig;
        this.resourceService = resourceService;
        this.queueService = queueService;
    }

    @Override
    public void execute(SignatureTaskMessage signatureTaskMessage) {

        if (signatureTaskMessage.isResourceSpecificTrigger()) {
            resourceService.findOne(signatureTaskMessage.getTenantName(), ResourceKey.builder()
                    .resourceIdHash(ResourceKey.computeResourceIdHash(signatureTaskMessage.getResourceId()))
                    .resourceType(signatureTaskMessage.getResourceType())
                    .externalAccountId(signatureTaskMessage.getExternalAccountId())
                    .build()).map(resource -> {
                Set<GCPSignature> signatureClass = gcpConfig.getSignatureClass(signatureTaskMessage.getResourceType(),
                        signatureTaskMessage.getTenantName());
                signatureClass.forEach(gcpSignature -> gcpSignature.check(resource));
                LOGGER.info("Evaluated {} signatures for resource: {}, resourceType: {}", signatureClass.size(), resource.getResourceId(),
                        resource.getResourceKey().getResourceType());
                return signatureClass.size();
            }).orElseGet(() -> {
                LOGGER.error("Invalid state: Possibly no resource found in db for type: {}, resourceId: {} for accountId: {}. Needs a retry...",
                        signatureTaskMessage.getResourceType(), signatureTaskMessage.getResourceId(), signatureTaskMessage.getExternalAccountId());
                throw new RuntimeException("invalid resource state for signature exception..");
            });
        } else if (signatureTaskMessage.isSignatureSpecificTrigger()) {
            // get resource type from signature.
            Set<ResourceType> signatureResourceTypes = gcpConfig.getSignatureResourceTypes(signatureTaskMessage.getSignatureId());
            signatureResourceTypes.forEach(resourceType -> resourceService.findResourcesByType(signatureTaskMessage.getTenantName(),
                    signatureTaskMessage.getExternalAccountId(),
                    resourceType.getResourceType()).forEach(resource -> gcpConfig.getSignatureById(signatureTaskMessage.getSignatureId())
                    .map(gcpSignature -> {
                        gcpSignature.check(resource);
                        return gcpSignature;
                    }).orElseGet(() -> {
                        LOGGER.warn("Invalid sigId: {} in signature re-eval payload. Ignoring this task...", signatureTaskMessage.getSignatureId());
                        return null;
                    })));
        } else if (signatureTaskMessage.isResourceTypeTrigger()) {
            // only to be used when a signature is re-enabled - an sqs message can be directly put on the queue here to re-process signatures of a particular resource type
            Stream<Resource> resourcesByType = resourceService.findResourcesByType(signatureTaskMessage.getTenantName(),
                    signatureTaskMessage.getExternalAccountId(), signatureTaskMessage.getResourceType());
            resourcesByType.forEach(resource -> {
                // queue signature task for each resource type
                queueService.queueSignatureForResource(SignatureRequest.builder()
                        .resourceType(signatureTaskMessage.getResourceType())
                        .resourceId(resource.getResourceId())
                        .tenant(signatureTaskMessage.getTenantName())
                        .externalAccountId(signatureTaskMessage.getExternalAccountId())
                        .build());
            });
        } else {
            LOGGER.warn("Invalid signature task message provided. Details: {}", signatureTaskMessage);
        }
    }
}
