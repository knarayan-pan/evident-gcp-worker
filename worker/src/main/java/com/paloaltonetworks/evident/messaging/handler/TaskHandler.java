package com.paloaltonetworks.evident.messaging.handler;

import com.paloaltonetworks.evident.messaging.task.TaskMessage;

/**
 * Task handler interface.
 */
public interface TaskHandler<T extends TaskMessage> {
    void execute(T taskMessage);
}
