package com.paloaltonetworks.evident.messaging.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.Nonnull;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@JsonTypeName(ResourceDedupTaskMessage.TASK_TYPE)
public class ResourceDedupTaskMessage extends BaseTaskMessage {

    public static final String TASK_TYPE = "resource_dedup";
    private static final String RESOURCE_TYPE = "resource_type";
    private static final String ORGANIZATION_ID = "organization_id";
    private static final String PROJECT_ID = "project_id";

    @JsonProperty(RESOURCE_TYPE)
    private final String resourceType;
    @JsonProperty(PROJECT_ID)
    private final String projectId;
    @JsonProperty(ORGANIZATION_ID)
    private final String organizationId;

    @Builder
    @JsonCreator
    public ResourceDedupTaskMessage(@Nonnull @JsonProperty(TENANT_NAME_FIELD) String tenantName,
                                    @Nonnull @JsonProperty(EXTERNAL_ID) String externalId,
                                    @Nonnull @JsonProperty(RESOURCE_TYPE) String resourceType,
                                    @Nonnull @JsonProperty(PROJECT_ID) String projectId,
                                    @JsonProperty(ORGANIZATION_ID) String organizationId) {
        super(tenantName, externalId);
        this.resourceType = resourceType;
        this.organizationId = organizationId;
        this.projectId = projectId;
    }

    @Override
    public String getTaskType() {
        return TASK_TYPE;
    }
}
