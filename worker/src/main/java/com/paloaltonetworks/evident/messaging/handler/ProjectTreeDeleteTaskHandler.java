package com.paloaltonetworks.evident.messaging.handler;

import com.google.common.collect.Iterables;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceMetadata;
import com.paloaltonetworks.evident.data_access.service.MonitoredResourceService;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.gcp.models.request.DeleteResourcesRequest;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.messaging.task.ProjectTreeDeleteTaskMessage;
import com.paloaltonetworks.evident.service.QueueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Component(ProjectTreeDeleteTaskMessage.TASK_TYPE)
public class ProjectTreeDeleteTaskHandler implements TaskHandler<ProjectTreeDeleteTaskMessage> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final QueueService queueService;
    private final ResourceService resourceService;

    @Autowired
    public ProjectTreeDeleteTaskHandler(QueueService queueService,
                                        @Qualifier(MonitoredResourceService.NAME) ResourceService resourceService) {
        this.queueService = queueService;
        this.resourceService = resourceService;
    }

    @Override
    public void execute(ProjectTreeDeleteTaskMessage taskMessage) {
        Set<String> resourceIds = new HashSet<>();
        // received a task message to remove resources that match a projectId and resource type.
        resourceService.findResourcesByType(taskMessage.getTenantName(), taskMessage.getExternalAccountId(),
                taskMessage.getResourceType()).forEach(resource -> {
            ResourceMetadata resourceMetadata = resource.getResourceMetadata(ResourceMetadata.class);
            if (resourceMetadata.getProjectId().equalsIgnoreCase(taskMessage.getProjectId())) {
                resourceIds.add(resource.getResourceId());
            }
        });
        LOGGER.info("Queue task for {} resources to end alerts for project: {}, accountId: {}", resourceIds.size(),
                taskMessage.getProjectId(), taskMessage.getExternalAccountId());

        // batch deletes
        Iterable<List<String>> partition = Iterables.partition(resourceIds, 20);
        for (List<String> set : partition) {
            // queue task to delete batch of resources
            queueService.queueBatchedResourceDelete(DeleteResourcesRequest.builder()
                    .resourceIds(new HashSet<>(set))
                    .resourceType(taskMessage.getResourceType())
                    .externalAccountId(taskMessage.getExternalAccountId())
                    .tenant(taskMessage.getTenantName())
                    .projectId(taskMessage.getProjectId())
                    .build());
        }
    }

}
