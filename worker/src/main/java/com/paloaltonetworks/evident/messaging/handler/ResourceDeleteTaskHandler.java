package com.paloaltonetworks.evident.messaging.handler;

import com.paloaltonetworks.evident.config.GCPConfig;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.data_access.service.MonitoredResourceService;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.esp.MonitoredESPClient;
import com.paloaltonetworks.evident.esp.model.AlertAttributes;
import com.paloaltonetworks.evident.esp.model.AlertData;
import com.paloaltonetworks.evident.esp.model.AlertRequest;
import com.paloaltonetworks.evident.messaging.task.ResourceDeleteTaskMessage;
import com.paloaltonetworks.evident.redis.MonitoredRedisClient;
import com.paloaltonetworks.evident.redis.RedisClient;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;

@Component(ResourceDeleteTaskMessage.TASK_TYPE)
public class ResourceDeleteTaskHandler implements TaskHandler<ResourceDeleteTaskMessage> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final ResourceService resourceService;
    private final GCPConfig gcpConfig;
    private final ESPClient espClient;
    private final RedisClient redisClient;

    @Autowired
    public ResourceDeleteTaskHandler(@Qualifier(MonitoredResourceService.NAME) ResourceService resourceService,
                                     GCPConfig gcpConfig, @Qualifier(MonitoredESPClient.NAME) ESPClient espClient,
                                     @Qualifier(MonitoredRedisClient.NAME) RedisClient redisClient) {
        this.resourceService = resourceService;
        this.gcpConfig = gcpConfig;
        this.espClient = espClient;
        this.redisClient = redisClient;
    }

    @Override
    public void execute(ResourceDeleteTaskMessage resourceDeleteTaskMessage) {
        resourceDeleteTaskMessage.getResourceIds().forEach(resourceId -> endAlertForResource(resourceId, resourceDeleteTaskMessage));
    }

    private void endAlertForResource(String resourceId, ResourceDeleteTaskMessage resourceDeleteTaskMessage) {
        Boolean status = resourceService.findOne(resourceDeleteTaskMessage.getTenantName(), ResourceKey.builder()
                .externalAccountId(resourceDeleteTaskMessage.getExternalAccountId())
                .resourceType(resourceDeleteTaskMessage.getResourceType())
                .resourceIdHash(ResourceKey.computeResourceIdHash(resourceId))
                .build()).map(resource -> {
            gcpConfig.getSignatureClass(resourceDeleteTaskMessage.getResourceType(), resourceDeleteTaskMessage.getTenantName()).forEach(gcpSignature -> {
                // send request to esp web
                val externalAccount = AlertAttributes.ExternalAccount.builder()
                        .id(resourceDeleteTaskMessage.getExternalAccountId())
                        .provider("google")
                        .build();
                val attributes = AlertAttributes.builder()
                        .externalAccount(externalAccount)
                        .signature(AlertAttributes.Signature.builder()
                                .identifier(gcpSignature.signatureId().getValue())
                                .build())
                        .resource(resourceId)
                        .status("pass")
                        .metadata(AlertAttributes.Metadata.builder().details(Collections.EMPTY_MAP).build())
                        .deletedAt(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").format(new Date()))
                        .region(AlertAttributes.Region.builder()
                                .name(StringUtils.lowerCase(resource.getRegion()))
                                .code(StringUtils.lowerCase(resource.getRegion()))
                                .build())
                        .build();
                AlertRequest req = AlertRequest.builder()
                        .data(AlertData.builder().attributes(attributes).build())
                        .build();

                try {
                    espClient.suppressAlert(resourceDeleteTaskMessage.getExternalAccountId(), req);
                } catch (IOException e) {
                    LOGGER.error("Error suppressing alert for resource: {}", resourceId, e);
                    throw new RuntimeException(e);
                }
            });
            // clear redis if key exists:
            boolean deleted = redisClient.deleteKey(resource.getResourceKey().redisKeyString());

            // send request to api layer to delete resource
            return resourceService.deleteResource(resourceDeleteTaskMessage.getTenantName(), ResourceKey.builder()
                    .externalAccountId(resourceDeleteTaskMessage.getExternalAccountId())
                    .resourceType(resourceDeleteTaskMessage.getResourceType())
                    .resourceIdHash(ResourceKey.computeResourceIdHash(resourceId))
                    .build());

        }).orElse(false);
        if (!status) {
            LOGGER.info("Received alert ending request for resource: {} that does not exist. Ignoring...", resourceId);
        } else {
            LOGGER.info("Alert successfully ended for resource: {}, accountId: {}",
                    resourceId, resourceDeleteTaskMessage.getExternalAccountId());
        }
    }
}
