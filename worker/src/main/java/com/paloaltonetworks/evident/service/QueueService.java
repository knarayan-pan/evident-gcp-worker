package com.paloaltonetworks.evident.service;

import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.google.common.base.Joiner;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.paloaltonetworks.aperture.ApertureSqsAsyncClient;
import com.paloaltonetworks.evident.gcp.models.request.DeleteResourcesRequest;
import com.paloaltonetworks.evident.gcp.models.request.GcpActivityLogRequest;
import com.paloaltonetworks.evident.gcp.models.request.ProjectTreeRefreshRequest;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.gcp.models.request.SignatureRequest;
import com.paloaltonetworks.evident.messaging.task.AuditLogMessage;
import com.paloaltonetworks.evident.messaging.task.ProjectRefreshTaskMessage;
import com.paloaltonetworks.evident.messaging.task.ProjectTreeDeleteTaskMessage;
import com.paloaltonetworks.evident.messaging.task.ResourceDedupTaskMessage;
import com.paloaltonetworks.evident.messaging.task.ResourceDeleteTaskMessage;
import com.paloaltonetworks.evident.messaging.task.SignatureTaskMessage;
import com.paloaltonetworks.evident.sqs_client.MonitoredSqsAsyncClient;
import com.paloaltonetworks.evident.config.ResourceWorkerQueues;
import com.paloaltonetworks.evident.messaging.task.ResourceRefreshTaskMessage;
import com.paloaltonetworks.evident.messaging.task.StartTaskMessage;
import com.paloaltonetworks.evident.utils.JsonUtils;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.lang.invoke.MethodHandles;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

@Component
public class QueueService {

    private final ApertureSqsAsyncClient apertureSqsAsyncClient;
    private final Cache<String, Boolean> msgCache;
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());


    @Autowired
    public QueueService(@Qualifier(MonitoredSqsAsyncClient.NAME) ApertureSqsAsyncClient apertureSqsAsyncClient) {
        this.apertureSqsAsyncClient = apertureSqsAsyncClient;
        msgCache = CacheBuilder
                .newBuilder()
                .expireAfterAccess(1, TimeUnit.HOURS)
                .build();
    }

    private String formattedQueueName(String externalAccountId, String queueName) {
        return Joiner.on("_").join(externalAccountId, queueName);
    }

    /**
     * Refresh a resource either or a family of resources indicated by the resource type.
     *
     * @param request {@link ResourceRefreshRequest}
     */
    public void queueResourceRefresh(ResourceRefreshRequest request) {
        apertureSqsAsyncClient.sendMessageAsync(formattedQueueName(request.getExternalAccountId(), ResourceWorkerQueues.FETCH_RESOURCES.getQueueName()),
                new SendMessageRequest()
                        .withMessageBody(JsonUtils.encode(ResourceRefreshTaskMessage.builder()
                                .externalId(request.getExternalAccountId())
                                .organizationId(request.getOrganizationId())
                                .tenantName(request.getTenant())
                                .resourceId(request.getResourceId())
                                .resourceType(request.getResourceType())
                                .projectId(request.getProjectId())
                                .zone(request.getZone())
                                .resourceName(request.getResourceName())
                                .reconcile(request.getReconcile())
                                .pageSize(request.getPageSize())
                                .pageToken(request.getPageToken()).build()))
                        .withMessageDeduplicationId(Joiner.on(":").join(request.getExternalAccountId(),
                                request.getResourceId() == null ? "N/A" : request.getResourceId(),
                                request.getResourceType(), ResourceRefreshTaskMessage.TASK_TYPE)));
    }

    /**
     * Queue a start task either periodically and also indicate if this is a refresh i.e. a full walk through for an account that is already onboarded
     *
     * @param tenant            the tenant/org name
     * @param externalAccountId the accountId
     * @param reconcile         boolean indicating if this is an already onboarded account for which we want to walk the tree
     */
    public void queueStartTask(@NonNull String tenant, @NonNull String externalAccountId, boolean reconcile) {
        apertureSqsAsyncClient.sendMessageAsync(formattedQueueName(externalAccountId, ResourceWorkerQueues.FETCH_RESOURCES.getQueueName()),
                new SendMessageRequest().withMessageBody(JsonUtils.encode(StartTaskMessage.builder()
                        .externalId(externalAccountId)
                        .tenantName(tenant)
                        .reconcile(reconcile)
                        .build())));
    }

    /**
     * Try to acquire the in process msg lock for sqs to prevent duplicate processing.
     *
     * @param msgId The sqs message id
     * @return boolean indicating if the lock was achieved or not.
     */
    public boolean acquireLocalMsgLock(@NonNull String msgId) {
        if (msgCache.getIfPresent(msgId) == null) {
            msgCache.put(msgId, Boolean.TRUE);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Must be called to clear the cached value
     *
     * @param msgId sqs msgId
     */
    public void clearLock(@Nonnull String msgId) {
        msgCache.invalidate(msgId);
    }

    /**
     * Get number of messages visible in this queue
     *
     * @param queueName name of the queue
     * @return the number of messages still available
     */
    public long getNumMessages(@Nonnull String externalAccountId, @Nonnull String queueName) {
        return apertureSqsAsyncClient.getNumMessages(formattedQueueName(externalAccountId, queueName));
    }

    /**
     * Queue tasks to fetch next set of audit logs
     *
     * @param request {@link GcpActivityLogRequest} object
     */
    public void queueAuditLogTask(GcpActivityLogRequest request) {
        apertureSqsAsyncClient.sendMessageAsync(formattedQueueName(request.getExternalAccountId(), ResourceWorkerQueues.AUDIT_LOG.getQueueName()),
                new SendMessageRequest().withMessageBody(JsonUtils.encode(AuditLogMessage.builder()
                        .externalId(request.getExternalAccountId())
                        .tenantName(request.getTenant())
                        .fromDate(request.getFromDate())
                        .toDate(request.getToDate())
                        .projectIds(request.getProjectIds())
                        .organizationId(request.getOrganizationId())
                        .pageToken(request.getPageToken())
                        // add a delay to ensure we dont exceed api limits for audit logs
                        .build())).withDelaySeconds(5));
    }

    public void queueDeleteProjectTask(ProjectTreeDeleteTaskMessage request) {
        apertureSqsAsyncClient.sendMessageAsync(formattedQueueName(request.getExternalAccountId(), ResourceWorkerQueues.FETCH_RESOURCES.getQueueName()),
                new SendMessageRequest().withMessageBody(JsonUtils.encode(request)));
    }

    public void queueResourceDelete(ResourceRefreshRequest request) {
        apertureSqsAsyncClient.sendMessageAsync(formattedQueueName(request.getExternalAccountId(), ResourceWorkerQueues.FETCH_RESOURCES.getQueueName()),
                new SendMessageRequest().withMessageBody(JsonUtils.encode(ResourceDeleteTaskMessage.builder()
                        .externalId(request.getExternalAccountId())
                        .tenantName(request.getTenant())
                        .resourceIds(Collections.singleton(request.getResourceId()))
                        .resourceType(request.getResourceType())
                        .projectId(request.getProjectId())
                        .build())));
    }

    public void queueBatchedResourceDelete(DeleteResourcesRequest request) {
        apertureSqsAsyncClient.sendMessageAsync(formattedQueueName(request.getExternalAccountId(), ResourceWorkerQueues.FETCH_RESOURCES.getQueueName()),
                new SendMessageRequest().withMessageBody(JsonUtils.encode(ResourceDeleteTaskMessage.builder()
                        .externalId(request.getExternalAccountId())
                        .tenantName(request.getTenant())
                        .resourceIds(request.getResourceIds())
                        .resourceType(request.getResourceType())
                        .projectId(request.getProjectId())
                        .build())));
    }

    /**
     * Task to queue project level refresh which will ensure that downstream resources tied to the project are also refreshed.
     * This is to only be used upon onboarding or at anytime we want to walk through the tree in GCP.
     * See {@link com.paloaltonetworks.evident.schedule.ReconciliationScheduler}  and
     * {@link com.paloaltonetworks.evident.schedule.IterativeScanScheduler} for more information
     *
     * @param request {@link ProjectTreeRefreshRequest} object containing parameters needed to process the project refresh
     */
    public void queueProjectRefresh(ProjectTreeRefreshRequest request) {
        apertureSqsAsyncClient.sendMessageAsync(formattedQueueName(request.getExternalAccountId(), ResourceWorkerQueues.FETCH_RESOURCES.getQueueName()),
                new SendMessageRequest().withMessageBody(JsonUtils.encode(ProjectRefreshTaskMessage.builder()
                        .externalId(request.getExternalAccountId())
                        .tenantName(request.getTenant())
                        .resourceId(request.getResourceId())
                        .organizationId(request.getOrganizationId())
                        .resourceName(request.getResourceName())
                        .resourceType(request.getResourceType())
                        .reconcile(request.getReconcile())
                        .build())));

    }

    /**
     * Queue task to evaluate signatures for this resource and resource type
     *
     * @param request {@link SignatureRequest} containing the details to use for evaluating signatures
     */
    public void queueSignatureForResource(SignatureRequest request) {
        LOGGER.debug("Queueing signature task for resource: {}", request.getResourceId());
        apertureSqsAsyncClient.sendMessageAsync(formattedQueueName(request.getExternalAccountId(), ResourceWorkerQueues.SIGNATURE.getQueueName()),
                new SendMessageRequest().withMessageBody(JsonUtils.encode(SignatureTaskMessage.builder()
                        .externalId(request.getExternalAccountId())
                        .tenantName(request.getTenant())
                        .resourceId(request.getResourceId())
                        .projectId(request.getProjectId())
                        .organizationId(request.getOrganizationId())
                        .resourceType(request.getResourceType())
                        .build()))
                        .withDelaySeconds(50));
    }

    public void queueSignatureForResourceWithDelay(SignatureRequest request, int delaySeconds) {
        LOGGER.debug("Queueing signature task for resource: {}", request.getResourceId());
        apertureSqsAsyncClient.sendMessageAsync(formattedQueueName(request.getExternalAccountId(), ResourceWorkerQueues.SIGNATURE.getQueueName()),
                new SendMessageRequest().withMessageBody(JsonUtils.encode(SignatureTaskMessage.builder()
                        .externalId(request.getExternalAccountId())
                        .tenantName(request.getTenant())
                        .resourceId(request.getResourceId())
                        .projectId(request.getProjectId())
                        .organizationId(request.getOrganizationId())
                        .resourceType(request.getResourceType())
                        .build()))
                        .withDelaySeconds(delaySeconds));
    }

    /**
     * Queue delayed task to dedup resources of a given type for a specific project
     *
     * @param request {@link ResourceDedupTaskMessage} object containing the details of the request
     */
    public void queueDedupTaskForProject(ResourceDedupTaskMessage request) {
        LOGGER.info("Queueing dedup for reconciliation task for resourceType: {}, projectId: {}", request.getResourceType(),
                request.getProjectId());
        apertureSqsAsyncClient.sendMessageAsync(formattedQueueName(request.getExternalAccountId(), ResourceWorkerQueues.FETCH_RESOURCES.getQueueName()),
                new SendMessageRequest().withMessageBody(JsonUtils.encode(request)).withDelaySeconds(600));
    }
}
