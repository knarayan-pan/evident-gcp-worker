package com.paloaltonetworks.evident.service;

import com.google.common.collect.Maps;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.data_access.service.MonitoredResourceService;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.gcp.models.AuditLog;
import com.paloaltonetworks.evident.gcp.models.request.GcpActivityLogRequest;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.messaging.task.ProjectTreeDeleteTaskMessage;
import com.paloaltonetworks.evident.utils.JsonUtils;
import lombok.val;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.lang.invoke.MethodHandles;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCP_PROJECT;
import static com.paloaltonetworks.evident.utils.ResourceUtil.REGION_GLOBAL;
import static com.paloaltonetworks.evident.utils.ResourceUtil.extractSQLInstanceName;
import static com.paloaltonetworks.evident.utils.ResourceUtil.makeResourceId;

@Service
public class GCPAuditLogService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static final String KEY_OPERATION = "operation";
    private static final String KEY_LAST = "last";
    private final QueueService queueService;
    private final ResourceService resourceService;

    @Autowired
    public GCPAuditLogService(QueueService queueService,
                              @Qualifier(MonitoredResourceService.NAME) ResourceService resourceService) {
        this.queueService = queueService;
        this.resourceService = resourceService;
    }

    /**
     * Process a single page of the audit log based on request parameters
     *
     * @param request {@link GcpActivityLogRequest} object containing request parameters
     */
    public void processAuditLogPage(@Nonnull GcpActivityLogRequest request, @Nonnull List<AuditLog> entries) {
        entries.forEach(auditLog -> ResourceType.fromResourceTypeString(auditLog.getResource().getType())
                .map(resourceType -> {
                    val projectId = auditLog.getResource().getLabels().get("project_id");
                    val resourceId = makeUniqueResourceId(auditLog, resourceType, projectId);
                    ResourceRefreshRequest resourceRequest = ResourceRefreshRequest.builder()
                            .externalAccountId(request.getExternalAccountId())
                            .tenant(request.getTenant())
                            .organizationId(request.getOrganizationId())
                            // every single resource will have project_id in the label - according to documentation here:
                            // https://cloud.google.com/logging/docs/reference/v2/rest/v2/monitoredResourceDescriptors/list#MonitoredResourceDescriptor
                            .projectId(projectId)
                            .resourceType(resourceType.getResourceType())
                            .resourceId(resourceId)
                            .zone(resourceType.getZoneLabelKey()
                                    .map(s -> auditLog.getResource().getLabels().get(s))
                                    .orElse(null))
                            .resourceName(auditLog.getProtoPayload().getResourceName())
                            .build();
                    String methodName = auditLog.getProtoPayload().getMethodName();
                    if (StringUtils.isEmpty(methodName)) {
                        LOGGER.warn("Received empty method name. Raw event: {}", JsonUtils.encode(auditLog));
                    } else if (!lastOperation(auditLog.getAdditionalProperties())) {
                        LOGGER.warn("This is not the last step of the operation ResourceType={}, MethodName={} Properties={}",
                                resourceType,
                                methodName,
                                JsonUtils.encode(auditLog.getAdditionalProperties()));
                    } else if (resourceType.resourceRequiresUpdate(methodName)) {
                        LOGGER.debug("Queuing resource update: {}", JsonUtils.encode(auditLog));
                        queueService.queueResourceRefresh(resourceRequest);
                    } else if (resourceType.resourceRequiresDelete(methodName)) {
                        LOGGER.info("Queuing resource delete: {}", JsonUtils.encode(auditLog));
                        queueService.queueResourceDelete(resourceRequest);
                        LOGGER.info("Successfully queued delete request for resourceId: {}, resourceType: {}, projectId: {}, accountId: {}",
                                resourceId, resourceRequest.getResourceType(), projectId, request.getExternalAccountId());
                    } else {
                        LOGGER.info("Ignored message ResourceType={}, MethodName={}", resourceType, methodName);
                    }
                    return resourceType;
                }));
    }

    /**
     * If there is no ResourceIdLabelKey defined for this type, create resource id with
     * project id and resource
     */
    private String makeUniqueResourceId(AuditLog auditLog, ResourceType resourceType, String projectId) {
        String resourceName = auditLog.getProtoPayload().getResourceName();
        switch (resourceType) {
            case GCP_PROJECT:
                return projectId;
            case GCE_SUBNETWORK:
                val region = resourceType.getZoneLabelKey()
                        .map(key -> auditLog.getResource().getLabels().get(key))
                        .orElse(REGION_GLOBAL);
                return makeResourceId(projectId, region, resourceType.getResourceIdLabelKey()
                        .map(key -> auditLog.getResource().getLabels().get(key))
                        .orElse(resourceName));
            case GCE_SQL:
                // unfortunately, there is no consistency in resource name for delete and insert for SQL
                // for deletes, the format is instances/pgsql-test-vimal-inst-a3
                // for inserts, it is instances/even-equinox-188119:pgsql-test-vimal-inst-a3
                return makeResourceId(projectId, resourceType.getResourceIdLabelKey()
                        .map(key -> extractSQLInstanceName(auditLog.getResource().getLabels().get(key)))
                        .orElse(extractSQLInstanceName(resourceName)));
            default:
                return makeResourceId(projectId, resourceType.getResourceIdLabelKey()
                        .map(key -> auditLog.getResource().getLabels().get(key))
                        .orElse(resourceName));

        }
    }


    private boolean lastOperation(Map<String, Object> additionalProperties) {
        try {
            val operation = (Map<String, Object>) Optional.ofNullable(additionalProperties)
                    .orElse(Maps.newHashMap())
                    .get(KEY_OPERATION);
            return operation == null || BooleanUtils.isTrue((Boolean) operation.get(KEY_LAST));
        } catch (Exception e) {
            // unknown operation - assume last
            LOGGER.warn("Exception parsing operation: {}", e.toString(), e);
            return true;
        }

    }

    public void cleanUpProjectAndResources(GcpActivityLogRequest request) {
        // Important: this projectIds is a list but it only will contain 1 project as this is waiting on a bug fix from
        // google where if an audit log request is made with multiple projects and 1 fails  then the whole request fails.
        // As a workaround - we are making a single audit log query per project to be able to reach this flow and handle cases
        // where access is removed or project is deleted.
        request.getProjectIds().stream().findFirst().map(projectId -> {
            resourceService.deleteResource(request.getTenant(), ResourceKey.builder()
                    .resourceType(GCP_PROJECT.getResourceType())
                    .externalAccountId(request.getExternalAccountId())
                    .resourceIdHash(ResourceKey.computeResourceIdHash(projectId))
                    .build());
            // queue tasks to delete resources that belong to this project by type.
            Arrays.stream(ResourceType.values()).forEach(resourceType -> queueService.queueDeleteProjectTask(ProjectTreeDeleteTaskMessage.builder()
                    .externalId(request.getExternalAccountId())
                    .organizationId(request.getOrganizationId())
                    .tenantName(request.getTenant())
                    .projectId(projectId)
                    .resourceType(resourceType.getResourceType())
                    .build()));
            return projectId;
        }).orElseGet(() -> {
            LOGGER.warn("Cleanup for project failed as projectId is empty. Invalid state.");
            return null;
        });

    }
}
