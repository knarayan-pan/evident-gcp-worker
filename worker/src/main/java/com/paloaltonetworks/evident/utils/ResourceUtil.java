package com.paloaltonetworks.evident.utils;

import com.google.common.base.Splitter;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceMetadata;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import lombok.val;
import org.apache.commons.collections.CollectionUtils;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Use this class to do some ugly patches that need to be done to support all the quirks google has in their apis
 * when it comes to resource identification and formatting.
 */
public class ResourceUtil {
    public static final String REGION_GLOBAL = "global";
    private static final String GCE_INSTANCE_GROUP_DELIMITER = "/";
    private static final String FW_NAME_GROUP_DELIMITER = "/";
    private static final String SQL_INSTANCE_GROUP_DELIMITER = ":";
    private static final String ZONE_GROUP_DELIMITER = "/";
    private static final String RESOURCE_NAME_GROUP_DELIMITER = "/";

    /**
     * Instance name format=projects/<project-name></projectname>/zones/<zone></zone>/instances/<instance>
     * This method extracts the instance name from the full qualified resource name format
     *
     * @param resourceName Full resource name as returned by api
     * @return Instance name by extracting last part of the full name
     */
    public static String extractGCEInstanceName(@NotNull String resourceName) {
        return extractLastToken(resourceName, GCE_INSTANCE_GROUP_DELIMITER);
    }

    /**
     * Resource name format=projects/<project-name></projectname>/zones/<zone></zone>/instances/<instance>
     * This method extracts the resource name from the full qualified resource name format
     *
     * @param resourceName Full resource name as returned by api
     * @return Resource name by extracting last part of the full name
     */
    public static String extractResourceName(@NotNull String resourceName) {
        return extractLastToken(resourceName, RESOURCE_NAME_GROUP_DELIMITER);
    }

    /**
     * SQL name format=projects:<zone></zone>:<instance></instance>
     * This method extracts the sql instance name from the fully-qualified resource name format
     *
     * @param resourceName Full resource name as returned by api
     * @return Instance name by extracting last part of the full name
     */
    public static String extractSQLInstanceName(@NotNull String resourceName) {
        return extractLastToken(resourceName, SQL_INSTANCE_GROUP_DELIMITER);
    }

    /**
     * Firewall name format=projects:<instance></instance>
     * This method extracts the resource name from the fully-qualified resource name format
     *
     * @param resourceName Full resource name as returned by api
     * @return Instance name by extracting last part of the full name
     */
    public static String extractFirewallName(@NotNull String resourceName) {
        return extractLastToken(resourceName, FW_NAME_GROUP_DELIMITER);
    }

    /**
     * fully-qualified zone name template is /zones/<zone></zone> This method extracts zone name from the fully-qualified zone name
     */
    public static String extractZone(String fqzn) {
        return Optional.ofNullable(fqzn)
                .map(zn -> Splitter.on(ZONE_GROUP_DELIMITER).omitEmptyStrings().splitToList(fqzn))
                .map(list -> list.get(list.size() - 1))
                .map(String::toLowerCase)
                .orElse(REGION_GLOBAL);
    }

    /**
     * Region format is region-name-a and this method will extract the first part (regoin-name) from the format
     * Unfortunately, we do this because gcp apis don't provide a region name without making an additional api call.
     */
    public static String extractRegionFromZone(String fqzn) {
        val zone = extractZone(fqzn);
        val last = zone.lastIndexOf('-');
        if (last == -1) {
            return zone;
        }
        return zone.substring(0, last);
    }

    /**
     * Makes a resource id with name and project id, making it unique for a resource which can have
     * same name in different projects
     */
    public static String makeResourceId(String projectId, String resourceName) {
        checkNotNull(projectId, "projectId cannot be null");
        checkNotNull(resourceName, "Resource Name cannot be null");
        return projectId + RESOURCE_NAME_GROUP_DELIMITER + extractResourceName(resourceName);
    }

    /**
     * Makes a resource id with name, region and project id, making it unique for a resource which can have
     * same name in different projects and regions
     */
    public static String makeResourceId(String projectId, String regionName, String resourceName) {
        checkNotNull(projectId, "projectId cannot be null");
        checkNotNull(resourceName, "Resource Name cannot be null");
        checkNotNull(regionName, "Region Name cannot be null");
        return projectId + RESOURCE_NAME_GROUP_DELIMITER + extractResourceName(regionName) +
                RESOURCE_NAME_GROUP_DELIMITER + extractResourceName(resourceName);
    }

    /**
     * Ugly patch to retrieve resourceId from the resource label string in case the projectId is prepended to the label with a colon from GCP.
     */
    public static String sanitizeResourceIdFromEventLog(String projectId, String resourceLabel) {
        checkNotNull(resourceLabel, "resourceLabel key cannot be null");
        checkNotNull(projectId, "projectId key cannot be null");
        List<String> resourceSplit = Splitter.on(":").splitToList(resourceLabel);
        return CollectionUtils.isEmpty(resourceSplit) ? resourceLabel : resourceSplit.size() > 1 ?
                resourceSplit.get(0).equalsIgnoreCase(projectId) && resourceSplit.size() == 2 ? resourceSplit.get(1) :
                        resourceLabel : resourceLabel;
    }

    private static String extractLastToken(@NotNull String resourceName, String delimiter) {
        checkNotNull(resourceName, "Resource Name cannot be null");
        return resourceName.contains(delimiter) ?
                resourceName.substring(resourceName.lastIndexOf(delimiter) + 1) :
                resourceName;
    }

    public static String makeUniqueResourceId(ResourceType resourceType, String projectId,
                                       Map<String, String> labels, String resourceName) {
        return resourceType.getResourceIdLabelKey()
            .map(key -> labels.get(key))
            .orElse(ResourceUtil.makeResourceId(projectId, resourceName));
    }
}
