package com.paloaltonetworks.evident.utils;

import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.BaseSubscriber;

import java.lang.invoke.MethodHandles;
import java.util.function.Consumer;

public class ReactorUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    /**
     * Simple reactive subscriber to handle errors in resource processing
     */
    static class QueueSubscriber<T> extends BaseSubscriber<T> {
        private final Consumer<T> consumer;

        QueueSubscriber(Consumer<T> consumer) {
            this.consumer = consumer;
        }

        @Override
        public void hookOnSubscribe(Subscription subscription) {
            request(1);
        }

        @Override
        public void hookOnNext(T context) {
            consumer.accept(context);
            request(1);
        }

        @Override
        public void hookOnError(Throwable t) {
            LOGGER.error("Exception received in processing resource ", t);
            throw new RuntimeException(t);
        }
    }

    public static <T> QueueSubscriber<T> subscriber(Consumer<T> consumer) {
        return new QueueSubscriber<>(consumer);
    }
}
