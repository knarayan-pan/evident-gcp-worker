package com.paloaltonetworks.evident.utils;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.paloaltonetworks.evident.messaging.task.TaskMessage;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class JsonUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static ObjectMapper mapper = new ObjectMapper();

    static {
        configureMapper(mapper);
    }

    private static void configureMapper(ObjectMapper mapper) {
        Set<NamedType> taskMessageClasses = new Reflections("com.paloaltonetworks.evident.messaging.task")
                .getSubTypesOf(TaskMessage.class)
                .stream().filter(aClass -> aClass.getAnnotation(JsonTypeName.class) != null)
                .map(aClass -> {
                    JsonTypeName annotation = aClass.getAnnotation(JsonTypeName.class);
                    return new NamedType(aClass, annotation.value());
                }).collect(Collectors.toSet());
        mapper.registerSubtypes(taskMessageClasses.toArray(new NamedType[]{}));
        mapper.registerModule(new JavaTimeModule())
                .registerModule(new Jdk8Module())
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    /**
     * Encodes an object, returning null if null is passed in
     *
     * @param obj to encode
     * @return json string, null if null is passed in
     */
    @Nullable
    public static String encode(Object obj) {
        try {
            return obj == null ? null : mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            LOGGER.error("Error serializing json {}", e.getMessage());
            return null;
        }
    }

    /**
     * Encodes an object, returning null if null is passed in
     *
     * @param obj       to encode
     * @param viewClass view class to use when serializing
     * @return json string, null if null is passed in
     */
    @Nullable
    public static String encodeWithView(Object obj, Class<?> viewClass) throws JsonProcessingException {
        return obj == null ? null : mapper.writerWithView(viewClass).writeValueAsString(obj);
    }


    /**
     * Decodes an object returning null if null is passed in
     *
     * @param json  json to decode
     * @param clazz class to decode to
     * @param <T>   return type
     * @return decoded object, null if null is passed in
     */
    @Nullable
    public static <T> T decodeValue(String json, Class<T> clazz) {
        try {
            return json == null ? null : mapper.readValue(json, clazz);
        } catch (IOException e) {
            LOGGER.error("Error de-serializing json {}", e.getMessage());
            return null;
        }
    }


    /**
     * Decodes an object returning null if null is passed in
     *
     * @param json         json to decode
     * @param valueTypeRef value type reference to decode to
     * @param <T>          return type
     * @return decoded object, null if null is passed in
     */
    @Nullable
    public static <T> T decodeValue(String json, TypeReference valueTypeRef) {
        try {
            return json == null ? null : mapper.readValue(json, valueTypeRef);
        } catch (IOException e) {
            LOGGER.error("Error de-serializing json {}", e.getMessage());
            return null;
        }
    }
}

