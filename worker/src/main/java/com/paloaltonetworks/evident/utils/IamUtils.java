package com.paloaltonetworks.evident.utils;

import com.google.common.collect.ImmutableList;
import com.paloaltonetworks.evident.enrichment.metadata.IamPolicy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class IamUtils {
    public static boolean isServiceUser(String user) {
        return user != null && user.toLowerCase().startsWith("serviceaccount:");
    }

    public static boolean isRegularUser(String userId) {
        return userId != null && userId.toLowerCase().startsWith("user:");
    }

    public static boolean isAdminRole(String roleName) {
        if (roleName == null) {
            return false;
        }
        String lowerCaseName = roleName.toLowerCase();
        return ImmutableList.of("roles/editor", "roles/owner").stream()
            .anyMatch(str -> lowerCaseName.startsWith(str) || lowerCaseName.indexOf("/" + str) >= 0) ||
            lowerCaseName.endsWith("admin");
    }

    public static boolean isKmsAdminRole(String roleName) {
        return roleName != null &&
            roleName.equalsIgnoreCase("roles/cloudkms.admin");
    }

    public static boolean isKmsUserRole(String roleName) {
        return roleName != null &&
            ImmutableList.of("roles/cloudkms.cryptoKeyDecrypter",
                "roles/cloudkms.cryptoKeyEncrypter",
                "roles/cloudkms.cryptoKeyEncrypterDecrypter")
                .stream()
                .anyMatch(userRoleName -> userRoleName.equalsIgnoreCase(roleName));
    }

    public static boolean isKmsRole(String roleName) {
        return isKmsAdminRole(roleName) || isKmsUserRole(roleName);
    }

    public static Map<String, List<String>> getUserRoles(List<IamPolicy.RoleMembers> roleMembersList,
                                                         Predicate<String> userCheck, Predicate<String> roleCheck)
    {
        if (roleMembersList == null) {
            return Collections.emptyMap();
        }

        Map<String, List<String>> userRoles = new LinkedHashMap<>();
        roleMembersList.stream()
            .forEach(roleMembers -> {
                    boolean isRoleChecked = roleCheck.test(roleMembers.getRole());
                    roleMembers.getMembers().stream()
                        .filter(userCheck)
                        .forEach(member -> {
                            userRoles.putIfAbsent(member, new ArrayList<>());
                            if (isRoleChecked) {
                                userRoles.get(member).add(roleMembers.getRole());
                            }
                        });
                }
            );
        return userRoles;
    }
}
