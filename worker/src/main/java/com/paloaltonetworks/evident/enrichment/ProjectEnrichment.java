package com.paloaltonetworks.evident.enrichment;

import com.google.api.services.cloudresourcemanager.model.Policy;
import com.google.api.services.compute.model.Network;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.data_access.service.MonitoredResourceService;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.metadata.IamPolicy;
import com.paloaltonetworks.evident.enrichment.metadata.ProjectMetadata;
import com.paloaltonetworks.evident.enrichment.request.ProjectEnrichmentRequest;
import com.paloaltonetworks.evident.exceptions.GCPApiException;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.redis.MonitoredRedisClient;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import com.paloaltonetworks.evident.utils.JsonUtils;
import com.paloaltonetworks.evident.utils.ResourceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class ProjectEnrichment extends GCPEnrichment<ProjectEnrichmentRequest> {
    private final GCPApiService gcpApiService;

    @Autowired
    public ProjectEnrichment(@Qualifier(MonitoredResourceService.NAME) ResourceService resourceService,
                             @Qualifier(MonitoredRedisClient.NAME) RedisClient redisClient,
                             QueueService queueService,
                             GCPApiService gcpApiService) {
        super(resourceService, redisClient, queueService);
        this.gcpApiService = gcpApiService;
    }

    @Override
    public Mono<Resource> enrich(ProjectEnrichmentRequest enrichmentRequest) {
        Policy policy = null;
        List<Network> networks = new ArrayList<>();
        try {
            policy = gcpApiService.getProjectIamPolicy(enrichmentRequest);
            networks = Optional.ofNullable(gcpApiService.getProjectNetworks(enrichmentRequest).getItems())
                    .orElse(Collections.emptyList());
        } catch (GCPApiException e) {
            if (e.retryableException()) {
                throw e;
            }
        }
        List<ProjectMetadata.Network> convertedNetworks = networks.stream()
                .map(nw -> ProjectMetadata.Network.builder()
                        .name(nw.getName())
                        .id(nw.getId())
                        .creationTimestamp(nw.getCreationTimestamp())
                        .subnetworks(nw.getSubnetworks())
                        .build()
                )
                .collect(Collectors.toList());
        ProjectMetadata projectMetadata = ProjectMetadata.builder()
                .projectId(enrichmentRequest.getResourceId())
                .resourceName(enrichmentRequest.getProjectName())
                .orgId(enrichmentRequest.getOrgId())
                .iamPolicy(policy == null ? null : new IamPolicy(policy))
                .version(metadataVersionString())
                .networks(convertedNetworks)
                .build();
        Resource resource = Resource.builder()
                .resourceMetadata(JsonUtils.encode(projectMetadata))
                .resourceId(enrichmentRequest.getResourceId())
                .resourceKey(ResourceKey.builder()
                        .externalAccountId(enrichmentRequest.getExternalAccountId())
                        .resourceIdHash(ResourceKey.computeResourceIdHash(enrichmentRequest.getResourceId()))
                        .resourceType(ResourceType.GCP_PROJECT.getResourceType())
                        .build())
                .tenant(enrichmentRequest.getTenant())
                .cloudAppType(CloudAppType.GCP)
                .region(ResourceUtil.REGION_GLOBAL)
                .build();
        return Mono.justOrEmpty(resource);
    }

    @Override
    public String resourceType() {
        return ResourceType.GCP_PROJECT.getResourceType();
    }

}
