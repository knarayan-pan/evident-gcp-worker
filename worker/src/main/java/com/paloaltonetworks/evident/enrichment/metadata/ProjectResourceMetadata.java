package com.paloaltonetworks.evident.enrichment.metadata;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.paloaltonetworks.evident.data_access.models.ResourceMetadata;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ProjectResourceMetadata extends ResourceMetadata {
    private final IamPolicy iamPolicy;

    @Builder
    public ProjectResourceMetadata(Map<String, String> tags, String resourceName, String projectId, String version,
                           IamPolicy iamPolicy) {
        super(tags, resourceName, projectId, version);
        this.iamPolicy = iamPolicy;
    }
}
