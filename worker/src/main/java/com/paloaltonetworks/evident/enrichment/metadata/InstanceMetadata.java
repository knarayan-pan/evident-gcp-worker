package com.paloaltonetworks.evident.enrichment.metadata;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.paloaltonetworks.evident.data_access.models.ResourceMetadata;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.math.BigInteger;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class InstanceMetadata extends ResourceMetadata {
    private final boolean canIpForward;
    @NonNull
    private final String cpuPlatform;
    @NonNull
    private final String createTimestamp;
    @NonNull
    private final String description;
    @NonNull
    private final BigInteger id;
    @NonNull
    private final String name;
    @NonNull
    private final String selfLink;
    @NonNull
    private final String zone;
    @NonNull
    private final String machineType;
    @NonNull
    private final String status;

    private final Map<String, String> metadata;

    @Builder
    public InstanceMetadata(Map<String, String> tags, String resourceName,
                            String projectId, String version, boolean canIpForward, String cpuPlatform,
                            String createTimestamp, String description,
                            BigInteger id, String name, String selfLink,
                            String zone, String machineType, String status,
                            Map<String, String> metadata) {
        super(tags, resourceName, projectId, version);
        this.canIpForward = canIpForward;
        this.cpuPlatform = cpuPlatform;
        this.createTimestamp = createTimestamp;
        this.description = description;
        this.id = id;
        this.name = name;
        this.selfLink = selfLink;
        this.zone = zone;
        this.machineType = machineType;
        this.status = status;
        this.metadata = metadata;
    }
}
