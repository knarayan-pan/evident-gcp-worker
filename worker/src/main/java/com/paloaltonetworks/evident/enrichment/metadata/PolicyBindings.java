package com.paloaltonetworks.evident.enrichment.metadata;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.api.services.storage.model.Policy;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@Builder(toBuilder = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PolicyBindings {

    private String role;
    private Object condition;
    private List<String> members;

    public PolicyBindings(Policy.Bindings bindings) {
        this.role = bindings.getRole();
        this.condition = bindings.getCondition();
        this.members = bindings.getMembers();
    }
}
