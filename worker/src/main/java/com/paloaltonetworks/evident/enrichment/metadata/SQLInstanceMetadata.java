package com.paloaltonetworks.evident.enrichment.metadata;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.paloaltonetworks.evident.data_access.models.ResourceMetadata;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SQLInstanceMetadata extends ResourceMetadata {
    private final boolean autobackupEnabled;
    @NonNull
    private final String zone;
    @NonNull
    private final String connectionName;
    @NonNull
    private final String databaseVersion;
    @NonNull
    private final String region;
    @NonNull
    private final String selfLink;
    @NotNull
    private final List<Map<String, Object>> authorizedNetworks;
    private final boolean requireSSL;


    @Builder
    public SQLInstanceMetadata(Map<String, String> tags, String resourceName,
                               String projectId,
                               String version,
                               boolean autobackupEnabled,
                               String zone, String connectionName,
                               String databaseVersion,
                               String region, String selfLink,
                               List<Map<String, Object>> authorizedNetworks,
                               boolean requireSSL) {
        super(tags, resourceName, projectId, version);
        this.autobackupEnabled = autobackupEnabled;
        this.zone = zone;
        this.connectionName = connectionName;
        this.databaseVersion = databaseVersion;
        this.region = region;
        this.selfLink = selfLink;
        this.authorizedNetworks = authorizedNetworks;
        this.requireSSL = requireSSL;
    }
}
