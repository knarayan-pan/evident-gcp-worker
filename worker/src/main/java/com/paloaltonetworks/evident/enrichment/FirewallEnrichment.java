package com.paloaltonetworks.evident.enrichment;

import com.google.api.services.compute.model.Firewall;
import com.google.api.services.compute.model.FirewallList;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.data_access.service.MonitoredResourceService;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.metadata.FirewallMetadata;
import com.paloaltonetworks.evident.enrichment.request.ResourceEnrichmentRequest;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.gcp.models.request.FetchFirewallsRequest;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.redis.MonitoredRedisClient;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import com.paloaltonetworks.evident.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.paloaltonetworks.evident.utils.ResourceUtil.REGION_GLOBAL;
import static com.paloaltonetworks.evident.utils.ResourceUtil.makeResourceId;

/**
 * Firewall enrichment
 */
@Slf4j
@Component
public class FirewallEnrichment extends GCPResourceEnrichment<FirewallList, Firewall> {
    FirewallEnrichment(@Qualifier(MonitoredResourceService.NAME) ResourceService resourceService,
                       @Qualifier(MonitoredRedisClient.NAME) RedisClient redisClient,
                       QueueService queueService,
                       GCPApiService gcpApiService) {
        super(resourceService, redisClient, queueService, gcpApiService);
    }

    @Override
    public String resourceType() {
        return ResourceType.GCE_FIREWALL.getResourceType();
    }

    @Override
    String getNextPageToken(final FirewallList list) {
        return list.getNextPageToken();
    }

    @Override
    Resource toResource(final Firewall instance, final ResourceEnrichmentRequest request) {
        final String resourceId = makeResourceId(request.getProjectId(), instance.getName());
        return Resource.builder()
                .cloudAppType(CloudAppType.GCP)
                .resourceId(resourceId)
                .tenant(request.getTenant())
                .region(REGION_GLOBAL)
                .resourceKey(ResourceKey.builder()
                        .externalAccountId(request.getExternalAccountId())
                        .resourceType(resourceType())
                        .resourceIdHash(ResourceKey.computeResourceIdHash(resourceId))
                        .build())
                .resourceMetadata(JsonUtils.encode(FirewallMetadata.builder()
                        .creationTimeStamp(instance.getCreationTimestamp())
                        .disabled(BooleanUtils.toBoolean((Boolean) instance.get("disabled")))
                        .direction(instance.getDirection())
                        .network(instance.getNetwork())
                        .priority(instance.getPriority())
                        .allowedRules(allowed(instance))
                        .deniedRules(denied(instance))
                        .selfLink(instance.getSelfLink())
                        .resourceName(instance.getName())
                        .projectId(request.getProjectId())
                        .build()))
                .build();
    }

    @Override
    List<Firewall> extractGCPResourceList(final FirewallList list) {
        return Optional.ofNullable(list.getItems()).orElse(Collections.emptyList());
    }

    @Override
    FirewallList findAllResources(final ResourceEnrichmentRequest request) {
        return gcpApiService.getFirewalls(
                FetchFirewallsRequest.builder()
                        .externalAccountId(request.getExternalAccountId())
                        .tenant(request.getTenant())
                        .projectId(request.getProjectId())
                        .pageToken(request.getPageToken())
                        .build());
    }

    @Override
    Firewall findOneResource(final ResourceEnrichmentRequest request) {
        return gcpApiService.getFirewall(
                ResourceRefreshRequest.builder()
                        .tenant(request.getTenant())
                        .externalAccountId(request.getExternalAccountId())
                        .resourceType(resourceType())
                        .resourceId(request.getResourceId())
                        .resourceName(request.getResourceName())
                        .projectId(request.getProjectId())
                        .zone(request.getZone())
                        .build());
    }

    private List<FirewallMetadata.RulesMetadata> allowed(final Firewall instance) {
        val list = instance.getAllowed();
        return Optional.ofNullable(list)
                .orElse(Collections.emptyList())
                .stream()
                .map(allowed -> FirewallMetadata.RulesMetadata.builder()
                        .sourceIPRanges(instance.getSourceRanges())
                        .protocol(allowed.getIPProtocol())
                        .ports(allowed.getPorts()).build())
                .collect(Collectors.toList());
    }

    private List<FirewallMetadata.RulesMetadata> denied(final Firewall instance) {
        val list = instance.getDenied();
        return Optional.ofNullable(list)
                .orElse(Collections.emptyList())
                .stream()
                .map(allowed -> FirewallMetadata.RulesMetadata.builder()
                        .sourceIPRanges(instance.getSourceRanges())
                        .protocol(allowed.getIPProtocol())
                        .ports(allowed.getPorts()).build())
                .collect(Collectors.toList());
    }

}
