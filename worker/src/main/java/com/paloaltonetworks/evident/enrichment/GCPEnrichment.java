package com.paloaltonetworks.evident.enrichment;

import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.request.EnrichmentRequest;
import com.paloaltonetworks.evident.gcp.models.request.SignatureRequest;
import com.paloaltonetworks.evident.messaging.task.ResourceDedupTaskMessage;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import com.paloaltonetworks.evident.utils.ReactorUtils;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.CoreSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.lang.invoke.MethodHandles;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static com.paloaltonetworks.evident.schedule.ReconciliationScheduler.DEFAULT_RECONCILIATION_INTERVAL_IN_HOURS;

public abstract class GCPEnrichment<V extends EnrichmentRequest> implements Enrichment<V> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static final String ENRICHMENT_VERSION = "v1.0";
    protected final QueueService queueService;
    private final ResourceService resourceService;
    private final RedisClient redisClient;


    GCPEnrichment(ResourceService resourceService, RedisClient redisClient, QueueService queueService) {
        this.resourceService = resourceService;
        this.redisClient = redisClient;
        this.queueService = queueService;
    }

    @Override
    public Flux<Resource> enrichList(V enrichmentRequest) {
        return Flux.empty();
    }

    @Override
    public void applyForCollection(V request) {
        enrichList(request)
                .flatMap((Function<Resource, Mono<Resource>>) resource -> {
                    Mono<Resource> resourceMono = resourceService.findOne(request.getTenant(), resource.getResourceKey())
                            .map((Function<Resource, Mono<Resource>>)
                                    fromResource -> resource.isSimilar(fromResource) ?
                                            Mono.empty() : Mono.just(resource)
                            ).orElse(Mono.just(resource));
                    // publish redis marker for recon
                    // if part of reconciliation then post to redis
                    if (request.isReconcile()) {
                        // push key to redis
                        val key = resource.getResourceKey().redisKeyString();
                        LOGGER.debug("Pushing resource key: {} to redis for reconciliation state checking...", key);
                        redisClient.setKey(key, Boolean.TRUE.toString()
                                , Math.toIntExact(TimeUnit.HOURS.toSeconds(DEFAULT_RECONCILIATION_INTERVAL_IN_HOURS)));

                    }
                    return resourceMono;
                })
                .switchIfEmpty(new Mono<Resource>() {
                    @Override
                    public void subscribe(CoreSubscriber<? super Resource> actual) {
                        LOGGER.debug("No update to db required for resource: {}", request.getResourceId());
                    }
                })
                .doOnNext(resource -> redisClient.getResourceLock(request.getExternalAccountId(), resource.getResourceId()))
                // upsert to db
                .doOnNext(resource -> upsertResource(request.getTenant(), request.getExternalAccountId(), resource))
                .doOnNext(resource -> {
                    if (request.getQueueSignatureTask()) {
                        // queue sqs task to run signatures on this resource
                        LOGGER.info("Queueing task to scan signature for resourceType: {}, resourceId: {}, project: {}",
                                request.getResourceType(), resource.getResourceId(), request.getProjectId());
                        queueService.queueSignatureForResource(SignatureRequest.builder()
                                .externalAccountId(request.getExternalAccountId())
                                .tenant(request.getTenant())
                                .projectId(request.getProjectId())
                                .resourceId(resource.getResourceId())
                                .resourceType(request.getResourceType())
                                .build());
                    }
                })
                .doOnError(throwable -> LOGGER.error("Error encountered in updating resource to cassandra. ResourceId: {}, AccountId: {}, Error: {}",
                        request.getResourceId(), request.getExternalAccountId(), throwable.getMessage(), throwable))
                .subscribeOn(Schedulers.immediate()).subscribe(ReactorUtils.subscriber(this::handle));
    }

    @Override
    public void apply(V request) {
        enrich(request)
                // check cassandra and check if the resource exists and is in same state
                // if not then go ahead with upsert
                // if it does exist then check if they are similar - if they are similar then do not proceed. Otherwise
                // proceed with the new resource - see associated unit tests for test cases
                .flatMap((Function<Resource, Mono<Resource>>) resource ->
                        resourceService.findOne(request.getTenant(), resource.getResourceKey())
                                .map((Function<Resource, Mono<Resource>>)
                                        fromResource -> resource.isSimilar(fromResource) ?
                                                Mono.empty() : Mono.just(resource)
                                ).orElse(Mono.just(resource)))
                .switchIfEmpty(new Mono<Resource>() {
                    @Override
                    public void subscribe(CoreSubscriber<? super Resource> actual) {
                        LOGGER.debug("No update to db required for resource: {}", request.getResourceId());
                    }
                })
                .doOnNext(resource -> redisClient.getResourceLock(request.getExternalAccountId(), request.getResourceId()))
                // upsert to db
                .doOnNext(resource -> upsertResource(request.getTenant(), request.getExternalAccountId(), resource))
                .doOnNext(resource -> {
                    // push resource key to redis to prevent iterative resources to be deleted in reconciliation checks
                    val key = resource.getResourceKey().redisKeyString();
                    LOGGER.debug("Pushing resource key: {} to redis for reconciliation state checking...", key);
                    redisClient.setKey(key, Boolean.TRUE.toString()
                            , Math.toIntExact(TimeUnit.HOURS.toSeconds(DEFAULT_RECONCILIATION_INTERVAL_IN_HOURS)));
                })
                .doOnSuccess(resource -> {
                    // queue sqs task to run signatures on this resource
                    queueService.queueSignatureForResource(SignatureRequest.builder()
                            .externalAccountId(request.getExternalAccountId())
                            .tenant(request.getTenant())
                            .projectId(request.getProjectId())
                            .resourceId(resource.getResourceId())
                            .resourceType(resource.getResourceKey().getResourceType())
                            .build());
                })
                .doOnError(throwable -> LOGGER.error("Error encountered in updating resource to cassandra. ResourceId: {}, AccountId: {}, Error: {}",
                        request.getResourceId(), request.getExternalAccountId(), throwable.getMessage()))
                .subscribeOn(Schedulers.immediate()).subscribe(ReactorUtils.subscriber(this::handle));
    }

    @Override
    public String metadataVersionString() {
        return ENRICHMENT_VERSION;
    }

    protected void handle(Resource resource) {
        LOGGER.info("Completed processing update for resource: {}, accountID: {}", resource.getResourceId(), resource.getResourceKey().getExternalAccountId());
    }

    protected void upsertResource(String tenant, String externalAccountId, Resource resource) {
        boolean upsert = resourceService.upsert(tenant, externalAccountId, resource);
        if (!upsert) {
            throw new RuntimeException("could not upsert resource to ApiLayer");
        }
    }

    /**
     * Queue dedup task as a part of reconciliation phase if we are on the last page.
     *
     * @param request       {@link V} request containing details on the current task
     * @param nextPageToken the next page token found for this enrich list phase
     */
    protected void optionallyQueueDedupTask(V request, String nextPageToken) {
        if (StringUtils.isEmpty(nextPageToken) && request.isReconcile()) {
            // queue delayed dedup task
            queueService.queueDedupTaskForProject(ResourceDedupTaskMessage.builder()
                    .externalId(request.getExternalAccountId())
                    .tenantName(request.getTenant())
                    .projectId(request.getProjectId())
                    .resourceType(request.getResourceType())
                    .build());
        } else {
            LOGGER.debug("Not queueing dedup task due to state of request. Next Page Token: {}, Reconcile: {}", nextPageToken, request.isReconcile());
        }
    }
}
