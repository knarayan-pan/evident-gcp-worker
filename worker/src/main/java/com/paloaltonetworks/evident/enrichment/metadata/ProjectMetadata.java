package com.paloaltonetworks.evident.enrichment.metadata;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.paloaltonetworks.evident.data_access.models.ResourceMetadata;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ProjectMetadata extends ResourceMetadata {
    String orgId;
    IamPolicy iamPolicy;
    List<Network> networks;

    @Builder
    public ProjectMetadata(Map<String, String> tags,
                           String resourceName,
                           String projectId,
                           String version,
                           String orgId,
                           IamPolicy iamPolicy,
                           List<Network> networks)
    {
        super(tags, resourceName, projectId, version);
        this.orgId = orgId;
        this.iamPolicy = iamPolicy;
        this.networks = networks;
    }

    @Data
    @Builder
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonDeserialize(builder = Network.NetworkBuilder.class)
    public static class Network {
        String name;
        BigInteger id;
        String creationTimestamp;
        List<String> subnetworks;

        @JsonPOJOBuilder(withPrefix = "")
        public static final class NetworkBuilder {
        }
    }
}
