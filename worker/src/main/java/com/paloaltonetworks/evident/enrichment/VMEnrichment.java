package com.paloaltonetworks.evident.enrichment;

import com.google.api.services.compute.model.Instance;
import com.google.api.services.compute.model.InstanceAggregatedList;
import com.google.api.services.compute.model.Metadata;
import com.google.common.collect.Lists;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.data_access.service.MonitoredResourceService;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.metadata.InstanceMetadata;
import com.paloaltonetworks.evident.enrichment.request.ResourceEnrichmentRequest;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.gcp.models.request.FetchComputeInstancesRequest;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.redis.MonitoredRedisClient;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import com.paloaltonetworks.evident.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.paloaltonetworks.evident.utils.ResourceUtil.extractRegionFromZone;
import static com.paloaltonetworks.evident.utils.ResourceUtil.makeResourceId;

/**
 * Virtual machine enrichment
 */
@Slf4j
@Component
public class VMEnrichment extends GCPResourceEnrichment<InstanceAggregatedList, Instance> {
    private static final String FORMAT_TIME_ZONE = "T.*";

    VMEnrichment(@Qualifier(MonitoredResourceService.NAME) ResourceService resourceService,
                 @Qualifier(MonitoredRedisClient.NAME) RedisClient redisClient,
                 QueueService queueService,
                 GCPApiService gcpApiService) {
        super(resourceService, redisClient, queueService, gcpApiService);
    }

    @Override
    public String resourceType() {
        return ResourceType.GCE_INSTANCE.getResourceType();
    }

    @Override
    Instance findOneResource(final ResourceEnrichmentRequest request) {
        return gcpApiService.getComputeInstance(
                ResourceRefreshRequest.builder()
                        .tenant(request.getTenant())
                        .externalAccountId(request.getExternalAccountId())
                        .resourceType(resourceType())
                        .resourceId(request.getResourceId())
                        .resourceName(request.getResourceName())
                        .projectId(request.getProjectId())
                        .zone(request.getZone())
                        .build());
    }

    @Override
    InstanceAggregatedList findAllResources(final ResourceEnrichmentRequest request) {
        return gcpApiService.getComputeInstances(
                FetchComputeInstancesRequest.builder()
                        .externalAccountId(request.getExternalAccountId())
                        .tenant(request.getTenant())
                        .projectId(request.getProjectId())
                        .pageToken(request.getPageToken())
                        .build());
    }

    @Override
    List<Instance> extractGCPResourceList(final InstanceAggregatedList list) {
        return Optional.ofNullable(list.getItems())
                .map(Map::values)
                .orElse(Lists.newArrayList())
                .stream()
                .flatMap(list1 ->
                        Optional.ofNullable(list1.getInstances())
                                .map(Collection::stream)
                                .orElse(Stream.of()))
                .collect(Collectors.toList());
    }

    @Override
    String getNextPageToken(final InstanceAggregatedList list) {
        return list.getNextPageToken();
    }

    @Override
    Resource toResource(final Instance instance, final ResourceEnrichmentRequest request) {
        val resourceId = makeResourceId(request.getProjectId(), instance.getName());
        return Resource.builder()
                .cloudAppType(CloudAppType.GCP)
                .resourceId(resourceId)
                .tenant(request.getTenant())
                .region(extractRegionFromZone(instance.getZone()))
                .resourceKey(ResourceKey.builder()
                        .externalAccountId(request.getExternalAccountId())
                        .resourceType(resourceType())
                        .resourceIdHash(ResourceKey.computeResourceIdHash(String.valueOf(resourceId)))
                        .build())
                .resourceMetadata(JsonUtils.encode(InstanceMetadata.builder()
                        .canIpForward(Boolean.TRUE.equals(instance.getCanIpForward()))
                        .cpuPlatform(instance.getCpuPlatform())
                        .machineType(instance.getMachineType())
                        .zone(instance.getZone())
                        .metadata(Optional.ofNullable(instance.getMetadata())
                                .map(Metadata::getItems)
                                .map(items -> items
                                        .stream()
                                        .collect(Collectors.toMap(Metadata.Items::getKey, Metadata.Items::getValue)))
                                .orElse(null))
                        .createTimestamp(instance.getCreationTimestamp())
                        .description(instance.getDescription())
                        .id(instance.getId())
                        .projectId(request.getProjectId())
                        .name(instance.getName())
                        .selfLink(instance.getSelfLink())
                        .status(instance.getStatus())
                        .version(metadataVersionString())
                        .tags(instance.getLabels())
                        .build()))
                .build();
    }
}
