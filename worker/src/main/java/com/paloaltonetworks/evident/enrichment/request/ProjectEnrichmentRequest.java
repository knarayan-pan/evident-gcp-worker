package com.paloaltonetworks.evident.enrichment.request;

import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.gcp.models.request.TenantAwareRequest;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * This is a special use case where we'd need to refresh a project. Usually most resources would belong to a project in GCP
 * so we'd almost always want to use {@link ResourceEnrichmentRequest} instead.
 */
@Getter
@EqualsAndHashCode(callSuper = true)
@ToString
public class ProjectEnrichmentRequest extends EnrichmentRequest implements TenantAwareRequest {
    private final String projectName;
    private final String orgId;

    @Builder(toBuilder = true)
    public ProjectEnrichmentRequest(String resourceId, String tenant, String externalAccountId,
                                    String projectName, String orgId, Boolean queueSignatureTask, Boolean retriedTask, Boolean reconcile) {
        // for the project enrichment - the resourceID and projectId are the same
        super(resourceId, resourceId, resourceId, tenant, null /* no zone for project */, externalAccountId,
                ResourceType.GCP_PROJECT.getResourceType(),
                queueSignatureTask, retriedTask, null, null, reconcile);
        this.projectName = projectName;
        this.orgId = orgId;
    }
}
