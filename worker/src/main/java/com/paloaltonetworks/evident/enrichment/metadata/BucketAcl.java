package com.paloaltonetworks.evident.enrichment.metadata;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.api.services.storage.model.BucketAccessControl;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BucketAcl {
    private String bucket;
    private String domain;
    private String email;
    private String entity;
    private String entityId;
    private String id;
    private String role;
    private String selfLink;
    private ProjectTeam projectTeam;

    public BucketAcl(BucketAccessControl accessControl) {
        this.bucket = accessControl.getBucket();
        this.domain = accessControl.getDomain();
        this.email = accessControl.getEmail();
        this.entity = accessControl.getEntity();
        this.entityId = accessControl.getEntityId();
        this.id = accessControl.getId();
        this.role = accessControl.getRole();
        this.selfLink = accessControl.getSelfLink();
        this.projectTeam = accessControl.getProjectTeam() == null ? null : ProjectTeam.builder()
                .projectNumber(accessControl.getProjectTeam().getProjectNumber())
                .team(accessControl.getProjectTeam().getTeam())
                .build();
    }

}
