package com.paloaltonetworks.evident.enrichment;

import com.google.api.services.dns.model.ManagedZone;
import com.google.api.services.dns.model.ManagedZonesListResponse;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.data_access.service.MonitoredResourceService;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.metadata.DefaultKeySpecs;
import com.paloaltonetworks.evident.enrichment.metadata.DnsManagedZoneMetadata;
import com.paloaltonetworks.evident.enrichment.request.ResourceEnrichmentRequest;
import com.paloaltonetworks.evident.exceptions.GCPApiException;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.gcp.models.request.ManagedZoneRequest;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.redis.MonitoredRedisClient;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import com.paloaltonetworks.evident.utils.JsonUtils;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import reactor.core.CoreSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.lang.invoke.MethodHandles;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.paloaltonetworks.evident.utils.ResourceUtil.REGION_GLOBAL;
import static com.paloaltonetworks.evident.utils.ResourceUtil.makeResourceId;

@Component
public class DnsManagedZoneEnrichment extends GCPEnrichment<ResourceEnrichmentRequest> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final GCPApiService gcpApiService;
    private final QueueService queueService;
    private final ResourceService resourceService;

    DnsManagedZoneEnrichment(@Qualifier(MonitoredResourceService.NAME) ResourceService resourceService,
                             @Qualifier(MonitoredRedisClient.NAME) RedisClient redisClient,
                             QueueService queueService,
                             GCPApiService gcpApiService) {
        super(resourceService, redisClient, queueService);
        this.gcpApiService = gcpApiService;
        this.queueService = queueService;
        this.resourceService = resourceService;
    }

    @Override
    public Mono<Resource> enrich(ResourceEnrichmentRequest enrichmentRequest) {
        Optional<ManagedZone> managedZoneOptional = Optional.empty();
        try {
            managedZoneOptional = Optional.ofNullable(gcpApiService.getManagedZoneDetails(ManagedZoneRequest.builder()
                    .tenant(enrichmentRequest.getTenant())
                    .externalAccountId(enrichmentRequest.getExternalAccountId())
                    .projectId(enrichmentRequest.getProjectId())
                    .managedZone(enrichmentRequest.getResourceId())
                    .build()));
        } catch (GCPApiException e) {
            // check of this needs to be returned
            if (e.retryableException()) {
                throw e;
            }
            LOGGER.warn("Exception getting Managed zone details. Enrichment request={}", enrichmentRequest, e);
        }

        return Mono.justOrEmpty(managedZoneOptional)
                .switchIfEmpty(new Mono<ManagedZone>() {
                    @Override
                    public void subscribe(CoreSubscriber<? super ManagedZone> actual) {
                        LOGGER.info("Managed Zone not fetched. No instance with id: {} available. Ignoring..",
                                enrichmentRequest.getResourceId());
                    }
                })
                .map(managedZone -> resourceBuilder(enrichmentRequest, managedZone)).or(Mono.empty());
    }

    @Override
    public Flux<Resource> enrichList(ResourceEnrichmentRequest enrichmentRequest) {
        ManagedZonesListResponse managedZones = gcpApiService.getManagedZones(ManagedZoneRequest.builder()
                .tenant(enrichmentRequest.getTenant())
                .externalAccountId(enrichmentRequest.getExternalAccountId())
                .projectId(enrichmentRequest.getProjectId())
                .pageToken(enrichmentRequest.getPageToken())
                .build());
        if (!enrichmentRequest.getRetriedTask() && !ObjectUtils.isEmpty(managedZones)
                && StringUtils.isNotEmpty(managedZones.getNextPageToken())) {
            ResourceRefreshRequest resourceRefreshRequest = ResourceRefreshRequest.builder()
                    .organizationId(enrichmentRequest.getOrgId())
                    .resourceType(enrichmentRequest.getResourceType())
                    .tenant(enrichmentRequest.getTenant())
                    .externalAccountId(enrichmentRequest.getExternalAccountId())
                    .projectId(enrichmentRequest.getProjectId())
                    .build();
            queueService.queueResourceRefresh(resourceRefreshRequest.toBuilder().pageToken(managedZones.getNextPageToken()).build());
        }
        Set<Resource> resourceSet = new HashSet<>();
        if (!CollectionUtils.isEmpty(managedZones.getManagedZones())) {
            resourceSet = managedZones.getManagedZones()
                    .stream()
                    .map(managedZone -> resourceBuilder(enrichmentRequest, managedZone)).collect(Collectors.toSet());
        }
        // todo - this enrichment must reuse GCPResourceEnrichment abstract class in which case these boilerplate code will not be required
        optionallyQueueDedupTask(enrichmentRequest, managedZones.getNextPageToken());
        return Flux.fromIterable(resourceSet);
    }

    private Resource resourceBuilder(ResourceEnrichmentRequest request, ManagedZone managedZone) {
        val resourceId = makeResourceId(request.getProjectId(), managedZone.getName());
        String dnsConfigState = null;
        // dns key specs may be empty.
        List<DefaultKeySpecs> defaultKeySpecsList = Collections.emptyList();
        if(!ObjectUtils.isEmpty(managedZone.getDnssecConfig())) {
            defaultKeySpecsList = CollectionUtils.isEmpty(managedZone.getDnssecConfig().getDefaultKeySpecs()) ?
                    Collections.emptyList() : managedZone
                    .getDnssecConfig()
                    .getDefaultKeySpecs()
                    .stream()
                    .map(DefaultKeySpecs::new)
                    .collect(Collectors.toList());
            dnsConfigState = managedZone.getDnssecConfig().getState();
        }

        return Resource.builder()
                .cloudAppType(CloudAppType.GCP)
                .resourceId(resourceId)
                .tenant(request.getTenant())
                .region(REGION_GLOBAL)
                .resourceKey(ResourceKey.builder()
                        .externalAccountId(request.getExternalAccountId())
                        .resourceType(resourceType())
                        .resourceIdHash(ResourceKey.computeResourceIdHash(resourceId))
                        .build())
                .resourceMetadata(JsonUtils.encode(DnsManagedZoneMetadata.builder()
                        .projectId(request.getProjectId())
                        .resourceName(managedZone.getName())
                        .tags(managedZone.getLabels())
                        .version(metadataVersionString())
                        .dnsSecConfigState(dnsConfigState)
                        .defaultKeySpecs(defaultKeySpecsList)
                        .dnsName(managedZone.getDnsName())
                        .id(String.valueOf(managedZone.getId()))
                        .build()))
                .build();
    }

    @Override
    public String resourceType() {
        return ResourceType.DNS_MANAGED_ZONE.getResourceType();
    }
}
