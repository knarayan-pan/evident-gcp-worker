package com.paloaltonetworks.evident.enrichment.request;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode(callSuper = true)
@ToString
public class ResourceEnrichmentRequest extends EnrichmentRequest {
    private final String orgId;

    @Builder(toBuilder = true)
    public ResourceEnrichmentRequest(String resourceId, String resourceName, String projectId, String tenant, String zone,
                                     String externalAccountId, String resourceType,
                                     String pageToken, Integer pageSize, String orgId, Boolean queueSignatureTask,
                                     Boolean retriedTask, Boolean reconcile) {
        super(resourceId, resourceName, projectId, tenant, zone, externalAccountId, resourceType,
                queueSignatureTask, retriedTask, pageToken, pageSize, reconcile);
        this.orgId = orgId;
    }
}
