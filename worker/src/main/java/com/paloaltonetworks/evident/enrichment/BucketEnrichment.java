package com.paloaltonetworks.evident.enrichment;

import com.google.api.services.storage.model.Bucket;
import com.google.api.services.storage.model.Buckets;
import com.google.api.services.storage.model.Policy;
import com.google.common.base.Predicates;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.data_access.service.MonitoredResourceService;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.metadata.BucketAcl;
import com.paloaltonetworks.evident.enrichment.metadata.BucketMetadata;
import com.paloaltonetworks.evident.enrichment.metadata.BucketPolicy;
import com.paloaltonetworks.evident.enrichment.request.ResourceEnrichmentRequest;
import com.paloaltonetworks.evident.exceptions.GCPApiException;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.gcp.models.request.FetchBucketsRequest;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.gcp.models.request.SignatureRequest;
import com.paloaltonetworks.evident.redis.MonitoredRedisClient;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import com.paloaltonetworks.evident.utils.JsonUtils;
import com.paloaltonetworks.evident.utils.ReactorUtils;
import lombok.val;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import reactor.core.CoreSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.lang.invoke.MethodHandles;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.paloaltonetworks.evident.schedule.ReconciliationScheduler.DEFAULT_RECONCILIATION_INTERVAL_IN_HOURS;
import static com.paloaltonetworks.evident.utils.ResourceUtil.makeResourceId;

@Component
public class BucketEnrichment extends GCPEnrichment<ResourceEnrichmentRequest> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final GCPApiService gcpApiService;
    private final QueueService queueService;
    private final ResourceService resourceService;
    private final RedisClient redisClient;

    @Autowired
    BucketEnrichment(@Qualifier(MonitoredResourceService.NAME) ResourceService resourceService,
                     @Qualifier(MonitoredRedisClient.NAME) RedisClient redisClient,
                     QueueService queueService,
                     GCPApiService gcpApiService) {
        super(resourceService, redisClient, queueService);
        this.gcpApiService = gcpApiService;
        this.queueService = queueService;
        this.resourceService = resourceService;
        this.redisClient = redisClient;
    }

    @Override
    public Mono<Resource> enrich(ResourceEnrichmentRequest enrichmentRequest) {
        Optional<Bucket> bucketOptional = Optional.empty();
        // if the resource does not exist then do not do anything
        try {
            bucketOptional = Optional.ofNullable(gcpApiService.getBucket(ResourceRefreshRequest.builder()
                    .tenant(enrichmentRequest.getTenant())
                    .externalAccountId(enrichmentRequest.getExternalAccountId())
                    .resourceType(resourceType())
                    .resourceId(enrichmentRequest.getResourceId())
                    .build()));
        } catch (GCPApiException e) {
            // check of this needs to be returned
            if (e.retryableException()) {
                throw e;
            }
        }

        return Mono.justOrEmpty(bucketOptional)
                .switchIfEmpty(new Mono<Bucket>() {
                    @Override
                    public void subscribe(CoreSubscriber<? super Bucket> actual) {
                        LOGGER.info("Bucket not fetched. No bucket with id: {} available. Ignoring..",
                                enrichmentRequest.getResourceId());
                    }
                })
                .flatMap((Function<Bucket, Mono<Resource>>) bucket -> {
                    Policy bucketPolicy = gcpApiService.getBucketPolicy(ResourceRefreshRequest.builder()
                            .resourceId(bucket.getId())
                            .resourceType(resourceType())
                            .externalAccountId(enrichmentRequest.getExternalAccountId())
                            .tenant(enrichmentRequest.getTenant())
                            .build());
                    return Mono.just(resourceBuilder(enrichmentRequest.getTenant(),
                            enrichmentRequest.getExternalAccountId(), enrichmentRequest.getProjectId(), bucket, bucketPolicy));
                }).or(Mono.empty());
    }

    @Override
    public String resourceType() {
        return ResourceType.GCS_STORAGE.getResourceType();
    }

    @Override
    public Flux<Resource> enrichList(ResourceEnrichmentRequest request) {
        ResourceRefreshRequest resourceRefreshRequest = ResourceRefreshRequest.builder()
                .organizationId(request.getOrgId())
                .resourceType(request.getResourceType())
                .tenant(request.getTenant())
                .externalAccountId(request.getExternalAccountId())
                .projectId(request.getProjectId())
                .build();
        Buckets bucketList = gcpApiService.getBuckets(FetchBucketsRequest.builder()
                .externalAccountId(request.getExternalAccountId())
                .tenant(request.getTenant())
                .projectId(request.getProjectId())
                .pageToken(request.getPageToken())
                .build());
        // queue next page of work if it exists and this task is not a retry
        if (!request.getRetriedTask() && !ObjectUtils.isEmpty(bucketList) && StringUtils.isNotEmpty(bucketList.getNextPageToken())) {
            queueService.queueResourceRefresh(resourceRefreshRequest.toBuilder().pageToken(bucketList.getNextPageToken()).build());
        }

        Set<Resource> resourceSet = new HashSet<>();
        if (CollectionUtils.isNotEmpty(bucketList.getItems())) {
            bucketList.getItems().forEach(bucket -> {
                try {
                    Policy bucketPolicy = gcpApiService.getBucketPolicy(ResourceRefreshRequest.builder()
                            .resourceId(bucket.getId())
                            .resourceType(resourceType())
                            .externalAccountId(request.getExternalAccountId())
                            .tenant(request.getTenant())
                            .build());
                    resourceSet.add(resourceBuilder(request.getTenant(), request.getExternalAccountId(), request.getProjectId(), bucket, bucketPolicy));
                } catch (GCPApiException e) {
                    LOGGER.warn("Received error for refreshing a single bucket's iampolicy. Requeuing this task for resourceId: {}. Error: {}", bucket.getId(), e);
                    queueService.queueResourceRefresh(resourceRefreshRequest.toBuilder().resourceId(bucket.getId()).build());
                }
            });
        }
        optionallyQueueDedupTask(request, bucketList.getNextPageToken());
        return CollectionUtils.isEmpty(resourceSet) ? Flux.empty() : Flux.fromIterable(resourceSet);
    }

    /**
     * Need to do this optimization to ensure that target log buckets dont get flagged incorrectly as a fail for access logging check
     * as some buckets that do not have logging can be target log buckets for other buckets. So ideally we'd want to wait for the full
     * onboarding sync to happen and then analyze the logging configuration to prevent race conditions
     *
     * @param request Enrichment request
     */
    @Override
    public void applyForCollection(ResourceEnrichmentRequest request) {
        // process current page
        enrichList(request)
                .flatMap((Function<Resource, Mono<Resource>>) resource -> {
                    Mono<Resource> resourceMono = resourceService.findOne(request.getTenant(), resource.getResourceKey())
                            .map((Function<Resource, Mono<Resource>>)
                                    fromResource -> resource.isSimilar(fromResource) ?
                                            Mono.empty() : Mono.just(resource)
                            ).orElse(Mono.just(resource));
                    // publish redis marker if in recon phase
                    if (request.isReconcile()) {
                        // push key to redis
                        val key = resource.getResourceKey().redisKeyString();
                        LOGGER.debug("Pushing resource key: {} to redis for reconciliation state checking...", key);
                        redisClient.setKey(key, Boolean.TRUE.toString()
                                , Math.toIntExact(TimeUnit.HOURS.toSeconds(DEFAULT_RECONCILIATION_INTERVAL_IN_HOURS)));

                    }
                    return resourceMono;
                })
                .switchIfEmpty(new Mono<Resource>() {
                    @Override
                    public void subscribe(CoreSubscriber<? super Resource> actual) {
                        LOGGER.debug("No update to db required for resource: {}", request.getResourceId());
                    }
                })
                .doOnNext(resource -> redisClient.getResourceLock(request.getExternalAccountId(), resource.getResourceId()))
                // upsert to db
                .doOnNext(resource -> upsertResource(request.getTenant(), request.getExternalAccountId(), resource))
                .doOnNext(resource -> {
                    // queue after a buffer to ensure all other buckets are enriched
                    // queue sqs task to run signatures on this resource
                    LOGGER.info("Queueing task to scan signature for bucket: {}, project: {}", resource.getResourceId(), request.getProjectId());
                    queueService.queueSignatureForResourceWithDelay(SignatureRequest.builder()
                            .externalAccountId(request.getExternalAccountId())
                            .tenant(request.getTenant())
                            .projectId(request.getProjectId())
                            .resourceId(resource.getResourceId())
                            .resourceType(request.getResourceType())
                            .build(), 300);
                })
                .doOnError(throwable -> LOGGER.error("Error encountered in updating resource to cassandra. ResourceId: {}, AccountId: {}, Error: {}",
                        request.getResourceId(), request.getExternalAccountId(), throwable.getMessage(), throwable))
                .subscribeOn(Schedulers.immediate()).subscribe(ReactorUtils.subscriber(this::handle));
    }

    private Resource resourceBuilder(String tenant, String externalAccountId, String projectId, Bucket bucket, Policy bucketPolicy) {
        val resourceId = makeResourceId(projectId, bucket.getId());
        return Resource.builder()
                .cloudAppType(CloudAppType.GCP)
                .resourceId(resourceId)
                .tenant(tenant)
                .resourceKey(ResourceKey.builder()
                        .externalAccountId(externalAccountId)
                        .resourceType(resourceType())
                        .resourceIdHash(ResourceKey.computeResourceIdHash(resourceId))
                        .build())
                .region(bucket.getLocation())
                .resourceMetadata(JsonUtils.encode(BucketMetadata.builder()
                        .projectId(projectId)
                        .location(bucket.getLocation())
                        .storageClass(bucket.getStorageClass())
                        .tags(bucket.getLabels())
                        .version(metadataVersionString())
                        .bucketAccessControl(CollectionUtils.isEmpty(bucket.getAcl()) ? null :
                                bucket.getAcl().stream().filter(Predicates.notNull()).map(BucketAcl::new).collect(Collectors.toList()))
                        .policy(bucketPolicy == null ? null : new BucketPolicy(bucketPolicy))
                        .logBucket(bucket.getLogging() == null ? null : bucket.getLogging().getLogBucket())
                        .logObjectPrefix(bucket.getLogging() == null ? null : bucket.getLogging().getLogObjectPrefix())
                        .build())).build();

    }

}
