package com.paloaltonetworks.evident.enrichment;

import static com.paloaltonetworks.evident.utils.ResourceUtil.makeResourceId;

import com.google.api.services.compute.model.Network;
import com.google.api.services.compute.model.NetworkList;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.data_access.service.MonitoredResourceService;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.metadata.NetworkMetadata;
import com.paloaltonetworks.evident.enrichment.request.ResourceEnrichmentRequest;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.redis.MonitoredRedisClient;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import com.paloaltonetworks.evident.utils.JsonUtils;
import com.paloaltonetworks.evident.utils.ResourceUtil;
import lombok.val;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

// GCE Network enrichment
@Component
public class NetworkEnrichment extends GCPResourceEnrichment<NetworkList, Network> {

    NetworkEnrichment(@Qualifier(MonitoredResourceService.NAME) ResourceService resourceService,
                      @Qualifier(MonitoredRedisClient.NAME) RedisClient redisClient,
                      QueueService queueService,
                      GCPApiService gcpApiService) {

        super(resourceService, redisClient, queueService, gcpApiService);
    }

    @Override
    public String resourceType() {
        return ResourceType.GCE_NETWORK.getResourceType();
    }

    @Override
    NetworkList findAllResources(ResourceEnrichmentRequest request) {
        return gcpApiService.getNetworks(ResourceRefreshRequest.builder()
                .externalAccountId(request.getExternalAccountId())
                .tenant(request.getTenant())
                .projectId(request.getProjectId())
                .pageToken(request.getPageToken())
                .build());
    }

    @Override
    List<Network> extractGCPResourceList(NetworkList list) {
        return Optional.ofNullable(list.getItems()).orElse(Collections.emptyList());
    }

    @Override
    Network findOneResource(ResourceEnrichmentRequest request) {
        return gcpApiService.getNetwork(ResourceRefreshRequest.builder()
                .tenant(request.getTenant())
                .externalAccountId(request.getExternalAccountId())
                .resourceType(resourceType())
                .resourceId(request.getResourceId())
                .resourceName(request.getResourceName())
                .projectId(request.getProjectId())
                .zone(request.getZone())
                .build());
    }

    @Override
    Resource toResource(Network instance, ResourceEnrichmentRequest request) {
        val resourceId = makeResourceId(request.getProjectId(), instance.getName());
        val convertedPeerings = instance.getPeerings().stream()
                .map(np -> NetworkMetadata.Peering.builder()
                        .autoCreateRoutes(np.getAutoCreateRoutes())
                        .name(np.getName())
                        .network(np.getNetwork())
                        .state(np.getState())
                        .stateDetails(np.getStateDetails())
                        .build()
                )
                .collect(Collectors.toList());
        return Resource.builder()
                .cloudAppType(CloudAppType.GCP)
                .resourceId(resourceId)
                .tenant(request.getTenant())
                .region(ResourceUtil.REGION_GLOBAL)
                .resourceKey(ResourceKey.builder()
                        .externalAccountId(request.getExternalAccountId())
                        .resourceType(resourceType())
                        .resourceIdHash(ResourceKey.computeResourceIdHash(resourceId))
                        .build())
                .resourceMetadata(JsonUtils.encode(
                        NetworkMetadata.builder()
                                .iPv4Range(instance.getIPv4Range())
                                .gatewayIPv4(instance.getGatewayIPv4())
                                .selfLink(instance.getSelfLink())
                                .autoCreateSubnetworks(instance.getAutoCreateSubnetworks())
                                .subnetworks(instance.getSubnetworks())
                                .peerings(convertedPeerings)
                                .routingConfig(NetworkMetadata.RoutingConfig.builder()
                                        .routingMode(instance.getRoutingConfig().getRoutingMode())
                                        .build()
                                )
                                .kind(instance.getKind())
                                .projectId(request.getProjectId())
                                .resourceName(instance.getName())
                                .creationTimestamp(instance.getCreationTimestamp())
                                .description(instance.getDescription())
                                .build()
                        )
                )
                .build();
    }

    @Override
    String getNextPageToken(NetworkList list) {
        return list.getNextPageToken();
    }
}
