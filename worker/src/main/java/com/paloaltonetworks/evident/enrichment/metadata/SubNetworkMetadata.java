package com.paloaltonetworks.evident.enrichment.metadata;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.paloaltonetworks.evident.data_access.models.ResourceMetadata;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigInteger;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SubNetworkMetadata extends ResourceMetadata {
    private final String kind;
    private final BigInteger id;
    private final String creationTimeStamp;
    private final String network;
    private final String ipCidrRanges;
    private final String gatewayAddress;
    private final String region;
    private final String selfLink;
    private final boolean privateIpGoogleAddress;
    private final String fingerprint;
    private final boolean allowSubnetCidrRoutesOverlap;
    private final boolean enableFlowLogs;

    @Builder
    public SubNetworkMetadata(Map<String, String> tags, String resourceName, String projectId,
                              String version, String kind, BigInteger id, String creationTimeStamp,
                              String network, String ipCidrRanges, String gatewayAddress, String region,
                              String selfLink, boolean privateIpGoogleAddress, String fingerprint,
                              boolean allowSubnetCidrRoutesOverlap, boolean enableFlowLogs) {
        super(tags, resourceName, projectId, version);
        this.kind = kind;
        this.id = id;
        this.creationTimeStamp = creationTimeStamp;
        this.network = network;
        this.ipCidrRanges = ipCidrRanges;
        this.gatewayAddress = gatewayAddress;
        this.region = region;
        this.selfLink = selfLink;
        this.privateIpGoogleAddress = privateIpGoogleAddress;
        this.fingerprint = fingerprint;
        this.allowSubnetCidrRoutesOverlap = allowSubnetCidrRoutesOverlap;
        this.enableFlowLogs = enableFlowLogs;
    }
}
