package com.paloaltonetworks.evident.enrichment.metadata;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.api.services.dns.model.DnsKeySpec;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.annotation.Nonnull;

@Data
@AllArgsConstructor
@Builder(toBuilder = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DefaultKeySpecs {

    private final String kind;
    private final String keyType;
    private final String algorithm;
    private final Long keyLength;

    public DefaultKeySpecs(@Nonnull DnsKeySpec defaultKeySpecs) {
        this.kind = defaultKeySpecs.getKind();
        this.keyType = defaultKeySpecs.getKeyType();
        this.algorithm = defaultKeySpecs.getAlgorithm();
        this.keyLength = defaultKeySpecs.getKeyLength();
    }
}
