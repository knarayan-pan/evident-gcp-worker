package com.paloaltonetworks.evident.enrichment.metadata;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.paloaltonetworks.evident.data_access.models.ResourceMetadata;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class DnsManagedZoneMetadata extends ResourceMetadata {

    private final String dnsSecConfigState;
    private final String dnsName;
    private final String description;
    private final String id;
    private final List<DefaultKeySpecs> defaultKeySpecsList;


    @Builder
    public DnsManagedZoneMetadata(Map<String, String> tags, String resourceName, String projectId, String version,
                                  String dnsSecConfigState, String dnsName, String description,
                                  String id, List<DefaultKeySpecs> defaultKeySpecs) {
        super(tags, resourceName, projectId, version);
        this.dnsSecConfigState = dnsSecConfigState;
        this.dnsName = dnsName;
        this.description = description;
        this.id = id;
        this.defaultKeySpecsList = defaultKeySpecs;
    }
}
