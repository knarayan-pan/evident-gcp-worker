package com.paloaltonetworks.evident.enrichment;


import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.enrichment.request.EnrichmentRequest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


public interface Enrichment<V extends EnrichmentRequest> {

    /**
     * Create a resource metadata object based on data received from the apiResponse object.
     *
     * @param enrichmentRequest list of parameters required to make the API calls from the cloud to extract required information
     * @return a Mono object containing the extracted information.
     */
    Mono<Resource> enrich(V enrichmentRequest);

    /**
     * Create a list of resource metadata objects based on data received from the apiResponse object list
     *
     * @param enrichmentRequest list of parameters required to make the API calls from the cloud to extract required information
     * @return a Flux object containing the set of enriched resource metadata objects
     */
    Flux<Resource> enrichList(V enrichmentRequest);

    /**
     * set the resource type that is mapped to this enrichment flow
     *
     * @return String containing the resource type - see {@link com.paloaltonetworks.evident.gcp.api.ResourceType}
     */
    String resourceType();

    /**
     * Set metadata version number - this is useful if we need to retrigger signature scans for new signatures for an existing resource enrichment type
     *
     * @return String indicating the version of the enrichment for that resource type
     */
    String metadataVersionString();

    /**
     * Apply the enrichment to downstream actions if its a single object
     *
     * @param request Enrichment request
     */
    void apply(V request);

    /**
     * Apply the enrichment for a list of resources of the same type. Used for api call optimization
     * where in a single list call we get most of the required information for enrichment.
     * <p>
     * Important: The implementation of this method is required to also queue the next page's task using
     * {@link com.paloaltonetworks.evident.service.QueueService}
     * <p>
     * <b>Important:</b> The default expected behavior of this method would be to use the implementation of the {@link Enrichment#enrichList(EnrichmentRequest)} method
     * to persist resource data to persistent stores if required and correspondingly trigger any downstream tasks. In specific cases - it
     * might be overridden by a specific implementation to only queue resource level tasks instead of handling them inline in a page. The following use case cases
     * provide an insight on how this method can be used
     * <p>
     * <b>Use case 1:</b> when we need to refresh a specific resource type and a single API call provides most of the data required to persist.
     * This will save unnecessary queueing.
     * <b>Use case 2:</b> For resource types where fetching related data is more involved (multiple API calls). In such cases -
     * this method could just be used to distribute work as tasks by queuing work for individual resources
     *
     * @param request Enrichment request
     */
    void applyForCollection(V request);

}
