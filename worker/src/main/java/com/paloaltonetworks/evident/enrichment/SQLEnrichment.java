package com.paloaltonetworks.evident.enrichment;

import com.google.api.client.util.DateTime;
import com.google.api.services.sqladmin.model.BackupConfiguration;
import com.google.api.services.sqladmin.model.DatabaseInstance;
import com.google.api.services.sqladmin.model.InstancesListResponse;
import com.google.api.services.sqladmin.model.IpConfiguration;
import com.google.api.services.sqladmin.model.Settings;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.data_access.service.MonitoredResourceService;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.metadata.SQLInstanceMetadata;
import com.paloaltonetworks.evident.enrichment.request.ResourceEnrichmentRequest;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.gcp.models.request.FetchSQLInstancesRequest;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.redis.MonitoredRedisClient;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import com.paloaltonetworks.evident.util.ImmutableMapBuilder;
import com.paloaltonetworks.evident.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.paloaltonetworks.evident.utils.ResourceUtil.REGION_GLOBAL;
import static com.paloaltonetworks.evident.utils.ResourceUtil.makeResourceId;

/**
 * Virtual machine enrichment
 */
@Slf4j
@Component
public class SQLEnrichment extends GCPResourceEnrichment<InstancesListResponse, DatabaseInstance> {

    SQLEnrichment(@Qualifier(MonitoredResourceService.NAME) ResourceService resourceService,
                  @Qualifier(MonitoredRedisClient.NAME) RedisClient redisClient,
                  QueueService queueService,
                  GCPApiService gcpApiService) {
        super(resourceService, redisClient, queueService, gcpApiService);
    }

    @Override
    public String resourceType() {
        return ResourceType.GCE_SQL.getResourceType();
    }

    @Override
    String getNextPageToken(final InstancesListResponse list) {
        return list.getNextPageToken();
    }

    @Override
    Resource toResource(final DatabaseInstance instance, final ResourceEnrichmentRequest request) {
        val resourceId = makeResourceId(request.getProjectId(), instance.getName());
        return Resource.builder()
                .cloudAppType(CloudAppType.GCP)
                .resourceId(resourceId)
                .tenant(request.getTenant())
                .region(Optional.ofNullable(instance.getRegion()).map(String::toLowerCase).orElse(REGION_GLOBAL))
                .resourceKey(ResourceKey.builder()
                        .externalAccountId(request.getExternalAccountId())
                        .resourceType(resourceType())
                        .resourceIdHash(ResourceKey.computeResourceIdHash(resourceId))
                        .build())
                .resourceMetadata(JsonUtils.encode(SQLInstanceMetadata.builder()
                        .autobackupEnabled(Optional.ofNullable(instance.getSettings())
                                .map(Settings::getBackupConfiguration)
                                .map(BackupConfiguration::getEnabled)
                                .orElse(false))
                        .authorizedNetworks(getAuthorizedNetworks(instance.getSettings()))
                        .requireSSL(Optional.ofNullable(instance.getSettings())
                                .map(Settings::getIpConfiguration)
                                .map(IpConfiguration::getRequireSsl)
                                .orElse(false))
                        .connectionName(instance.getConnectionName())
                        .databaseVersion(instance.getDatabaseVersion())
                        .region(instance.getRegion())
                        .selfLink(instance.getSelfLink())
                        .resourceName(instance.getName())
                        .zone(instance.getGceZone())
                        .version(metadataVersionString())
                        .projectId(request.getProjectId())
                        .build()))
                .build();
    }

    @Override
    List<DatabaseInstance> extractGCPResourceList(final InstancesListResponse list) {
        return Optional.ofNullable(list.getItems()).orElse(Collections.emptyList());
    }

    @Override
    DatabaseInstance findOneResource(final ResourceEnrichmentRequest request) {
        return gcpApiService.getSQLInstance(
                ResourceRefreshRequest.builder()
                        .tenant(request.getTenant())
                        .externalAccountId(request.getExternalAccountId())
                        .resourceType(resourceType())
                        .resourceId(request.getResourceId())
                        .resourceId(request.getResourceId())
                        .resourceName(request.getResourceName())
                        .projectId(request.getProjectId())
                        .zone(request.getZone())
                        .build());
    }

    @Override
    InstancesListResponse findAllResources(final ResourceEnrichmentRequest request) {
        return gcpApiService.getSQLInstances(
                FetchSQLInstancesRequest.builder()
                        .externalAccountId(request.getExternalAccountId())
                        .tenant(request.getTenant())
                        .projectId(request.getProjectId())
                        .pageToken(request.getPageToken())
                        .build());
    }

    private List<Map<String, Object>> getAuthorizedNetworks(Settings instanceSettings) {
        val aclEntryList = Optional.ofNullable(instanceSettings)
                .map(Settings::getIpConfiguration)
                .map(IpConfiguration::getAuthorizedNetworks)
                .orElse(Collections.emptyList());
        return aclEntryList.stream().map(aclEntry -> new ImmutableMapBuilder<String, Object>()
                .put("name", aclEntry.getName())
                .put("value", aclEntry.getValue())
                .put("expirationTime", Optional.ofNullable(aclEntry.getExpirationTime())
                        .map(DateTime::toStringRfc3339)
                        .orElse(null))
                .build()).collect(Collectors.toList());
    }

}
