package com.paloaltonetworks.evident.enrichment;

import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.request.EnrichmentRequest;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.lang.invoke.MethodHandles;

/**
 * Catch unknown resource type enrichment
 */
public class NoOpEnrichment extends GCPEnrichment {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public NoOpEnrichment() {
        this(null, null, null);
    }

    private NoOpEnrichment(ResourceService resourceService, RedisClient redisClient, QueueService queueService) {
        super(resourceService, redisClient, queueService);
    }

    @Override
    public Mono<Resource> enrich(EnrichmentRequest enrichmentRequest) {
        return Mono.empty();
    }

    @Override
    public Flux<Resource> enrichList(EnrichmentRequest enrichmentRequest) {
        return Flux.empty();
    }

    @Override
    public String resourceType() {
        return null;
    }

    @Override
    public void apply(EnrichmentRequest request) {
        LOGGER.warn("Resource type: {} is not supported for enrichment. Ignoring", request.getResourceType());
    }

    @Override
    public void applyForCollection(EnrichmentRequest request) {
        LOGGER.warn("Resource type: {} is not supported for enrichment. Ignoring", request.getResourceType());
    }
}
