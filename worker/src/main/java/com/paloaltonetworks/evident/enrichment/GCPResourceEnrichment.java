package com.paloaltonetworks.evident.enrichment;

import com.google.common.collect.Lists;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.request.ResourceEnrichmentRequest;
import com.paloaltonetworks.evident.exceptions.GCPApiException;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import reactor.core.CoreSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Enrichment for GCP resources
 *
 * @param <RESOURCE_LIST_TYPE> The cloud class type that identifies a list of GCP resources
 * @param <RESOURCE_TYPE>      The cloud class type that identifies a single GCP resource
 */
@Slf4j
public abstract class GCPResourceEnrichment<RESOURCE_LIST_TYPE, RESOURCE_TYPE> extends GCPEnrichment<ResourceEnrichmentRequest> {
    protected final GCPApiService gcpApiService;

    GCPResourceEnrichment(ResourceService resourceService, RedisClient redisClient, QueueService queueService,
                          GCPApiService gcpApiService) {
        super(resourceService, redisClient, queueService);
        this.gcpApiService = gcpApiService;
    }

    /**
     * Get the next page link from the current list of resources
     */
    abstract String getNextPageToken(final RESOURCE_LIST_TYPE list);

    /**
     * Convert GCP instance to Resource domain model
     */
    abstract Resource toResource(final RESOURCE_TYPE inst, final ResourceEnrichmentRequest request);

    /**
     * Get a list of GCP resources from the GCP resource list object
     */
    abstract List<RESOURCE_TYPE> extractGCPResourceList(RESOURCE_LIST_TYPE list);

    /**
     * Find the gcp resource by the request. Implement this method in subclasses to return the
     *
     * @param request {@link ResourceEnrichmentRequest}
     */
    abstract RESOURCE_TYPE findOneResource(final ResourceEnrichmentRequest request);

    /**
     * Find a list of gcp resource types by the request (encapsulated in gcp list type).
     * Implement this method in sub-enrichments to return GCP list object
     *
     * @param request {@link ResourceEnrichmentRequest}
     */
    abstract RESOURCE_LIST_TYPE findAllResources(final ResourceEnrichmentRequest request);

    /**
     * Enrichment for a list of resources.
     * 1. Find all resources from GCP - each sub-enrichment implements the specific list call.
     * 2. Submit the next page if a next page link exists.
     * 3. Collect the resources to set and return as a flux
     *
     * @param request {@link ResourceEnrichmentRequest}
     * @return
     */
    @Override
    public Flux<Resource> enrichList(ResourceEnrichmentRequest request) {
        RESOURCE_LIST_TYPE list = findAllResources(request);

        // queue next page of work if it exists and this task is not a retry
        if (!request.getRetriedTask() && !ObjectUtils.isEmpty(list) && StringUtils.isNotEmpty(getNextPageToken(list))) {
            ResourceRefreshRequest resourceRefreshRequest = ResourceRefreshRequest.builder()
                    .organizationId(request.getOrgId())
                    .resourceType(request.getResourceType())
                    .tenant(request.getTenant())
                    .externalAccountId(request.getExternalAccountId())
                    .projectId(request.getProjectId())
                    .build();
            queueService.queueResourceRefresh(resourceRefreshRequest.toBuilder().pageToken(getNextPageToken(list)).build());
        }
        // queue dedup task if this is partof reconciliation phase
        optionallyQueueDedupTask(request, getNextPageToken(list));
        val resourceSet = Optional.ofNullable(extractGCPResourceList(list))
                .orElse(Lists.newArrayList())
                .stream()
                .map(inst -> toResource(inst, request))
                .collect(Collectors.toSet());
        return Flux.fromIterable(resourceSet);
    }


    /**
     * Enrich a single resource
     *
     * @param request {@link ResourceEnrichmentRequest}
     */
    public Mono<Resource> enrich(ResourceEnrichmentRequest request) {
        Optional<RESOURCE_TYPE> instanceOptional = Optional.empty();
        try {
            instanceOptional = Optional.ofNullable(findOneResource(request));
        } catch (GCPApiException e) {
            // check of this needs to be returned
            if (e.retryableException()) {
                throw e;
            }
            log.warn("Exception getting resource. Enrichment request={}", request, e);
        }

        return Mono.justOrEmpty(instanceOptional)
                .switchIfEmpty(new Mono<RESOURCE_TYPE>() {
                    @Override
                    public void subscribe(CoreSubscriber<? super RESOURCE_TYPE> actual) {
                        log.info("Instance not fetched. No instance {} with id: {} available. Ignoring..",
                                request.getResourceType(),
                                request.getResourceId());
                    }
                })
                .map(inst -> toResource(inst, request))
                .or(Mono.empty());
    }
}
