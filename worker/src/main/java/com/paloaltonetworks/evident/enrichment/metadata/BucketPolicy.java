package com.paloaltonetworks.evident.enrichment.metadata;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.api.services.storage.model.Policy;
import com.google.common.base.Predicates;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.collections.CollectionUtils;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@Builder(toBuilder = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BucketPolicy {
    private String kind;
    private String resourceId;
    private List<PolicyBindings> bindings;

    public BucketPolicy(@Nonnull Policy policy) {
        this.kind = policy.getKind();
        this.resourceId = policy.getResourceId();
        this.bindings = CollectionUtils.isEmpty(policy.getBindings()) ? null :
                policy.getBindings().stream().filter(Predicates.notNull()).map(PolicyBindings::new).collect(Collectors.toList());
    }
}
