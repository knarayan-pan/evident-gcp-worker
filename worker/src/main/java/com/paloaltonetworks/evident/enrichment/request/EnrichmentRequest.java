package com.paloaltonetworks.evident.enrichment.request;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;

/**
 * Enrichment request base class that can be extended by individual resource types;
 */
@Getter
public class EnrichmentRequest {

    final String resourceId;
    final String resourceName;
    final String projectId;
    final String zone;
    final String tenant;
    final String externalAccountId;
    final String resourceType;
    // true unless explicitly stated otherwise.
    final Boolean queueSignatureTask;
    // flag to indicate this request is a retry in which case we may not want to queue subsequent pages.
    final Boolean retriedTask;
    // used for cases where we need to fetch a list of items from the cloud
    final String pageToken;
    final Integer pageSize;
    final Boolean reconcile;

    EnrichmentRequest(String resourceId, String resourceName, String projectId, String tenant, String zone, String externalAccountId, String resourceType,
                      Boolean queueSignatureTask, Boolean retriedTask, String pageToken, Integer pageSize, Boolean reconcile) {
        this.resourceId = resourceId;
        this.resourceName = resourceName;
        this.projectId = projectId;
        this.tenant = tenant;
        this.zone = zone;
        this.externalAccountId = externalAccountId;
        this.resourceType = resourceType;
        this.queueSignatureTask = queueSignatureTask == null ? Boolean.TRUE : queueSignatureTask;
        this.retriedTask = retriedTask == null ? false : retriedTask;
        this.pageToken = pageToken;
        this.pageSize = pageSize == null ? 20 : pageSize;
        this.reconcile = reconcile;
    }

    @JsonIgnore
    public boolean isReconcile() {
        return Boolean.TRUE.equals(reconcile);
    }
}
