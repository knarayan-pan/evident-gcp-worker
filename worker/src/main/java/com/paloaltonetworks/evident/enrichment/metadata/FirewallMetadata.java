package com.paloaltonetworks.evident.enrichment.metadata;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.paloaltonetworks.evident.data_access.models.ResourceMetadata;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class FirewallMetadata extends ResourceMetadata {
    @NonNull
    private final String selfLink;
    @NotNull
    private final String creationTimeStamp;
    @NotNull
    private final String direction;
    @NotNull
    private final BigInteger id;
    private final boolean disabled;

    private final String network;
    private final Integer priority;
    private final List<RulesMetadata> allowedRules;
    private final List<RulesMetadata> deniedRules;

    @Builder
    public FirewallMetadata(Map<String, String> tags,
                            String resourceName,
                            String projectId,
                            String version,
                            String selfLink,
                            String creationTimeStamp,
                            String direction,
                            BigInteger id,
                            boolean disabled,
                            String network,
                            Integer priority,
                            List<RulesMetadata> allowedRules,
                            List<RulesMetadata> deniedRules) {
        super(tags, resourceName, projectId, version);
        this.selfLink = selfLink;
        this.creationTimeStamp = creationTimeStamp;
        this.direction = direction;
        this.id = id;
        this.disabled = disabled;
        this.network = network;
        this.priority = priority;
        this.allowedRules = allowedRules;
        this.deniedRules = deniedRules;
    }

    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @EqualsAndHashCode
    @Builder
    public static class RulesMetadata {
        private final String protocol;
        private final List<String> ports;
        private final List<String> sourceIPRanges;
    }

}
