package com.paloaltonetworks.evident.enrichment.metadata;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.paloaltonetworks.evident.data_access.models.ResourceMetadata;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class NetworkMetadata extends ResourceMetadata {
    String iPv4Range;
    String gatewayIPv4;
    String selfLink;
    boolean autoCreateSubnetworks;
    List<String> subnetworks;
    List<Peering> peerings;
    RoutingConfig routingConfig;
    String kind;
    String creationTimestamp;
    String description;

    @Builder
    public NetworkMetadata(Map<String, String> tags,
                           String resourceName,
                           String projectId,
                           String version,
                           String iPv4Range,
                           String gatewayIPv4,
                           String selfLink,
                           boolean autoCreateSubnetworks,
                           List<String> subnetworks,
                           List<Peering> peerings,
                           RoutingConfig routingConfig,
                           String kind,
                           String creationTimestamp,
                           String description) {

        super(tags, resourceName, projectId, version);
        this.iPv4Range = iPv4Range;
        this.gatewayIPv4 = gatewayIPv4;
        this.selfLink = selfLink;
        this.autoCreateSubnetworks = autoCreateSubnetworks;
        this.subnetworks = subnetworks;
        this.peerings = peerings;
        this.routingConfig = routingConfig;
        this.kind = kind;
        this.creationTimestamp = creationTimestamp;
        this.description = description;
    }

    @Data
    @Builder
    @JsonDeserialize(builder = Peering.PeeringBuilder.class)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Peering {
        boolean autoCreateRoutes;
        String name;
        String network;
        String state;
        String stateDetails;

        @JsonPOJOBuilder(withPrefix = "")
        public static final class PeeringBuilder {
        }
    }

    @Data
    @Builder
    @JsonDeserialize(builder = RoutingConfig.RoutingConfigBuilder.class)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class RoutingConfig {
        String routingMode;

        @JsonPOJOBuilder(withPrefix = "")
        public static final class RoutingConfigBuilder {
        }
    }
}
