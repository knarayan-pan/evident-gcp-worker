package com.paloaltonetworks.evident.enrichment;

import com.google.api.services.compute.model.Subnetwork;
import com.google.api.services.compute.model.SubnetworkAggregatedList;
import com.google.common.collect.Lists;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import com.paloaltonetworks.evident.data_access.service.MonitoredResourceService;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.metadata.SubNetworkMetadata;
import com.paloaltonetworks.evident.enrichment.request.ResourceEnrichmentRequest;
import com.paloaltonetworks.evident.gcp.api.GCPApiService;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.redis.MonitoredRedisClient;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import com.paloaltonetworks.evident.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.paloaltonetworks.evident.utils.ResourceUtil.extractResourceName;
import static com.paloaltonetworks.evident.utils.ResourceUtil.makeResourceId;

/**
 * Subnetwork enrichment
 */
@Slf4j
@Component
public class SubnetworkEnrichment extends GCPResourceEnrichment<SubnetworkAggregatedList, Subnetwork> {
    SubnetworkEnrichment(@Qualifier(MonitoredResourceService.NAME) ResourceService resourceService,
                         @Qualifier(MonitoredRedisClient.NAME) RedisClient redisClient,
                         QueueService queueService,
                         GCPApiService gcpApiService) {
        super(resourceService, redisClient, queueService, gcpApiService);
    }

    @Override
    public String resourceType() {
        return ResourceType.GCE_SUBNETWORK.getResourceType();
    }

    @Override
    String getNextPageToken(final SubnetworkAggregatedList list) {
        return list.getNextPageToken();
    }

    @Override
    Resource toResource(final Subnetwork instance, final ResourceEnrichmentRequest request) {
        val resourceId = makeResourceId(request.getProjectId(), instance.getRegion(), instance.getName());
        return Resource.builder()
                .cloudAppType(CloudAppType.GCP)
                .resourceId(resourceId)
                .tenant(request.getTenant())
                .region(extractResourceName(instance.getRegion()))
                .resourceKey(ResourceKey.builder()
                        .externalAccountId(request.getExternalAccountId())
                        .resourceType(resourceType())
                        .resourceIdHash(ResourceKey.computeResourceIdHash(resourceId))
                        .build())
                .resourceMetadata(JsonUtils.encode(SubNetworkMetadata.builder()
                        .kind(instance.getKind())
                        .id(instance.getId())
                        .creationTimeStamp(instance.getCreationTimestamp())
                        .network(instance.getNetwork())
                        .ipCidrRanges(instance.getIpCidrRange())
                        .gatewayAddress(instance.getGatewayAddress())
                        .region(instance.getRegion())
                        .selfLink(instance.getSelfLink())
                        .privateIpGoogleAddress(BooleanUtils
                                .toBoolean(instance.getPrivateIpGoogleAccess()))
                        .fingerprint(instance.getFingerprint())
                        .allowSubnetCidrRoutesOverlap(BooleanUtils
                                .toBoolean((Boolean)instance.get("allowSubnetCidrRoutesOverlap")))
                        .enableFlowLogs(BooleanUtils.toBoolean(instance.getEnableFlowLogs()))
                        .resourceName(instance.getName())
                        .projectId(request.getProjectId())
                        .build()))
                .build();
    }

    @Override
    List<Subnetwork> extractGCPResourceList(final SubnetworkAggregatedList list) {
        return Optional.ofNullable(list.getItems())
                .map(Map::values)
                .orElse(Lists.newArrayList())
                .stream()
                .flatMap(scopedList ->
                        Optional.ofNullable(scopedList.getSubnetworks())
                                .map(Collection::stream)
                                .orElse(Stream.of()))
                .collect(Collectors.toList());
    }

    @Override
    SubnetworkAggregatedList findAllResources(final ResourceEnrichmentRequest request) {
        return gcpApiService.getSubNetworks(ResourceRefreshRequest.builder()
                    .externalAccountId(request.getExternalAccountId())
                    .tenant(request.getTenant())
                    .projectId(request.getProjectId())
                    .pageToken(request.getPageToken())
                    .build());
    }

    @Override
    Subnetwork findOneResource(final ResourceEnrichmentRequest request) {
        return gcpApiService.getSubNetwork(ResourceRefreshRequest.builder()
                        .tenant(request.getTenant())
                        .externalAccountId(request.getExternalAccountId())
                        .resourceType(resourceType())
                        .resourceId(request.getResourceId())
                        .resourceName(request.getResourceName())
                        .projectId(request.getProjectId())
                        .zone(request.getZone())
                        .build());
    }
}
