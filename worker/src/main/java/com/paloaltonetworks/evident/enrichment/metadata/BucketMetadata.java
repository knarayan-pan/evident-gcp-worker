package com.paloaltonetworks.evident.enrichment.metadata;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.paloaltonetworks.evident.data_access.models.ResourceMetadata;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
// https://cloud.google.com/storage/docs/json_api/v1/buckets#resource
public class BucketMetadata extends ResourceMetadata {

    private final Date dateCreated;
    private final Date updated;
    // https://cloud.google.com/storage/docs/json_api/v1/bucketAccessControls#resource
    private final List<BucketAcl> bucketAccessControl;
    private final BucketPolicy policy;
    private final String logBucket;
    private final String logObjectPrefix;
    private final String storageClass;
    private final String location;


    @Builder
    public BucketMetadata(Map<String, String> tags, String resourceName, String projectId, String version,
                          Date dateCreated, Date updated,
                          List<BucketAcl> bucketAccessControl,
                          BucketPolicy policy, String logBucket, String logObjectPrefix, String storageClass,
                          String location) {
        super(tags, resourceName, projectId, version);
        this.dateCreated = dateCreated;
        this.updated = updated;
        this.bucketAccessControl = bucketAccessControl;
        this.policy = policy;
        this.logBucket = logBucket;
        this.logObjectPrefix = logObjectPrefix;
        this.storageClass = storageClass;
        this.location = location;
    }


}
