package com.paloaltonetworks.evident.enrichment.metadata;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.api.services.cloudresourcemanager.model.Policy;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@Builder(toBuilder = true)
public class IamPolicy {
    private List<RoleMembers> roleMembers;
    private List<AuditConfig> auditConfigs;

    public IamPolicy(@NonNull Policy policy) {
        roleMembers =
            Optional.ofNullable(policy.getBindings())
                .orElse(Collections.emptyList())
                .stream()
                .map(binding ->
                    IamPolicy.RoleMembers.builder()
                        .role(binding.getRole())
                        .members(binding.getMembers())
                        .build())
                .collect(Collectors.toList());
       auditConfigs =
            Optional.ofNullable(policy.getAuditConfigs())
                .orElse(Collections.emptyList())
                .stream()
                .map(auditConfig ->
                    IamPolicy.AuditConfig.builder()
                        .service(auditConfig.getService())
                        .auditLogConfigs(
                            Optional.ofNullable(auditConfig.getAuditLogConfigs())
                                .orElse(Collections.emptyList())
                                .stream()
                                .map(logConfig ->
                                    IamPolicy.AuditLogConfig.builder()
                                        .logType(logConfig.getLogType())
                                        .exemptedMembers(logConfig.getExemptedMembers())
                                        .build()
                                )
                                .collect(Collectors.toList())
                        )
                        .build()
                )
                .collect(Collectors.toList());
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder(toBuilder = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class RoleMembers {
        private String role;
        private List<String> members;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder(toBuilder = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class AuditConfig {
        private List<AuditLogConfig> auditLogConfigs;
        private String service;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder(toBuilder = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class AuditLogConfig {
        private String logType;
        private List<String> exemptedMembers;
    }
}
