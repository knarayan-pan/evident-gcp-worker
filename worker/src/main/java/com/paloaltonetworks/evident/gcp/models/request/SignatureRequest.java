package com.paloaltonetworks.evident.gcp.models.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class SignatureRequest implements TenantAwareRequest {
    private final String tenant;
    private final String resourceId;
    private final String projectId;
    private final String organizationId;
    private final String resourceType;
    private final String externalAccountId;
}
