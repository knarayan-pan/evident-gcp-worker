package com.paloaltonetworks.evident.gcp.api;

import com.codahale.metrics.MetricRegistry;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.common.base.Stopwatch;
import com.paloaltonetworks.aperture.MetricConstants;
import com.paloaltonetworks.aperture.Tags;
import com.paloaltonetworks.evident.exceptions.GCPApiException;
import com.paloaltonetworks.evident.gcp.models.request.TenantAwareRequest;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

@Aspect
@Order(Integer.MAX_VALUE)
@Configuration
@Slf4j
public class MonitoredMethodAdvice {
    private static final int CLIENT_SIDE_PROCESSING_ERROR = 504;

    private final MetricRegistry metricRegistry;

    public MonitoredMethodAdvice(MetricRegistry metricRegistry) {
        this.metricRegistry = metricRegistry;
    }

    @Around("execution(@Monitored * com.paloaltonetworks.evident.gcp.api.DefaultGCPApiService.*(..))")
    public Object aroundExec(ProceedingJoinPoint jp) throws Throwable {
        val request = (TenantAwareRequest) Stream.of(jp.getArgs())
                .filter(o -> o instanceof TenantAwareRequest)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Invalid request - can't be monitored"));
        return monitor(jp, request);
    }

    private Object monitor(ProceedingJoinPoint jp, TenantAwareRequest request) {
        Stopwatch timer = Stopwatch.createStarted();
        val method = jp.getSignature().getName();
        val tags = Tags.builder().tag("tenant", request.getTenant())
                .tag("externalAccountId", request.getExternalAccountId())
                .tag("method", method);
        try {
            val result = execute(jp, request, method);
            tags.tag(MetricConstants.STATUS_TAG_NAME, String.valueOf(200));
            return result;
        } catch (GCPApiException e) {
            tags.tag(MetricConstants.STATUS_TAG_NAME, String.valueOf(e.getStatus()));
            throw e;
        } catch (RuntimeException e) {
            throw e;
        } catch (Throwable e) {
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.FAILURE_TAG);
            throw new RuntimeException(e);
        } finally {
            timer.stop();
            metricRegistry.timer(tags.build().toMetricName("timer.gcp_client"))
                    .update(timer.elapsed(TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS);
        }
    }

    private Object execute(ProceedingJoinPoint jp, TenantAwareRequest request, String method) throws Throwable {
        try {
            return jp.proceed();
        } catch (GoogleJsonResponseException e) {
            log.error("Received error from server for {} call. Account: {}, Request: {}, error: {}",
                    method, request.getExternalAccountId(), request, e.getMessage());
            throw new GCPApiException(e.getStatusCode(), e.getMessage(), e);
        } catch (IOException e) {
            log.error("Error initializing client for account: {}", request.getExternalAccountId());
            throw new GCPApiException(CLIENT_SIDE_PROCESSING_ERROR,
                    String.format("Error retrieving network for account: %s due to : %s",
                            request.getExternalAccountId(), e.getMessage()), e);
        }
    }
}
