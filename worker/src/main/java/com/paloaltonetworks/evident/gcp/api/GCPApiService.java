package com.paloaltonetworks.evident.gcp.api;

import com.google.api.services.cloudresourcemanager.model.ListProjectsResponse;
import com.google.api.services.compute.model.Firewall;
import com.google.api.services.compute.model.FirewallList;
import com.google.api.services.compute.model.Instance;
import com.google.api.services.compute.model.InstanceAggregatedList;
import com.google.api.services.compute.model.InstanceList;
import com.google.api.services.compute.model.Network;
import com.google.api.services.compute.model.NetworkList;
import com.google.api.services.compute.model.Subnetwork;
import com.google.api.services.compute.model.SubnetworkAggregatedList;
import com.google.api.services.dns.model.ManagedZone;
import com.google.api.services.dns.model.ManagedZonesListResponse;
import com.google.api.services.sqladmin.model.DatabaseInstance;
import com.google.api.services.sqladmin.model.InstancesListResponse;
import com.google.api.services.storage.model.Bucket;
import com.google.api.services.storage.model.Buckets;
import com.google.api.services.storage.model.Policy;
import com.paloaltonetworks.evident.enrichment.request.ProjectEnrichmentRequest;
import com.paloaltonetworks.evident.exceptions.GCPApiException;
import com.paloaltonetworks.evident.gcp.models.AuditLogPage;
import com.paloaltonetworks.evident.gcp.models.request.FetchBucketsRequest;
import com.paloaltonetworks.evident.gcp.models.request.FetchComputeInstancesRequest;
import com.paloaltonetworks.evident.gcp.models.request.FetchFirewallsRequest;
import com.paloaltonetworks.evident.gcp.models.request.FetchSQLInstancesRequest;
import com.paloaltonetworks.evident.gcp.models.request.GcpActivityLogRequest;
import com.paloaltonetworks.evident.gcp.models.request.ManagedZoneRequest;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.gcp.models.request.TenantAwareRequest;
import org.springframework.lang.Nullable;

import javax.annotation.Nonnull;
import java.util.Optional;

public interface GCPApiService {
    /**
     * Get list of projects for this externalAccount.
     *
     * @param request   {@link TenantAwareRequest} Tenant and external id info
     * @param pageToken the page token to use to continue paginating to next page
     * @return {@link ListProjectsResponse} object containing the list of projects and optional next page token if available.
     */
    ListProjectsResponse getProjects(@Nonnull TenantAwareRequest request, @Nullable String pageToken);

    /**
     * Get a list of compute instances
     *
     * @param request {@link FetchComputeInstancesRequest}
     * @return {@link InstanceList} list of compute instances
     */
    InstanceAggregatedList getComputeInstances(@Nonnull FetchComputeInstancesRequest request) throws GCPApiException;

    /**
     * Fetch single compute instance based on resource id
     *
     * @param request {@link ResourceRefreshRequest}
     * @return an {@link Instance} object or null if it doesn't exist
     */
    Instance getComputeInstance(@Nonnull ResourceRefreshRequest request);

    /**
     * Fetch firewalls
     *
     * @param request {@link FetchFirewallsRequest}
     * @return an {@link FirewallList} List if firewalls
     */
    FirewallList getFirewalls(@Nonnull FetchFirewallsRequest request) throws GCPApiException;

    /**
     * Fetch firewall
     *
     * @param request {@link ResourceRefreshRequest}
     * @return an {@link FirewallList} object or null if it doesn't exist
     */
    Firewall getFirewall(@Nonnull ResourceRefreshRequest request) throws GCPApiException;

    /**
     * Fetch a list of buckets that belong to a project
     *
     * @param request {@link FetchBucketsRequest} object containing the request parameters to fetch list of buckets.
     * @return {@link Buckets} object containing the list of buckets
     */
    Buckets getBuckets(@Nonnull FetchBucketsRequest request) throws GCPApiException;

    /**
     * Retrieve a specific bucket from GCP
     *
     * @param request {@link ResourceRefreshRequest}
     * @return {@link Bucket} information regarding a specific bucket
     * @throws GCPApiException when there is an error from the cloud
     */
    Bucket getBucket(@Nonnull ResourceRefreshRequest request) throws GCPApiException;

    /**
     * Fetch a bucket's policy definition that determines exposure and access
     *
     * @param request {@link ResourceRefreshRequest}
     * @return a {@link Policy} object indicating the bucket policy
     */
    Policy getBucketPolicy(@Nonnull ResourceRefreshRequest request) throws GCPApiException;

    /**
     * Retrieve a specific sql instance from GCP
     *
     * @param request {@link ResourceRefreshRequest}
     * @return {@link DatabaseInstance} information regarding a specific sql intance
     * @throws GCPApiException when there is an error from the cloud
     */
    DatabaseInstance getSQLInstance(ResourceRefreshRequest request) throws GCPApiException;


    /**
     * Fetch a list of sql instances that belong to a project
     *
     * @param request {@link FetchSQLInstancesRequest} object containing the request parameters to fetch list of buckets.
     * @return {@link InstancesListResponse} object containing the list of sql instances
     */
    InstancesListResponse getSQLInstances(FetchSQLInstancesRequest request) throws GCPApiException;

    /**
     * Fetch activity logs from gcp for the time range and additional filters
     *
     * @param request {@link GcpActivityLogRequest} object containing the required parameters to fetch activity logs
     * @return {@link AuditLogPage} page object containing audit logs from GCP
     */
    AuditLogPage getAuditLogs(@Nonnull GcpActivityLogRequest request);

    /**
     * Fetch the organization Id of the account
     *
     * @param request {@link TenantAwareRequest} Tenant and external id info
     * @param domain  the domain used for the Gsuite account
     * @return Optional string indicating the organization id
     */
    Optional<String> organizationId(@Nonnull TenantAwareRequest request, @Nonnull String domain);

    /**
     * Fetch list of managed zones for a given project
     *
     * @param request {@link ManagedZoneRequest} object containing the project and the account to use to make the api call
     * @return the {@link ManagedZonesListResponse} response
     */
    ManagedZonesListResponse getManagedZones(@Nonnull ManagedZoneRequest request);

    /**
     * Fetch details of a managed zone for a given project
     *
     * @param request {@link ManagedZoneRequest} object containing the project and the account to use to make the api call
     * @return the {@link ManagedZonesListResponse} response
     */
    ManagedZone getManagedZoneDetails(@Nonnull ManagedZoneRequest request);

    /**
     * Fetch networks
     *
     * @param request {@link ResourceRefreshRequest}
     * @return an {@link NetworkList}
     */
    NetworkList getNetworks(@Nonnull ResourceRefreshRequest request);

    /**
     * Fetch networks for current project scope
     *
     * @param request {@link ProjectEnrichmentRequest}
     * @return an {@link NetworkList}
     */
    NetworkList getProjectNetworks(@Nonnull ProjectEnrichmentRequest request);

    /**
     * Fetch network
     *
     * @param request {@link ResourceRefreshRequest}
     * @return an {@link Network}
     */
    Network getNetwork(@Nonnull ResourceRefreshRequest request);

    /**
     * Fetch subnetworks
     *
     * @param request {@link ResourceRefreshRequest}
     * @return an {@link NetworkList} List if firewalls
     */
    SubnetworkAggregatedList getSubNetworks(@Nonnull ResourceRefreshRequest request);

    /**
     * Fetch subnetwork
     *
     * @param request {@link ResourceRefreshRequest}
     * @return an {@link Network} object or null if it doesn't exist
     */
    Subnetwork getSubNetwork(@Nonnull ResourceRefreshRequest request);

    /**
     * Fetch IamPolicy for current project scope
     *
     * @param request {@link ProjectEnrichmentRequest} object containing the project id
     * @return the {@link com.google.api.services.cloudresourcemanager.model.Policy} for the project scope
     */
    com.google.api.services.cloudresourcemanager.model.Policy getProjectIamPolicy(@Nonnull ProjectEnrichmentRequest request);
}
