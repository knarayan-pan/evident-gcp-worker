package com.paloaltonetworks.evident.gcp.models.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FetchOrganizationIdRequest implements TenantAwareRequest {
    private final String tenant;
    private final String externalAccountId;
}
