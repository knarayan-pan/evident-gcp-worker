package com.paloaltonetworks.evident.gcp.api;

import com.paloaltonetworks.evident.gcp.models.request.TenantAwareRequest;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import java.util.stream.Stream;

@Aspect
@Order(0)
@Configuration
@Slf4j
public class RateLimitingAdvice {
    private final GCPRateLimitingService gcpRateLimitingService;

    public RateLimitingAdvice(GCPRateLimitingService gcpRateLimitingService) {
        this.gcpRateLimitingService = gcpRateLimitingService;
    }

    @Before("execution(@RateLimited * com.paloaltonetworks.evident.gcp.api.DefaultGCPApiService.*(..))")
    public void beforeExec(JoinPoint jp) throws Throwable {
        val request = (TenantAwareRequest) Stream.of(jp.getArgs())
                .filter(o -> o instanceof TenantAwareRequest)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Invalid request - can't enforce ratelimit"));
        if (!gcpRateLimitingService.getRateLimitLock(request.getTenant(), request.getExternalAccountId(),
                request.getApiType())) {
            log.error("Could not acquire rate limiting lock for account: {}, Request=",
                    request.getExternalAccountId(),
                    request);
            throw new RuntimeException("Received rate limit lock");
        }
    }
}
