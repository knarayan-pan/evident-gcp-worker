package com.paloaltonetworks.evident.gcp.models.request;

import com.paloaltonetworks.evident.gcp.api.GCPApiType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class ManagedZoneRequest implements TenantAwareRequest {

    private final String tenant;
    private final String projectId;
    private final String externalAccountId;
    private final String managedZone;
    private final String pageToken;
    @Builder.Default
    private final Long pageSize = 30L;


    @Override
    public GCPApiType getApiType() {
        return GCPApiType.DNS;
    }
}
