package com.paloaltonetworks.evident.gcp.api;

import com.paloaltonetworks.evident.gcp.token.GCPTokenService;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class GCPAuthInterceptor implements Interceptor {

    private final GCPTokenService gcpTokenService;
    private final String externalAccountId;


    public GCPAuthInterceptor(GCPTokenService gcpTokenService, String externalAccountId) {
        this.gcpTokenService = gcpTokenService;
        this.externalAccountId = externalAccountId;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        String oauthToken = gcpTokenService.getOauthToken(externalAccountId);
        Request request = chain.request();
        return chain.proceed(request.newBuilder().header("Authorization",
                String.format("Bearer %s", oauthToken)).build());
    }
}
