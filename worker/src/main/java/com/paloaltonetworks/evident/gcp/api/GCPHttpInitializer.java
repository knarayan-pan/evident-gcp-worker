package com.paloaltonetworks.evident.gcp.api;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class GCPHttpInitializer extends GoogleCredential {
    private static final int MAX_RETRIES = 6;

    GCPHttpInitializer(HttpTransport httpTransport, JsonFactory jsonFactory) {
        super(new GoogleCredential.Builder().setTransport(httpTransport).setJsonFactory(jsonFactory));
    }

    @Override
    public void initialize(HttpRequest httpRequest) throws IOException {
        httpRequest.setInterceptor(this);
        httpRequest.setNumberOfRetries(MAX_RETRIES);
        httpRequest.setConnectTimeout((int) TimeUnit.MINUTES.toMillis(3));
        httpRequest.setReadTimeout((int) TimeUnit.MINUTES.toMillis(3));
        httpRequest.setUnsuccessfulResponseHandler(new CustomResponseHandler());
        httpRequest.setThrowExceptionOnExecuteError(true);
    }
}
