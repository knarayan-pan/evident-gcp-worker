package com.paloaltonetworks.evident.gcp.api;

import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.redis.MonitoredRateLimitingService;
import com.paloaltonetworks.evident.redis.RateLimitingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.lang.invoke.MethodHandles;
import java.util.concurrent.TimeUnit;

@Component
public class GCPRateLimitingService {

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static final int RATE_LIMIT_MAX_ATTEMPTS_DEFAULT = 30;
    private static final int RATE_LIMIT_WAIT_TIME_IN_MS_DEFAULT = 100;

    private final RateLimitingService rateLimitingService;

    @Autowired
    public GCPRateLimitingService(@Qualifier(MonitoredRateLimitingService.NAME) RateLimitingService rateLimitingService) {
        this.rateLimitingService = rateLimitingService;
    }

    public boolean getRateLimitLock(@Nonnull final String tenant, @Nonnull final String externalAccountId, @Nonnull GCPApiType apiType) {
        // todo: override default below with consul values as indicated in apiType.getRateLimitConsulKey.
        // consulService.getIntForTenant(tenant, apiType.getRateLimitConsulKey(), apiType.getDefaultRateLimit());
        return rateLimitingService.getRateLimitLock(externalAccountId, apiType.toString(), CloudAppType.GCP.name(),
                apiType.getDefaultRateLimit(), TimeUnit.SECONDS, RATE_LIMIT_MAX_ATTEMPTS_DEFAULT,
                RATE_LIMIT_WAIT_TIME_IN_MS_DEFAULT, TimeUnit.MILLISECONDS);
    }
}
