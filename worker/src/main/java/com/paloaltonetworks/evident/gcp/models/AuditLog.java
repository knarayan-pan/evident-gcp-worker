package com.paloaltonetworks.evident.gcp.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * https://cloud.google.com/logging/docs/reference/v2/rest/v2/LogEntry
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuditLog {

    private String logName;
    private GcpMonitoredResource resource;
    private String timestamp;
    private String receiveTimestamp;
    private String severity;
    private String insertId;
    private GcpProtoPayload protoPayload;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
