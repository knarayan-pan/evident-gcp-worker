package com.paloaltonetworks.evident.gcp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuditLogPage {

    private List<AuditLog> entries;
    private String nextPageToken;

    @JsonIgnore
    public Boolean hasNextPage() {
        return !StringUtils.isEmpty(nextPageToken);
    }
}
