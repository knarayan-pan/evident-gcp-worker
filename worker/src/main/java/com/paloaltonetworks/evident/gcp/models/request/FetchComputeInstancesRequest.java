package com.paloaltonetworks.evident.gcp.models.request;

import lombok.Builder;
import lombok.Data;

/**
 * Request to fetch compute instances
 */
@Data
@Builder
public class FetchComputeInstancesRequest implements TenantAwareRequest {
    private final String tenant;
    private final String projectId;
    private final String externalAccountId;
    private final String pageToken;
    private final String zone;
    @Builder.Default
    private final Long pageSize = 30L;
}
