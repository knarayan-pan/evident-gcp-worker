package com.paloaltonetworks.evident.gcp.models.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class ProjectTreeRefreshRequest implements TenantAwareRequest {
    private final String tenant;
    private final String resourceId;
    private final String resourceName;
    private final String organizationId;
    private final String resourceType;
    private final String externalAccountId;
    private final String pageToken;
    @Builder.Default
    private final Long pageSize = 30L;
    private final Boolean reconcile;
}
