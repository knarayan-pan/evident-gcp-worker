package com.paloaltonetworks.evident.gcp.api;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.cloudresourcemanager.CloudResourceManager;
import com.google.api.services.compute.Compute;
import com.google.api.services.dns.Dns;
import com.google.api.services.sqladmin.SQLAdmin;
import com.google.api.services.storage.Storage;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.paloaltonetworks.evident.esp.CachingHeaderInterceptor;
import com.paloaltonetworks.evident.gcp.token.GCPTokenService;
import com.paloaltonetworks.evident.gcp.token.MonitoredGCPTokenService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Dispatcher;
import okhttp3.OkHttpClient;
import okhttp3.internal.Util;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Configuration
@Slf4j
public class APIConfig {
    private static long OKHTTP_CACHE_SIZE = 10485760; // 10 MB by default
    private static int OKHTTP_CACHE_TIMEOUT = 20; // 20 sec by default
    private static final String CACHE_DIR = "tmp/okhttp/gcpapi/%s";
    private static final String APP_NAME = "evident-gcp-service-account";

    @Bean
    @SneakyThrows({GeneralSecurityException.class, IOException.class})
    public GCPHttpInitializer gcpHttpInitializer() {
        return new GCPHttpInitializer(GoogleNetHttpTransport.newTrustedTransport(),
                JacksonFactory.getDefaultInstance());
    }

    @Bean
    @SneakyThrows({GeneralSecurityException.class, IOException.class})
    public Compute compute(GCPHttpInitializer gcpHttpInitializer) {
        return new com.google.api.services.compute.Compute.Builder(GoogleNetHttpTransport.newTrustedTransport(),
                JacksonFactory.getDefaultInstance(),
                gcpHttpInitializer)
                .setApplicationName(APP_NAME)
                .build();
    }

    @Bean
    @SneakyThrows({GeneralSecurityException.class, IOException.class})
    public CloudResourceManager cloudResourceManager(GCPHttpInitializer gcpHttpInitializer) {
        return new CloudResourceManager.Builder(GoogleNetHttpTransport.newTrustedTransport(),
                JacksonFactory.getDefaultInstance(),
                gcpHttpInitializer)
                .setApplicationName(APP_NAME)
                .build();
    }

    @Bean
    @SneakyThrows({GeneralSecurityException.class, IOException.class})
    public Storage storage(GCPHttpInitializer gcpHttpInitializer) {
        return new com.google.api.services.storage.Storage.Builder(GoogleNetHttpTransport.newTrustedTransport(),
                JacksonFactory.getDefaultInstance(),
                gcpHttpInitializer)
                .setApplicationName(APP_NAME)
                .build();
    }

    @Bean
    @SneakyThrows({GeneralSecurityException.class, IOException.class})
    public SQLAdmin sqlAdmin(GCPHttpInitializer gcpHttpInitializer) {
        return new com.google.api.services.sqladmin.SQLAdmin.Builder(GoogleNetHttpTransport.newTrustedTransport(),
                JacksonFactory.getDefaultInstance(),
                gcpHttpInitializer)
                .setApplicationName(APP_NAME)
                .build();
    }

    @Bean
    @SneakyThrows({GeneralSecurityException.class, IOException.class})
    public Dns dnsClient(GCPHttpInitializer gcpHttpInitializer) {
        return new Dns.Builder(GoogleNetHttpTransport.newTrustedTransport(),
                JacksonFactory.getDefaultInstance(),
                gcpHttpInitializer)
                .setApplicationName(APP_NAME)
                .build();
    }

    @Bean
    public LoadingCache<String, OkHttpClient> httpClientCache(
            @Qualifier(MonitoredGCPTokenService.NAME) GCPTokenService gcpTokenService) {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(log.isTraceEnabled() ?
                HttpLoggingInterceptor.Level.BODY :
                HttpLoggingInterceptor.Level.BASIC);
        return CacheBuilder.<String, OkHttpClient>newBuilder().build(new CacheLoader<String, OkHttpClient>() {
            @Override
            public OkHttpClient load(String externalAccountId) throws Exception {
                return new OkHttpClient.Builder()
                        .addNetworkInterceptor(new CachingHeaderInterceptor(OKHTTP_CACHE_TIMEOUT))
                        .addInterceptor(new GCPAuthInterceptor(gcpTokenService, externalAccountId))
                        .addInterceptor(loggingInterceptor)
                        .dispatcher(new Dispatcher(new ThreadPoolExecutor(0, 100,
                                30, TimeUnit.SECONDS,
                                new SynchronousQueue<>(),
                                Util.threadFactory("GCP Api Http Dispatcher", false))))
                        .cache(new okhttp3.Cache(new File(String.format(CACHE_DIR, externalAccountId)),
                                OKHTTP_CACHE_SIZE))
                        .build();

            }
        });
    }
}
