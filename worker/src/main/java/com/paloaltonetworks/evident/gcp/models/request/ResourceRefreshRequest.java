package com.paloaltonetworks.evident.gcp.models.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class ResourceRefreshRequest implements TenantAwareRequest {
    private final String tenant;
    private final String projectId;
    private final String organizationId;
    private final String resourceId;
    private final String resourceType;
    private final String externalAccountId;
    private final String pageToken;
    private final String zone;
    private final String resourceName;
    @Builder.Default
    private final Integer pageSize = 30;
    private final Boolean reconcile;
}
