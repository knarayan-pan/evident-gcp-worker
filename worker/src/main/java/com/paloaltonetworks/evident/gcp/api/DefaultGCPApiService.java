package com.paloaltonetworks.evident.gcp.api;

import com.google.api.services.cloudresourcemanager.CloudResourceManager;
import com.google.api.services.cloudresourcemanager.model.GetIamPolicyRequest;
import com.google.api.services.cloudresourcemanager.model.ListProjectsResponse;
import com.google.api.services.cloudresourcemanager.model.SearchOrganizationsRequest;
import com.google.api.services.cloudresourcemanager.model.SearchOrganizationsResponse;
import com.google.api.services.compute.Compute;
import com.google.api.services.compute.model.Firewall;
import com.google.api.services.compute.model.FirewallList;
import com.google.api.services.compute.model.Instance;
import com.google.api.services.compute.model.InstanceAggregatedList;
import com.google.api.services.compute.model.Network;
import com.google.api.services.compute.model.NetworkList;
import com.google.api.services.compute.model.Subnetwork;
import com.google.api.services.compute.model.SubnetworkAggregatedList;
import com.google.api.services.dns.Dns;
import com.google.api.services.dns.model.ManagedZone;
import com.google.api.services.dns.model.ManagedZonesListResponse;
import com.google.api.services.sqladmin.SQLAdmin;
import com.google.api.services.sqladmin.model.DatabaseInstance;
import com.google.api.services.sqladmin.model.InstancesListResponse;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.model.Bucket;
import com.google.api.services.storage.model.Buckets;
import com.google.api.services.storage.model.Policy;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicates;
import com.google.common.cache.LoadingCache;
import com.paloaltonetworks.evident.enrichment.request.ProjectEnrichmentRequest;
import com.paloaltonetworks.evident.exceptions.GCPApiException;
import com.paloaltonetworks.evident.gcp.models.AuditLogPage;
import com.paloaltonetworks.evident.gcp.models.request.FetchBucketsRequest;
import com.paloaltonetworks.evident.gcp.models.request.FetchComputeInstancesRequest;
import com.paloaltonetworks.evident.gcp.models.request.FetchFirewallsRequest;
import com.paloaltonetworks.evident.gcp.models.request.FetchSQLInstancesRequest;
import com.paloaltonetworks.evident.gcp.models.request.GcpActivityLogRequest;
import com.paloaltonetworks.evident.gcp.models.request.ManagedZoneRequest;
import com.paloaltonetworks.evident.gcp.models.request.ResourceRefreshRequest;
import com.paloaltonetworks.evident.gcp.models.request.TenantAwareRequest;
import com.paloaltonetworks.evident.gcp.token.GCPTokenService;
import com.paloaltonetworks.evident.gcp.token.MonitoredGCPTokenService;
import com.paloaltonetworks.evident.utils.JsonUtils;
import lombok.SneakyThrows;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static com.paloaltonetworks.evident.utils.ResourceUtil.extractFirewallName;
import static com.paloaltonetworks.evident.utils.ResourceUtil.extractGCEInstanceName;
import static com.paloaltonetworks.evident.utils.ResourceUtil.extractResourceName;
import static com.paloaltonetworks.evident.utils.ResourceUtil.extractSQLInstanceName;

@Primary
@Component
public class DefaultGCPApiService implements GCPApiService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private final CloudResourceManager cloudresourcemanager;
    private final Storage storage;
    private final Compute compute;
    private final SQLAdmin sqlAdmin;
    private final Dns dnsClient;
    private final GCPTokenService gcpTokenService;
    private final LoadingCache<String, OkHttpClient> httpClientCache;
    private static final int CLIENT_SIDE_PROCESSING_ERROR = 504;

    public DefaultGCPApiService(@Qualifier(MonitoredGCPTokenService.NAME) GCPTokenService gcpTokenService,
                                CloudResourceManager cloudResourceManager,
                                Storage storage,
                                Compute compute,
                                SQLAdmin sqlAdmin,
                                Dns dnsClient, LoadingCache<String, OkHttpClient> httpClientCache) {
        this.gcpTokenService = gcpTokenService;
        this.cloudresourcemanager = cloudResourceManager;
        this.storage = storage;
        this.compute = compute;
        this.sqlAdmin = sqlAdmin;
        this.dnsClient = dnsClient;
        this.httpClientCache = httpClientCache;
    }

    @Override
    @RateLimited
    @Monitored
    @SneakyThrows(IOException.class)
    public ListProjectsResponse getProjects(@Nonnull TenantAwareRequest request, @Nullable String pageToken) {
            return cloudresourcemanager
                    .projects()
                    .list()
                    .setPageToken(pageToken)
                    .setOauthToken(gcpTokenService.getOauthToken(request.getExternalAccountId()))
                    .execute();

    }

    @Override
    @RateLimited
    @Monitored
    @SneakyThrows(IOException.class)
    public InstanceAggregatedList getComputeInstances(@Nonnull FetchComputeInstancesRequest request) {
            return compute.instances()
                    .aggregatedList(request.getProjectId())
                    .setOauthToken(gcpTokenService.getOauthToken(request.getExternalAccountId()))
                    .setPageToken(request.getPageToken())
                    .setMaxResults(request.getPageSize())
                    .execute();
    }

    @Override
    @RateLimited
    @Monitored
    @SneakyThrows(IOException.class)
    public Instance getComputeInstance(ResourceRefreshRequest request) {
            return compute.instances()
                    .get(request.getProjectId(), request.getZone(), extractGCEInstanceName(request.getResourceName()))
                    .setOauthToken(gcpTokenService.getOauthToken(request.getExternalAccountId()))
                    .execute();
    }

    @Override
    @RateLimited
    @Monitored
    @SneakyThrows(IOException.class)
    public FirewallList getFirewalls(@Nonnull FetchFirewallsRequest request) {
            return compute.firewalls()
                    .list(request.getProjectId())
                    .setOauthToken(gcpTokenService.getOauthToken(request.getExternalAccountId()))
                    .execute();
    }

    @Override
    @RateLimited
    @Monitored
    @SneakyThrows(IOException.class)
    public Firewall getFirewall(@Nonnull ResourceRefreshRequest request) {
            return compute.firewalls()
                    .get(request.getProjectId(), extractFirewallName(request.getResourceName()))
                    .setOauthToken(gcpTokenService.getOauthToken(request.getExternalAccountId()))
                    .execute();
    }


    @Override
    @RateLimited
    @Monitored
    @SneakyThrows(IOException.class)
    public Buckets getBuckets(@Nonnull FetchBucketsRequest request) {
            return storage.buckets().list(request.getProjectId())
                    .setOauthToken(gcpTokenService.getOauthToken(request.getExternalAccountId()))
                    .setPageToken(request.getPageToken())
                    .setMaxResults(request.getPageSize())
                    .setProjection("full").execute();
    }

    @Override
    @RateLimited
    @Monitored
    @SneakyThrows(IOException.class)
    public Bucket getBucket(@Nonnull ResourceRefreshRequest request) {
            return storage.buckets().get(extractResourceName(request.getResourceId()))
                    .setOauthToken(gcpTokenService.getOauthToken(request.getExternalAccountId()))
                    .execute();
    }

    @Override
    @RateLimited
    @Monitored
    @SneakyThrows(IOException.class)
    public Policy getBucketPolicy(@Nonnull ResourceRefreshRequest request) {
            return storage.buckets().getIamPolicy(extractResourceName(request.getResourceId()))
                    .setOauthToken(gcpTokenService.getOauthToken(request.getExternalAccountId()))
                    .execute();
    }

    @Override
    @RateLimited
    @Monitored
    @SneakyThrows(IOException.class)
    public DatabaseInstance getSQLInstance(ResourceRefreshRequest request) {
            return sqlAdmin.instances().get(request.getProjectId(), extractResourceName(extractSQLInstanceName(request.getResourceId())))
                    .setOauthToken(gcpTokenService.getOauthToken(request.getExternalAccountId()))
                    .execute();
    }

    @Override
    @RateLimited
    @Monitored
    @SneakyThrows(IOException.class)
    public InstancesListResponse getSQLInstances(FetchSQLInstancesRequest request) {
            return sqlAdmin.instances().list(request.getProjectId())
                    .setOauthToken(gcpTokenService.getOauthToken(request.getExternalAccountId()))
                    .execute();
    }

    @Override
    @RateLimited
    @Monitored
    public AuditLogPage getAuditLogs(@Nonnull GcpActivityLogRequest request) {
        return getAuditLogs(request.getTenant(), request.getExternalAccountId(), request.getProjectIds(), request.getOrganizationId(),
                request.activityFilter(), request.getPageToken());
    }

    private AuditLogPage getAuditLogs(@Nonnull String tenant, @Nonnull String externalAccountId,
                                      @Nonnull Set<String> projectIds, @Nullable String organizationId,
                                      @Nonnull String filter, @Nullable String token) {
        return doPostAndGetAuditLogs(externalAccountId, projectIds, organizationId, filter, token);
    }

    private AuditLogPage doPostAndGetAuditLogs(@Nonnull String externalAccountId, @Nonnull Set<String> projectIds, String organizationId,
                                               @Nonnull String filter, @Nullable String token) throws GCPApiException {

        Response response = null;
        try {
            OkHttpClient client = httpClientCache.get(externalAccountId);
            Set<String> resourceNames = projectIds.stream().filter(Predicates.notNull()).map(new Function<String, String>() {
                @javax.annotation.Nullable
                @Override
                public String apply(@javax.annotation.Nullable String input) {
                    return String.format("projects/%s", input);
                }
            }).collect(Collectors.toSet());
            // add the organizationId to the list
            if (StringUtils.isNotEmpty(organizationId)) {
                resourceNames.add(String.format("organizations/%s", organizationId));
            }

            response = client.newCall(new Request.Builder()
                    .url("https://logging.googleapis.com/v2/entries:list")
                    .post(RequestBody.create(MediaType.parse("application/json"), new JSONObject()
                            .put("resourceNames", resourceNames)
                            .put("filter", filter)
                            .put("pageSize", 50)
                            .put("pageToken", token)
                            .toString()))
                    .build()).execute();
            if (!response.isSuccessful()) {
                LOGGER.error("Received error from server for account: {} - response: {}", externalAccountId,
                        response.body() == null ? "N/A" : response.body().string());
                throw new GCPApiException(response.code(), String.format("Error from GCP in fetching event logs for account: %s", externalAccountId));
            } else {
                // if successful
                if (response.body() == null) {
                    LOGGER.error("Received empty response from server for audit logs for account: {}", externalAccountId);
                    return new AuditLogPage();
                }
                return JsonUtils.decodeValue(response.body().string(), AuditLogPage.class);
            }
        } catch (IOException | ExecutionException e) {
            LOGGER.error("Error initializing client for querying gcp audit logs for account: {} Reason: {}", externalAccountId, e.getMessage());
            throw new GCPApiException(CLIENT_SIDE_PROCESSING_ERROR, e.getMessage(), e);
        } finally {
            if (response != null) {
                response.close();
            }
        }

    }

    @Override
    @RateLimited
    @Monitored
    @SneakyThrows(IOException.class)
    public Optional<String> organizationId(@NotNull TenantAwareRequest request, @Nonnull String domain) {
            SearchOrganizationsRequest searchOrganizationsRequest = new SearchOrganizationsRequest();
            searchOrganizationsRequest.setFilter(String.format("domain:%s", domain));
            SearchOrganizationsResponse response = cloudresourcemanager
                    .organizations()
                    .search(searchOrganizationsRequest)
                    .setOauthToken(gcpTokenService.getOauthToken(request.getExternalAccountId()))
                    .execute();
            return CollectionUtils.isEmpty(response.getOrganizations()) ? Optional.empty() :
                    Optional.ofNullable(response.getOrganizations().get(0).getName()).filter(Predicates.notNull())
                            .flatMap(new Function<String, Optional<String>>() {
                                @javax.annotation.Nullable
                                @Override
                                public Optional<String> apply(@javax.annotation.Nullable String input) {
                                    return StringUtils.isEmpty(input) ? Optional.empty() :
                                            Optional.of(StringUtils.substringAfter(input, "/"));
                                }
                            });
    }

    @Override
    @RateLimited
    @Monitored
    @SneakyThrows(IOException.class)
    public ManagedZonesListResponse getManagedZones(@Nonnull ManagedZoneRequest request) {
            return dnsClient.managedZones().list(request.getProjectId())
                    .setOauthToken(gcpTokenService.getOauthToken(request.getExternalAccountId()))
                    .execute();
    }

    @Override
    @RateLimited
    @Monitored
    @SneakyThrows(IOException.class)
    public ManagedZone getManagedZoneDetails(@Nonnull ManagedZoneRequest request) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(request.getManagedZone()),
                "managed zone name cannot be empty to fetch details");
        return dnsClient.managedZones().get(request.getProjectId(), extractResourceName(request.getManagedZone()))
                .setOauthToken(gcpTokenService.getOauthToken(request.getExternalAccountId()))
                .execute();
    }

    @Override
    @RateLimited
    @Monitored
    @SneakyThrows(IOException.class)
    public NetworkList getNetworks(@Nonnull ResourceRefreshRequest request) {
        return compute.networks()
                .list(request.getProjectId())
                .setOauthToken(gcpTokenService.getOauthToken(request.getExternalAccountId()))
                .setPageToken(request.getPageToken())
                .setMaxResults(request.getPageSize().longValue())
                .execute();
    }

    @Override
    @RateLimited
    @Monitored
    @SneakyThrows(IOException.class)
    public NetworkList getProjectNetworks(@Nonnull ProjectEnrichmentRequest request) {
        return compute.networks()
                .list(request.getProjectId())
                .setOauthToken(gcpTokenService.getOauthToken(request.getExternalAccountId()))
                .setPageToken(request.getPageToken())
                .setMaxResults(request.getPageSize().longValue())
                .execute();
    }

    @Override
    @RateLimited
    @Monitored
    @SneakyThrows(IOException.class)
    public Network getNetwork(@Nonnull ResourceRefreshRequest request) {
        return compute.networks()
                .get(request.getProjectId(), extractResourceName(request.getResourceName()))
                .setOauthToken(gcpTokenService.getOauthToken(request.getExternalAccountId()))
                .execute();
    }

    @Override
    @RateLimited
    @Monitored
    @SneakyThrows(IOException.class)
    public SubnetworkAggregatedList getSubNetworks(@Nonnull ResourceRefreshRequest request) {
        return compute.subnetworks()
                .aggregatedList(request.getProjectId())
                .setOauthToken(gcpTokenService.getOauthToken(request.getExternalAccountId()))
                .setPageToken(request.getPageToken())
                .setMaxResults(request.getPageSize().longValue())
                .execute();
    }

    @Override
    @RateLimited
    @Monitored
    @SneakyThrows(IOException.class)
    public Subnetwork getSubNetwork(@Nonnull ResourceRefreshRequest request) {
        return compute.subnetworks()
                .get(request.getProjectId(), request.getZone(), extractResourceName(request.getResourceName()))
                .setOauthToken(gcpTokenService.getOauthToken(request.getExternalAccountId()))
                .execute();
    }

    @Override
    @RateLimited
    @Monitored
    @SneakyThrows(IOException.class)
    public com.google.api.services.cloudresourcemanager.model.Policy getProjectIamPolicy(@Nonnull ProjectEnrichmentRequest request) {
        return cloudresourcemanager
                .projects().getIamPolicy(request.getProjectId(), new GetIamPolicyRequest())
                .setOauthToken(gcpTokenService.getOauthToken(request.getExternalAccountId()))
                .execute();
    }
}
