package com.paloaltonetworks.evident.gcp.api;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpUnsuccessfulResponseHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.regex.Pattern;

import static com.google.api.client.http.HttpStatusCodes.STATUS_CODE_UNAUTHORIZED;

public class CustomResponseHandler implements HttpUnsuccessfulResponseHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static final Pattern TOKEN_PATTERN = Pattern.compile("(Bearer) +(.*)");

    CustomResponseHandler() {
    }

    @Override
    public boolean handleResponse(HttpRequest request, HttpResponse response, boolean supportsRetry) throws IOException {
        LOGGER.debug("Request: {}", request.getUrl());
        int statusCode = response.getStatusCode();
        LOGGER.debug("Received http status code: {}", response.getStatusCode());
        switch (statusCode) {
            case STATUS_CODE_UNAUTHORIZED:
                return true;
            default:
                // if any server error - then retry
                return statusCode / 100 == 5;

        }
    }
}
