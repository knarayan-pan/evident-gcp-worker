package com.paloaltonetworks.evident.gcp.token;

import com.codahale.metrics.MetricRegistry;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.common.base.Stopwatch;
import com.paloaltonetworks.aperture.MetricConstants;
import com.paloaltonetworks.aperture.Tags;
import com.paloaltonetworks.evident.exceptions.RedisClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.concurrent.TimeUnit;

@Component(MonitoredGCPTokenService.NAME)
public class MonitoredGCPTokenService implements GCPTokenService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    public static final String NAME = "MonitoredGCPTokenService";
    private final GCPTokenService delegate;
    private final MetricRegistry metricRegistry;

    @Autowired
    public MonitoredGCPTokenService(GCPTokenService delegate, MetricRegistry metricRegistry) {
        this.delegate = delegate;
        this.metricRegistry = metricRegistry;
    }

    @Override
    public String getOauthToken(String externalAccountId) throws RedisClientException {
        Stopwatch timer = Stopwatch.createStarted();
        Tags.TagsBuilder tag = Tags.builder()
                .tag("method", "getOauthToken")
                .tag("externalAccountId", externalAccountId);
        try {
            String oauthToken = delegate.getOauthToken(externalAccountId);
            tag.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.SUCCCESS_TAG);
            return oauthToken;
        } catch (Exception e) {
            tag.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.FAILURE_TAG);
            throw new RuntimeException(e);
        } finally {
            timer.stop();
            metricRegistry.timer(tag.build().toMetricName("timer.gcp_token_service"))
                    .update(timer.elapsed(TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS);
        }
    }

    @Override
    public ServiceAccountCredentials getServiceAccountCredential(@Nonnull String externalAccountId) throws IOException {
        Stopwatch timer = Stopwatch.createStarted();
        Tags.TagsBuilder tag = Tags.builder()
                .tag("method", "getServiceAccountCredential")
                .tag("externalAccountId", externalAccountId);
        try {
            ServiceAccountCredentials serviceAccountCredential = delegate.getServiceAccountCredential(externalAccountId);
            tag.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.SUCCCESS_TAG);
            return serviceAccountCredential;
        } catch (Exception e) {
            tag.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.FAILURE_TAG);
            throw new RuntimeException(e);
        } finally {
            timer.stop();
            metricRegistry.timer(tag.build().toMetricName("timer.gcp_token_service"))
                    .update(timer.elapsed(TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS);
        }
    }

}
