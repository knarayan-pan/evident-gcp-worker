package com.paloaltonetworks.evident.gcp.api;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;

//https://cloud.google.com/logging/docs/api/v2/resource-list
public enum ResourceType {
    GCP_PROJECT("project", ImmutableSet.of("CreateProject", "UpdateProject", "SetIamPolicy"), ImmutableSet.of("DeleteProject"), "project_id", null),
    GCE_INSTANCE("gce_instance",
            ImmutableSet.of("compute.instances.insert",
                    "compute.instances.setMetadata"),
            ImmutableSet.of("compute.instances.delete"),
            null, "zone"),
    GCS_STORAGE("gcs_bucket", ImmutableSet.of(
            "storage.buckets.create",
            "storage.buckets.update",
            "storage.setIamPermissions"),
            ImmutableSet.of(
                    "storage.buckets.delete"), "bucket_name", null),
    GCE_SQL("cloudsql_database",
            ImmutableSet.of("cloudsql.instances.insert",
                    "cloudsql.instances.update", "cloudsql.instances.create"),
            ImmutableSet.of("cloudsql.instances.delete"),
            "database_id", "zone"),
    DNS_MANAGED_ZONE("dns_managed_zone", ImmutableSet.of("dns.managedZones.create"),
            ImmutableSet.of("dns.managedZones.delete"), "zone_name", null),
    GCE_FIREWALL("gce_firewall_rule",
            ImmutableSet.of(
                    "compute.firewalls.insert",
                    "compute.firewalls.update",
                    "compute.firewalls.patch"),
            ImmutableSet.of("compute.firewalls.delete"), null, null),
    GCE_NETWORK("gce_network",
            ImmutableSet.of(
                    "compute.networks.insert",
                    "compute.networks.update",
                    "compute.networks.patch"),
            ImmutableSet.of("compute.networks.delete"), "network_name", null),
    GCE_SUBNETWORK("gce_subnetwork",
            ImmutableSet.of(
                    "compute.subnetworks.insert",
                    "compute.subnetworks.update",
                    "compute.subnetworks.patch"),
            ImmutableSet.of("compute.subnetworks.delete"), "subnetwork_name", "location");
    ;

    private final String resourceType;
    private final Set<String> methodNamesRequiringResourceRefresh;
    private final Set<String> methodNamesRequiringResourceDelete;
    private final Optional<String> resourceIdLabelKey;
    private final Optional<String> zoneLabelKey;

    ResourceType(String resourceType, Set<String> methodNamesRequiringResourceRefresh,
                 Set<String> methodNamesRequiringResourceDelete, String resourceIdLabelKey,
                 String zoneLabelKey) {
        this.resourceType = resourceType;
        this.methodNamesRequiringResourceRefresh = methodNamesRequiringResourceRefresh;
        this.methodNamesRequiringResourceDelete = methodNamesRequiringResourceDelete;
        this.resourceIdLabelKey = Optional.ofNullable(resourceIdLabelKey);
        this.zoneLabelKey = Optional.ofNullable(zoneLabelKey);
    }


    @Override
    public String toString() {
        return resourceType;
    }

    public String getResourceType() {
        return resourceType;
    }

    public Boolean resourceRequiresUpdate(@Nonnull String methodName) {
        return Optional.ofNullable(methodNamesRequiringResourceRefresh)
                .map(methodNameSet -> methodNameSet
                        .stream()
                        .anyMatch(s -> methodName.toLowerCase().contains(s.toLowerCase())))
                .orElse(false);
    }

    public Boolean resourceRequiresDelete(@Nonnull String methodName) {
        return Optional.ofNullable(methodNamesRequiringResourceDelete)
                .map(methodNameSet -> methodNameSet
                        .stream()
                        .anyMatch(s -> methodName.toLowerCase().contains(s.toLowerCase())))
                .orElse(false);
    }

    public Optional<String> getResourceIdLabelKey() {
        return resourceIdLabelKey;
    }

    public Optional<String> getZoneLabelKey() {
        return zoneLabelKey;
    }

    public static String getResourceTypeFilterString() {
        return Joiner.on("=").join("resource.type", String.format("(%s)",
                Joiner.on(" OR ")
                        .join(Arrays.stream(ResourceType.values())
                                .map(ResourceType::getResourceType).toArray(String[]::new))));
    }

    public static Optional<ResourceType> fromResourceTypeString(@Nonnull String resourceType) {
        for (ResourceType type : values()) {
            if (type.getResourceType().equalsIgnoreCase(resourceType)) {
                return Optional.of(type);
            }
        }
        return Optional.empty();
    }
}
