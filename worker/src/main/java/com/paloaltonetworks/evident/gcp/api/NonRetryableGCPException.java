package com.paloaltonetworks.evident.gcp.api;

import com.paloaltonetworks.evident.exceptions.GCPApiException;

public class NonRetryableGCPException extends GCPApiException {

    public NonRetryableGCPException(int status, String message) {
        super(status, message);
    }

    public NonRetryableGCPException(GCPApiException e) {
        super(e.getStatus(), e.getMessage());
    }
}
