package com.paloaltonetworks.evident.gcp.token;

import com.google.api.client.http.HttpResponseException;
import com.google.api.services.compute.ComputeScopes;
import com.google.api.services.dns.DnsScopes;
import com.google.api.services.sqladmin.SQLAdminScopes;
import com.google.api.services.storage.StorageScopes;
import com.google.auth.oauth2.AccessToken;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableSet;
import com.google.common.util.concurrent.UncheckedExecutionException;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.esp.MonitoredESPClient;
import com.paloaltonetworks.evident.esp.NoOpESPClient;
import com.paloaltonetworks.evident.esp.model.ESPApiResponse;
import com.paloaltonetworks.evident.exceptions.GCPApiException;
import com.paloaltonetworks.evident.exceptions.GCPTokenException;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Primary
@Component
public class DefaultGCPTokenService implements GCPTokenService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final ESPClient espClient;
    private final Set<String> scopes;
    private static final Integer TOKEN_TTL_SECONDS = 50 * 60; // 50 min
    private final Cache<String, ServiceAccountCredentials> serviceAccountCredentialsCache;
    private final LoadingCache<String, String> tokenCache;
    private final static Object LOCK = new Object();

    public DefaultGCPTokenService(@Qualifier(MonitoredESPClient.NAME) ESPClient espClient) {
        this.espClient = espClient;
        scopes = ImmutableSet.<String>builder()
                .add(StorageScopes.DEVSTORAGE_FULL_CONTROL)
                .add(StorageScopes.CLOUD_PLATFORM_READ_ONLY)
                .add(ComputeScopes.COMPUTE_READONLY)
                .add(SQLAdminScopes.CLOUD_PLATFORM)
                .add(DnsScopes.NDEV_CLOUDDNS_READONLY)
                .build();
        serviceAccountCredentialsCache = CacheBuilder.<String, ServiceAccountCredentials>newBuilder()
                .expireAfterWrite(2, TimeUnit.HOURS).build();
        // local token cache
        tokenCache = CacheBuilder.newBuilder()
                .maximumSize(5000)
                .expireAfterWrite(50, TimeUnit.MINUTES)
                .build(new CacheLoader<String, String>() {
                    @Override
                    public String load(String key) throws Exception {
                        return getNewToken(key);
                    }
                });

    }


    @SuppressWarnings("deprecation") // this is a dependency on vault
    @Override
    @Nullable
    public String getOauthToken(String externalAccountId) {
        try {
            return tokenCache.get(externalAccountId);
        } catch (UncheckedExecutionException | ExecutionException | GCPApiException e) {
            LOGGER.error("Failed to refresh token for account: {}", externalAccountId, e);
            throw new GCPTokenException(String.format("Token refresh failed for account: %s", externalAccountId));
        }
    }

    private String getNewToken(String externalAccountId) {
        ServiceAccountCredentials serviceAccountCredentials;
        try {
            serviceAccountCredentials = getServiceAccountCredential(externalAccountId);
            AccessToken accessToken = serviceAccountCredentials.refreshAccessToken();
            return accessToken.getTokenValue();
        } catch (IOException e) {
            LOGGER.error("Error refreshing token for external Account Id: {} - reason: {}", externalAccountId, e.getMessage());
            // usually it'd be a bad request if this is a failure.
            int statusCode = 400;
            if (e.getCause() instanceof HttpResponseException) {
                statusCode = ((HttpResponseException) e.getCause()).getStatusCode();
            }
            throw new GCPApiException(statusCode, String.format("Error retrieving token for accountId: %s, status code: %s", externalAccountId, statusCode));
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public ServiceAccountCredentials getServiceAccountCredential(@Nonnull String externalAccountId) throws
            IOException {
        if (serviceAccountCredentialsCache.getIfPresent(externalAccountId) != null) {
            return serviceAccountCredentialsCache.getIfPresent(externalAccountId);
        } else {
            synchronized (LOCK) {
                if (serviceAccountCredentialsCache.getIfPresent(externalAccountId) == null) {
                    ESPApiResponse espApiResponse = espClient.gcpAccount(externalAccountId);
                    val builder = ServiceAccountCredentials
                            .fromStream(new ByteArrayInputStream(espApiResponse.getData().getAttributes()
                                    .get(NoOpESPClient.JSON_CREDS_KEY).toString().getBytes()))
                            .toBuilder()
                            .setScopes(scopes);
                    val serviceAccountCredentials = builder.setServiceAccountUser(builder.getClientEmail()).build();
                    serviceAccountCredentialsCache.put(externalAccountId, serviceAccountCredentials);
                }
                return serviceAccountCredentialsCache.getIfPresent(externalAccountId);
            }
        }
    }
}
