package com.paloaltonetworks.evident.gcp.models.request;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder(toBuilder = true)
public class DeleteResourcesRequest implements TenantAwareRequest {
    private final String tenant;
    private final String projectId;
    private final String organizationId;
    private final Set<String> resourceIds;
    private final String resourceType;
    private final String externalAccountId;
}
