package com.paloaltonetworks.evident.gcp.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GcpProtoPayload {

    private String methodName;
    private String resourceName;

    @JsonIgnore
    private Map<String, Object> additionalAttributes = new HashMap<>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalAttributes() {
        return additionalAttributes;
    }

    @JsonAnySetter
    public void setAdditionalAttributes(String name, Object value) {
        this.additionalAttributes.put(name, value);
    }
}
