package com.paloaltonetworks.evident.gcp.api;

/**
 * This enum is used to keep track of different api types relevant to the cloud provider api and different restrictions on them
 */
public enum GCPApiType {

    DEFAULT(100, Constants.RATE_LIMIT_PREFIX + "DEFAULT"),
    AUDIT_LOGS(30, Constants.RATE_LIMIT_PREFIX + "AUDIT_LOGS"),
    DNS(30, Constants.RATE_LIMIT_PREFIX + "DNS");


    private final int defaultRateLimit;
    private final String rateLimitConsulKey;

    GCPApiType(int defaultRateLimit, String rateLimitConsulKey) {
        this.defaultRateLimit = defaultRateLimit;
        this.rateLimitConsulKey = rateLimitConsulKey;
    }

    public int getDefaultRateLimit() {
        return defaultRateLimit;
    }

    public String getRateLimitConsulKey() {
        return rateLimitConsulKey;
    }

    @Override
    public String toString() {
        return this.name();
    }

    public static class Constants {
        static final String RATE_LIMIT_PREFIX = "worker/gcp/RATE_LIMIT_PREFIX_PER_SECOND_";
        // todo - add more granularity for rate limiting prefix.
    }
}
