package com.paloaltonetworks.evident.gcp.models.request;

import com.paloaltonetworks.evident.gcp.api.GCPApiType;

/**
 * A request that has tenancy information
 */
public interface TenantAwareRequest {
    String getTenant();
    String getExternalAccountId();

    default GCPApiType getApiType() {
        return GCPApiType.DEFAULT;
    }
}
