package com.paloaltonetworks.evident.gcp.models.request;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.gcp.api.GCPApiType;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.TimeZone;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class GcpActivityLogRequest implements TenantAwareRequest {

    @Nonnull
    private String tenant;
    @Nonnull
    private String externalAccountId;
    @Nonnull
    private Set<String> projectIds;
    @Nullable
    private String organizationId;
    @Nonnull
    private Date fromDate;
    @Nonnull
    private Date toDate;
    @Nullable
    private String pageToken;

    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    /**
     * Must ensure that we do not do too much batching here as it can cause the overall filter to exceed the limit of 20000 characters.
     * We may need to make another api request with another batch of projectIds to split the calls if required
     * <p>
     * See: https://cloud.google.com/logging/docs/reference/v2/rest/v2/entries/list
     *
     * @return String containing the serialized filter to use in the api call
     */
    public String activityFilter() {
        String resourceTypeFilterString = ResourceType.getResourceTypeFilterString();
        ImmutableSet.Builder<String> builder = ImmutableSet.builder();
        for (String projectId : projectIds) {
            builder.add(String.format("projects/%s/logs/cloudaudit.googleapis.com%%2Factivity", projectId));
        }
        String logNameFilter = String.format("logName=(%s)", Joiner.on(" OR ").join(builder.build()));
        String dateFilter = String.format("timestamp>=\"%s\" timestamp<=\"%s\"", formattedDate(fromDate), formattedDate(toDate));
        return Joiner.on(" ").join(logNameFilter, resourceTypeFilterString, dateFilter);
    }

    private String formattedDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        return format.format(date);
    }

    @Override
    public GCPApiType getApiType() {
        return GCPApiType.AUDIT_LOGS;
    }
}
