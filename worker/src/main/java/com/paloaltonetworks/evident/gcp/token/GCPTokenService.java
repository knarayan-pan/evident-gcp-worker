package com.paloaltonetworks.evident.gcp.token;

import com.google.auth.oauth2.ServiceAccountCredentials;
import com.paloaltonetworks.evident.exceptions.RedisClientException;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 * Responsible for providing the required authentication token for making api calls to GCP for analyzing resources.
 */
public interface GCPTokenService {

    /**
     * Get the oauthToken for an externalAccount
     *
     * @param externalAccountId the externalAccountId for the account being scanned.
     * @return String containing the oauth token
     * @throws RedisClientException when there is an issue talking to
     */
    String getOauthToken(String externalAccountId);

    /**
     * Get service account credentials based on external account information
     *
     * @param externalAccountId the externalAccountId for the account being scanned.
     * @return String containing the oauth token
     * @throws IOException when there is an issue talking to
     */
    ServiceAccountCredentials getServiceAccountCredential(@Nonnull String externalAccountId) throws IOException;
}
