package com.paloaltonetworks.evident.gcp.models.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FetchFirewallsRequest implements TenantAwareRequest {
    private final String tenant;
    private final String projectId;
    private final String externalAccountId;
    private final String pageToken;
    @Builder.Default
    private final Long pageSize = 30L;
}
