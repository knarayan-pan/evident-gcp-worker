package com.paloaltonetworks.evident.exceptions;

public class GCPTokenException extends RuntimeException {

    public GCPTokenException(String message) {
        super(message);
    }
}
