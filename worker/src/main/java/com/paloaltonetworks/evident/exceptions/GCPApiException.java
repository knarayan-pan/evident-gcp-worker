package com.paloaltonetworks.evident.exceptions;


import org.springframework.http.HttpStatus;

public class GCPApiException extends RuntimeException {
    private final int status;
    private final String message;

    public GCPApiException(int status, String message) {
        super(message);
        this.status = status;
        this.message = message;
    }

    public GCPApiException(int status, String message, Throwable cause) {
        super(message, cause);
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public boolean retryableException() {
        return getStatus() != HttpStatus.NOT_FOUND.value() && getStatus() != HttpStatus.FORBIDDEN.value();
    }
}
