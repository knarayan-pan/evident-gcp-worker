package com.paloaltonetworks.evident.config;

public enum ResourceWorkerQueues {
    AUDIT_LOG("audit_log"),
    FETCH_RESOURCES("fetch_resources"),
    SIGNATURE("signature");

    private final String queueName;

    ResourceWorkerQueues(String queueName) {
        this.queueName = queueName;
    }

    public String getQueueName() {
        return queueName;
    }

    @Override
    public String toString() {
        return this.name().toLowerCase();
    }
}
