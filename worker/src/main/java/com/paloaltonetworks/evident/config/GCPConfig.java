package com.paloaltonetworks.evident.config;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.paloaltonetworks.aperture.consul.ConsulService;
import com.paloaltonetworks.evident.enrichment.GCPEnrichment;
import com.paloaltonetworks.evident.enrichment.NoOpEnrichment;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.signatures.GCPSignature;
import com.paloaltonetworks.evident.signatures.Signature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Configuration
public class GCPConfig {

    private static final String KEY_DISABLED_SIGNATURES = "/v1/worker/gcp/signatures/DISABLED_LIST";
    private static final String DELIMITER_DISABLED_LIST = "\\,";
    private final List<GCPEnrichment> enrichmentList;
    private final List<GCPSignature> signatures;
    private final ConsulService consulService;

    @Autowired
    GCPConfig(List<GCPEnrichment> enrichmentList,
              List<GCPSignature> signatures,
              ConsulService consulService) {
        this.enrichmentList = enrichmentList;
        this.signatures = signatures;
        this.consulService = consulService;
    }

    @Bean
    public JsonFactory googleJsonFactory() {
        return JacksonFactory.getDefaultInstance();
    }

    @Bean
    public HttpTransport googleHttpTransport() throws GeneralSecurityException, IOException {
        return GoogleNetHttpTransport.newTrustedTransport();
    }

    public GCPEnrichment getEnrichmentClass(@Nonnull String resourceType) {
        return enrichmentList.stream()
                .filter(gcpEnrichment -> gcpEnrichment.resourceType().equalsIgnoreCase(resourceType))
                .findFirst().orElse(new NoOpEnrichment());
    }

    public Set<GCPSignature> getSignatureClass(@Nonnull String resourceType, String tenantName) {
        Set<String> disabledSignatures = Sets.newHashSet(consulService.getValueForTenant(tenantName, KEY_DISABLED_SIGNATURES, "")
                .split(DELIMITER_DISABLED_LIST));
        return signatures
                .stream()
                .filter(gcpSignature -> gcpSignature
                        .resourceTypes()
                        .stream()
                        .map(ResourceType::getResourceType)
                        .collect(Collectors.toSet())
                        .contains(resourceType))
                .filter(gcpSignature -> !disabledSignatures.contains(gcpSignature.signatureId().getValue()))
                .distinct().collect(Collectors.toSet());
    }

    /**
     * Get list of resource types for a given signature
     *
     * @param signatureId String indicating the signature Id.
     * @return Set of {@link ResourceType} objects that are related to this signature
     */
    public Set<ResourceType> getSignatureResourceTypes(@Nonnull String signatureId) {
        ImmutableSet.Builder<ResourceType> builder = ImmutableSet.builder();
        signatures.stream()
                .filter(gcpSignature -> gcpSignature != null && gcpSignature.signatureId().getValue().equalsIgnoreCase(signatureId))
                .map(Signature::resourceTypes)
                .forEach(resourceTypes -> {
                    for (ResourceType resourceType : resourceTypes) {
                        builder.add(resourceType);
                    }
                });
        return builder.build();
    }

    /**
     * Get specific signature implementation based on id;
     *
     * @param sigId the sig id value in the enum {@link com.paloaltonetworks.evident.signatures.SignatureIdentifier}
     * @return Optional implementation of the signature
     */
    public Optional<GCPSignature> getSignatureById(String sigId) {
        return signatures.stream().filter(gcpSignature -> gcpSignature.signatureId().getValue().equals(sigId)).findFirst();
    }
}
