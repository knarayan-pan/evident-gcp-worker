package com.paloaltonetworks.evident.config;

import com.paloaltonetworks.evident.messaging.queue.MessagePoller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Objects;

@Component
public class ContextClosedHandler implements ApplicationListener<ContextClosedEvent> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final List<MessagePoller> queuePollerList;

    @Autowired
    public ContextClosedHandler(List<MessagePoller> queuePollerList) {
        this.queuePollerList = queuePollerList;
    }

    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        LOGGER.info("ContextClosedHandler.onApplicationEvent Staring");
        if(CollectionUtils.isEmpty(queuePollerList)) {
            LOGGER.info("No active pollers available. Nothing to close/dispose");
        } else {
            queuePollerList.stream().filter(Objects::nonNull).forEach(MessagePoller::stop);
        }
    }
}
