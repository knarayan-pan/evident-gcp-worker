package com.paloaltonetworks.evident.config;

import com.paloaltonetworks.aperture.consul.ConsulService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.lang.invoke.MethodHandles;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Configuration
public class ThreadConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private PropertyHandler propertyHandler;

    /*
        https://stackoverflow.com/questions/13834692/threads-configuration-based-on-no-of-cpu-cores
		N_threads = N_cpu * U_cpu * (1 + W / C)
		Where:
		N_threads is the optimal number of threads
		N_cpu is the number of processors, which you can obtain from Runtime.getRuntime().availableProcessors();
		U_cpu is the target CPU utilization (1 Øif you want to use the full available resources)
		W / C is the ratio of wait time to compute time (0 for CPU-bound task, maybe 10 or 100 for slow I/O tasks)
	*/

    private static final String EVENTS_THREADS_COUNT_KEY = "/v1/worker/gcp/EVENTS_THREAD_COUNT";
    private static final Integer EVENTS_THREAD_COUNT_DEFAULT_VALUE = 10;
    private static final String RESOURCE_REFRESH_THREADS_COUNT_KEY = "/v1/worker/gcp/RESOURCE_REFRESH_THREADS_COUNT_KEY";
    private static final Integer RESOURCE_REFRESH_THREAD_COUNT_DEFAULT_VALUE = 12;
    private static final String RESOURCE_SIGNATURE_THREADS_COUNT_KEY = "/v1/worker/gcp/RESOURCE_SIGNATURE_THREADS_COUNT_KEY";
    private static final Integer RESOURCE_SIGNATURE_THREAD_COUNT_DEFAULT_VALUE = 10;

    @Autowired
    public ThreadConfig(PropertyHandler propertyHandler) {
        this.propertyHandler = propertyHandler;
    }

    @Bean
    public int eventsThreadCount(ConsulService consulService) {
        int eventsThreadCount = consulService.getIntForTenant(propertyHandler.getTenant(), EVENTS_THREADS_COUNT_KEY,
                EVENTS_THREAD_COUNT_DEFAULT_VALUE);
        LOGGER.debug("eventsThreadCount = {}", eventsThreadCount);
        return eventsThreadCount;
    }

    @Bean
    public int resourceRefreshThreadCount(ConsulService consulService) {
        int resourceRefreshThreadCount = consulService.getIntForTenant(propertyHandler.getTenant(), RESOURCE_REFRESH_THREADS_COUNT_KEY,
                RESOURCE_REFRESH_THREAD_COUNT_DEFAULT_VALUE);
        LOGGER.debug("resourceRefreshThreadCount = {}", resourceRefreshThreadCount);
        return resourceRefreshThreadCount;
    }

    @Bean
    public int signatureThreadCount(ConsulService consulService) {
        int signatureThreadCount = consulService.getIntForTenant(propertyHandler.getTenant(), RESOURCE_SIGNATURE_THREADS_COUNT_KEY,
                RESOURCE_SIGNATURE_THREAD_COUNT_DEFAULT_VALUE);
        LOGGER.debug("signatureThreadCount = {}", signatureThreadCount);
        return signatureThreadCount;
    }


    @Bean(destroyMethod = "shutdown")
    public Executor taskExecutor() {
        return Executors.newScheduledThreadPool(1);
    }

}
