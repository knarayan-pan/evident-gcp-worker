package com.paloaltonetworks.evident.schedule;

import org.springframework.lang.Nullable;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.Executor;

public abstract class AbstractScheduler implements SchedulingConfigurer {
    private final Executor taskExecutor;

    AbstractScheduler(Executor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    /**
     * Get scheduler interval from consul in seconds
     *
     * @return integer indicating the scheduler interval in seconds
     */
    abstract int getSchedulerInterval();

    /**
     * schedule task that gets called on the interval trigger
     */
    abstract void schedule();

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setScheduler(taskExecutor);
        taskRegistrar.addTriggerTask(this::schedule, new Trigger() {
            @Nullable
            @Override
            public Date nextExecutionTime(TriggerContext triggerContext) {
                int scheduledInterval = getSchedulerInterval();
                Calendar nextExecutionTime = new GregorianCalendar();
                Date lastActualExecutionTime = triggerContext.lastActualExecutionTime();
                nextExecutionTime.setTime(lastActualExecutionTime != null ? lastActualExecutionTime : new Date());
                nextExecutionTime.add(Calendar.SECOND, scheduledInterval + 1);
                return nextExecutionTime.getTime();
            }
        });
    }
}
