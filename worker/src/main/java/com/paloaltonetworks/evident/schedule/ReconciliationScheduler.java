package com.paloaltonetworks.evident.schedule;

import com.paloaltonetworks.aperture.consul.ConsulService;
import com.paloaltonetworks.evident.config.PropertyHandler;
import com.paloaltonetworks.evident.config.ResourceWorkerQueues;
import com.paloaltonetworks.evident.exceptions.RedisClientException;
import com.paloaltonetworks.evident.redis.MonitoredRedisClient;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.lang.invoke.MethodHandles;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableScheduling
public class ReconciliationScheduler extends AbstractScheduler {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    static final String RECONCILIATION_INTERVAL_IN_HOURS_KEY = "worker/gcp/RECONCILIATION_INTERVAL_IN_HOURS";
    public static final int DEFAULT_RECONCILIATION_INTERVAL_IN_HOURS = 12;
    private static final String LOCK_FORMAT = "%s_gcp_reconcile";

    private final ConsulService consulService;
    private final PropertyHandler propertyHandler;
    private final RedisClient redisClient;
    private final QueueService queueService;

    public ReconciliationScheduler(ConsulService consulService, Executor taskExecutor, PropertyHandler propertyHandler,
                                   @Qualifier(MonitoredRedisClient.NAME) RedisClient redisClient, QueueService queueService) {
        super(taskExecutor);
        this.consulService = consulService;
        this.propertyHandler = propertyHandler;
        this.redisClient = redisClient;
        this.queueService = queueService;
    }

    @Override
    int getSchedulerInterval() {
        return (int) TimeUnit.HOURS.toSeconds(consulService.getIntForTenant(propertyHandler.getTenant(),
                RECONCILIATION_INTERVAL_IN_HOURS_KEY, DEFAULT_RECONCILIATION_INTERVAL_IN_HOURS));
    }

    @Override
    void schedule() {
        // get lock
        long scheduledInterval = getSchedulerInterval();
        String lockId = String.format(LOCK_FORMAT, propertyHandler.getExternalId());
        try {
            Optional<String> token = redisClient.acquireLock(lockId, scheduledInterval);
            if (token.isPresent()) {
                LOGGER.info("Acquired lock for Reconciliation task scheduling");
                queueService.queueStartTask(propertyHandler.getTenant(), propertyHandler.getExternalId(), true);
            }
        } catch (RedisClientException e) {
            LOGGER.error("Error connecting to Redis proxy service: {}. Will retry on next schedule", e.getMessage());
        }
    }
}
