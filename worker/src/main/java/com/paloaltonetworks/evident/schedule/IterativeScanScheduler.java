package com.paloaltonetworks.evident.schedule;

import com.paloaltonetworks.aperture.consul.ConsulService;
import com.paloaltonetworks.evident.config.PropertyHandler;
import com.paloaltonetworks.evident.config.ResourceWorkerQueues;
import com.paloaltonetworks.evident.exceptions.RedisClientException;
import com.paloaltonetworks.evident.redis.MonitoredRedisClient;
import com.paloaltonetworks.evident.redis.RedisClient;
import com.paloaltonetworks.evident.service.QueueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.lang.invoke.MethodHandles;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableScheduling
public class IterativeScanScheduler extends AbstractScheduler {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    static final String ITERATIVE_SCAN_INTERVAL_IN_MIN_KEY = "worker/gcp/ITERATIVE_SCAN_INTERVAL_IN_MIN";
    static final int DEFAULT_ITERATIVE_SCAN_INTERVAL_IN_MIN = 1;
    private static final String LOCK_FORMAT = "%s_gcp_iterative";

    private final ConsulService consulService;
    private final PropertyHandler propertyHandler;
    private final RedisClient redisClient;
    private final QueueService queueService;

    @Autowired
    public IterativeScanScheduler(ConsulService consulService, Executor taskExecutor,
                                  PropertyHandler propertyHandler, @Qualifier(MonitoredRedisClient.NAME) RedisClient redisClient,
                                  QueueService queueService) {
        super(taskExecutor);
        this.consulService = consulService;
        this.propertyHandler = propertyHandler;
        this.redisClient = redisClient;
        this.queueService = queueService;
    }

    @Override
    int getSchedulerInterval() {
        return (int) TimeUnit.MINUTES.toSeconds(consulService.getIntForTenant(propertyHandler.getTenant(),
                ITERATIVE_SCAN_INTERVAL_IN_MIN_KEY, DEFAULT_ITERATIVE_SCAN_INTERVAL_IN_MIN));
    }

    @Override
    void schedule() {

        // get lock
        int scheduledInterval = getSchedulerInterval();
        String lockId = String.format(LOCK_FORMAT, propertyHandler.getExternalId());
        try {
            Optional<String> token = redisClient.acquireLock(lockId, scheduledInterval);
            if (token.isPresent()) {
                long auditLogBackup = queueService.getNumMessages(propertyHandler.getExternalId(),
                        ResourceWorkerQueues.AUDIT_LOG.getQueueName());
                long resourceFetchQueue = queueService.getNumMessages(propertyHandler.getExternalId(),
                        ResourceWorkerQueues.FETCH_RESOURCES.getQueueName());

                if (auditLogBackup + resourceFetchQueue > 0L) {
                    LOGGER.warn("Iterative tasks are still being processed - will not queue this iteration. Backpressure on queue: is {}",
                            (auditLogBackup + resourceFetchQueue));
                    return;
                }
                LOGGER.info("Acquired lock for iterative task scheduling");
                queueService.queueStartTask(propertyHandler.getTenant(), propertyHandler.getExternalId(), false);
            }
        } catch (RedisClientException e) {
            LOGGER.error("Error connecting to Redis proxy service: {}. Will retry on next schedule", e.getMessage());
        }
    }
}
