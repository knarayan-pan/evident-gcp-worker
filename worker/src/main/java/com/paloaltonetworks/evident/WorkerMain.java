package com.paloaltonetworks.evident;


import com.paloaltonetworks.evident.config.PropertyHandler;
import com.paloaltonetworks.evident.config.ResourceWorkerQueues;
import com.paloaltonetworks.evident.messaging.queue.MessagePoller;
import com.paloaltonetworks.evident.service.QueueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.cassandra.CassandraDataAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.lang.invoke.MethodHandles;
import java.util.List;

@SpringBootApplication
@EnableAutoConfiguration(exclude = {CassandraDataAutoConfiguration.class})
public class WorkerMain {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private List<MessagePoller> queuePollerList;
    private final QueueService queueService;
    private final PropertyHandler propertyHandler;

    @Autowired
    public WorkerMain(List<MessagePoller> queuePollerList, QueueService queueService, PropertyHandler propertyHandler) {
        this.queuePollerList = queuePollerList;
        this.queueService = queueService;
        this.propertyHandler = propertyHandler;
    }

    public static void main(String... args) {
        new SpringApplicationBuilder(WorkerMain.class)
                .web(WebApplicationType.REACTIVE)
                .run(args);
    }

    @EventListener
    public void deployPollers(ApplicationReadyEvent event) {
        queuePollerList.forEach(messagePoller -> {
            try {
                messagePoller.start(messagePoller.getThreadCount());
            } catch (InterruptedException e) {
                LOGGER.error("Thread creation was interrupted for queue: {} due to {}",
                        messagePoller.getClass().getName(), e.getMessage());
            }
        });
        long resourceFetchQueue = queueService.getNumMessages(propertyHandler.getExternalId(),
                ResourceWorkerQueues.FETCH_RESOURCES.getQueueName());

        if (resourceFetchQueue > 0L) {
            LOGGER.warn("Resource refresh tasks are still being processed - will not queue start task at this time..");
        } else {
            LOGGER.info("Queueing start task to initiate work...");
            // queue first start task.
            queueService.queueStartTask(propertyHandler.getTenant(), propertyHandler.getExternalId(), false);
        }
    }
}
