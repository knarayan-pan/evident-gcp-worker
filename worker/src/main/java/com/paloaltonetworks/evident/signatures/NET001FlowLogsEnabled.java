package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.enrichment.metadata.SubNetworkMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import lombok.val;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.Set;

import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCE_SUBNETWORK;
import static com.paloaltonetworks.evident.signatures.SignatureIdentifier.GCP_NET_001;

/*
 * Ensure autobackup is not enabled
 */
@Component
public class NET001FlowLogsEnabled extends GCPSignature {
    private static final String PASS_MESSAGE = "Flow logs is enabled on subnet %s.";
    private static final String FAIL_MESSAGE = "Flow logs is not enabled on subnet %s.";

    NET001FlowLogsEnabled(ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    public SignatureIdentifier signatureId() {
        return GCP_NET_001;
    }

    @Override
    public Set<ResourceType> resourceTypes() {
        return ImmutableSet.of(GCE_SUBNETWORK);
    }

    @Override
    public Mono<JSONObject> metadata(Resource resource) {
        JSONObject metadata = new JSONObject();
        val resourceMetadata = resource.getResourceMetadata(SubNetworkMetadata.class);
        metadata.put("name", resourceMetadata.getResourceName());
        metadata.put("creationTimeStamp", resourceMetadata.getCreationTimeStamp());
        metadata.put("network", resourceMetadata.getNetwork());
        metadata.put("ipCidrRange", resourceMetadata.getIpCidrRanges());
        metadata.put("enableFlowLogs", resourceMetadata.isEnableFlowLogs());
        metadata.put("project_id", resourceMetadata.getProjectId());
        metadata.put("region", resourceMetadata.getRegion());
        return Mono.just(metadata);
    }

    @Override
    public Tuple2<AlertStatus, String> alertStatusAndMessage(Resource resource) {
        val inst = resource.getResourceMetadata(SubNetworkMetadata.class);
        return inst.isEnableFlowLogs() ? passTuple(String.format(PASS_MESSAGE, inst.getSelfLink()))
                : failTuple(String.format(FAIL_MESSAGE, inst.getSelfLink()));
    }
}
