package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.enrichment.metadata.DefaultKeySpecs;
import com.paloaltonetworks.evident.enrichment.metadata.DnsManagedZoneMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import lombok.val;
import org.apache.commons.collections.CollectionUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DNS002KeySigningCheck extends GCPSignature {
    private static final String PASS_MESSAGE = "RSASHA1 is not in use for key signing in managed zone %s in project %s";
    private static final String FAIL_MESSAGE = "RSASHA1 is in use for key signing in managed zone %s in project %s";

    DNS002KeySigningCheck(ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    public SignatureIdentifier signatureId() {
        return SignatureIdentifier.GCP_DNS_002;
    }

    @Override
    public Set<ResourceType> resourceTypes() {
        return ImmutableSet.of(ResourceType.DNS_MANAGED_ZONE);
    }

    @Override
    public Mono<JSONObject> metadata(Resource resource) {
        JSONObject metadata = new JSONObject();
        val resourceMetadata = resource.getResourceMetadata(DnsManagedZoneMetadata.class);
        metadata.put("projectId", resourceMetadata.getProjectId());
        metadata.put("name", resourceMetadata.getResourceName());
        metadata.put("dnsName", resourceMetadata.getDnsName());
        metadata.put("configState", resourceMetadata.getDnsSecConfigState());
        metadata.put("defaultKeySpecs", resourceMetadata.getDefaultKeySpecsList());
        metadata.put("description", resourceMetadata.getDescription());
        metadata.put("tags", resourceMetadata.getTags());
        return Mono.just(metadata);
    }

    @Override
    public Tuple2<AlertStatus, String> alertStatusAndMessage(Resource resource) {
        val resourceMetadata = resource.getResourceMetadata(DnsManagedZoneMetadata.class);
        Set<DefaultKeySpecs> filteredKeySpec = CollectionUtils.isEmpty(resourceMetadata.getDefaultKeySpecsList()) ?
                new HashSet<>() : resourceMetadata.getDefaultKeySpecsList()
                .stream()
                .filter(defaultKeySpecs -> "keySigning".equalsIgnoreCase(defaultKeySpecs.getKeyType()) &&
                        "rsasha1".equalsIgnoreCase(defaultKeySpecs.getAlgorithm())).collect(Collectors.toSet());

        return ("on".equalsIgnoreCase(resourceMetadata.getDnsSecConfigState()) && CollectionUtils.isNotEmpty(filteredKeySpec)) ?
                failTuple(String.format(FAIL_MESSAGE, resourceMetadata.getResourceName(), resourceMetadata.getProjectId())) :
                passTuple(String.format(PASS_MESSAGE, resourceMetadata.getResourceName(), resourceMetadata.getProjectId()));
    }
}
