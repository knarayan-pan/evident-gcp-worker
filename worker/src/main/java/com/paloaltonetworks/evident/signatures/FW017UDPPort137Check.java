package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.paloaltonetworks.evident.esp.ESPClient;
import org.springframework.stereotype.Component;

import static com.paloaltonetworks.evident.signatures.AbstractExactPortCheck.Protocol.UDP;
import static com.paloaltonetworks.evident.signatures.SignatureIdentifier.GCP_FW_017;

@Component
public class FW017UDPPort137Check extends AbstractExactPortCheck {

    FW017UDPPort137Check(ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    public SignatureIdentifier signatureId() {
        return GCP_FW_017;
    }

    @Override
    Protocol protocol() {
        return UDP;
    }

    @Override
    int port() {
        return 137;
    }
}
