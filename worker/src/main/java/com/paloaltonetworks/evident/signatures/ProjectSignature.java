package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.Sets;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.enrichment.metadata.ProjectMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.esp.MonitoredESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.utils.JsonUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.Set;

public class ProjectSignature extends GCPSignature {

    @Autowired
    public ProjectSignature(@Qualifier(MonitoredESPClient.NAME) ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    public SignatureIdentifier signatureId() {
        return SignatureIdentifier.GCP_PRO_001;
    }

    @Override
    public Set<ResourceType> resourceTypes() {
        return Sets.newHashSet(ResourceType.GCP_PROJECT);
    }

    @Override
    public Mono<JSONObject> metadata(Resource resource) {
        return Mono.just(new JSONObject(JsonUtils.encode(resource.getResourceMetadata(ProjectMetadata.class))));
    }

    @Override
    public Tuple2<AlertStatus, String> alertStatusAndMessage(Resource resource) {
        return passTuple(String.format("project passed signature %s", signatureId()));
    }
}
