package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.enrichment.metadata.ProjectMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.utils.IamUtils;
import lombok.val;
import org.apache.commons.lang3.tuple.Triple;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCP_PROJECT;
import static com.paloaltonetworks.evident.signatures.SignatureIdentifier.GCP_IAM_005;

/*
 * Ensure User does not have ServiceAccountUser role at the project level
 */
@Component
public class IAM005NoServiceAccountUser extends AbstractMultiAlertsSignature {
    private static final String PASS_MESSAGE = "User %s does not have ServiceAccountUser role at the project level.";
    private static final String FAIL_MESSAGE = "User %s has ServiceAccountUser role at the project level.";
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    IAM005NoServiceAccountUser(ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    public SignatureIdentifier signatureId() {
        return GCP_IAM_005;
    }

    @Override
    public Set<ResourceType> resourceTypes() {
        return ImmutableSet.of(GCP_PROJECT);
    }

    protected Mono<Map<String, Triple<AlertStatus, String, JSONObject>>> alertsStatusMessageAndMetadata(Resource resource) {
        val resourceMetadata = resource.getResourceMetadata(ProjectMetadata.class);
        if (resourceMetadata.getIamPolicy() == null) {
            // gcp api calls throw non-retriable exceptions when retrieving IamPolicy
            LOGGER.error("Resource null for signature GCP:IAM-005");
            return Mono.empty();
        }
        val roleMembers = resourceMetadata.getIamPolicy().getRoleMembers();
        Map<String, List<String>> memberRoleMap = IamUtils.getUserRoles(roleMembers, Predicates.alwaysTrue(), Predicates.alwaysTrue());
        Map<String, Triple<AlertStatus, String, JSONObject>> userStatusMessageMap = statusMessageMetadataMap(memberRoleMap);
        return Mono.just(userStatusMessageMap);
    }

    /**
     *
     * @param memberRoleMap
     * @return Map of member to Triple
     */
    private Map<String, Triple<AlertStatus, String, JSONObject>> statusMessageMetadataMap(Map<String, List<String>> memberRoleMap) {
        Map<String, Triple<AlertStatus, String, JSONObject>> statusMessageMetadataMap = Maps.newLinkedHashMap();
        memberRoleMap.forEach((user, roles) -> {
            AlertStatus status = (roles.contains("roles/iam.serviceAccountUser")) ? AlertStatus.FAIL : AlertStatus.PASS;
            String message = (status == AlertStatus.PASS) ? String.format(PASS_MESSAGE, user) : String.format(FAIL_MESSAGE, user);
            JSONObject metadata = new JSONObject();
            metadata.put("email", user);
            metadata.put("roles", roles);
            statusMessageMetadataMap.put(user, Triple.of(status, message, metadata));
        });
        return statusMessageMetadataMap;
    }
}
