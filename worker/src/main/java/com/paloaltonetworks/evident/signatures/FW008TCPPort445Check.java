package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.paloaltonetworks.evident.esp.ESPClient;
import org.springframework.stereotype.Component;

import static com.paloaltonetworks.evident.signatures.AbstractExactPortCheck.Protocol.TCP;
import static com.paloaltonetworks.evident.signatures.SignatureIdentifier.GCP_FW_008;

@Component
public class FW008TCPPort445Check extends AbstractExactPortCheck {

    FW008TCPPort445Check(ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    public SignatureIdentifier signatureId() {
        return GCP_FW_008;
    }

    @Override
    Protocol protocol() {
        return TCP;
    }

    @Override
    int port() {
        return 445;
    }
}
