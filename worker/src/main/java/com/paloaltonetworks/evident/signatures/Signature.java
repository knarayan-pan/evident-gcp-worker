package com.paloaltonetworks.evident.signatures;

import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import org.json.JSONObject;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import javax.annotation.Nonnull;
import java.util.Set;

public interface Signature {

    /**
     * Every implementation must provide the signature identifier that is unique
     * See {@link SignatureIdentifier} for more details
     *
     * @return {@link SignatureIdentifier}
     */
    SignatureIdentifier signatureId();

    /**
     * Set of resource types this signature applies to
     *
     * @return {@link ResourceType} set
     */
    Set<ResourceType> resourceTypes();

    /**
     * List of metadata to return to ESP web to indicate to customer the state of the resource
     *
     * @param resource the {@link Resource} object from persistent storage if available to apply/check this signature for
     * @return {@link JSONObject} containing a representation of the resource state
     */
    Mono<JSONObject> metadata(Resource resource);

    /**
     * Provide the alert status and the message to use to report the alert to ESP Web.
     *
     * @param resource the {@link Resource} object from persistent storage if available to apply/check this signature for
     * @return {@link Tuple2} indicating the status of the alert and associated message
     */
    Tuple2<AlertStatus, String> alertStatusAndMessage(Resource resource);

    /**
     * entry point to signature checking where other helper methods are used to determine what needs to be reported
     *
     * @param resource the {@link Resource} object from persistent storage if available to apply/check this signature for
     */
    void check(@Nonnull Resource resource);

    // default alert status and message
    default Tuple2<AlertStatus, String> passTuple(String message) {
        return Tuples.of(AlertStatus.PASS, message);
    }

    default Tuple2<AlertStatus, String> failTuple(String message) {
        return Tuples.of(AlertStatus.FAIL, message);
    }

    default Tuple2<AlertStatus, String> infoTuple(String message) {
        return Tuples.of(AlertStatus.INFO, message);
    }

    default Tuple2<AlertStatus, String> warnTuple(String message) {
        return Tuples.of(AlertStatus.WARN, message);
    }
}
