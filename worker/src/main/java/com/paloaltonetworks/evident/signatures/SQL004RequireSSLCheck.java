package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.enrichment.metadata.SQLInstanceMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import lombok.val;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCE_SQL;
import static com.paloaltonetworks.evident.signatures.SignatureIdentifier.GCP_SQL_003;
import static com.paloaltonetworks.evident.signatures.SignatureIdentifier.GCP_SQL_004;

/**
 * Ensure that Cloud SQL database instance requires all incoming connections to use SSL
 */
@Component
public class SQL004RequireSSLCheck extends GCPSignature {
    private static final String PASS_MESSAGE = "Cloud SQL %s in project %s has SSL connection enforcement.";
    private static final String FAIL_MESSAGE = "Cloud SQL %s in project %s does not have SSL connection enforcement.";

    SQL004RequireSSLCheck(ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    public SignatureIdentifier signatureId() {
        return GCP_SQL_004;
    }

    @Override
    public Set<ResourceType> resourceTypes() {
        return ImmutableSet.of(GCE_SQL);
    }

    @Override
    public Mono<JSONObject> metadata(Resource resource) {
        JSONObject metadata = new JSONObject();
        val resourceMetadata = resource.getResourceMetadata(SQLInstanceMetadata.class);
        metadata.put("name", resourceMetadata.getResourceName());
        metadata.put("requireSSL", resourceMetadata.isRequireSSL());
        metadata.put("connectionName", resourceMetadata.getConnectionName());
        metadata.put("databaseVersion", resourceMetadata.getDatabaseVersion());
        metadata.put("zone", resourceMetadata.getZone());
        metadata.put("projectId", resourceMetadata.getProjectId());
        metadata.put("region", resourceMetadata.getRegion());
        metadata.put("selfLink", resourceMetadata.getSelfLink());
        return Mono.just(metadata);
    }

    @Override
    public Tuple2<AlertStatus, String> alertStatusAndMessage(Resource resource) {
        val inst = resource.getResourceMetadata(SQLInstanceMetadata.class);
        return inst.isRequireSSL() ?
                passTuple(String.format(PASS_MESSAGE, inst.getResourceName(), inst.getProjectId())) :
                failTuple(String.format(FAIL_MESSAGE, inst.getResourceName(), inst.getProjectId()));
    }
}
