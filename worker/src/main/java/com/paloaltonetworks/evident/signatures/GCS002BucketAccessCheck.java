package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.enrichment.metadata.BucketAcl;
import com.paloaltonetworks.evident.enrichment.metadata.BucketMetadata;
import com.paloaltonetworks.evident.enrichment.metadata.PolicyBindings;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.esp.MonitoredESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import org.apache.commons.collections.CollectionUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.Set;

import static com.paloaltonetworks.evident.utils.ResourceUtil.extractResourceName;

@Component
public class GCS002BucketAccessCheck extends GCPSignature {
    private final String PASS_MESSAGE = "Bucket %s in project %s does not allow public access";
    private final String FAIL_MESSAGE = "Bucket %s in project %s allows public access";
    private final String ALL_AUTHENTICATED_USERS = "allAuthenticatedUsers";
    private final String ALL_USERS = "allUsers";
    private final Set<String> publicAccessMembership = ImmutableSet.<String>builder()
            .add(ALL_AUTHENTICATED_USERS)
            .add(ALL_USERS)
            .build();

    GCS002BucketAccessCheck(@Qualifier(MonitoredESPClient.NAME) ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    public SignatureIdentifier signatureId() {
        return SignatureIdentifier.GCP_GCS_002;
    }

    @Override
    public Set<ResourceType> resourceTypes() {
        return ImmutableSet.<ResourceType>builder()
                .add(ResourceType.GCS_STORAGE)
                .build();
    }

    @Override
    public Mono<JSONObject> metadata(Resource resource) {
        JSONObject metadata = new JSONObject();
        BucketMetadata resourceMetadata = resource.getResourceMetadata(BucketMetadata.class);
        metadata.put("projectId", resourceMetadata.getProjectId());
        metadata.put("name", extractResourceName(resource.getResourceId()));
        metadata.put("dateCreated", resourceMetadata.getDateCreated());
        metadata.put("location", resourceMetadata.getLocation());
        metadata.put("storageClass", resourceMetadata.getStorageClass());
        metadata.put("policy", resourceMetadata.getPolicy());
        metadata.put("acl", resourceMetadata.getBucketAccessControl());
        metadata.put("tags", resourceMetadata.getTags());
        return Mono.just(metadata);
    }

    @Override
    public Tuple2<AlertStatus, String> alertStatusAndMessage(Resource resource) {
        BucketMetadata bucketMetadata = resource.getResourceMetadata(BucketMetadata.class);
        if (CollectionUtils.isNotEmpty(bucketMetadata.getPolicy().getBindings())) {
            for (PolicyBindings bindings : bucketMetadata.getPolicy().getBindings()) {
                if (CollectionUtils.containsAny(bindings.getMembers(), publicAccessMembership)) {
                    return failTuple(String.format(FAIL_MESSAGE, resource.getResourceId(), bucketMetadata.getProjectId()));
                }
            }
        }
        // also check the acl and ensure that it does not allow access
        if (CollectionUtils.isNotEmpty(bucketMetadata.getBucketAccessControl())) {
            for (BucketAcl accessControl : bucketMetadata.getBucketAccessControl()) {
                if (publicAccessMembership.contains(accessControl.getEntity())) {
                    return failTuple(String.format(FAIL_MESSAGE, resource.getResourceId(), bucketMetadata.getProjectId()));
                }
            }
        }
        return passTuple(String.format(PASS_MESSAGE, resource.getResourceId(), bucketMetadata.getProjectId()));
    }
}
