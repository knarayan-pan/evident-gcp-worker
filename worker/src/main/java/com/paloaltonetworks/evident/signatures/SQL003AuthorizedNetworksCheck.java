package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.enrichment.metadata.SQLInstanceMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import lombok.val;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCE_SQL;
import static com.paloaltonetworks.evident.signatures.SignatureIdentifier.GCP_SQL_003;

/**
 * Ensure that Cloud SQL database Instances are not open to the world
 */
@Component
public class SQL003AuthorizedNetworksCheck extends GCPSignature {
    private static final String PASS_MESSAGE = "Cloud SQL %s in project %s is not exposed publicly.";
    private static final String FAIL_MESSAGE = "Cloud SQL %s in project %s is exposed publicly.";
    private static final String EXPOSED_IP_MASK = "0.0.0.0";
    private static final String EXPOSED_CIDR = "/0";

    SQL003AuthorizedNetworksCheck(ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    public SignatureIdentifier signatureId() {
        return GCP_SQL_003;
    }

    @Override
    public Set<ResourceType> resourceTypes() {
        return ImmutableSet.of(GCE_SQL);
    }

    @Override
    public Mono<JSONObject> metadata(Resource resource) {
        JSONObject metadata = new JSONObject();
        val resourceMetadata = resource.getResourceMetadata(SQLInstanceMetadata.class);
        metadata.put("name", resourceMetadata.getResourceName());
        metadata.put("authorizedNetworks", exposedNetworks(resourceMetadata));
        metadata.put("connectionName", resourceMetadata.getConnectionName());
        metadata.put("databaseVersion", resourceMetadata.getDatabaseVersion());
        metadata.put("zone", resourceMetadata.getZone());
        metadata.put("project_id", resourceMetadata.getProjectId());
        metadata.put("region", resourceMetadata.getRegion());
        metadata.put("selfLink", resourceMetadata.getSelfLink());
        return Mono.just(metadata);
    }

    @Override
    public Tuple2<AlertStatus, String> alertStatusAndMessage(Resource resource) {
        val inst = resource.getResourceMetadata(SQLInstanceMetadata.class);
        return exposedNetworks(inst).isEmpty() ?
                passTuple(String.format(PASS_MESSAGE, inst.getResourceName(), inst.getProjectId())) :
                failTuple(String.format(FAIL_MESSAGE, inst.getResourceName(), inst.getProjectId()));
    }

    private List<Map<String, Object>> exposedNetworks(SQLInstanceMetadata inst) {
        return inst.getAuthorizedNetworks()
                .stream()
                .filter(naclMap ->
                        Optional.of(naclMap)
                                .map(nacl -> (String) nacl.get("value"))
                                .map(val -> (val.contains(EXPOSED_IP_MASK) || val.contains(EXPOSED_CIDR)))
                                .orElse(false))
                .collect(Collectors.toList());
    }
}
