package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.enrichment.metadata.InstanceMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import lombok.val;
import org.apache.commons.lang3.BooleanUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.Optional;
import java.util.Set;

import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCE_INSTANCE;
import static com.paloaltonetworks.evident.signatures.SignatureIdentifier.GCP_GCE_003;

/**
 * Compliance Mappings: CIS 3.7
 * Ensure 'Enable connecting to serial ports' is not enabled
 */
@Component
public class GCE003SerialPortsEnabledCheck extends GCPSignature {
    private static final String PASS_MESSAGE = "VM Instance %s in project %s does not have connecting to serial ports enabled.";
    private static final String FAIL_MESSAGE = "VM Instance %s in project %s has connecting to serial ports enabled.";
    private static final String KEY_SERIAL_PORT_ENABLE = "serial-port-enable";

    GCE003SerialPortsEnabledCheck(ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    public SignatureIdentifier signatureId() {
        return GCP_GCE_003;
    }

    @Override
    public Set<ResourceType> resourceTypes() {
        return ImmutableSet.of(GCE_INSTANCE);
    }

    @Override
    public Mono<JSONObject> metadata(Resource resource) {
        JSONObject metadata = new JSONObject();
        val resourceMetadata = resource.getResourceMetadata(InstanceMetadata.class);
        metadata.put("zone", resourceMetadata.getZone());
        metadata.put("projectId", resourceMetadata.getProjectId());
        metadata.put("name", resourceMetadata.getName());
        metadata.put("description", resourceMetadata.getDescription());
        metadata.put("cpuPlatform", resourceMetadata.getCpuPlatform());
        metadata.put("id", resourceMetadata.getId());
        metadata.put("machineType", resourceMetadata.getMachineType());
        metadata.put("createTimestamp", resourceMetadata.getCreateTimestamp());
        metadata.put("selfLink", resourceMetadata.getSelfLink());
        metadata.put("status", resourceMetadata.getStatus());
        metadata.put("canIpForward", resourceMetadata.isCanIpForward());
        metadata.put("tags", resourceMetadata.getTags());
        metadata.put("metadata", ImmutableMap.of(KEY_SERIAL_PORT_ENABLE, serialPortEnabled(resourceMetadata)));
        return Mono.just(metadata);    }

    @Override
    public Tuple2<AlertStatus, String> alertStatusAndMessage(Resource resource) {
        val inst = resource.getResourceMetadata(InstanceMetadata.class);
        return serialPortEnabled(inst) ?
                failTuple(String.format(FAIL_MESSAGE, inst.getName(), inst.getProjectId())) :
                passTuple(String.format(PASS_MESSAGE, inst.getName(), inst.getProjectId()));
    }

    private Boolean serialPortEnabled(InstanceMetadata inst) {
        return Optional.ofNullable(inst.getMetadata())
                .map(md -> md.get(KEY_SERIAL_PORT_ENABLE))
                .map(BooleanUtils::toBoolean)
                .orElse(false);
    }
}
