package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.enrichment.metadata.IamPolicy;
import com.paloaltonetworks.evident.enrichment.metadata.ProjectMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import lombok.val;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class IAM001CloudAuditLogging extends GCPSignature {
    private final static String PASS_MESSAGE = "Project %s has Admin Activity and Data Access enabled for all services and all members.";
    private final static String FAIL_ALL_MESSAGE = "Project %s does not have AuditConfigs configured.";
    private final static String FAIL_PARTIAL_MESSAGE = "Project %s does not have AuditConfigs configured for all logType/services/members.";
    private final static String WARN_MESSAGE = "No iam policy retrieved for project %s.";

    IAM001CloudAuditLogging(ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    public SignatureIdentifier signatureId() {
        return SignatureIdentifier.GCP_IAM_001;
    }

    @Override
    public Set<ResourceType> resourceTypes() {
        return ImmutableSet.of(ResourceType.GCP_PROJECT);
    }

    @Override
    public Mono<JSONObject> metadata(Resource resource) {
        JSONObject metadata = new JSONObject();
        val resourceMetadata = resource.getResourceMetadata(ProjectMetadata.class);
        metadata.put("projectId", resourceMetadata.getProjectId());
        metadata.put("name", resourceMetadata.getResourceName());
        metadata.put("auditConfigs", resourceMetadata.getIamPolicy().getAuditConfigs());
        return Mono.just(metadata);
    }

    @Override
    public Tuple2<AlertStatus, String> alertStatusAndMessage(Resource resource) {
        List<String> reqLogTypes = Arrays.asList("ADMIN_READ", "DATA_READ", "DATA_WRITE");

        val resourceMetadata = resource.getResourceMetadata(ProjectMetadata.class);
        String resourceName = resourceMetadata.getResourceName();
        val iamPolicy = resourceMetadata.getIamPolicy();
        if (iamPolicy == null) {
            return warnTuple(String.format(WARN_MESSAGE, resourceName));
        }

        if (iamPolicy.getAuditConfigs().size() > 0) {
            Set<IamPolicy.AuditLogConfig> goodALCs = iamPolicy.getAuditConfigs().stream()
                    .filter(ac -> "allServices".equalsIgnoreCase(ac.getService()))
                    .flatMap(ac -> ac.getAuditLogConfigs().stream())
                    .filter(alc -> null == alc.getExemptedMembers() || alc.getExemptedMembers().isEmpty())
                    .filter(alc -> reqLogTypes.contains(alc.getLogType()))
                    .collect(Collectors.toSet());
            if (goodALCs.size() == reqLogTypes.size()) {
                return passTuple(String.format(PASS_MESSAGE, resourceName));
            } else {
                return failTuple(String.format(FAIL_PARTIAL_MESSAGE, resourceName));
            }
        }
        return failTuple(String.format(FAIL_ALL_MESSAGE, resourceName));

    }
}
