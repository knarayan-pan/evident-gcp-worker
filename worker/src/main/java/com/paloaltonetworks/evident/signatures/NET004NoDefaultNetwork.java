package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.enrichment.metadata.ProjectMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import lombok.val;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.Optional;
import java.util.Set;

// Check that there is no "default" network for project.
@Component
public class NET004NoDefaultNetwork extends GCPSignature {
    public static final String PASS_MESSAGE = "Default network does not exists on project %s.";
    public static final String FAIL_MESSAGE = "Found default network on project %s.";

    public NET004NoDefaultNetwork(ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    public SignatureIdentifier signatureId() {
        return SignatureIdentifier.GCP_NET_004;
    }

    @Override
    public Set<ResourceType> resourceTypes() {
        return ImmutableSet.of(ResourceType.GCP_PROJECT);
    }

    @Override
    public Mono<JSONObject> metadata(Resource resource) {
        JSONObject metadata = new JSONObject();
        val projectMetadata = resource.getResourceMetadata(ProjectMetadata.class);
        metadata.put("name", projectMetadata.getResourceName());
        metadata.put("orgId", projectMetadata.getOrgId());
        Optional<ProjectMetadata.Network> defaultNetwork = projectMetadata.getNetworks().stream()
                .filter(nw -> nw.getName().equalsIgnoreCase("default"))
                .findFirst();
        if (defaultNetwork.isPresent()) {
            metadata.put("defaultNetwork", defaultNetwork.get());
        } else {
            metadata.put("defaultNetwork", JSONObject.NULL);
        }
        return Mono.just(metadata);
    }

    @Override
    public Tuple2<AlertStatus, String> alertStatusAndMessage(Resource resource) {
        val projectMetadata = resource.getResourceMetadata(ProjectMetadata.class);
        val networks = projectMetadata.getNetworks();
        Optional<ProjectMetadata.Network> defaultNetwork = networks.stream()
                .filter(nw -> nw.getName().equalsIgnoreCase("default"))
                .findFirst();
        if (defaultNetwork.isPresent()) {
            return failTuple(String.format(FAIL_MESSAGE, projectMetadata.getProjectId()));
        } else {
            return passTuple(String.format(PASS_MESSAGE, projectMetadata.getProjectId()));
        }
    }
}
