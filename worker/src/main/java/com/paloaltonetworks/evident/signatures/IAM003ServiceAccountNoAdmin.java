package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.enrichment.metadata.ProjectMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.utils.IamUtils;
import com.paloaltonetworks.evident.utils.JsonUtils;
import com.paloaltonetworks.evident.utils.ResourceUtil;
import lombok.val;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import reactor.core.publisher.Mono;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class IAM003ServiceAccountNoAdmin extends AbstractMultiAlertsSignature {
    private static final String PASS_MESSAGE = "Service account %s does not have any admin privileges.";
    private static final String FAIL_MESSAGE = "Service account %s has admin privileges.";

    IAM003ServiceAccountNoAdmin(ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    protected Mono<Map<String, Triple<AlertStatus, String, JSONObject>>> alertsStatusMessageAndMetadata(Resource resource) {
        val resourceMetadata = resource.getResourceMetadata(ProjectMetadata.class);
        if (resourceMetadata.getIamPolicy() == null) {
            // gcp api calls throw non-retriable exceptions when retrieving IamPolicy
            return Mono.empty();
        }

        val serviceAdminRoles = IamUtils.getUserRoles(resourceMetadata.getIamPolicy().getRoleMembers(),
            IamUtils::isServiceUser, IamUtils::isAdminRole);
        return Mono.just(
            serviceAdminRoles.entrySet().stream()
                .map(entry -> {
                    AlertStatus status;
                    String message;
                    if (CollectionUtils.isEmpty(entry.getValue())) {
                        status = AlertStatus.PASS;
                        message = String.format(PASS_MESSAGE, entry.getKey());
                    }
                    else {
                        status = AlertStatus.FAIL;
                        message = String.format(FAIL_MESSAGE, entry.getKey());
                    }
                    Map<String, Object> metadataMap = new LinkedHashMap<>();
                    metadataMap.put("serviceAccountEmail", entry.getKey());
                    metadataMap.put("adminLevelRoles", entry.getValue());
                    String alertId = ResourceUtil.makeResourceId(resourceMetadata.getProjectId(), entry.getKey());
                    return ImmutablePair.of(
                        alertId, ImmutableTriple.of(status, message, new JSONObject(JsonUtils.encode(metadataMap))));
                }).collect(Collectors.toMap(ImmutablePair::getLeft, ImmutablePair::getRight))
        );
    }

    @Override
    public SignatureIdentifier signatureId() {
        return SignatureIdentifier.GCP_IAM_003;
    }

    @Override
    public Set<ResourceType> resourceTypes() {
        return ImmutableSet.of(ResourceType.GCP_PROJECT);
    }
}
