package com.paloaltonetworks.evident.signatures.predicates;

import com.paloaltonetworks.evident.enrichment.metadata.FirewallMetadata;
import org.springframework.util.CollectionUtils;

import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Predicates to evaluate firewall rules
 */
public class FirewallRules {
    private static final Pattern RANGE_PATTERN = Pattern.compile("(\\d+)\\-(\\d+)");
    private static final String ALL_PROTOCOLS_PORTS = "all";

    /**
     * Returns Matching source IP predicate
     *
     * @param ipAddress IP Address to match
     */
    public static Predicate<? super FirewallMetadata.RulesMetadata> matchingSourceIP(String ipAddress) {
        return rule -> rule.getSourceIPRanges().contains(ipAddress);
    }

    /**
     * Returns Matching port predicate
     *
     * @param port port to match
     */
    public static Predicate<FirewallMetadata.RulesMetadata> matchingPort(Integer port) {
        return isWildCard().or(allPortsAllowed()).or(isExactMatch(port)).or(portRangeIncludes(port));
    }

    /**
     * Returns Matching protocol predicate
     *
     * @param protocol to match
     */
    public static Predicate<FirewallMetadata.RulesMetadata> matchingProtocol(String protocol) {
        return isWildCard().or(isExactProtocolMatch(protocol));
    }

    private static Predicate<FirewallMetadata.RulesMetadata> allPortsAllowed() {
        return rule -> CollectionUtils.isEmpty(rule.getPorts());
    }

    private static Predicate<FirewallMetadata.RulesMetadata> isExactMatch(int port) {
        return rule -> rule.getPorts().stream().anyMatch(portRule -> portRule.equals(Integer.toString(port)));
    }

    private static Predicate<FirewallMetadata.RulesMetadata> isExactProtocolMatch(String protocol) {
        return rule -> protocol.equalsIgnoreCase(rule.getProtocol());
    }


    private static Predicate<FirewallMetadata.RulesMetadata> portRangeIncludes(int port) {
        return rule -> rule.getPorts().stream().anyMatch(portRule -> {
                    Matcher matcher = RANGE_PATTERN.matcher(portRule);
                    if (matcher.matches()) {
                        int fromPort = Integer.parseInt(matcher.group(1));
                        int toPort = Integer.parseInt(matcher.group(2));
                        return port >= fromPort && port <= toPort;
                    }
                    return false;
                }
        );
    }

    private static Predicate<FirewallMetadata.RulesMetadata> isWildCard() {
        return rule -> ALL_PROTOCOLS_PORTS.equalsIgnoreCase(rule.getProtocol());
    }
}
