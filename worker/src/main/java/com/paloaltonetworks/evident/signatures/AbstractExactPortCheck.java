package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.enrichment.metadata.FirewallMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import lombok.val;
import org.json.JSONObject;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCE_FIREWALL;
import static com.paloaltonetworks.evident.signatures.predicates.FirewallRules.matchingPort;
import static com.paloaltonetworks.evident.signatures.predicates.FirewallRules.matchingProtocol;
import static com.paloaltonetworks.evident.signatures.predicates.FirewallRules.matchingSourceIP;

public abstract class AbstractExactPortCheck extends GCPSignature {

    private static final String DIRECTION_INGRESS = "INGRESS";
    private final static String DEFAULT_PASS_MSG = "Firewall rule %s for project %s does not have %s port %d exposed globally.";
    private final static String DEFAULT_FAIL_MSG = "Firewall rule %s for project %s has %s port %d exposed globally.";
    private static final String SOURCE_IP_RANGE = "0.0.0.0/0";

    AbstractExactPortCheck(ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    /**
     * Port number to assert
     */
    abstract int port();

    /**
     * Protocol to assert
     */
    abstract Protocol protocol();

    @Override
    public Set<ResourceType> resourceTypes() {
        return ImmutableSet.of(GCE_FIREWALL);
    }

    @Override
    public Mono<JSONObject> metadata(Resource resource) {
        return metadata(resource, FirewallMetadata.class);
    }

    @Override
    public Tuple2<AlertStatus, String> alertStatusAndMessage(Resource resource) {
        val resourceMetadata = resource.getResourceMetadata(FirewallMetadata.class);
        return openRules(resource).isEmpty() ?
                passTuple(String.format(passMessage(), resourceMetadata.getResourceName(), resourceMetadata.getProjectId(),
                        protocol(), port())) :
                failTuple(String.format(failMessage(), resourceMetadata.getResourceName(), resourceMetadata.getProjectId(),
                        protocol(), port()));
    }

    /**
     * Return pass message
     */
    String passMessage() {
        return DEFAULT_PASS_MSG;
    }

    /**
     * Returns fail message
     *
     * @return
     */
    String failMessage() {
        return DEFAULT_FAIL_MSG;
    }

    /**
     * Source IP to assert
     */
    String sourceIPRange() {
        return SOURCE_IP_RANGE;
    }

    /**
     * Returns a list of rules that violate this signature
     */
    private List<FirewallMetadata.RulesMetadata> openRules(Resource resource) {
        val fw = resource.getResourceMetadata(FirewallMetadata.class);
        return Optional.ofNullable(fw.getAllowedRules())
                .map(data -> data.stream()
                        .filter(rule -> !fw.isDisabled() && isInbound(fw.getDirection()) && isOpen(rule))
                        .collect(Collectors.toList()))
                .orElse(Collections.emptyList());
    }

    private boolean isInbound(@NotNull String direction) {
        return DIRECTION_INGRESS.equalsIgnoreCase(direction);
    }

    private boolean isOpen(FirewallMetadata.RulesMetadata rule) {
        return matchingProtocol(protocol().name()).and(
                matchingSourceIP(sourceIPRange())).and(
                matchingPort(port())).test(rule);
    }

    enum Protocol {
        TCP,
        UDP
    }
}
