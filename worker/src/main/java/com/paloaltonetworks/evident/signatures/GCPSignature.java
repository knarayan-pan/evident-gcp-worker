package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Joiner;
import com.google.common.base.Predicates;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import com.paloaltonetworks.aperture.MetricConstants;
import com.paloaltonetworks.aperture.Tags;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.esp.model.AlertAttributes;
import com.paloaltonetworks.evident.esp.model.AlertData;
import com.paloaltonetworks.evident.esp.model.AlertRequest;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.utils.JsonUtils;
import com.paloaltonetworks.evident.utils.ReactorUtils;
import lombok.val;
import org.apache.commons.collections.MapUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import reactor.core.CoreSubscriber;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.util.function.Tuple2;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.lowerCase;

public abstract class GCPSignature implements Signature {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final ESPClient espClient;
    private final MetricRegistry metricRegistry;

    GCPSignature(ESPClient espClient, MetricRegistry metricRegistry) {
        this.espClient = espClient;
        this.metricRegistry = metricRegistry;
    }

    public MetricRegistry getMetricRegistry() {
        return metricRegistry;
    }

    /**
     * Utility method to return metadata based on parameteric type
     *
     * @param resource
     * @param metadataClass
     * @return
     */
    public Mono<JSONObject> metadata(Resource resource, Class<? extends ResourceMetadata> metadataClass) {
        return Mono.just(new JSONObject(JsonUtils.encode(resource.getResourceMetadata(metadataClass))));
    }

    /**
     * Sanitize string value for sending data in alert.
     *
     * @param value String object that must be pushed as a part of the alert
     * @return String indicating either the value or an empty String.
     */
    public String sanitizedValue(String value) {
        return StringUtils.isEmpty(value) ? "" : value;
    }

    @Override
    public void check(@Nonnull Resource resource) {
        Tags.TagsBuilder tags = Tags.builder()
                .tag("externalAccountId", resource.getResourceKey().getExternalAccountId())
                .tag("sigId", signatureId().getValue())
                .tag("type", Joiner.on("_").join(resourceTypes()
                        .stream()
                        .map(ResourceType::getResourceType).filter(Predicates.notNull()).collect(Collectors.toSet())));
        Stopwatch started = Stopwatch.createStarted();
        try {
            checker(resource);
            tags.tag("status", MetricConstants.SUCCCESS_TAG);
        } catch (Exception e) {
            tags.tag("status", MetricConstants.FAILURE_TAG);
            // throw it back to upstream error handlers
            throw e;
        } finally {
            metricRegistry.timer(tags.build().toMetricName("timer.gcp_sigs")).update(started.stop().elapsed(TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS);
        }
    }

    protected void processAlert(Resource resource, String alertId, AlertStatus status, String message, JSONObject metadataJson) {
        // add status message to metadata
        String externalAccountId = resource.getResourceKey().getExternalAccountId();
        // update response
        metadataJson.put("message", message);
        // prepare alert
        String alertDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").format(new Date());
        val region = AlertAttributes.Region.builder()
            // todo - fix region code and label
            .code(lowerCase(resource.getRegion()))
            .name(lowerCase(resource.getRegion()))
            .build();
        AlertRequest alertRequest = AlertRequest.builder()
            .data(AlertData.builder()
                .attributes(AlertAttributes.builder()
                    .externalAccount(AlertAttributes.ExternalAccount.builder()
                        .id(externalAccountId)
                        .provider("google")
                        .build())
                    .signature(AlertAttributes.Signature.builder()
                        .identifier(signatureId().getValue())
                        .build())
                    .status(status.toString())
                    .resource(alertId)
                    .region(region)
                    .metadata(AlertAttributes.Metadata.builder()
                        .details(metadataJson.toMap())
                        .build())
                    .tags(processTags(resource.getResourceMetadata(ResourceMetadata.class).getTags()))
                    .createdAt(alertDate)
                    .build())
                .build())
            .build();

        LOGGER.debug("externalAccountId={} made alert: {}", externalAccountId, alertRequest);
        // post to esp
        try {
            espClient.createAlert(externalAccountId, alertRequest);
        } catch (IOException e) {
            throw new RuntimeException("Error posting alert to ESP franklin.", e);
        }
    }

    private void checker(@Nonnull Resource resource) {
        metadata(resource).switchIfEmpty(new Mono<JSONObject>() {
            @Override
            public void subscribe(CoreSubscriber<? super JSONObject> actual) {
                LOGGER.info("No metadata retrieved for resourceId: {}. No alert will be processed for this resource", resource.getResourceId());
            }
        }).doOnNext(jsonObject -> {
            // get status and message
            Tuple2<AlertStatus, String> statusAndMessage = alertStatusAndMessage(resource);
            processAlert(resource, resource.getResourceId(), statusAndMessage.getT1(), statusAndMessage.getT2(), jsonObject);
        }).doOnError(throwable -> LOGGER.error("Error posting alert for esp for account: {}", resource.getResourceKey().getExternalAccountId()))
                .subscribeOn(Schedulers.immediate())
                .subscribe(ReactorUtils.subscriber(jsonObject -> LOGGER.info("Completed posting alert for resource: {}, accountID: {}",
                        resource.getResourceId(), resource.getResourceKey().getExternalAccountId())));
    }

    @VisibleForTesting
    List<Map<String, String>> processTags(Map<String, String> tags) {
        if (MapUtils.isEmpty(tags)) {
            return new ArrayList<>();
        }
        List<Map<String, String>> espTags = new ArrayList<>();
        tags.keySet().forEach(key -> {
            espTags.add(ImmutableMap.of("key", key, "value", tags.get(key)));
        });
        return espTags;
    }

}
