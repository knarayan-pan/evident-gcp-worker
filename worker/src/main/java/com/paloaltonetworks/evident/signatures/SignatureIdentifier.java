package com.paloaltonetworks.evident.signatures;

public enum SignatureIdentifier {
    GCP_PRO_001("GCP:PRO-001"),

    // Storage signatures
    GCP_GCS_001("GCP:GCS-001"),
    GCP_GCS_002("GCP:GCS-002"),

    // GCE signature
    GCP_GCE_001("GCP:GCE-001"),
    GCP_GCE_002("GCP:GCE-002"),
    GCP_GCE_003("GCP:GCE-003"),

    // cloud sql signatures
    GCP_SQL_001("GCP:SQL-001"),
    GCP_SQL_003("GCP:SQL-003"),
    GCP_SQL_004("GCP:SQL-004"),

    // DNSSec - CloudDNS check.
    GCP_DNS_001("GCP:DNS-001"),
    GCP_DNS_002("GCP:DNS-002"),

    // firewall rules signatures
    GCP_FW_001("GCP:FW-001"),
    GCP_FW_002("GCP:FW-002"),
    GCP_FW_003("GCP:FW-003"),
    GCP_FW_004("GCP:FW-004"),
    GCP_FW_005("GCP:FW-005"),
    GCP_FW_006("GCP:FW-006"),
    GCP_FW_007("GCP:FW-007"),
    GCP_FW_008("GCP:FW-008"),
    GCP_FW_009("GCP:FW-009"),
    GCP_FW_010("GCP:FW-010"),
    GCP_FW_011("GCP:FW-011"),
    GCP_FW_012("GCP:FW-012"),
    GCP_FW_013("GCP:FW-013"),
    GCP_FW_014("GCP:FW-014"),
    GCP_FW_015("GCP:FW-015"),
    GCP_FW_016("GCP:FW-016"),
    GCP_FW_017("GCP:FW-017"),
    GCP_FW_018("GCP:FW-018"),
    GCP_FW_019("GCP:FW-019"),
    GCP_FW_020("GCP:FW-020"),

    // IAM signatures
    GCP_IAM_001("GCP:IAM-001"),
    GCP_IAM_002("GCP:IAM-002"),
    GCP_IAM_003("GCP:IAM-003"),
    GCP_IAM_004("GCP:IAM-004"),
    GCP_IAM_005("GCP:IAM-005"),

    // net signatures
    GCP_NET_001("GCP:NET-001"),
    GCP_NET_002("GCP:NET-002"),
    GCP_NET_003("GCP:NET-003"),
    GCP_NET_004("GCP:NET-004");
    
    private final String value;

    SignatureIdentifier(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
