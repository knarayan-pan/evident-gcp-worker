package com.paloaltonetworks.evident.signatures;


public enum AlertStatus {
    INFO, WARN, PASS, FAIL;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
