package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.enrichment.metadata.SubNetworkMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import lombok.val;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.Set;

import static com.paloaltonetworks.evident.gcp.api.ResourceType.GCE_SUBNETWORK;
import static com.paloaltonetworks.evident.signatures.SignatureIdentifier.GCP_NET_002;

/*
 * Ensure PrivateIpGoogleAccess is enabled
 */
@Component
public class NET002PrivateIpAccessEnabled extends GCPSignature {
    private static final String PASS_MESSAGE = "PrivateIpGoogleAccess exists and is set to true for subnet %s.";
    private static final String FAIL_MESSAGE = "PrivateIpGoogleAccess doesn't exists or is not set to true for subnet %s.";

    NET002PrivateIpAccessEnabled(ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    public SignatureIdentifier signatureId() {
        return GCP_NET_002;
    }

    @Override
    public Set<ResourceType> resourceTypes() {
        return ImmutableSet.of(GCE_SUBNETWORK);
    }

    @Override
    public Mono<JSONObject> metadata(Resource resource) {
        JSONObject metadata = new JSONObject();
        val resourceMetadata = resource.getResourceMetadata(SubNetworkMetadata.class);
        metadata.put("name", resourceMetadata.getResourceName());
        metadata.put("creationTimeStamp", resourceMetadata.getCreationTimeStamp());
        metadata.put("network", resourceMetadata.getNetwork());
        metadata.put("ipCidrRange", resourceMetadata.getIpCidrRanges());
        metadata.put("PrivateIpGoogleAccess", resourceMetadata.isPrivateIpGoogleAddress());
        metadata.put("project_id", resourceMetadata.getProjectId());
        metadata.put("region", resourceMetadata.getRegion());
        return Mono.just(metadata);
    }

    @Override
    public Tuple2<AlertStatus, String> alertStatusAndMessage(Resource resource) {
        val inst = resource.getResourceMetadata(SubNetworkMetadata.class);
        return inst.isPrivateIpGoogleAddress() ? passTuple(String.format(PASS_MESSAGE, inst.getSelfLink()))
                : failTuple(String.format(FAIL_MESSAGE, inst.getSelfLink()));
    }
}
