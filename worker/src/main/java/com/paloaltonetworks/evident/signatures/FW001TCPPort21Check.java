package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.paloaltonetworks.evident.esp.ESPClient;
import org.springframework.stereotype.Component;

import static com.paloaltonetworks.evident.signatures.AbstractExactPortCheck.Protocol.TCP;
import static com.paloaltonetworks.evident.signatures.SignatureIdentifier.GCP_FW_001;

/**
 * Check if TCP port 21 is open
 */
@Component
public class FW001TCPPort21Check extends AbstractExactPortCheck {

    public FW001TCPPort21Check(ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    public SignatureIdentifier signatureId() {
        return GCP_FW_001;
    }

    @Override
    int port() {
        return 21;
    }

    @Override
    Protocol protocol() {
        return TCP;
    }

}
