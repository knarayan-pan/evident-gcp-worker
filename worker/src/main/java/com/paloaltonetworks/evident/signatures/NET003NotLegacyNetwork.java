package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.enrichment.metadata.NetworkMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import lombok.val;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.Set;

// Check that network is not "legacy" (does not have subnetworks).
@Component
public class NET003NotLegacyNetwork extends GCPSignature {
    private static final String PASS_MESSAGE = "Subnetworks exist for network %s.";
    private static final String FAIL_MESSAGE = "Subnetworks do not exist for legacy network %s.";

    public NET003NotLegacyNetwork(ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    public SignatureIdentifier signatureId() {
        return SignatureIdentifier.GCP_NET_003;
    }

    @Override
    public Set<ResourceType> resourceTypes() {
        return ImmutableSet.of(ResourceType.GCE_NETWORK);
    }

    @Override
    public Mono<JSONObject> metadata(Resource resource) {
        JSONObject metadata = new JSONObject();
        val networkMetadata = resource.getResourceMetadata(NetworkMetadata.class);
        metadata.put("name", networkMetadata.getResourceName());
        metadata.put("projectId", networkMetadata.getProjectId());
        metadata.put("creationTimestamp", networkMetadata.getCreationTimestamp());
        metadata.put("IPv4Range", networkMetadata.getIPv4Range());
        metadata.put("gatewayIPv4", networkMetadata.getGatewayIPv4());
        metadata.put("subnetworks", networkMetadata.getSubnetworks());
        return Mono.just(metadata);
    }

    @Override
    public Tuple2<AlertStatus, String> alertStatusAndMessage(Resource resource) {
        val networkMetadata = resource.getResourceMetadata(NetworkMetadata.class);
        val subnetworks = networkMetadata.getSubnetworks();
        if (subnetworks == null || subnetworks.isEmpty()) {
            return failTuple(String.format(FAIL_MESSAGE, networkMetadata.getSelfLink()));
        } else {
            return passTuple(String.format(PASS_MESSAGE, networkMetadata.getSelfLink()));
        }
    }
}
