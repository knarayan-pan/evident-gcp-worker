package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.github.benmanes.caffeine.cache.CacheLoader;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.service.MonitoredResourceService;
import com.paloaltonetworks.evident.data_access.service.ResourceService;
import com.paloaltonetworks.evident.enrichment.metadata.BucketMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.esp.MonitoredESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuple3;
import reactor.util.function.Tuples;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import java.lang.invoke.MethodHandles;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static com.paloaltonetworks.evident.utils.ResourceUtil.extractResourceName;

@Component
public class GCS001BucketLoggingTurnedOnCheck extends GCPSignature {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final ResourceService resourceService;
    private final String PASS_MESSAGE_SOURCE_BUCKET = "Bucket %s in project %s has access logs and storage logs enabled";
    private final String PASS_MESSAGE_TARGET_BUCKET = "Bucket %s in project %s is the target bucket for access and storage logs.";
    private final String FAIL_MESSAGE_SOURCE_BUCKET = "Bucket %s in project %s does not have access logs or storage logs enabled";
    // cache of buckets which are being monitored by a specific bucket for a specific account
    private final LoadingCache<Tuple3<String, String, String>, Resource> targetBucketCache =
            Caffeine.newBuilder()
                    .expireAfterWrite(1, TimeUnit.HOURS)
                    .maximumSize(1000)
                    .build(new CacheLoader<Tuple3<String, String, String>, Resource>() {
                        @CheckForNull
                        @Override
                        public Resource load(@Nonnull Tuple3<String, String, String> tenantExternalId) {
                            return loadCache(tenantExternalId.getT1(), tenantExternalId.getT2(), tenantExternalId.getT3()).orElse(null);
                        }
                    });

    private Optional<Resource> loadCache(String tenant, String externalAccountId, String bucketId) {
        return resourceService.findResourcesByType(tenant, externalAccountId, ResourceType.GCS_STORAGE.getResourceType())
                .filter(Predicates.notNull())
                .filter(resource -> {
                    BucketMetadata bucketMetadata = resource.getResourceMetadata(BucketMetadata.class);
                    return bucketMetadata != null && bucketMetadata.getLogBucket() != null &&
                            StringUtils.equalsIgnoreCase(bucketMetadata.getLogBucket(),
                                    bucketId);
                }).findFirst();
    }

    @Autowired
    GCS001BucketLoggingTurnedOnCheck(@Qualifier(MonitoredESPClient.NAME) ESPClient espClient,
                                     @Qualifier(MonitoredResourceService.NAME) ResourceService resourceService, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
        this.resourceService = resourceService;
    }

    @Override
    public SignatureIdentifier signatureId() {
        return SignatureIdentifier.GCP_GCS_001;
    }

    @Override
    public Set<ResourceType> resourceTypes() {
        return ImmutableSet.<ResourceType>builder()
                .add(ResourceType.GCS_STORAGE)
                .build();
    }

    @Override
    public Mono<JSONObject> metadata(Resource resource) {
        JSONObject metadata = new JSONObject();
        BucketMetadata resourceMetadata = resource.getResourceMetadata(BucketMetadata.class);
        metadata.put("projectId", resourceMetadata.getProjectId());
        metadata.put("logging", ImmutableMap.builder()
                // immutable maps cannot have null in them.
                .put("logBucket", sanitizedValue(resourceMetadata.getLogBucket()))
                .put("logObjectPrefix", sanitizedValue(resourceMetadata.getLogObjectPrefix())).build());
        metadata.put("name", extractResourceName(resource.getResourceId()));
        metadata.put("dateCreated", resourceMetadata.getDateCreated());
        metadata.put("location", resourceMetadata.getLocation());
        metadata.put("storageClass", resourceMetadata.getStorageClass());
        metadata.put("tags", resourceMetadata.getTags());
        return Mono.just(metadata);
    }

    @Override
    public Tuple2<AlertStatus, String> alertStatusAndMessage(Resource resource) {
        // check if this resource has logging at all. If not then check if this
        // belongs to the list of known target buckets seen so far - if not then trigger an alert.
        BucketMetadata bucketMetadata = resource.getResourceMetadata(BucketMetadata.class);
        if (StringUtils.isNotEmpty(bucketMetadata.getLogBucket())) {
            return passTuple(String.format(PASS_MESSAGE_SOURCE_BUCKET, resource.getResourceId(), bucketMetadata.getProjectId()));
        } else {
            return isTargetBucket(resource);
        }
    }

    private Tuple2<AlertStatus, String> isTargetBucket(Resource resource) {
        // is this bucket a target bucket for some other bucket?
        Resource first;
        first = targetBucketCache.get(Tuples.of(resource.getTenant(), resource.getResourceKey().getExternalAccountId(), extractResourceName(resource.getResourceId())));
        BucketMetadata resourceMetadata = resource.getResourceMetadata(BucketMetadata.class);
        LOGGER.info("Bucket - {}, TargetBucket - {}", resource.getResourceId(), Optional.ofNullable(first).map(Resource::getResourceId).orElse("N/A"));
        return !ObjectUtils.isEmpty(first) ? passTuple(String.format(PASS_MESSAGE_TARGET_BUCKET, first.getResourceId(), resourceMetadata.getProjectId()))
                : failTuple(String.format(FAIL_MESSAGE_SOURCE_BUCKET, resource.getResourceId(), resourceMetadata.getProjectId()));
    }
}
