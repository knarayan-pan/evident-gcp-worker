package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.paloaltonetworks.evident.esp.ESPClient;
import org.springframework.stereotype.Component;

import static com.paloaltonetworks.evident.signatures.AbstractExactPortCheck.Protocol.UDP;
import static com.paloaltonetworks.evident.signatures.SignatureIdentifier.GCP_FW_019;

@Component
public class FW019UDPPort445Check extends AbstractExactPortCheck {

    FW019UDPPort445Check(ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    public SignatureIdentifier signatureId() {
        return GCP_FW_019;
    }

    @Override
    Protocol protocol() {
        return UDP;
    }

    @Override
    int port() {
        return 445;
    }
}
