package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.base.Joiner;
import com.google.common.base.Predicates;
import com.google.common.base.Stopwatch;
import com.paloaltonetworks.aperture.MetricConstants;
import com.paloaltonetworks.aperture.Tags;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.utils.ReactorUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.CoreSubscriber;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.util.function.Tuple2;

import javax.annotation.Nonnull;
import java.lang.invoke.MethodHandles;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public abstract class AbstractMultiAlertsSignature extends GCPSignature {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    AbstractMultiAlertsSignature(ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    public Mono<JSONObject> metadata(Resource resource) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Tuple2<AlertStatus, String> alertStatusAndMessage(Resource resource) {
        throw new UnsupportedOperationException();
    }

    abstract protected Mono<Map<String, Triple<AlertStatus, String, JSONObject>>> alertsStatusMessageAndMetadata(Resource resource);

    @Override
    public void check(@Nonnull Resource resource) {
        Tags.TagsBuilder tags = Tags.builder()
                .tag("externalAccountId", resource.getResourceKey().getExternalAccountId())
                .tag("sigId", signatureId().getValue())
                .tag("type", Joiner.on("_").join(resourceTypes().stream()
                        .map(ResourceType::getResourceType)
                        .filter(Predicates.notNull()).collect(Collectors.toSet())));
        Stopwatch started = Stopwatch.createStarted();
        try {
            long count = multiAlertChecker(resource);
            tags.tag("alert_count", String.valueOf(count));
            tags.tag("status", MetricConstants.SUCCCESS_TAG);
        } catch (Exception e) {
            tags.tag("status", MetricConstants.FAILURE_TAG);
            // throw it back to upstream error handlers
            throw e;
        } finally {
            getMetricRegistry().timer(tags.build().toMetricName("timer.gcp_sigs")).update(started.stop().elapsed(TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS);
        }
    }

    private long multiAlertChecker(@Nonnull Resource resource) {
        AtomicLong count = new AtomicLong();
        alertsStatusMessageAndMetadata(resource).switchIfEmpty(new Mono<Map<String, Triple<AlertStatus, String, JSONObject>>>() {
            @Override
            public void subscribe(CoreSubscriber<? super Map<String, Triple<AlertStatus, String, JSONObject>>> actual) {
                LOGGER.info("No metadata retrieved for resourceId: {}. No alert will be processed for this resource", resource.getResourceId());
            }
        }).doOnNext(statusMsgMetadataMap ->
                statusMsgMetadataMap.forEach(
                        (alertId, statusMsgMetadata) -> {
                            processAlert(resource, alertId, statusMsgMetadata.getLeft(), statusMsgMetadata.getMiddle(), statusMsgMetadata.getRight());
                            count.incrementAndGet();
                        }
                )
        ).doOnError(throwable -> LOGGER.error("Error posting alert for esp for account: {}", resource.getResourceKey().getExternalAccountId()))
                .subscribeOn(Schedulers.immediate())
                .subscribe(ReactorUtils.subscriber(jsonObject -> LOGGER.info("Completed posting alert for resource: {}, accountID: {}",
                        resource.getResourceId(), resource.getResourceKey().getExternalAccountId())));
        return count.get();
    }
}
