package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.enrichment.metadata.IamPolicy;
import com.paloaltonetworks.evident.enrichment.metadata.ProjectMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.util.ImmutableMapBuilder;
import lombok.val;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component
public class IAM002CorpLoginCreds extends GCPSignature {
    private final static String PASS_MESSAGE = "Project %s does not have any Gmail accounts.";
    private final static String FAIL_MESSAGE = "Project %s has one or more Gmail accounts.";
    private final static String WARN_MESSAGE = "No iam policy retrieved for project %s";

    IAM002CorpLoginCreds(ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    public SignatureIdentifier signatureId()   {
        return SignatureIdentifier.GCP_IAM_002;
    }


    @Override
    public Set<ResourceType> resourceTypes() {
        return ImmutableSet.of(ResourceType.GCP_PROJECT);
    }

    @Override
    public Mono<JSONObject> metadata(Resource resource) {
        JSONObject metadata = new JSONObject();
        val resourceMetadata = resource.getResourceMetadata(ProjectMetadata.class);
        metadata.put("projectId", resourceMetadata.getProjectId());
        metadata.put("name", resourceMetadata.getResourceName());
        metadata.put("offendingUsers", offendingUsers(resourceMetadata.getIamPolicy()));
        return Mono.just(metadata);
    }

    @Override
    public Tuple2<AlertStatus, String> alertStatusAndMessage(Resource resource) {
        val resourceMetadata = resource.getResourceMetadata(ProjectMetadata.class);
        String resourceName = resourceMetadata.getResourceName();
        val iamPolicy = resourceMetadata.getIamPolicy();
        if (iamPolicy == null) {
            return warnTuple(String.format(WARN_MESSAGE, resourceName));
        }
        if (offendingUsers(resourceMetadata.getIamPolicy()).size() > 0) {
            return failTuple(String.format(FAIL_MESSAGE, resourceName));
        } else {
            return passTuple(String.format(PASS_MESSAGE, resourceName));
        }
    }

    private Set<Map<String, String>> offendingUsers(IamPolicy policy) {
        HashSet<Map<String, String>> result = new HashSet<>();
        if (policy != null) {
            policy.getRoleMembers().forEach(roleMember -> roleMember.getMembers().forEach(member -> {
                if (member.toLowerCase().endsWith("gmail.com")) {
                    result.add(new ImmutableMapBuilder()
                            .put(roleMember.getRole(), member)
                            .build());
                }
            }));
        }
        return result;
    }
}
