package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.paloaltonetworks.evident.esp.ESPClient;
import org.springframework.stereotype.Component;

import static com.paloaltonetworks.evident.signatures.AbstractExactPortCheck.Protocol.TCP;
import static com.paloaltonetworks.evident.signatures.SignatureIdentifier.GCP_FW_009;

@Component
public class FW009TCPPort1433Check extends AbstractExactPortCheck {

    FW009TCPPort1433Check(ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    public SignatureIdentifier signatureId() {
        return GCP_FW_009;
    }

    @Override
    Protocol protocol() {
        return TCP;
    }

    @Override
    int port() {
        return 1433;
    }
}
