package com.paloaltonetworks.evident.signatures;

import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.enrichment.metadata.ProjectMetadata;
import com.paloaltonetworks.evident.esp.ESPClient;
import com.paloaltonetworks.evident.gcp.api.ResourceType;
import com.paloaltonetworks.evident.utils.IamUtils;
import com.paloaltonetworks.evident.utils.JsonUtils;
import com.paloaltonetworks.evident.utils.ResourceUtil;
import lombok.val;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import reactor.core.publisher.Mono;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class IAM004NotBothKmsAdminAndUser extends AbstractMultiAlertsSignature {
    private static final String PASS_MESSAGE = "User %s has Cloud KMS admin role or Cloud KMS non-admin role, but not both.";
    private static final String FAIL_MESSAGE = "User %s has both Cloud KMS admin and non-admin roles.";
    private static final String INFO_MESSAGE = "User %s has no cloud KMS role.";

    IAM004NotBothKmsAdminAndUser(ESPClient espClient, MetricRegistry metricRegistry) {
        super(espClient, metricRegistry);
    }

    @Override
    protected Mono<Map<String, Triple<AlertStatus, String, JSONObject>>> alertsStatusMessageAndMetadata(Resource resource) {
        val resourceMetadata = resource.getResourceMetadata(ProjectMetadata.class);
        if (resourceMetadata.getIamPolicy() == null) {
            // gcp api calls throw non-retriable exceptions when retrieving IamPolicy
            return Mono.empty();
        }

        val userRoles = IamUtils.getUserRoles(resourceMetadata.getIamPolicy().getRoleMembers(),
            IamUtils::isRegularUser, IamUtils::isKmsRole);
        return Mono.just(
            userRoles.entrySet().stream()
                .map(entry -> {
                    AlertStatus status;
                    String message;
                    if (CollectionUtils.isEmpty(entry.getValue())) {
                        status = AlertStatus.INFO;
                        message = String.format(INFO_MESSAGE, entry.getKey());
                    }
                    else {
                        if (entry.getValue().stream().anyMatch(IamUtils::isKmsAdminRole) &&
                            entry.getValue().stream().anyMatch(IamUtils::isKmsUserRole)) {
                            status = AlertStatus.FAIL;
                            message = String.format(FAIL_MESSAGE, entry.getKey());
                        }
                        else {
                            status = AlertStatus.PASS;
                            message = String.format(PASS_MESSAGE, entry.getKey());
                        }
                    }
                    Map<String, Object> metadataMap = new LinkedHashMap<>();
                    metadataMap.put("userEmail", entry.getKey());
                    metadataMap.put("kmsRoles", entry.getValue());
                    String alertId = ResourceUtil.makeResourceId(resourceMetadata.getProjectId(), entry.getKey());
                    return ImmutablePair.of(
                        alertId, ImmutableTriple.of(status, message, new JSONObject(JsonUtils.encode(metadataMap))));
                }).collect(Collectors.toMap(ImmutablePair::getLeft, ImmutablePair::getRight))
        );
    }

    @Override
    public SignatureIdentifier signatureId() {
        return SignatureIdentifier.GCP_IAM_004;
    }

    @Override
    public Set<ResourceType> resourceTypes() {
        return ImmutableSet.of(ResourceType.GCP_PROJECT);
    }
}
