CREATE KEYSPACE localhost 
  WITH REPLICATION = { 
   'class' : 'SimpleStrategy', 
   'replication_factor' : 2 
  } ;
  
CREATE TABLE localhost.resources ( 
external_account_id text, 
tenant text, 
resource_type text, 
resource_id_hash bigint,
resource_id text,
cloudapp_type text,
region text,
metadata text,
date_created timestamp,
date_updated timestamp,
PRIMARY KEY ((external_account_id, resource_type), resource_id_hash));

CREATE TABLE localhost.account_status (
external_account_id text,
cloudapp_type text,
scan_status text,
onboarding_date timestamp,
date_created timestamp,
date_updated timestamp,
last_event_scan_time timestamp,
metadata text,
PRIMARY KEY (external_account_id)
);