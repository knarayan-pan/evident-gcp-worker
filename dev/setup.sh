#!/bin/sh

echo "Setting up containers for development environment"
docker-compose up -d
echo "Containers setup complete..."
echo "Update consul keys"
python back-up-consul.py --verbose --restore --host http://127.0.0.1:8500 --file bkp.json
echo "Consul key update complete"
echo "Waiting for cassandra to start up before schema can be applied.. If this fails please retry later re-running this same script"
sleep 10
echo "Update schema in cassandra"
echo 'cqlsh -f /app/create_key_space_and_resource.sql'|docker exec -i cassandra-local bash
echo "Completed schema update in cassandra"
echo "If prometheus container goes out of sync with server time please use this script as an alias in .bash_rc: alias fix-drift='docker run --rm --privileged alpine hwclock -s'"
