GCP Resource Worker
===================

The resource worker is responsible for ingesting resources (both onboarding and on change) from GCP cloud and persist state in apilayer.
If there are changes in state to a resource - a signature task will be triggered for that resource based on the resource type.

Design document reference: https://cirroscope.atlassian.net/wiki/spaces/PCE/pages/570196012/GCP+Integration+Design

## Requirements

* Java 8
* Python 2.7 and above with requests installed. I.e pip install requests
* Lombok annotation processing enabled.
* AWS account with credentials embedded in the jvm runtime environment (can be set easily via intellij) See - https://docs.aws.amazon.com/cli/latest/userguide/cli-environment.html
* Docker engine (if you want to run with end to end dependencies) - *Will require Docker 18.06 and above*
* Docker compose installed and running.
* Add docker registry 10.4.39.238:5000 to 'insecure registries' in Docker for mac. More info can be found here: https://stackoverflow.com/questions/32808215/where-to-set-the-insecure-registry-flag-on-mac-os
* Please also update docker engine for mac to point to the internal docker repo at 10.4.39.238:5000 - to see the list of current images - use http://10.4.39.238:5000/v2/_catalog

## Build

To build - we can use the gradle job. The CI and the Debian jobs can be found here: https://khan.paloaltonetworks.local:8443/view/Evident-GCP%20/

## Development Environment

Please use the script located at dev/setup.sh to bootstrap the environment. This will create all dependencies as containers and populate the required consul keys and also setup the
sql schema in cassandra.

    ```
    $dev> ./setup.sh
    .....
    .....
    Completed schema update in cassandra
    ```

To Validate:
* Navigating to http://localhost:8500/ on the browser and confirming the presence of consul keys under Key/value section
* Confirm cassandra keyspace is setup with tables
* Navigate to http://localhost:10000/ to look at all containers that are running.

The environment is now ready for the worker to consume.

## Docker registry

We have an on-premise docker registry that is being used to save images. Any bug fixes/changes to the dependent component will require a new version of the image pushed to the repository.
The docker-compose.yml file in dev/ folder is designed to fetch the latest versions for all dependencies by default.

Docker registry can be located at: **10.4.39.238:5000**

If your using docker for mac - please add the registry in the advanced preferences section to be able to push/pull new updates to the registry.

## Run configuration

The spring container can be setup to run as a spring boot app with an external properties file to provide the initial configuration.
This can be accomplished using **-Dspring.config.location="<gcp_repo_absolute_path>/sample.properties"**

You can use a different/random external account ID to set up this process mimicking per account processing. This change can be made in the properties file indicated above.

### Configuration required
- The properties file to use as a VM spring config
- The AWS environment variables

![alt text](https://bitbucket.paloaltonetworks.local/projects/EV/repos/gcp-worker/browse/env_vars.png)



