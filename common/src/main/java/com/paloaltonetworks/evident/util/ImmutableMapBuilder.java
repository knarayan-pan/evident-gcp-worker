package com.paloaltonetworks.evident.util;

import com.google.common.collect.Maps;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * A fluent wrapper to build immutable map that allows null values
 */
public class ImmutableMapBuilder<K, V> {

    private final LinkedHashMap<K, V> container = Maps.newLinkedHashMap();

    public ImmutableMapBuilder<K, V> put(K key, V value) {
        this.container.put(key, value);
        return this;
    }

    public ImmutableMapBuilder<K, V> putAll(Map<? extends K, ? extends V> map) {
        this.container.putAll(map);
        return this;
    }

    public Map<K, V> build() {
        return Collections.unmodifiableMap(new HashMap<>(container));
    }
}
