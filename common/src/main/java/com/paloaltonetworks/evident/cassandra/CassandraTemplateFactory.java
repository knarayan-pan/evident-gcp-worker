package com.paloaltonetworks.evident.cassandra;


import org.springframework.data.cassandra.core.CassandraTemplate;

public interface CassandraTemplateFactory {

    /**
     * Retrieve the keyspace name
     * @return String indicating the keyspace being used for this workload.
     */
    String getTenantKeyspace();

    /**
     * Template to connect to cassandra for a particular tenant.
     *
     * @param tenant the tenant name for this worker which would be used to look up the cassandra cluster information
     * @return the {@link CassandraTemplate} object containing the cached connection information.
     */
    CassandraTemplate getCassandraTemplate(String tenant);
}
