package com.paloaltonetworks.evident.cassandra;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.HostDistance;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.QueryOptions;
import com.datastax.driver.core.policies.RoundRobinPolicy;
import com.github.benmanes.caffeine.cache.CacheLoader;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.paloaltonetworks.aperture.consul.ConsulService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import java.lang.invoke.MethodHandles;
import java.util.concurrent.TimeUnit;

@Service
public class DefaultCassandraTemplateFactory implements CassandraTemplateFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final LoadingCache<String, CassandraTemplate> tenantTemplateCache;
    private final String tenantKeyspace;
    final static String CASSANDRA_CLUSTER_KEYSPACE_KEY = "global/database/cassandra/KEYSPACE";
    // todo PCDEV-49  look up cassandra nodes from making the service call and get a list of all instances that are running.
    final static String CASSANDRA_CLUSTER_GLOBAL_KEY = "global/database/cassandra/CASSANDRA_HOST";


    // cassandra properties
    private final static String CASSANDRA_CLUSTER_IDLE_CONNECTION_TIMEOUT_KEY = "global/database/cassandra/IDLE_CONNECTION_TIMEOUT_MIN";
    private final static int CASSANDRA_CLUSTER_IDLE_CONNECTION_TIMEOUT_DEFAULT = 2;
    private final static String CASSANDRA_CLUSTER_MAX_CONNECTION_TIMEOUT_KEY = "global/database/cassandra/MAX_CONNECTION_TIMEOUT_MIN";
    private final static int CASSANDRA_CLUSTER_MAX_CONNECTION_TIMEOUT_DEFAULT = 1;
    private final static String CASSANDRA_CLUSTER_HEARTBEAT_INTERVAL_KEY = "global/database/cassandra/HEARTBEAT_INTERVAL_MIN";
    private final static int CASSANDRA_CLUSTER_HEARTBEAT_INTERVAL_DEFAULT = 1;

    // auth & ssl enabled or disabled.
    final static String CASSANDRA_AUTH_ENABLED = "global/database/cassandra/AUTH_ENABLED";
    private final static boolean CASSANDRA_AUTH_ENABLED_DEFAULT = false;
    private final static String CASSANDRA_AUTH_USERNAME = "global/database/cassandra/USERNAME";
    private final static String CASSANDRA_AUTH_PASSWORD = "global/database/cassandra/PASSWORD";
    private final static String CASSANDRA_SSL_ENABLED = "global/database/cassandra/SSL_ENABLED";

    private final static String CASSANDRA_CLUSTER_HOST_DEFAULT = "localhost";
    private final static int CASSANDRA_CLUSTER_PORT_DEFAULT = 9042;

    // todo - add username and password look up options including ssl support if required.

    @Autowired
    public DefaultCassandraTemplateFactory(ConsulService consulService) {
        this.tenantTemplateCache = Caffeine.newBuilder()
                .expireAfterWrite(1, TimeUnit.HOURS)
                .maximumSize(1000)
                .build(new CassandraTemplateCacheLoader(consulService));
        Optional<String> value = consulService.getValue(CASSANDRA_CLUSTER_KEYSPACE_KEY);
        this.tenantKeyspace = value.isPresent() ? value.get() : null;
    }

    @Override
    public String getTenantKeyspace() {
        return tenantKeyspace;
    }

    @Override
    public CassandraTemplate getCassandraTemplate(String tenant) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(tenant), "tenant name cannot be empty");
        return tenantTemplateCache.get(tenant);
    }

    private class CassandraTemplateCacheLoader implements CacheLoader<String, CassandraTemplate> {
        private final ConsulService consulService;

        CassandraTemplateCacheLoader(ConsulService consulService) {
            this.consulService = consulService;
        }


        @CheckForNull
        @Override
        public CassandraTemplate load(@Nonnull String tenantName) {
            com.google.common.base.Optional<String> globalClusterHost = consulService.getValue(CASSANDRA_CLUSTER_GLOBAL_KEY);
            if (!globalClusterHost.isPresent()) {
                LOGGER.error("Could not find the default cassandra cluster host to use. Will require consul update and restart this service.");
                return null;
            }
            String clusterString = globalClusterHost.get();

            Iterable<String> addresses = Splitter.on(",").split(clusterString);
            Cluster.Builder clusterBuilder = Cluster.builder()
                    .withLoadBalancingPolicy(new RoundRobinPolicy())
                    .withQueryOptions(new QueryOptions().setConsistencyLevel(ConsistencyLevel.ONE));
            for (String address : addresses) {
                // todo - hostname lookup issue:
                // - fix this in more elegant way - host.docker.internal is required by
                // apilayer consumer that runs as a container to talk to cassandra -
                // however if we run this outside without a container then this host becomes invalid
                if ("host.docker.internal".equalsIgnoreCase(address)) {
                    clusterBuilder.addContactPoint("localhost");
                } else {
                    clusterBuilder.addContactPoint(address);
                }
            }

            PoolingOptions poolingOptions = new PoolingOptions();

            // based on documentation - we want to set the host distance to remote to ensure that we dont keep too many option
            // connections.
            poolingOptions.setConnectionsPerHost(HostDistance.REMOTE, 2, 5);
            poolingOptions.setIdleTimeoutSeconds((int) TimeUnit.MINUTES.toSeconds(consulService.getInt(CASSANDRA_CLUSTER_IDLE_CONNECTION_TIMEOUT_KEY,
                    CASSANDRA_CLUSTER_IDLE_CONNECTION_TIMEOUT_DEFAULT)));
            poolingOptions.setPoolTimeoutMillis((int) TimeUnit.MINUTES.toMillis(consulService.getInt(CASSANDRA_CLUSTER_MAX_CONNECTION_TIMEOUT_KEY,
                    CASSANDRA_CLUSTER_MAX_CONNECTION_TIMEOUT_DEFAULT)));
            poolingOptions.setHeartbeatIntervalSeconds((int) TimeUnit.MINUTES.toMillis(consulService.getInt(CASSANDRA_CLUSTER_HEARTBEAT_INTERVAL_KEY,
                    CASSANDRA_CLUSTER_HEARTBEAT_INTERVAL_DEFAULT)));
            clusterBuilder.withPoolingOptions(poolingOptions);
            boolean authEnabled = consulService.getBoolean(CASSANDRA_AUTH_ENABLED, CASSANDRA_AUTH_ENABLED_DEFAULT);
            if (authEnabled) {
                String username = consulService.getValue(CASSANDRA_AUTH_USERNAME, "");
                String password = consulService.getValue(CASSANDRA_AUTH_PASSWORD, "");
                if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
                    LOGGER.error("Cassandra auth is enabled but no username or password is set. Will not create cassandra client");
                    return null;
                }
                clusterBuilder.withCredentials(username, password);
            }

            // todo: Check if data compression is needed in transfer https://docs.datastax.com/en/developer/java-driver/2.1/manual/compression/
            //todo support cassandra over ssl
            Cluster cluster = clusterBuilder.build();
            // do not connect to cassandra yet but only initialize - lazy loading.
            return new CassandraTemplate(cluster.newSession());
        }
    }
}
