package com.paloaltonetworks.evident.exceptions;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.concurrent.TimeUnit;

@Data
@EqualsAndHashCode(callSuper = false)
public class RetryOperationException extends RuntimeException {
    private long duration;
    private final TimeUnit timeUnit;

    @Builder
    public RetryOperationException(String message, Throwable cause, long duration, TimeUnit timeUnit) {
        super(message, cause);
        this.duration = duration;
        this.timeUnit = timeUnit;
    }
}
