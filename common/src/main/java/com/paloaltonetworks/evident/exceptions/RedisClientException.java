package com.paloaltonetworks.evident.exceptions;

public class RedisClientException extends RuntimeException {

    public RedisClientException(String message) {
        super(message);
    }

    public RedisClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
