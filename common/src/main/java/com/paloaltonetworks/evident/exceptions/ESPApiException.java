package com.paloaltonetworks.evident.exceptions;

import java.io.IOException;

/**
 * ESP API exception.
 */
public final class ESPApiException extends IOException {

    public ESPApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public ESPApiException(String message) {
        super(message);
    }

    public ESPApiException(Throwable cause) {
        super(cause);
    }
}
