package com.paloaltonetworks.evident.exceptions;

/**
 * Exception thrown when a task has failed and cannot be retried.
 */
public class FatalTaskErrorException extends RuntimeException {
    public FatalTaskErrorException(String message) {
        super(message);
    }

    public FatalTaskErrorException(String message, Throwable t) {
        super(message, t);
    }
}