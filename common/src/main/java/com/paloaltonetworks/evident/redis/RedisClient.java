package com.paloaltonetworks.evident.redis;

import com.paloaltonetworks.evident.exceptions.RedisClientException;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Optional;

public interface RedisClient {

    /**
     * Get a lock for an identifier from the redis proxy service. The host and port information are retrieved from consul
     *
     * @param id  the id to achieve a lock on
     * @param ttl the time in seconds to acquire the lock
     * @return Optional token returned after acquiring the lock. If no token is returned that that means the lock was not acquired
     * @throws IOException in case of an error or timeout talking to the redis proxy service
     */
    Optional<String> acquireLock(@Nonnull String id, long ttl) throws RedisClientException;

    /**
     * Get a temporary expiring lock for a resource
     *
     * @param externalAccountId The external account Id
     * @param resourceId        the resourceId of the cloud resource
     * @return
     */
    boolean getResourceLock(String externalAccountId, String resourceId);

    /**
     * Release lock for an identifier from the redis proxy service. The host and port information are retrieved from consul
     *
     * @param id    the id to achieve a lock on
     * @param token the time in seconds to acquire the lock
     * @return Boolean indicating if a lock was acquired or not.
     * @throws RedisClientException in case of an error or timeout talking to the redis proxy service
     */
    boolean releaseLock(@Nonnull String id, @Nonnull String token) throws RedisClientException;

    /**
     * Set a key in redis temporarily for a finite amount of time.
     *
     * @param key   the key string
     * @param value the value
     * @param ttl   the time it needs to be on redis
     * @return boolean indicating if the value was set or not
     * @throws RedisClientException when there was an error in constructing the request to the redis proxy service
     */
    boolean setKey(@Nonnull String key, @Nonnull String value, int ttl) throws RedisClientException;

    /**
     * Delete a key from redis
     *
     * @param key key to be deleted
     * @return boolean indicating the status of the delete request
     */
    boolean deleteKey(@Nonnull String key);

    /**
     * Get a value for a key set in redis cache
     *
     * @param key the key string for which we need to get the value.
     * @return Optional String indicating the value of the key
     * @throws RedisClientException if there is an error in connecting with the redis proxy service
     */
    Optional<String> getKey(@Nonnull String key) throws RedisClientException;
}
