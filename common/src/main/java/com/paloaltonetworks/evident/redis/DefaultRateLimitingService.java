package com.paloaltonetworks.evident.redis;

import com.google.common.annotations.VisibleForTesting;
import com.paloaltonetworks.evident.exceptions.RedisClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import com.paloaltonetworks.evident.exceptions.FatalTaskErrorException;

import javax.annotation.Nonnull;
import java.lang.invoke.MethodHandles;
import java.util.concurrent.TimeUnit;

@Primary
@Component
public class DefaultRateLimitingService implements RateLimitingService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private final RedisClient defaultRedisClient;
    private static final String RATE_LIMIT_LOCK_KEY_FORMAT = "rate_limit_lock_%s_%s_%s";

    @Autowired
    public DefaultRateLimitingService(@Qualifier(MonitoredRedisClient.NAME) RedisClient defaultRedisClient) {
        this.defaultRedisClient = defaultRedisClient;
    }

    @VisibleForTesting
    String generateLockPrefix(@Nonnull String externalId, @Nonnull String cloudappType, @Nonnull String apiType) {
        return String.format(RATE_LIMIT_LOCK_KEY_FORMAT, externalId, cloudappType, apiType);
    }

    @Override
    public boolean getRateLimitLock(@Nonnull String externalAccountId, @Nonnull String apiType,
                                    @Nonnull String cloudAppType, int rateLimit, @Nonnull TimeUnit rateLimitTimeUnit,
                                    int maxAttempts, int exponentialWaitDuration, @Nonnull TimeUnit exponentialWaitDurationTimeUnit) {
        if (rateLimit <= 0) {
            return true;
        }

        String lockPrefix = generateLockPrefix(externalAccountId, cloudAppType, apiType);

        for (int attempt = 1; attempt <= maxAttempts; ++attempt) {
            // trying to find an available lock in the set of <maxLockCount> locks
            for (int lockId = 0; lockId < rateLimit; ++lockId) {
                String lockKey = lockPrefix + "_" + lockId;
                try {
                    if (defaultRedisClient.acquireLock(lockKey, rateLimitTimeUnit.toSeconds(1)).isPresent()) {
                        LOGGER.debug("Rate limiting Redis lock #{}/{} with prefix={} acquired successfully after " +
                                "{}/{} attempts.", lockId, rateLimit, lockPrefix, attempt, maxAttempts);
                        return true;
                    }
                } catch (RedisClientException e) {
                    LOGGER.warn("Received exception in trying to get rate limit lock for extAccountId: {}, " +
                            "apiType: {}", externalAccountId, apiType, e);
                }
            }
            long sleepDuration = (long) (exponentialWaitDuration * Math.pow(2, attempt));
            try {
                exponentialWaitDurationTimeUnit.sleep(sleepDuration);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new FatalTaskErrorException("", e);
            }
        }

        LOGGER.warn("Unable to get rate limiting lock with prefix={}, maxLockCount={}, maxAttempts={}.", lockPrefix,
                rateLimit, maxAttempts);
        return false;
    }
}
