package com.paloaltonetworks.evident.redis;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableMap;
import com.paloaltonetworks.aperture.consul.ConsulService;
import com.paloaltonetworks.evident.config.CustomObjectMapper;
import com.paloaltonetworks.evident.exceptions.RedisClientException;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Primary
@Component
public class DefaultRedisClient implements RedisClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    static final String REDIS_PROXY_HOST_TMP_KEY = "global/config/v1/redis-proxy/HOST";
    static final String REDIS_PROXY_HOST_DEFAULT = "localhost";
    static final String REDIS_PROXY_PORT_TMP_KEY = "global/config/v1/redis-proxy/PORT";
    static final int REDIS_PROXY_PORT_DEFAULT = 10001;
    private static final String REDIS_PROXY_API_PATH = "api/v1";
    private final OkHttpClient okHttpClient;
    private final ConsulService consulService;

    @Autowired
    public DefaultRedisClient(ConsulService consulService) {
        // probably fetch these params from consul too?
        this.okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .build();
        this.consulService = consulService;
    }

    @VisibleForTesting
    DefaultRedisClient(OkHttpClient okHttpClient, ConsulService consulService) {
        this.okHttpClient = okHttpClient;
        this.consulService = consulService;
    }


    @Override
    public Optional<String> acquireLock(@Nonnull String id, long ttl) throws RedisClientException {
        // todo: use consulService.hostsAndPorts() once redis proxy service has registered with consul using service name
        String host = consulService.getValue(REDIS_PROXY_HOST_TMP_KEY, REDIS_PROXY_HOST_DEFAULT);
        int port = consulService.getInt(REDIS_PROXY_PORT_TMP_KEY, REDIS_PROXY_PORT_DEFAULT);
        Response response = null;
        try {
            Request request = new Request.Builder()
                    .get()
                    .url(String.format("http://%s:%s/%s/lock/%s?ttl=%s", host, port, REDIS_PROXY_API_PATH, sanitize(id), ttl))
                    .build();
            response = okHttpClient.newCall(request).execute();

            boolean acquiredLock;
            String token;

            if (response.isSuccessful() && response.body() != null) {
                JSONObject resObject = new JSONObject(response.body().string());
                acquiredLock = resObject.getBoolean("acquired");
                token = resObject.isNull("token") ? null : resObject.getString("token");
            } else {
                LOGGER.error("Received unsuccessful response from redis proxy for acquiring lock - status_code: {} ", response.code());
                throw new RedisClientException(String.format("Received unsuccessful response from redis proxy for acquiring lock - status_code: %s", response.code()));
            }
            LOGGER.debug("Tried acquiring lock for key:{} - acquired:{}", id, acquiredLock);
            return acquiredLock ? Optional.ofNullable(token) : Optional.empty();
        } catch (IOException e) {
            throw new RedisClientException(e.getMessage(), e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    private boolean getLockWithRetry(String key) {
        for (int currentTryCount = 0; currentTryCount < 5; ++currentTryCount) {
            int finalCurrentTryCount = currentTryCount;
            boolean acquiredLock = acquireLock(key, 2).map(s -> {
                LOGGER.debug("Acquired lock with key={} after {} tries (expires in {} {})", key, finalCurrentTryCount,
                        2, "Seconds");
                return true;
            }).orElse(false);

            if (!acquiredLock) {
                long sleepDuration = (long) (1 * Math.pow(2, currentTryCount));
                LOGGER.info("Sleeping for {} {} before next attempt ({}/{}) to get lock with key={}",
                        sleepDuration, "seconds", currentTryCount, 5, key);
                try {
                    TimeUnit.SECONDS.sleep(sleepDuration);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    throw new RedisClientException("Interrupted while waiting for redis lock", e);
                }
            } else {
                return true;
            }
        }
        LOGGER.warn("Could NOT acquire lock with key={} after maximum number of attempts ({}).", key, 5);
        return false;
    }

    @Override
    public boolean getResourceLock(String externalAccountId, String resourceId) {
        String RESOURCE_LOCK_FORMAT = "%s_%s";
        return getLockWithRetry(String.format(RESOURCE_LOCK_FORMAT, externalAccountId, resourceId));
    }

    @Override
    public boolean releaseLock(@Nonnull String id, @Nonnull String token) throws RedisClientException {
        // todo: use consulService.hostsAndPorts() once redis proxy service has registered with consul using service name
        String host = consulService.getValue(REDIS_PROXY_HOST_TMP_KEY, REDIS_PROXY_HOST_DEFAULT);
        int port = consulService.getInt(REDIS_PROXY_PORT_TMP_KEY, REDIS_PROXY_PORT_DEFAULT);
        Response response = null;
        try {
            Request request = new Request.Builder()
                    .delete()
                    .url(String.format("http://%s:%s/%s/lock/%s?token=%s", host, port, REDIS_PROXY_API_PATH, sanitize(id), token))
                    .build();
            response = okHttpClient.newCall(request).execute();
            return response.isSuccessful() && response.body() != null &&
                    new JSONObject(response.body().string()).getBoolean("released");
        } catch (IOException e) {
            throw new RedisClientException(e.getMessage(), e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }


    @Override
    public boolean setKey(@Nonnull String key, @Nonnull String value, int ttl) throws RedisClientException {
        String host = consulService.getValue(REDIS_PROXY_HOST_TMP_KEY, REDIS_PROXY_HOST_DEFAULT);
        int port = consulService.getInt(REDIS_PROXY_PORT_TMP_KEY, REDIS_PROXY_PORT_DEFAULT);
        String payload = CustomObjectMapper.encode(ImmutableMap.<String, String>builder().put("value", value).build());
        if (StringUtils.isEmpty(payload)) {
            LOGGER.warn("Invalid key being set to redis for key: {} and value: {}. Ignoring this operation", key, value);
            return false;
        }

        Response response = null;
        try {
            Request request = new Request.Builder()
                    .put(RequestBody.create(MediaType.parse("application/json"), payload))
                    .url(String.format("http://%s:%s/%s/string/%s?ttl=%s", host, port, REDIS_PROXY_API_PATH, sanitize(key), ttl))
                    .build();
            response = okHttpClient.newCall(request).execute();

            boolean acquiredLock;

            if (response.isSuccessful() && response.body() != null) {
                JSONObject resObject = new JSONObject(response.body().string());
                acquiredLock = resObject.getBoolean("success");
            } else {
                LOGGER.error("Received unsuccessful response from redis proxy for acquiring lock - status_code: {} ", response.code());
                throw new RedisClientException(String.format("Received unsuccessful response from redis proxy for acquiring lock - status_code: %s", response.code()));
            }
            return acquiredLock;
        } catch (IOException e) {
            throw new RedisClientException(e.getMessage(), e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    @Override
    public boolean deleteKey(@Nonnull String key) {
        String host = consulService.getValue(REDIS_PROXY_HOST_TMP_KEY, REDIS_PROXY_HOST_DEFAULT);
        int port = consulService.getInt(REDIS_PROXY_PORT_TMP_KEY, REDIS_PROXY_PORT_DEFAULT);

        Response response = null;
        try {
            Request request = new Request.Builder()
                    .delete()
                    .url(String.format("http://%s:%s/%s/string/%s", host, port, REDIS_PROXY_API_PATH, sanitize(key)))
                    .build();
            response = okHttpClient.newCall(request).execute();

            boolean deleteStatus;

            if (response.isSuccessful() && response.body() != null) {
                JSONObject resObject = new JSONObject(response.body().string());
                deleteStatus = resObject.getBoolean("success");
            } else {
                LOGGER.error("Received unsuccessful response from redis proxy for delete key {} - status_code: {} ", key, response.code());
                throw new RedisClientException(String.format("Received unsuccessful response from redis proxy for delete key %s - status_code: %s", key, response.code()));
            }
            return deleteStatus;
        } catch (IOException e) {
            throw new RedisClientException(e.getMessage(), e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    @Override
    public Optional<String> getKey(@Nonnull String key) throws RedisClientException {
        String host = consulService.getValue(REDIS_PROXY_HOST_TMP_KEY, REDIS_PROXY_HOST_DEFAULT);
        int port = consulService.getInt(REDIS_PROXY_PORT_TMP_KEY, REDIS_PROXY_PORT_DEFAULT);
        Response response = null;
        try {
            Request request = new Request.Builder()
                    .get()
                    .url(String.format("http://%s:%s/%s/string/%s", host, port, REDIS_PROXY_API_PATH, sanitize(key)))
                    .build();
            response = okHttpClient.newCall(request).execute();
            JSONObject responseObject;
            if (response.isSuccessful() && response.body() != null) {
                responseObject = new JSONObject(response.body().string());
            } else {
                LOGGER.error("Received unsuccessful response from redis proxy for acquiring lock - status_code: {} ", response.code());
                throw new RedisClientException(String.format("Received unsuccessful response from redis proxy for acquiring lock - status_code: %s", response.code()));
            }
            String value = responseObject.isNull("value") ? null : responseObject.getString("value");
            return Optional.ofNullable(value);
        } catch (IOException e) {
            throw new RedisClientException(e.getMessage(), e);
        } finally {
            if (response != null) {
                response.close();
            }
        }

    }

    /**
     * Remove special characters
     */
    private String sanitize(String id) {
        return id.replaceAll("/", ":");
    }
}
