package com.paloaltonetworks.evident.redis;

import javax.annotation.Nonnull;
import java.util.concurrent.TimeUnit;

public interface RateLimitingService {

    /**
     * Get a ratelimit lock based on apitype and cloudapp
     *
     * @param externalAccountId               the external account id for the customer
     * @param apiType                         the serialized enum type of the api or resource type
     * @param cloudAppType                    the cloudapp for which this rate limit is being enforced
     * @param rateLimit                       the ratelimit value to no exceed from the client
     * @param rateLimitTimeUnit               the timeunit of the ratelimit specified above
     * @param maxAttempts                     the max number of attempts to make before we fail to achieve a rate limit lock.
     * @param exponentialWaitDuration         the exponential backoff wait time interval
     * @param exponentialWaitDurationTimeUnit the unit of time interval specified for the exponentialWaitDuration
     * @return boolean indicating if a rate limit lock was achieved or not.
     */
    boolean getRateLimitLock(final @Nonnull String externalAccountId, final @Nonnull String apiType,
                             final @Nonnull String cloudAppType, final int rateLimit, final @Nonnull TimeUnit rateLimitTimeUnit,
                             final int maxAttempts, final int exponentialWaitDuration, final @Nonnull TimeUnit exponentialWaitDurationTimeUnit);
}
