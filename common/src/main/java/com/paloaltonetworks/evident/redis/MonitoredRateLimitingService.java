package com.paloaltonetworks.evident.redis;

import com.codahale.metrics.MetricRegistry;
import com.google.common.base.Stopwatch;
import com.paloaltonetworks.aperture.MetricConstants;
import com.paloaltonetworks.aperture.Tags;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

@Qualifier(MonitoredRateLimitingService.NAME)
@Component
public class MonitoredRateLimitingService implements RateLimitingService {
    public static final String NAME = "MonitoredRateLimitingService";
    private final RateLimitingService delegate;
    private final MetricRegistry metricRegistry;

    @Autowired
    public MonitoredRateLimitingService(RateLimitingService delegate, MetricRegistry metricRegistry) {
        this.delegate = delegate;
        this.metricRegistry = metricRegistry;
    }

    @Override
    public boolean getRateLimitLock(@Nonnull String externalAccountId, @Nonnull String apiType, @Nonnull String cloudAppType,
                                    int rateLimit, @Nonnull TimeUnit rateLimitTimeUnit, int maxAttempts, int exponentialWaitDuration,
                                    @Nonnull TimeUnit exponentialWaitDurationTimeUnit) {
        return monitor(() -> delegate.getRateLimitLock(externalAccountId, apiType, cloudAppType, rateLimit,
                rateLimitTimeUnit, maxAttempts, exponentialWaitDuration, exponentialWaitDurationTimeUnit), Tags.builder()
                .tag("method", "getRateLimitLock")
                .tag("extAccountId", externalAccountId));

    }

    private <T> T monitor(Callable<T> callable, Tags.TagsBuilder tags) {
        Stopwatch timer = Stopwatch.createStarted();
        try {
            T val = callable.call();
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.SUCCCESS_TAG);
            return val;
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.FAILURE_TAG);
            throw new RuntimeException(e);
        } finally {
            timer.stop();
            metricRegistry.timer(tags.build().toMetricName("timer.rate_limit_lock"))
                    .update(timer.elapsed(TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS);
        }
    }

}
