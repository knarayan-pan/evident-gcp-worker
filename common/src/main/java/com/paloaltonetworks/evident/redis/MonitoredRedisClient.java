package com.paloaltonetworks.evident.redis;

import com.codahale.metrics.MetricRegistry;
import com.google.common.base.Stopwatch;
import com.paloaltonetworks.aperture.MetricConstants;
import com.paloaltonetworks.aperture.Tags;
import com.paloaltonetworks.evident.exceptions.RedisClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.lang.invoke.MethodHandles;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

@Component(MonitoredRedisClient.NAME)
public class MonitoredRedisClient implements RedisClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    public static final String NAME = "MonitoredRedisClient";
    private final RedisClient delegate;
    private final MetricRegistry metricRegistry;

    @Autowired
    public MonitoredRedisClient(RedisClient delegate, MetricRegistry metricRegistry) {
        this.delegate = delegate;
        this.metricRegistry = metricRegistry;
    }

    @Override
    public Optional<String> acquireLock(@Nonnull String id, long ttl) throws RedisClientException {
        return monitor(() -> delegate.acquireLock(id, ttl), Tags.builder()
                .tag("method", "acquireLock"));
    }

    @Override
    public boolean getResourceLock(String externalAccountId, String resourceId) {
        return monitor(() -> delegate.getResourceLock(externalAccountId, resourceId), Tags.builder()
                .tag("method", "getResourceLock")
                .tag("externalAccountId", externalAccountId));
    }

    @Override
    public boolean releaseLock(@Nonnull String id, @Nonnull String token) throws RedisClientException {
        return monitor(() -> delegate.releaseLock(id, token), Tags.builder()
                .tag("method", "releaseLock"));
    }

    @Override
    public boolean setKey(@Nonnull String key, @Nonnull String value, int ttl) throws RedisClientException {
        return monitor(() -> delegate.setKey(key, value, ttl), Tags.builder()
                .tag("method", "setKey"));
    }

    @Override
    public boolean deleteKey(@Nonnull String key) {
        return monitor(() -> delegate.deleteKey(key), Tags.builder()
                .tag("method", "deleteKey"));
    }

    @Override
    public Optional<String> getKey(@Nonnull String key) throws RedisClientException {
        return monitor(() -> delegate.getKey(key), Tags.builder()
                .tag("method", "getKey"));
    }

    private <T> T monitor(Callable<T> callable, Tags.TagsBuilder tags) {
        Stopwatch timer = Stopwatch.createStarted();
        try {
            T val = callable.call();
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.SUCCCESS_TAG);
            return val;
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.FAILURE_TAG);
            throw new RuntimeException(e);
        } finally {
            timer.stop();
            metricRegistry.timer(tags.build().toMetricName("timer.redis_client"))
                    .update(timer.elapsed(TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS);
        }
    }

}
