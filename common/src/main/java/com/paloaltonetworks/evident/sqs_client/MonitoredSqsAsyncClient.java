package com.paloaltonetworks.evident.sqs_client;

import com.amazonaws.services.sqs.model.ChangeMessageVisibilityResult;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.CreateQueueResult;
import com.amazonaws.services.sqs.model.DeleteMessageResult;
import com.amazonaws.services.sqs.model.DeleteQueueResult;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.codahale.metrics.MetricRegistry;
import com.google.common.base.Stopwatch;
import com.paloaltonetworks.aperture.ApertureSqsAsyncClient;
import com.paloaltonetworks.aperture.MetricConstants;
import com.paloaltonetworks.aperture.Tags;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

@Primary
@Component(MonitoredSqsAsyncClient.NAME)
public class MonitoredSqsAsyncClient implements ApertureSqsAsyncClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    public static final String NAME = "MonitoredSqsAsyncClient";
    private final ApertureSqsAsyncClient delegate;
    private final MetricRegistry metricRegistry;

    @Autowired
    public MonitoredSqsAsyncClient(
            @Qualifier("apertureSqsAsyncClient") Optional<ApertureSqsAsyncClient> delegate,
            MetricRegistry metricRegistry) {
        this.delegate = delegate.orElse(null);
        this.metricRegistry = metricRegistry;
    }

    @Override
    public CompletableFuture<SendMessageResult> sendMessageAsync(String s, SendMessageRequest sendMessageRequest) {
        return monitor(() -> delegate.sendMessageAsync(s, sendMessageRequest),
                Tags.builder()
                        .tag("method", "sendMessageAsync"));
    }

    @Override
    public CompletableFuture<Optional<Message>> getMessageAsync(boolean b, String s) {
        return monitor(() -> delegate.getMessageAsync(b, s),
                Tags.builder()
                        .tag("method", "getMessageAsync"));
    }

    @Override
    public CompletableFuture<Optional<Message>> getMessageAsync(boolean b, String s, ReceiveMessageRequest
            receiveMessageRequest) {
        return delegate.getMessageAsync(b, s, receiveMessageRequest);
    }

    @Override
    public CompletableFuture<Optional<ChangeMessageVisibilityResult>> extendMessageVisibilityTimeout(String queueName,
                                                                                                     String receiptHandle, long duration, TimeUnit timeUnit) {
        return monitor(() -> delegate.extendMessageVisibilityTimeout(queueName, receiptHandle, duration, timeUnit),
                Tags.builder()
                        .tag("method", "extendMessageVisibilityTimeout"));
    }

    @Override
    public boolean isClosed() {
        return delegate.isClosed();
    }

    @Override
    public SendMessageResult sendMessage(String s, SendMessageRequest sendMessageRequest) {
        return delegate.sendMessage(s, sendMessageRequest);
    }

    @Override
    public Optional<Message> getMessage(boolean b, String s) {
        return monitor(() -> delegate.getMessage(b, s), Tags.builder().tag("method", "getMessage"));
    }

    @Override
    public Optional<Message> getMessage(boolean b, String s, ReceiveMessageRequest receiveMessageRequest) {
        return monitor(() -> delegate.getMessage(b, s, receiveMessageRequest),
                Tags.builder().tag("method", "getMessage"));
    }

    @Override
    public List<Message> getMessages(boolean b, String s, int i) {
        return monitor(() -> delegate.getMessages(b, s, i),
                Tags.builder().tag("method", "getMessages"));
    }

    @Override
    public List<Message> getMessages(boolean b, String s, int i, ReceiveMessageRequest receiveMessageRequest) {
        return monitor(() -> delegate.getMessages(b, s, i, receiveMessageRequest),
                Tags.builder().tag("method", "getMessages"));
    }

    @Override
    public DeleteMessageResult deleteMessage(String queueName, Message message) {
        return monitor(() -> delegate.deleteMessage(queueName, message),
                Tags.builder()
                        .tag("method", "deleteMessage"));
    }

    @Override
    public long getNumMessages(String s) {
        return monitor(() -> delegate.getNumMessages(s), Tags.builder().tag("method", "getNumMessages"));
    }

    @Override
    public CreateQueueResult createQueue(String s, CreateQueueRequest createQueueRequest) {
        return delegate.createQueue(s, createQueueRequest);
    }

    @Override
    public DeleteQueueResult deleteQueue(String queueName) {
        return monitor(() -> delegate.deleteQueue(queueName), Tags.builder().tag("method", "deleteQueue"));
    }

    @Override
    public GetQueueAttributesResult getQueueAttributes(GetQueueAttributesRequest getQueueAttributesRequest) {
        return delegate.getQueueAttributes(getQueueAttributesRequest);
    }

    private <T> T monitor(Callable<T> callable, Tags.TagsBuilder tags) {
        Stopwatch timer = Stopwatch.createStarted();
        try {
            T val = callable.call();
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.SUCCCESS_TAG);
            return val;
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.FAILURE_TAG);
            throw new RuntimeException(e);
        } finally {
            timer.stop();
            metricRegistry.timer(tags.build().toMetricName("timer.sqs_client"))
                    .update(timer.elapsed(TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS);
        }
    }

    private void monitor(Runnable runnable, Tags.TagsBuilder tags) {
        Stopwatch timer = Stopwatch.createStarted();
        try {
            runnable.run();
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.SUCCCESS_TAG);
        } catch (RuntimeException e) {
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.FAILURE_TAG);
            throw e;
        } finally {
            timer.stop();
            metricRegistry.timer(tags.build().toMetricName("timer.sqs_client"))
                    .update(timer.elapsed(TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS);
        }
    }

    @Override
    public void close() throws Exception {
        delegate.close();
    }
}

