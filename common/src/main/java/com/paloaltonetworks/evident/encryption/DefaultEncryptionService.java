package com.paloaltonetworks.evident.encryption;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.paloaltonetworks.aperture.ApertureVaultException;
import com.paloaltonetworks.aperture.VaultService;
import com.paloaltonetworks.evident.config.VaultProperties;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

// todo - make this a service - vault tmp token dependency.
public class DefaultEncryptionService implements EncryptionService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    public static final String NAME = "defaultEncryptionService";
    public static final String VAULT_ENCRYPTED_STRING_PREFIX = "vault";
    private final VaultProperties vaultProperties;
    private final VaultService vaultService;
    private final Cache<String, String> cache = CacheBuilder.newBuilder()
            .expireAfterAccess(600, TimeUnit.SECONDS)
            .maximumSize(30).build();

    public DefaultEncryptionService(VaultProperties vaultProperties, VaultService vaultService) {
        this.vaultProperties = vaultProperties;
        this.vaultService = vaultService;
    }

    @Override
    public String encrypt(String keyname, String plainText) {
        try {
            return vaultService.encrypt(keyname, plainText, vaultProperties.getAppId(), vaultProperties.getUserId());
        } catch (ApertureVaultException e) {
            throw new RuntimeException("Unable to encrypt text.", e);
        }
    }

    @Override
    public CompletionStage<String> encryptAsync(String keyname, String plainText) {
        return CompletableFuture.supplyAsync(() -> encrypt(keyname, plainText));
    }

    @Override
    public String decrypt(String keyname, String ciphertext) {
        String ifPresent = cache.getIfPresent(keyname);
        if (StringUtils.isNotEmpty(ifPresent)) {
            return ifPresent;
        }
        String result;
        synchronized (DefaultEncryptionService.class) {
            if (StringUtils.isNotEmpty(cache.getIfPresent(keyname))) {
                result = cache.getIfPresent(keyname);
            } else {

                if (!StringUtils.startsWith(ciphertext, VAULT_ENCRYPTED_STRING_PREFIX)) {
                    LOGGER.info("Ciphertext was already decrypted");
                    result = ciphertext;
                } else {
                    try {
                        result = vaultService.decrypt(keyname, ciphertext, vaultProperties.getAppId(), vaultProperties.getUserId());
                    } catch (ApertureVaultException e) {
                        throw new RuntimeException("Unable to decrypt text.", e);
                    }
                }
                cache.put(keyname, result);
            }
        }
        return result;
    }
}
