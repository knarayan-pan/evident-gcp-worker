package com.paloaltonetworks.evident.encryption;

import java.util.concurrent.CompletionStage;

public interface EncryptionService {
    /**
     * Encrypts a string
     *
     * @param keyname   encryption key name
     * @param plainText to encrypt
     * @return encrypted string
     */
    String encrypt(String keyname, String plainText);

    /**
     * Encrypts a string
     *
     * @param keyname   encryption key name
     * @param plainText to encrypt
     * @return {@link CompletionStage <String>} of encrypted string
     */
    CompletionStage<String> encryptAsync(String keyname, String plainText);

    /**
     * Decrypts a ciphertext.
     *
     * @param keyname    encryption key name
     * @param ciphertext to decrypt
     * @return decrypted string
     */
    String decrypt(String keyname, String ciphertext);
}
