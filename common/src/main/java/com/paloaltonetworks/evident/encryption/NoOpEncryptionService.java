package com.paloaltonetworks.evident.encryption;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class NoOpEncryptionService implements EncryptionService {
    @Override
    public String encrypt(String keyname, String plainText) {
        return plainText;
    }

    @Override
    public CompletionStage<String> encryptAsync(String keyname, String plainText) {
        return CompletableFuture.completedFuture(plainText);
    }

    @Override
    public String decrypt(String keyname, String ciphertext) {
        return ciphertext;
    }
}
