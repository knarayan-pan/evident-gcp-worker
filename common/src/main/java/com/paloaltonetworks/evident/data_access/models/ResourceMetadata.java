package com.paloaltonetworks.evident.data_access.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResourceMetadata {

    @JsonProperty("tags")
    private Map<String, String> tags;
    @JsonProperty("resource_name")
    private String resourceName;
    @JsonProperty("project_id")
    private String projectId;
    @JsonProperty("version")
    private String version;

}
