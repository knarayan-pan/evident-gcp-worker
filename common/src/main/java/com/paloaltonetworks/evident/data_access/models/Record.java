package com.paloaltonetworks.evident.data_access.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.json.JSONObject;

/**
 * Interface that every table must implement to talk to apilayer/cassandra data store.
 */
public interface Record {

    /**
     * Method that would need to be implemented to retrieve the primary key of the record that is being operated on in a format that's easy to query using K=V format.
     *
     * @return a json object containing the fields that would act as primary key to a specific record
     */
    @JsonIgnore
    JSONObject getKey();

    /**
     * Method that would povide the partitioning key to a set of records in cassandra. This is useful when we need to perform range queries as an e.g.
     * @return a Json object containing the fields that would be the primary key for the table.
     */
    @JsonIgnore
    JSONObject getPartitioningKey();

}
