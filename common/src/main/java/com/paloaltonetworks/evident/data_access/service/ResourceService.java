package com.paloaltonetworks.evident.data_access.service;

import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

public interface ResourceService {

    /**
     * Upsert a given resource record to apilayer
     *
     * @param tenant     the customer/tenant name
     * @param externalId the current account id
     * @param resource   the {@link Resource} object that needs to be upserted.
     * @return boolean indicating if the change request was issued.
     */
    boolean upsert(@Nonnull String tenant, @Nonnull String externalId, @Nonnull Resource resource);

    /**
     * Upsert a given resource record to apilayer
     *
     * @param tenant      the customer/tenant name
     * @param resourceKey the {@link ResourceKey} indicating the unique primary key of the resource
     * @param update      the {@link Map} object that contains the fields to be updated with its values.
     * @return boolean indicating if the change request was issued.
     */
    boolean update(@Nonnull String tenant, @Nonnull ResourceKey resourceKey, @Nonnull JSONObject update);

    /**
     * Upsert a given resource record to apilayer
     *
     * @param tenant      the customer/tenant name
     * @param resourceKey the {@link ResourceKey} indicating the unique primary key of the resource
     * @return {@link Resource} object if available in DB for this resource key
     */
    Optional<Resource> findOne(@Nonnull String tenant, @Nonnull ResourceKey resourceKey);

    /**
     * Upsert a given resource record to apilayer
     *
     * @param tenant       the customer/tenant name
     * @param externalId   the current account id
     * @param resourceType the resource type we wish to filter by
     * @return List of {@link Resource} objects if available in DB
     */
    Stream<Resource> findResourcesByType(@Nonnull String tenant, @Nonnull String externalId, @Nonnull String resourceType);

    /**
     * Delete a given resource record to apilayer
     *
     * @param tenant      the customer/tenant name
     * @param resourceKey the {@link ResourceKey} indicating the unique primary key of the resource
     * @return boolean indicating if the change was applied
     */
    boolean deleteResource(@Nonnull String tenant, @Nonnull ResourceKey resourceKey);
}
