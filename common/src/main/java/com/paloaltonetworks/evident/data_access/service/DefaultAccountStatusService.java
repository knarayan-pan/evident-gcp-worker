package com.paloaltonetworks.evident.data_access.service;

import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Update;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.paloaltonetworks.evident.cassandra.CassandraTemplateFactory;
import com.paloaltonetworks.evident.config.CustomObjectMapper;
import com.paloaltonetworks.evident.data_access.models.AccountMetadata;
import com.paloaltonetworks.evident.data_access.models.AccountStatus;
import com.paloaltonetworks.evident.data_access.models.ScanStatus;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

import static com.datastax.driver.core.querybuilder.QueryBuilder.eq;
import static com.datastax.driver.core.querybuilder.QueryBuilder.set;

@Primary
@Service
public class DefaultAccountStatusService implements AccountStatusService {

    private final CassandraTemplateFactory templateFactory;

    @Autowired
    public DefaultAccountStatusService(CassandraTemplateFactory templateFactory) {
        this.templateFactory = templateFactory;
    }

    @Override
    public void upsert(@Nonnull String tenant, @Nonnull String externalId, @Nonnull AccountStatus status) throws JsonProcessingException {
        Preconditions.checkArgument(StringUtils.equals(status.getExternalAccountId(), externalId),
                "insert must be done for the same external account id");
        CassandraTemplate cassandraTemplate = templateFactory.getCassandraTemplate(tenant);
        cassandraTemplate.getCqlOperations().execute(QueryBuilder.insertInto(templateFactory.getTenantKeyspace(), AccountStatus.TABLE_NAME)
                .json(CustomObjectMapper.encode(status)));
    }

    private boolean update(@Nonnull String tenant, @Nonnull String externalId, Map<String, Object> updateMap) {
        CassandraTemplate cassandraTemplate = templateFactory.getCassandraTemplate(tenant);

        Update.Assignments with = QueryBuilder.update(templateFactory.getTenantKeyspace(), AccountStatus.TABLE_NAME).with();
        for (String field : updateMap.keySet()) {
            if (!AccountStatus.EXTERNAL_ACCOUNT_ID.equalsIgnoreCase(field)) {
                with = with.and(set(field, updateMap.get(field)));
            }
        }
        // populate primary key
        Update.Where where = with.where(eq(AccountStatus.EXTERNAL_ACCOUNT_ID, externalId));
        return cassandraTemplate.getCqlOperations().execute(where);
    }

    @Override
    public Optional<AccountStatus> getStatus(@Nonnull String tenant, @Nonnull String externalId) {
        CassandraTemplate cassandraTemplate = templateFactory.getCassandraTemplate(tenant);
        return Optional.ofNullable(cassandraTemplate.selectOne(QueryBuilder.select().from(templateFactory.getTenantKeyspace(),
                AccountStatus.TABLE_NAME).where(eq(AccountStatus.EXTERNAL_ACCOUNT_ID, externalId)), AccountStatus.class));

    }

    @Override
    public boolean updateScanStatus(@Nonnull String tenant, @Nonnull String externalId, ScanStatus scanStatus) {
        return update(tenant, externalId, ImmutableMap.<String, Object>builder().put(AccountStatus.SCAN_STATUS, scanStatus.getStatus()).build());
    }

    @Override
    public boolean updateOnboardingDate(@Nonnull String tenant, @Nonnull String externalId, Date onboardingDate) {
        return update(tenant, externalId, ImmutableMap.<String, Object>builder().put(AccountStatus.ONBOARDING_DATE, onboardingDate).build());
    }

    @Override
    public boolean updateMetadata(@Nonnull String tenant, @Nonnull String externalId, AccountMetadata metadata) {
        String serializedMetadata = CustomObjectMapper.encode(metadata);
        return !StringUtils.isEmpty(serializedMetadata) &&
                update(tenant, externalId, ImmutableMap.<String, Object>builder().put(AccountStatus.METADATA, serializedMetadata).build());
    }

    @Override
    public boolean updateLastScanEventTime(@Nonnull String tenant, @Nonnull String externalId, Date lastEventScanTime) {
        return update(tenant, externalId, ImmutableMap.<String, Object>builder().put(AccountStatus.LAST_EVENT_SCAN_TIME, lastEventScanTime).build());
    }

}
