package com.paloaltonetworks.evident.data_access.service;

import com.codahale.metrics.MetricRegistry;
import com.google.common.base.Stopwatch;
import com.paloaltonetworks.aperture.MetricConstants;
import com.paloaltonetworks.aperture.Tags;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.lang.invoke.MethodHandles;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

@Service
@Qualifier(MonitoredResourceService.NAME)
public class MonitoredResourceService implements ResourceService {
    public static final String NAME = "MonitoredResourceService";
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final MetricRegistry metricRegistry;
    private final ResourceService delegate;

    @Autowired
    public MonitoredResourceService(MetricRegistry metricRegistry, ResourceService delegate) {
        this.metricRegistry = metricRegistry;
        this.delegate = delegate;
    }


    @Override
    public boolean upsert(@Nonnull String tenant, @Nonnull String externalId, @Nonnull Resource resource) {
        return monitor(() -> delegate.upsert(tenant, externalId, resource), Tags.builder()
                .tag("method", "upsert")
                .tag("externalAccountId", externalId)
                .tag("tenant", tenant));
    }

    @Override
    public boolean update(@Nonnull String tenant, @Nonnull ResourceKey resourceKey, @Nonnull JSONObject update) {
        return monitor(() -> delegate.update(tenant, resourceKey, update), Tags.builder()
                .tag("method", "update")
                .tag("externalAccountId", resourceKey.getExternalAccountId())
                .tag("tenant", tenant));
    }

    @Override
    public Optional<Resource> findOne(@Nonnull String tenant, @Nonnull ResourceKey resourceKey) {
        return monitor(() -> delegate.findOne(tenant, resourceKey), Tags.builder()
                .tag("method", "findOne")
                .tag("externalAccountId", resourceKey.getExternalAccountId())
                .tag("tenant", tenant));
    }

    @Override
    public Stream<Resource> findResourcesByType(@Nonnull String tenant, @Nonnull String externalId, @Nonnull String resourceType) {
        return monitor(() -> delegate.findResourcesByType(tenant, externalId, resourceType), Tags.builder()
                .tag("method", "findResourcesByType")
                .tag("externalAccountId", externalId)
                .tag("tenant", tenant));
    }

    @Override
    public boolean deleteResource(@Nonnull String tenant, @Nonnull ResourceKey resourceKey) {
        return monitor(() -> delegate.deleteResource(tenant, resourceKey), Tags.builder()
                .tag("method", "deleteResource")
                .tag("externalAccountId", resourceKey.getExternalAccountId())
                .tag("tenant", tenant));
    }


    private <T> T monitor(Callable<T> callable, Tags.TagsBuilder tags) {
        Stopwatch timer = Stopwatch.createStarted();
        try {
            T val = callable.call();
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.SUCCCESS_TAG);
            return val;
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.FAILURE_TAG);
            throw new RuntimeException(e);
        } finally {
            timer.stop();
            metricRegistry.timer(tags.build().toMetricName("timer.db.resource"))
                    .update(timer.elapsed(TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS);
        }
    }
}
