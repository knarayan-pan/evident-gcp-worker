package com.paloaltonetworks.evident.data_access.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.paloaltonetworks.evident.config.CustomObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.io.IOException;
import java.util.Date;
import java.util.Optional;

@Data
@Builder(toBuilder = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class AccountStatus implements Record {
    public static final String TABLE_NAME = "account_status";
    public static final String EXTERNAL_ACCOUNT_ID = "external_account_id";
    private static final String CLOUDAPP_TYPE = "cloudapp_type";
    public static final String SCAN_STATUS = "scan_status";
    public static final String ONBOARDING_DATE = "onboarding_date";
    private static final String DATE_CREATED = "date_created";
    private static final String DATE_UPDATED = "date_updated";
    public static final String METADATA = "metadata";
    public static final String LAST_EVENT_SCAN_TIME = "last_event_scan_time";

    // primary key
    @JsonProperty(EXTERNAL_ACCOUNT_ID)
    @PrimaryKey(EXTERNAL_ACCOUNT_ID)
    private String externalAccountId;
    @JsonProperty(CLOUDAPP_TYPE)
    @Column(CLOUDAPP_TYPE)
    private CloudAppType cloudAppType;
    @JsonProperty(SCAN_STATUS)
    @Column(SCAN_STATUS)
    @Builder.Default
    private ScanStatus scanStatus = ScanStatus.not_started;

    @JsonProperty(ONBOARDING_DATE)
    @Column(ONBOARDING_DATE)
    private Date onboardingDate;
    @JsonProperty(DATE_CREATED)
    @Column(DATE_CREATED)
    private Date dateCreated;
    @JsonProperty(DATE_UPDATED)
    @Column(DATE_UPDATED)
    @Builder.Default
    private Date dateUpdated = new Date();
    @JsonProperty(LAST_EVENT_SCAN_TIME)
    @Column(LAST_EVENT_SCAN_TIME)
    private Date lastEventScanTime;
    // serialized version of AccountMetadata object - useful as we dont need to do schema change for any new data we want to track.
    @JsonProperty(METADATA)
    @Column(METADATA)
    private String metadata;

    @JsonIgnore
    @Override
    public JSONObject getKey() {
        return new JSONObject()
                .put(EXTERNAL_ACCOUNT_ID, externalAccountId);
    }

    @JsonIgnore
    @Override
    public JSONObject getPartitioningKey() {
        return new JSONObject()
                .put(EXTERNAL_ACCOUNT_ID, externalAccountId);
    }

    @JsonIgnore
    public Optional<AccountMetadata> getAccountMetadata() {
        try {
            return StringUtils.isEmpty(metadata) ? Optional.empty() :
                    Optional.ofNullable(CustomObjectMapper.decodeValue(metadata, AccountMetadata.class));
        } catch (IOException e) {
            return Optional.empty();
        }
    }
}
