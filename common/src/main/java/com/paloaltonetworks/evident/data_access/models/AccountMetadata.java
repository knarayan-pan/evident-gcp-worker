package com.paloaltonetworks.evident.data_access.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.MoreObjects;
import com.paloaltonetworks.evident.config.CustomObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Set;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountMetadata {

    @JsonProperty("organization_id")
    private String orgId;

    public String toJson() throws JsonProcessingException {
        return CustomObjectMapper.encode(this);
    }
}
