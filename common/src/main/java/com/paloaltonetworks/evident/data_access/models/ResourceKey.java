package com.paloaltonetworks.evident.data_access.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.hash.Hashing;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import javax.annotation.Nonnull;
import java.util.Objects;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@PrimaryKeyClass
public class ResourceKey {
    public static final String EXTERNAL_ACCOUNT_ID = "external_account_id";
    public static final String RESOURCE_TYPE = "resource_type";
    public static final String RESOURCE_ID_HASH = "resource_id_hash";


    @JsonProperty(EXTERNAL_ACCOUNT_ID)
    @Nonnull
    @PrimaryKeyColumn(value = EXTERNAL_ACCOUNT_ID, ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    private String externalAccountId;

    @JsonProperty(RESOURCE_ID_HASH)
    @Nonnull
    @PrimaryKeyColumn(value = RESOURCE_ID_HASH, ordinal = 1, type = PrimaryKeyType.PARTITIONED)
    private Integer resourceIdHash;

    @JsonProperty(RESOURCE_TYPE)
    @Nonnull
    @PrimaryKeyColumn(value = RESOURCE_TYPE, ordinal = 2, type = PrimaryKeyType.CLUSTERED)
    private String resourceType;

    public boolean isValid() {
        return !(StringUtils.isEmpty(externalAccountId) || StringUtils.isEmpty(resourceType));
    }

    public static Integer computeResourceIdHash(@Nonnull String resourceId) {
        return Hashing.murmur3_32(13).hashString(resourceId.toUpperCase(), Charsets.UTF_8).asInt();
    }

    public String redisKeyString() {
        return Joiner.on(":").join(externalAccountId, resourceType, resourceIdHash);
    }

    boolean isSimilar(ResourceKey other) {
        return other != null && (Objects.equals(this.getExternalAccountId(), other.getExternalAccountId()) &&
                Objects.equals(this.getResourceIdHash(), other.getResourceIdHash()) &&
                Objects.equals(this.getResourceType(), other.getResourceType()));
    }
}
