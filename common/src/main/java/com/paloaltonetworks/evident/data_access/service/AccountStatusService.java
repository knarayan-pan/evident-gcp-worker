package com.paloaltonetworks.evident.data_access.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.paloaltonetworks.evident.data_access.models.AccountMetadata;
import com.paloaltonetworks.evident.data_access.models.AccountStatus;
import com.paloaltonetworks.evident.data_access.models.ScanStatus;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.Optional;

public interface AccountStatusService {
    /**
     * Upserts an account record
     *
     * @param tenant     the tenant/customer name retrived from task message or properties file.
     * @param externalId the unique external account id of this install
     * @param status     the account status pojo
     * @throws JsonProcessingException when the status object cannot be serialized into json
     */
    void upsert(@Nonnull String tenant, @Nonnull String externalId, @Nonnull AccountStatus status) throws JsonProcessingException;

    /**
     * Retrieves a single account status object based on the external account Id.
     *
     * @param tenant     the tenant/customer name retrived from task message or properties file.
     * @param externalId the unique external account id of this install
     * @return {@link AccountStatus} optional if present.
     */
    Optional<AccountStatus> getStatus(@Nonnull String tenant, @Nonnull String externalId);

    /**
     * Update scan status for this external account.
     *
     * @param tenant     the tenant/customer name retrived from task message or properties file.
     * @param externalId the unique external account id of this install
     * @param scanStatus the {@link ScanStatus} enum indicating the status of the scan
     * @return boolean indicating if the change was applied
     */
    boolean updateScanStatus(@Nonnull String tenant, @Nonnull String externalId, ScanStatus scanStatus);

    /**
     * Update scan status for this external account.
     *
     * @param tenant         the tenant/customer name retrived from task message or properties file.
     * @param externalId     the unique external account id of this install
     * @param onboardingDate the {@link Date} the account onboarded
     * @return boolean indicating if the change was applied
     */
    boolean updateOnboardingDate(@Nonnull String tenant, @Nonnull String externalId, Date onboardingDate);

    /**
     * Update scan status for this external account.
     *
     * @param tenant     the tenant/customer name retrived from task message or properties file.
     * @param externalId the unique external account id of this install
     * @param metadata   the {@link AccountMetadata} change that needs to be updated to reflect any account specific information.
     * @return boolean indicating if the change was applied
     */
    boolean updateMetadata(@Nonnull String tenant, @Nonnull String externalId, AccountMetadata metadata);

    /**
     * Update scan status for this external account.
     *
     * @param tenant            the tenant/customer name retrived from task message or properties file.
     * @param externalId        the unique external account id of this install
     * @param lastEventScanTime the {@link Date} object indicating the last time the events were scanned. This is to be
     *                          treated as an event marker for the google account. Useful for polling scenarios.
     * @return boolean indicating if the change was applied
     */
    boolean updateLastScanEventTime(@Nonnull String tenant, @Nonnull String externalId, Date lastEventScanTime);
}
