package com.paloaltonetworks.evident.data_access.models;

public enum CloudAppType {
    GCP,
    AZURE,
    AWS
}
