package com.paloaltonetworks.evident.data_access.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import com.paloaltonetworks.evident.config.CustomObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

import static com.paloaltonetworks.evident.data_access.models.ResourceKey.EXTERNAL_ACCOUNT_ID;
import static com.paloaltonetworks.evident.data_access.models.ResourceKey.RESOURCE_ID_HASH;
import static com.paloaltonetworks.evident.data_access.models.ResourceKey.RESOURCE_TYPE;

@Data
@Builder(toBuilder = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class Resource implements Record {
    public static final String TABLE_NAME = "resources";
    public static final String TENANT = "tenant";
    public static final String CLOUDAPP_TYPE = "cloudapp_type";
    public static final String DATE_CREATED = "date_created";
    public static final String DATE_UPDATED = "date_updated";
    public static final String METADATA = "metadata";
    public static final String RESOURCE_ID = "resource_id";
    public static final String REGION = "region";

    @PrimaryKey
    @JsonIgnore
    @Nonnull
    private ResourceKey resourceKey;
    @JsonProperty(TENANT)
    @Column(TENANT)
    private String tenant;
    @JsonProperty(RESOURCE_ID)
    @Column(RESOURCE_ID)
    private String resourceId;
    @JsonProperty(CLOUDAPP_TYPE)
    @Column(CLOUDAPP_TYPE)
    private CloudAppType cloudAppType;
    @JsonProperty(REGION)
    @Column(REGION)
    private String region;
    // serialized version of the resource metadata
    @JsonProperty(METADATA)
    @Column(METADATA)
    @Nonnull
    private String resourceMetadata;
    @JsonProperty(DATE_UPDATED)
    @Column(DATE_UPDATED)
    @Builder.Default
    private Date dateUpdated = new Date();


    @JsonIgnore
    @Override
    public JSONObject getKey() {
        return resourceKey == null ? null : new JSONObject()
                .put(EXTERNAL_ACCOUNT_ID, resourceKey.getExternalAccountId())
                .put(RESOURCE_TYPE, resourceKey.getResourceType())
                .put(RESOURCE_ID_HASH, resourceKey.getResourceIdHash());
    }

    @JsonIgnore
    @Override
    public JSONObject getPartitioningKey() {
        return resourceKey == null ? null : new JSONObject()
                .put(EXTERNAL_ACCOUNT_ID, resourceKey.getExternalAccountId())
                .put(RESOURCE_TYPE, resourceKey.getResourceType());
    }

    @JsonIgnore
    public int getResourceIdHash() {
        return Hashing.murmur3_32(13).hashString(resourceId.toUpperCase(), Charsets.UTF_8).asInt();
    }

    @JsonIgnore
    public <T extends ResourceMetadata> T getResourceMetadata(Class<T> tClass) {
        try {
            return CustomObjectMapper.decodeValue(resourceMetadata, tClass);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Cannot deserialize metadata information for resource: %s", resourceId), e);
        }
    }

    public boolean isSimilar(Resource other) {
        if (other == null) {
            return false;
        }
        boolean isSimilar = StringUtils.equalsIgnoreCase(this.region, other.region);
        isSimilar &= Objects.equals(this.resourceId, other.resourceId);
        isSimilar &= resourceKey.isSimilar(other.resourceKey);
        isSimilar &= Objects.equals(tenant, other.tenant);
        // map equality
        try {
            Map<String, Object> thisMetadata = CustomObjectMapper.decodeValue(resourceMetadata, Map.class);
            Map<String, Object> otherMetadata = CustomObjectMapper.decodeValue(other.resourceMetadata, Map.class);
            isSimilar &= Objects.deepEquals(thisMetadata, otherMetadata);
        } catch (IOException e) {
            isSimilar &= false;
        }
        return isSimilar;
    }
}
