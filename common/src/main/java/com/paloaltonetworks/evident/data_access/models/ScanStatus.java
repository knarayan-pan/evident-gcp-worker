package com.paloaltonetworks.evident.data_access.models;

public enum ScanStatus {

    not_started("not_started"),
    scanning("scanning"),
    stopped("stopped");

    private final String status;

    ScanStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return status.toLowerCase();
    }

}
