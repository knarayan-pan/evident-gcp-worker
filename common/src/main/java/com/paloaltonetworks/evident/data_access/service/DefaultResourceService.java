package com.paloaltonetworks.evident.data_access.service;

import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.google.common.annotations.VisibleForTesting;
import com.paloaltonetworks.evident.apilayer.ApiLayerClient;
import com.paloaltonetworks.evident.apilayer.ApiLayerClientFactory;
import com.paloaltonetworks.evident.apilayer.ApiLayerException;
import com.paloaltonetworks.evident.apilayer.ApiLayerTopic;
import com.paloaltonetworks.evident.cassandra.CassandraTemplateFactory;
import com.paloaltonetworks.evident.config.CustomObjectMapper;
import com.paloaltonetworks.evident.data_access.models.Resource;
import com.paloaltonetworks.evident.data_access.models.ResourceKey;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.lang.invoke.MethodHandles;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Stream;

import static com.datastax.driver.core.querybuilder.QueryBuilder.eq;
import static com.datastax.driver.core.querybuilder.QueryBuilder.gte;
import static com.datastax.driver.core.querybuilder.QueryBuilder.lte;
import static com.paloaltonetworks.evident.data_access.models.ResourceKey.EXTERNAL_ACCOUNT_ID;
import static com.paloaltonetworks.evident.data_access.models.ResourceKey.RESOURCE_ID_HASH;
import static com.paloaltonetworks.evident.data_access.models.ResourceKey.RESOURCE_TYPE;

@Service
@Primary
public class DefaultResourceService implements ResourceService {

    private final CassandraTemplateFactory templateFactory;
    private final ApiLayerClientFactory apiLayerClientFactory;
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    public DefaultResourceService(CassandraTemplateFactory templateFactory, ApiLayerClientFactory apiLayerClientFactory) {
        this.templateFactory = templateFactory;
        this.apiLayerClientFactory = apiLayerClientFactory;
    }

    @VisibleForTesting
    boolean validateResourceForStorage(@Nonnull Resource resource) {
        return resource.getResourceKey() != null
                && resource.getResourceKey().isValid()
                && StringUtils.isNotEmpty(resource.getResourceId());
    }

    @Override
    public boolean upsert(@Nonnull String tenant, @Nonnull String externalId, @Nonnull Resource resource) {
        if (!validateResourceForStorage(resource)) {
            LOGGER.error("Resource {} is not in a valid state. Skipping upsert operation.", resource);
        }
        ApiLayerClient client = apiLayerClientFactory.getClient(tenant);
        // update date_updated
        resource.setDateUpdated(new Date());
        try {
            client.postUpsert(ApiLayerTopic.RESOURCES, tenant, resource.getKey(), new JSONObject(CustomObjectMapper.encode(resource)));
            return true;
        } catch (ApiLayerException e) {
            LOGGER.error("Could not upsert {} for account: {} - Reason: {}", resource.getResourceId(), externalId, e.getMessage(), e);
            return false;
        }
    }

    private String getFormattedUpdatedAtTime() {
        return CustomObjectMapper.DATE_TIME_FORMAT.format(new Date());
    }

    @Override
    public boolean update(@Nonnull String tenant, @Nonnull ResourceKey resourceKey, @Nonnull JSONObject update) {
        if (!resourceKey.isValid()) {
            LOGGER.error("ResourceKey {} is not in a valid state. Skipping update operation.", resourceKey);
        }
        ApiLayerClient client = apiLayerClientFactory.getClient(tenant);
        JSONObject query = new JSONObject()
                .put(EXTERNAL_ACCOUNT_ID, resourceKey.getExternalAccountId())
                .put(RESOURCE_ID_HASH, resourceKey.getResourceIdHash())
                .put(RESOURCE_TYPE, resourceKey.getResourceType());
        // always add date-updated
        update.put(Resource.DATE_UPDATED, new Date().getTime());


        try {
            client.postUpdate(ApiLayerTopic.RESOURCES, tenant, query, update);
            return true;
        } catch (ApiLayerException e) {
            LOGGER.error("Could not update {} for account: {} - Reason: {}", resourceKey.getResourceIdHash(),
                    resourceKey.getExternalAccountId(), e.getMessage(), e);
            return false;
        }
    }

    @Override
    public Optional<Resource> findOne(@Nonnull String tenant, @Nonnull ResourceKey resourceKey) {
        if (!resourceKey.isValid()) {
            LOGGER.error("ResourceKey {} is not in a valid state. Skipping find operation.", resourceKey);
        }

        CassandraTemplate cassandraTemplate = templateFactory.getCassandraTemplate(tenant);
        return Optional.ofNullable(cassandraTemplate.selectOne(QueryBuilder.select().from(templateFactory.getTenantKeyspace(),
                Resource.TABLE_NAME).where(eq(ResourceKey.EXTERNAL_ACCOUNT_ID, resourceKey.getExternalAccountId()))
                .and(eq(ResourceKey.RESOURCE_TYPE, resourceKey.getResourceType()))
                .and(eq(ResourceKey.RESOURCE_ID_HASH, resourceKey.getResourceIdHash())), Resource.class));

    }

    @Override
    public Stream<Resource> findResourcesByType(@Nonnull String tenant, @Nonnull String externalId, @Nonnull String resourceType) {
        CassandraTemplate cassandraTemplate = templateFactory.getCassandraTemplate(tenant);
        return cassandraTemplate.stream(QueryBuilder.select().from(templateFactory.getTenantKeyspace(),
                Resource.TABLE_NAME).where(eq(ResourceKey.EXTERNAL_ACCOUNT_ID, externalId))
                        .and(eq(ResourceKey.RESOURCE_TYPE, resourceType))
                        .and(gte(ResourceKey.RESOURCE_ID_HASH, Long.MIN_VALUE))
                        .and(lte(ResourceKey.RESOURCE_ID_HASH, Long.MAX_VALUE))
                        .setFetchSize(1000)
                , Resource.class);
    }

    @Override
    public boolean deleteResource(@Nonnull String tenant, @Nonnull ResourceKey resourceKey) {
        if (!resourceKey.isValid()) {
            LOGGER.error("ResourceKey {} is not in a valid state. Skipping update operation.", resourceKey);
        }
        ApiLayerClient client = apiLayerClientFactory.getClient(tenant);
        JSONObject query = new JSONObject()
                .put(EXTERNAL_ACCOUNT_ID, resourceKey.getExternalAccountId())
                .put(RESOURCE_ID_HASH, resourceKey.getResourceIdHash())
                .put(RESOURCE_TYPE, resourceKey.getResourceType());
        try {
            client.postDelete(ApiLayerTopic.RESOURCES, tenant, query);
            return true;
        } catch (ApiLayerException e) {
            LOGGER.error("Could not delete {} for account: {} - Reason: {}", resourceKey.getResourceIdHash(),
                    resourceKey.getExternalAccountId(), e.getMessage(), e);
            return false;
        }

    }


}
