package com.paloaltonetworks.evident.data_access.service;

import com.codahale.metrics.MetricRegistry;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Stopwatch;
import com.paloaltonetworks.aperture.MetricConstants;
import com.paloaltonetworks.aperture.Tags;
import com.paloaltonetworks.evident.data_access.models.AccountMetadata;
import com.paloaltonetworks.evident.data_access.models.AccountStatus;
import com.paloaltonetworks.evident.data_access.models.ScanStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.lang.invoke.MethodHandles;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

@Service
@Qualifier(MonitoredAccountStatusService.NAME)
public class MonitoredAccountStatusService implements AccountStatusService {
    public static final String NAME = "MonitoredAccountStatusService";
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final AccountStatusService delegate;
    private final MetricRegistry metricRegistry;

    @Autowired
    public MonitoredAccountStatusService(AccountStatusService accountStatusService, MetricRegistry metricRegistry) {
        this.delegate = accountStatusService;
        this.metricRegistry = metricRegistry;
    }

    @Override
    public void upsert(@Nonnull String tenant, @Nonnull String externalId, @Nonnull AccountStatus status) throws JsonProcessingException {
        monitor(() -> {
            try {
                delegate.upsert(tenant, externalId, status);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }, Tags.builder()
                .tag("method", "upsert")
                .tag("externalAccountId", externalId)
                .tag("tenant", tenant));
    }

    @Override
    public Optional<AccountStatus> getStatus(@Nonnull String tenant, @Nonnull String externalId) {
        return monitor(() -> delegate.getStatus(tenant, externalId), Tags.builder()
                .tag("method", "getStatus")
                .tag("externalAccountId", externalId)
                .tag("tenant", tenant));
    }

    @Override
    public boolean updateScanStatus(@Nonnull String tenant, @Nonnull String externalId, ScanStatus scanStatus) {
        return monitor(() -> delegate.updateScanStatus(tenant, externalId, scanStatus), Tags.builder()
                .tag("method", "updateScanStatus")
                .tag("externalAccountId", externalId)
                .tag("tenant", tenant));
    }

    @Override
    public boolean updateOnboardingDate(@Nonnull String tenant, @Nonnull String externalId, Date onboardingDate) {
        return monitor(() -> delegate.updateOnboardingDate(tenant, externalId, onboardingDate), Tags.builder()
                .tag("method", "updateOnboardingDate")
                .tag("externalAccountId", externalId)
                .tag("tenant", tenant));
    }

    @Override
    public boolean updateMetadata(@Nonnull String tenant, @Nonnull String externalId, AccountMetadata metadata) {
        return monitor(() -> delegate.updateMetadata(tenant, externalId, metadata), Tags.builder()
                .tag("method", "updateMetadata")
                .tag("externalAccountId", externalId)
                .tag("tenant", tenant));
    }

    @Override
    public boolean updateLastScanEventTime(@Nonnull String tenant, @Nonnull String externalId, Date lastEventScanTime) {
        return monitor(() -> delegate.updateLastScanEventTime(tenant, externalId, lastEventScanTime), Tags.builder()
                .tag("method", "updateLastScanEventTime")
                .tag("externalAccountId", externalId)
                .tag("tenant", tenant));
    }

    private <T> T monitor(Callable<T> callable, Tags.TagsBuilder tags) {
        Stopwatch timer = Stopwatch.createStarted();
        try {
            T val = callable.call();
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.SUCCCESS_TAG);
            return val;
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.FAILURE_TAG);
            throw new RuntimeException(e);
        } finally {
            timer.stop();
            metricRegistry.timer(tags.build().toMetricName("timer.db.account_status"))
                    .update(timer.elapsed(TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS);
        }
    }

    private void monitor(Runnable runnable, Tags.TagsBuilder tags) {
        Stopwatch timer = Stopwatch.createStarted();
        try {
            runnable.run();
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.SUCCCESS_TAG);
        } catch (RuntimeException e) {
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.FAILURE_TAG);
            throw e;
        } finally {
            timer.stop();
            metricRegistry.timer(tags.build().toMetricName("timer.db.account_status"))
                    .update(timer.elapsed(TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS);
        }
    }
}
