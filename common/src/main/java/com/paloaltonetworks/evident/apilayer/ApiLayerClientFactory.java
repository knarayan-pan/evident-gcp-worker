package com.paloaltonetworks.evident.apilayer;

public interface ApiLayerClientFactory {
    /**
     * Get api layer client for this tenant
     *
     * @param tenant the customer/account name.
     * @return {@link ApiLayerClient}
     */
    ApiLayerClient getClient(final String tenant);

}
