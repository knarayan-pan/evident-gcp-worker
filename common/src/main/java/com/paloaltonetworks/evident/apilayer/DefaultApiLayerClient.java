package com.paloaltonetworks.evident.apilayer;

import com.github.rholder.retry.RetryException;
import com.github.rholder.retry.Retryer;
import com.github.rholder.retry.RetryerBuilder;
import com.github.rholder.retry.StopStrategies;
import com.github.rholder.retry.WaitStrategies;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class DefaultApiLayerClient implements ApiLayerClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static final String POSTABLE_STRING = "{\"topic\":\"%s\",\"message\":{\"cs_user\":\"%s\"," +
            "\"commit_databases\":%s,\"task_type\":\"%s\",\"query\":%s,\"document\":%s}}";
    public static final String API_LAYER_URL_FORMAT = "http://%s:%d/database/write";
    public static final String COMMIT_DB_ELASTICSEARCH = "elasticsearch";
    public static final String COMMIT_DB_CASSANDRA = "cassandra";

    private static final Retryer<Void> RETRYER = RetryerBuilder.<Void>newBuilder()
            .retryIfExceptionOfType(ApiLayerException.class)
            .withWaitStrategy(WaitStrategies.fibonacciWait(TimeUnit.SECONDS.toMillis(1), 5, TimeUnit.SECONDS))
            .withStopStrategy(StopStrategies.stopAfterAttempt(6))
            .build();

    public static Retryer<Void> getClientRetryer() {
        return RETRYER;
    }

    private final String host;
    private final int port;
    private final OkHttpClient okHttpClient;


    public DefaultApiLayerClient(String host, int port, OkHttpClient okHttpClient) {
        this.host = host;
        this.port = port;
        this.okHttpClient = okHttpClient;
    }

    @Override
    public String getApiLayerHost() {
        return host;
    }

    @Override
    public int getApiLayerPort() {
        return port;
    }

    @Override
    public void postUpdate(ApiLayerTopic topic, String tenant, JSONObject query, JSONObject updateDoc) throws ApiLayerException {
        String postData = buildPostString(topic, tenant, ApiLayerTopic.OpType.UPDATE, query, updateDoc);
        doPostAndRetry(postData);
    }

    @Override
    public void postDelete(ApiLayerTopic topic, String tenant, JSONObject query) throws ApiLayerException {
        String postData = buildPostString(topic, tenant, ApiLayerTopic.OpType.DELETE, query, new JSONObject());
        doPostAndRetry(postData);
    }

    @Override
    public void postInsert(ApiLayerTopic topic, String tenant, JSONObject query, JSONObject updateDoc) throws ApiLayerException {
        String postData = buildPostString(topic, tenant, ApiLayerTopic.OpType.INSERT, query, updateDoc);
        doPostAndRetry(postData);
    }

    private JSONObject mergeJson(JSONObject query, JSONObject update) {
        // upserts must contain primary key information too
        for (String key : query.keySet()) {
            update.put(key, query.get(key));
        }
        return update;
    }

    @Override
    public void postUpsert(ApiLayerTopic topic, String tenant, JSONObject query, JSONObject updateDoc) throws ApiLayerException {
        updateDoc = mergeJson(query, updateDoc);
        String postData = buildPostString(topic, tenant, ApiLayerTopic.OpType.UPSERT, query, updateDoc);
        doPostAndRetry(postData);
    }


    private void doPostAndRetry(final String postData) throws ApiLayerException {
        // application level retrying logic
        try {
            RETRYER.call(() -> {
                doPost(postData);
                return null;
            });
        } catch (ExecutionException e) {
            throw new ApiLayerException("Could not post to ApiLayer: unexpected exception:", e);
        } catch (RetryException e) {
            throw new ApiLayerException(String.format("Could not post to ApiLayer (%s attempt)", e.getNumberOfFailedAttempts()), e);
        }

    }

    private void doPost(final String postData) throws IOException {
        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), postData);
        LOGGER.debug("Post Data {}", postData);
        Response response = null;
        try {
            Request req = new Request.Builder()
                    .url(String.format(API_LAYER_URL_FORMAT, host, port))
                    .post(body)
                    .build();
            response = okHttpClient.newCall(req).execute();
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    @VisibleForTesting
    protected String buildPostString(ApiLayerTopic topic, String tenant, ApiLayerTopic.OpType op, JSONObject query, JSONObject updateDoc) {

        Preconditions.checkNotNull(tenant, "The tenant is null");
        Preconditions.checkNotNull(updateDoc, "updateDoc cannot be null");
        Preconditions.checkNotNull(query, "query object cannot be empty");

        String csUser = String.format("service@%s.com", tenant);

        Set<String> commitDbs = topic.getCommitDbs();
        LOGGER.debug("commitDbs={}", commitDbs);

        if (query.keySet().size() == 0) {
            throw new IllegalArgumentException(String.format("Cannot post API layer request: "
                    + "both id and query are missing (cs_user=%s, topic=%s, op=%s)", csUser, topic, op));
        }

        return String.format(POSTABLE_STRING, topic.toString(), csUser, new Gson().toJson(commitDbs), op.name(),
                query.toString(), updateDoc.toString());
    }
}
