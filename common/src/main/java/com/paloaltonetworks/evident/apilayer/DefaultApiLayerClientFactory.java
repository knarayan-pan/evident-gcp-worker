package com.paloaltonetworks.evident.apilayer;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.paloaltonetworks.aperture.consul.ConsulService;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.util.concurrent.TimeUnit;

@Component
@Primary
public class DefaultApiLayerClientFactory implements ApiLayerClientFactory {

    private final ConsulService consulService;

    private static final Integer APICLIENT_READ_TIMEOUT = (int) TimeUnit.SECONDS.toMillis(30);
    private static final Integer APICLIENT_CONNECT_TIMEOUT = (int) TimeUnit.SECONDS.toMillis(30);

    private static final String APICLIENT_READ_TIMEOUT_KEY = "apilayer/okhttpclient/read_timeout";
    private static final String APICLIENT_CONNECT_TIMEOUT_KEY = "apilayer/okhttpclient/connect_timeout";

    private static final String API_LAYER_HOST_KEY = "apilayer/HOST";
    private static final String API_LAYER_HOST_DEFAULT = "api.dataservice";
    private static final String API_LAYER_PORT_KEY = "apilayer/PORT";
    private static final int API_LAYER_PORT_DEFAULT = 8080;

    private static final Object lock = new Object();
    private Cache<String, ApiLayerClient> tenantClients;

    @Autowired
    public DefaultApiLayerClientFactory(ConsulService consulService) {
        this.consulService = consulService;
        this.tenantClients = CacheBuilder.newBuilder()
                .maximumSize(10)
                .build();
    }

    @Override
    public ApiLayerClient getClient(String tenant) {
        if (tenantClients.getIfPresent(tenant) != null) {
            return tenantClients.getIfPresent(tenant);
        } else {
            synchronized (lock) {
                if (tenantClients.getIfPresent(tenant) != null) {
                    return tenantClients.getIfPresent(tenant);
                } else {
                    String apiLayerHost = getApiLayerHost(tenant);
                    Integer apiLayerPort = getApiLayerPort(tenant);
                    Integer readTimeout = getReadTimeout(tenant);
                    Integer connectionTimeout = getConnectionTimeout(tenant);
                    OkHttpClient okHttpClient = new OkHttpClient.Builder()
                            .readTimeout(readTimeout, TimeUnit.SECONDS)
                            .connectTimeout(connectionTimeout, TimeUnit.SECONDS)
                            .build();
                    ApiLayerClient apiLayerClient = new DefaultApiLayerClient(apiLayerHost, apiLayerPort, okHttpClient);
                    tenantClients.put(tenant, apiLayerClient);
                    return apiLayerClient;
                }
            }
        }
    }

    @Nonnull
    private Integer getConnectionTimeout(String tenant) {
        return consulService.getIntForTenant(tenant, APICLIENT_CONNECT_TIMEOUT_KEY, APICLIENT_CONNECT_TIMEOUT);
    }

    @Nonnull
    private Integer getReadTimeout(String tenant) {
        return consulService.getIntForTenant(tenant, APICLIENT_READ_TIMEOUT_KEY, APICLIENT_READ_TIMEOUT);
    }

    @Nonnull
    private Integer getApiLayerPort(String tenant) {
        return consulService.getIntForTenant(tenant, API_LAYER_PORT_KEY, API_LAYER_PORT_DEFAULT);
    }

    @Nonnull
    private String getApiLayerHost(String tenant) {
        return consulService.getValueForTenant(tenant, API_LAYER_HOST_KEY, API_LAYER_HOST_DEFAULT);
    }
}
