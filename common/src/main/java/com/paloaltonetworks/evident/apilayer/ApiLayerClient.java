package com.paloaltonetworks.evident.apilayer;

import org.json.JSONObject;

public interface ApiLayerClient {

    /**
     * Get api layer host
     *
     * @return String containing the apilayer host
     */
    String getApiLayerHost();

    /**
     * Get api layer port
     *
     * @return String containing the apilayer port
     */
    int getApiLayerPort();


    /**
     * Update a document in apilayer
     *
     * @param topic     {@link ApiLayerTopic}the kafka topic name to use to send the data
     * @param tenant    The customer/tenant name to use
     * @param query     the query JsonObject containing required information to filter the document to do the update
     * @param updateDoc the map of key-values indicating the changes.
     * @throws ApiLayerException thrown when apilayer cannot accept the request
     */
    void postUpdate(ApiLayerTopic topic, String tenant, JSONObject query, JSONObject updateDoc) throws ApiLayerException;


    /**
     * Delete a document via apilayer
     *
     * @param topic  {@link ApiLayerTopic}the kafka topic name to use to send the data
     * @param tenant The customer/tenant name to use
     * @param query  the query JsonObject containing required information to filter the document to do the update
     * @throws ApiLayerException thrown when apilayer cannot accept the request
     */
    void postDelete(ApiLayerTopic topic, String tenant, JSONObject query) throws ApiLayerException;

    /**
     * Insert a document via apilayer
     *
     * @param topic     {@link ApiLayerTopic}the kafka topic name to use to send the data
     * @param tenant    The customer/tenant name to use
     * @param query     the query JsonObject containing required information to filter the document to do the update
     * @param updateDoc the map of key-values indicating the changes.
     * @throws ApiLayerException thrown when apilayer cannot accept the request
     */
    void postInsert(ApiLayerTopic topic, String tenant, JSONObject query, JSONObject updateDoc) throws ApiLayerException;

    /**
     * Insert or update a document in apilayer
     *
     * @param topic     {@link ApiLayerTopic}the kafka topic name to use to send the data
     * @param tenant    The customer/tenant name to use
     * @param query     the query JsonObject containing required information to filter the document to do the update
     * @param updateDoc the map of key-values indicating the changes.
     * @throws ApiLayerException thrown when apilayer cannot accept the request
     */
    void postUpsert(ApiLayerTopic topic, String tenant, JSONObject query, JSONObject updateDoc) throws ApiLayerException;
}
