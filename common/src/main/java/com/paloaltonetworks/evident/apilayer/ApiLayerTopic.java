package com.paloaltonetworks.evident.apilayer;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

import static com.paloaltonetworks.evident.apilayer.DefaultApiLayerClient.COMMIT_DB_CASSANDRA;

public enum ApiLayerTopic {

    // add ES in future
    RESOURCES("apilayer-resources", ImmutableSet.<String>builder().add(COMMIT_DB_CASSANDRA).build()),
    EVENTS("apilayer-events", ImmutableSet.<String>builder().add(COMMIT_DB_CASSANDRA).build());


    private final String topic;
    private final Set<String> commitDbs;

    ApiLayerTopic(String topic, Set<String> commitDbs) {
        this.topic = topic;
        this.commitDbs = commitDbs;
    }

    public String getTopic() {
        return topic;
    }

    public Set<String> getCommitDbs() {
        return commitDbs;
    }

    @Override
    public String toString() {
        return topic;
    }

    @VisibleForTesting
    protected enum OpType {
        UPDATE,
        UPSERT,
        INSERT,
        DELETE;
    }

}
