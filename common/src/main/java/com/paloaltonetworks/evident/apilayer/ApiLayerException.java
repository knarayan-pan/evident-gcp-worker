package com.paloaltonetworks.evident.apilayer;

public class ApiLayerException extends Exception {

    public ApiLayerException() {
        super();
    }

    public ApiLayerException(String message) {
        super(message);
    }

    public ApiLayerException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApiLayerException(Throwable cause) {
        super(cause);
    }
}
