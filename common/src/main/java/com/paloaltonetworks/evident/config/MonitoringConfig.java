package com.paloaltonetworks.evident.config;

import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.ScheduledReporter;
import com.paloaltonetworks.aperture.ApertureGarbageCollectorMetricSet;
import com.paloaltonetworks.aperture.ApertureMemoryUsageGaugeSet;
import com.paloaltonetworks.aperture.ApertureSystemMetricSet;
import com.paloaltonetworks.aperture.ApertureThreadStatesGaugeSet;
import com.paloaltonetworks.aperture.consul.ConsulService;
import metrics_influxdb.InfluxdbReporter;
import metrics_influxdb.UdpInfluxdbProtocol;
import metrics_influxdb.api.measurements.KeyValueMetricMeasurementTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.lang.invoke.MethodHandles;
import java.util.concurrent.TimeUnit;

@Configuration
public class MonitoringConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private ConsulService consulService;

    private static String METRICS_HOST_VALUE_DEFAULT = "localhost";
    private static Integer METRICS_PORT_VALUE_DEFAULT = 8126;
    private static String METRICS_HOST_KEY = "global/infra/worker/stats/host";
    private static String METRICS_PORT_KEY = "global/infra/worker/stats/port";

    @Autowired
    public MonitoringConfig(ConsulService consulService) {
        this.consulService = consulService;
    }

    @Bean
    public String metricsHost() {
        String metricsHost = consulService.getValue(METRICS_HOST_KEY, METRICS_HOST_VALUE_DEFAULT);
        LOGGER.debug("metricsHost = {}", metricsHost);
        return metricsHost;
    }

    @Bean
    public int metricsPort() {
        int metricsPort = consulService.getInt(METRICS_PORT_KEY, METRICS_PORT_VALUE_DEFAULT);
        LOGGER.debug("metricsPort = {}", metricsPort);
        return metricsPort;
    }

    /**
     * The metrics will be posted to a telegraf instance which will then be used to convert to prometheus output for the server
     * to scrape the metrics
     */
    @PostConstruct
    public void init() {
        ScheduledReporter reporter = InfluxdbReporter.forRegistry(metricRegistry())
                .protocol(new UdpInfluxdbProtocol(metricsHost(), metricsPort()))
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .filter(MetricFilter.ALL)
                .skipIdleMetrics(false)
                .transformer(new KeyValueMetricMeasurementTransformer())
                .build();
        reporter.start(15, TimeUnit.SECONDS);
    }

    @Bean
    public MetricRegistry metricRegistry() {
        MetricRegistry metricRegistry = new MetricRegistry();
        metricRegistry.registerAll(new ApertureThreadStatesGaugeSet());
        metricRegistry.registerAll(new ApertureMemoryUsageGaugeSet());
        metricRegistry.registerAll(new ApertureGarbageCollectorMetricSet());
        metricRegistry.registerAll(new ApertureSystemMetricSet());
        return metricRegistry;
    }


}
