package com.paloaltonetworks.evident.config;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSCredentialsProviderChain;
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import com.amazonaws.services.sqs.buffered.AmazonSQSBufferedAsyncClient;
import com.amazonaws.services.sqs.buffered.QueueBufferConfig;
import com.paloaltonetworks.aperture.ApertureSqsAsyncClient;
import com.paloaltonetworks.aperture.CachingQueueUrlResolver;
import com.paloaltonetworks.aperture.DefaultApertureSqsAsyncClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import java.lang.invoke.MethodHandles;
import java.util.concurrent.TimeUnit;

@Configuration
public class AwsConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Bean
    @Profile("default")
    public AmazonSQSAsync amazonSqs() {
        AmazonSQSAsync client = AmazonSQSAsyncClientBuilder.standard()
                .withClientConfiguration(new ClientConfiguration()
                        .withMaxConnections(100))
                .withCredentials(credentialsProvider())
                .build();
        QueueBufferConfig queueBufferConfig = new QueueBufferConfig()
                .withMaxDoneReceiveBatches(100)
                .withMaxBatchSize(10)
                .withVisibilityTimeoutSeconds((int) TimeUnit.MINUTES.toSeconds(2));
        return new AmazonSQSBufferedAsyncClient(client, queueBufferConfig);
    }

    @Bean
    @Primary
    public AWSCredentialsProvider credentialsProvider() {
        return new AWSCredentialsProviderChain(InstanceProfileCredentialsProvider.getInstance(),
                new EnvironmentVariableCredentialsProvider());
    }

    @Bean
    public ApertureSqsAsyncClient apertureSqsAsyncClient(@Value("${sqs_queue_prefix}") String queuePrefix,
                                                         AmazonSQSAsync amazonSqs) {
        return new DefaultApertureSqsAsyncClient(queuePrefix, amazonSqs, new CachingQueueUrlResolver(amazonSqs));
    }

}
