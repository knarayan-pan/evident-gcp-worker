package com.paloaltonetworks.evident.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class VaultProperties {

    private final String appId;
    private final String userId;


    public VaultProperties(@Value("${vault.app_id}") String appId, @Value("${vault.user_id}") String userId) {
        this.appId = appId;
        this.userId = userId;
    }
}
