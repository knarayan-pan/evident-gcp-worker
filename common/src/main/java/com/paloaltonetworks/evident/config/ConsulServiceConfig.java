package com.paloaltonetworks.evident.config;

import com.ecwid.consul.v1.ConsulClient;
import com.paloaltonetworks.aperture.consul.ConsulService;
import com.paloaltonetworks.aperture.consul.DefaultConsulService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConsulServiceConfig {

    @Value("${consul_host:consul}")
    private String consulHost;


    @Bean
    public ConsulService consulService() {
        ConsulClient consulClient = new ConsulClient(consulHost);
        return new DefaultConsulService(consulClient);
    }

}
