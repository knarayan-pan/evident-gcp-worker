package com.paloaltonetworks.evident.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.text.SimpleDateFormat;

public class CustomObjectMapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static ObjectMapper mapper = new ObjectMapper();
    public static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    static {
        objectMapper();
    }

    private static ObjectMapper objectMapper() {
        mapper.registerModule(new JavaTimeModule())
                .registerModule(new Jdk8Module())
                .setDateFormat(DATE_TIME_FORMAT)
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return mapper;
    }

    /**
     * Encodes an object, returning null if null is passed in
     *
     * @param obj to encode
     * @return json string, null if null is passed in
     */
    @Nullable
    public static String encode(Object obj) {
        try {
            return obj == null ? null : mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    /**
     * Decodes an object returning null if null is passed in
     *
     * @param json  json to decode
     * @param clazz class to decode to
     * @param <T>   return type
     * @return decoded object, null if null is passed in
     * @throws IOException if is unable to decode
     */
    @Nullable
    public static <T> T decodeValue(String json, Class<T> clazz) throws IOException {
        return json == null ? null : mapper.readValue(json, clazz);
    }
}
