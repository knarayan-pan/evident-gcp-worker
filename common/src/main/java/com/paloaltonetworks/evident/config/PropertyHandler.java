package com.paloaltonetworks.evident.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import java.util.Optional;

@Configuration
@Getter
public class PropertyHandler {
    // account specific information
    @Value("${external_id}")
    private String externalId;
    @Value("${tenant}")
    private String tenant;

    // sqs queue specific information.
    @Value("${sqs_queue_prefix}")
    private String sqsQueuePrefix;
    @Value("${queue_msg_timeout:7200}")
    private Long queueMessageTimeout;
    @Value("${queue_msg_lifetime:1209600}")
    private Long queueMessageLifetime;
    @Value("${queue_max_backoff:4}")
    private Long queueMaxBackoff;

    // consul information
    @Value("${consul_host:consul}")
    private String consulHost;
    @Value("${consul_port:8500}")
    private Integer consulPort;

    public Optional<String> getTenantCsUser() {
        if (StringUtils.isEmpty(tenant)) {
            return Optional.empty();
        } else {
            return Optional.ofNullable(String.format("service@%s.com", tenant.replaceAll("[-+.^:,]", "")));
        }
    }

    public String formattedQueuePrefix() {
        return String.format("%s_%s_", sqsQueuePrefix, externalId);
    }

}
