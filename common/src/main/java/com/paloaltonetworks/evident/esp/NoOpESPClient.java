package com.paloaltonetworks.evident.esp;

import com.paloaltonetworks.evident.config.CustomObjectMapper;
import com.paloaltonetworks.evident.esp.model.AlertRequest;
import com.paloaltonetworks.evident.esp.model.ESPApiResponse;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;

// this will be used as a stub until ESP Full stack implements the API.
@Component(NoOpESPClient.NAME)
public class NoOpESPClient implements ESPClient {
    public static final String NAME = "NoOpESPClient";
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static final String TEMP_CREDS_FILE_PATH = System.getenv("gcp_creds_path");

    @Override
    public ESPApiResponse gcpAccount(String externalAccountId) throws IOException {
        LOGGER.debug("Returning stubbed api-response to receive gcp account information");
        String json = readTempFile(TEMP_CREDS_FILE_PATH);
        return CustomObjectMapper.decodeValue(json, ESPApiResponse.class);
    }

    /**
     * temporary method to read credentials from a local file
     *
     * @param tempCredsFilePath absolute path to the file containing the json credentials
     * @return
     */
    private String readTempFile(String tempCredsFilePath) {
        try (InputStream is = FileUtils.openInputStream(new File(TEMP_CREDS_FILE_PATH))) {
            StringBuilder builder = new StringBuilder();
            int ch;
            while ((ch = is.read()) != -1) {
                builder.append((char) ch);
            }
            return builder.toString();
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public ESPApiResponse createAlert(@Nonnull String externalAccountId, @Nonnull AlertRequest alertRequest) throws IOException {
        LOGGER.info("Stubbing the alert being pushed to ESP Web for request: {}", alertRequest);
        return ESPApiResponse.builder().build();
    }

    @Override
    public ESPApiResponse suppressAlert(@Nonnull String externalAccountId, @Nonnull AlertRequest alertRequest) throws IOException {
        LOGGER.info("Stubbing the alert being suppressed by ESP Web for request: {}", alertRequest);
        return ESPApiResponse.builder().build();
    }
}
