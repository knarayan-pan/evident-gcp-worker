package com.paloaltonetworks.evident.esp.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

/**
 * Inner data model for ESP API /alert request.
 */
@Value
@Builder
public class AlertData {
    @NonNull
    @JsonProperty
    AlertAttributes attributes;
}
