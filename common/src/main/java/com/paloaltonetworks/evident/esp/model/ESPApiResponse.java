package com.paloaltonetworks.evident.esp.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Value;

import java.util.List;
import java.util.Map;

/**
 * Model for response from ESP API.
 */
@Value
@Builder
public class ESPApiResponse {

    @JsonProperty
    ESPApiData data;

    @JsonProperty
    Map<String, String> links;

    @JsonProperty
    List<ESPApiData> included;
}
