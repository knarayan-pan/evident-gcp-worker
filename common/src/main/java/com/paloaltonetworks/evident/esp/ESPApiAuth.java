package com.paloaltonetworks.evident.esp;

import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.val;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Clock;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Optional;

/**
 * OkHttp Interceptor to add esp_web authorization headers to request.
 */
public final class ESPApiAuth implements Interceptor {
    private static final String algorithm = "HmacSHA1";
    private final String accessKeyId;
    private final String secretAccessKeyId;

    ESPApiAuth(@NonNull String accessKeyId, @NonNull String secretAccessKeyId) {
        this.accessKeyId = accessKeyId;
        this.secretAccessKeyId = secretAccessKeyId;
    }

    @Override
    @SneakyThrows(IOException.class)
    public Response intercept(@NonNull Chain chain) {
        val originalReq = chain.request();
        val secretKeySpec = new SecretKeySpec(secretAccessKeyId.getBytes(StandardCharsets.UTF_8), algorithm);
        val bodyOpt = Optional.ofNullable(originalReq.body());
        val bodyString = bodyOpt.map(ESPApiAuth::bodyToString).orElse("null");
        val md5Body = base64MD5(bodyString);
        val date = rfc1123ZonedDateTime();
        val authorization = authString(originalReq, md5Body, date, accessKeyId, secretKeySpec);
        val newRequest = originalReq.newBuilder()
                .header("Authorization", authorization)
                .header("Date", date)
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .header("Content-MD5", md5Body)
                .build();
        return chain.proceed(newRequest);
    }

    @SneakyThrows(IOException.class)
    private static String bodyToString(RequestBody requestBody){
        val buffer = new Buffer();
        requestBody.writeTo(buffer);
        return buffer.readUtf8();
    }

    private String base64MD5(String str) {
        val md5Bytes = DigestUtils.md5(str);
        val base64Encoded = Base64.encodeBase64(md5Bytes);
        return new String(base64Encoded, StandardCharsets.UTF_8);
    }

    private String rfc1123ZonedDateTime() {
        val rfc1123 = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss 'GMT'")
                .withLocale(Locale.US);
        return ZonedDateTime.now(Clock.systemUTC()).format(rfc1123);
    }

    @SneakyThrows({NoSuchAlgorithmException.class, InvalidKeyException.class})
    private String signature(String data, SecretKeySpec secretKeySpec) {
        val mac = Mac.getInstance(algorithm);
        mac.init(secretKeySpec);
        val rawHmac = mac.doFinal(data.getBytes(StandardCharsets.UTF_8));
        return new String(Base64.encodeBase64(rawHmac), StandardCharsets.UTF_8);
    }

    private String authString(Request request,
                              String md5Body,
                              String date,
                              String accessKeyId,
                              SecretKeySpec secretKeySpec) {

        val endpoint = request.url().toString().replaceFirst(".*(?=/api)", "");
        val accept = "application/json";
        val signature = signature(
                String.format("%s,%s,%s,%s,%s", request.method(), accept, md5Body, endpoint, date),
                secretKeySpec
        );
        return String.format("APIAuth %s:%s", accessKeyId, signature);
    }
}
