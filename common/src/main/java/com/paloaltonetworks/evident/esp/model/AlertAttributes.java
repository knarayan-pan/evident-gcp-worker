package com.paloaltonetworks.evident.esp.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

/**
 * Inner attributes model for ESP API /alert request.
 */
@Value
@Builder
public class AlertAttributes {
    @NonNull
    @JsonProperty("external_account")
    ExternalAccount externalAccount;

    @NonNull
    @JsonProperty
    String status;

    @NonNull
    @JsonProperty
    String resource; // resourceId

    @JsonProperty
    Region region;

    @NonNull
    @JsonProperty
    Signature signature;

    @JsonProperty
    String error;

    @NonNull
    @JsonProperty
    Metadata metadata;

    @JsonProperty
    java.util.List<java.util.Map<String, String>> tags;

    @JsonProperty("created_at")
    String createdAt;

    @JsonProperty("deleted_at")
    String deletedAt;

    @Value
    @Builder
    public static class ExternalAccount {
        @NonNull
        @JsonProperty
        String id;

        @NonNull
        @JsonProperty
        String provider;
    }

    @Value
    @Builder
    public static class Region {
        @NonNull
        @JsonProperty
        String code;

        @NonNull
        @JsonProperty
        String name;
    }

    @Value
    @Builder
    public static class Signature {
        @JsonProperty
        String identifier;
    }

    @Value
    @Builder
    public static class Metadata {
        @NonNull
        @JsonProperty
        public java.util.Map<String, Object> details;
    }
}
