package com.paloaltonetworks.evident.esp;

import com.codahale.metrics.MetricRegistry;
import com.google.common.base.Stopwatch;
import com.paloaltonetworks.aperture.MetricConstants;
import com.paloaltonetworks.aperture.Tags;
import com.paloaltonetworks.evident.esp.model.AlertRequest;
import com.paloaltonetworks.evident.esp.model.ESPApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

@Component(MonitoredESPClient.NAME)
public class MonitoredESPClient implements ESPClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    public static final String NAME = "MonitoredESPClient";
    private final MetricRegistry metricRegistry;
    private final ESPClient delegate;

    @Autowired
    public MonitoredESPClient(MetricRegistry metricRegistry, ESPClient espClient) {
        this.metricRegistry = metricRegistry;
        this.delegate = espClient;
    }

    @Override
    public ESPApiResponse gcpAccount(@Nonnull String externalAccountId) throws IOException {
        return monitor(() -> delegate.gcpAccount(externalAccountId), Tags.builder()
                .tag("method", "gcpAccount")
                .tag("externalAccountId", externalAccountId));
    }

    @Override
    public ESPApiResponse createAlert(@Nonnull String externalAccountId, @Nonnull AlertRequest alertRequest) throws IOException {
        return monitor(() -> delegate.createAlert(externalAccountId, alertRequest), Tags.builder()
                .tag("method", "createAlert")
                .tag("externalAccountId", externalAccountId));
    }

    @Override
    public ESPApiResponse suppressAlert(@Nonnull String externalAccountId, @Nonnull AlertRequest alertRequest) throws IOException {
        return monitor(() -> delegate.suppressAlert(externalAccountId, alertRequest), Tags.builder()
                .tag("method", "suppressAlert")
                .tag("externalAccountId", externalAccountId));
    }

    private <T> T monitor(Callable<T> callable, Tags.TagsBuilder tags) {
        Stopwatch timer = Stopwatch.createStarted();
        try {
            T val = callable.call();
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.SUCCCESS_TAG);
            return val;
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            tags.tag(MetricConstants.STATUS_TAG_NAME, MetricConstants.FAILURE_TAG);
            throw new RuntimeException(e);
        } finally {
            timer.stop();
            metricRegistry.timer(tags.build().toMetricName("timer.esp_client"))
                    .update(timer.elapsed(TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS);
        }
    }
}
