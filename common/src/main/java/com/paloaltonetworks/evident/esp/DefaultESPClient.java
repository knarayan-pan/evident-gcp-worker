package com.paloaltonetworks.evident.esp;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Joiner;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.paloaltonetworks.aperture.consul.ConsulService;
import com.paloaltonetworks.evident.config.CustomObjectMapper;
import com.paloaltonetworks.evident.esp.model.AlertRequest;
import com.paloaltonetworks.evident.esp.model.ESPApiResponse;
import com.paloaltonetworks.evident.exceptions.ESPApiException;
import lombok.val;
import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.Dispatcher;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.TlsVersion;
import okhttp3.internal.Util;
import okhttp3.logging.HttpLoggingInterceptor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Primary
@Component
public class DefaultESPClient implements ESPClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    static final String ESP_CLIENT_ACCESS_ID_KEY = "global/config/v1/worker/esp_web/ACCESS_ID";
    static final String ESP_CLIENT_SECRET_KEY_ID_KEY = "global/config/v1/worker/esp_web/SECRET_KEY";
    static final String ESP_FRANKLIN_AUTH_TOKEN_KEY = "global/config/v1/worker/esp_web/FRANKLIN_AUTH_TOKEN";

    // franklin for alerts
    static final String ESP_FRANKLIN_HOST_KEY = "global/config/v1/worker/esp_web/FRANKLIN_HOST";
    private static final String ESP_FRANKLIN_HOST_DEFAULT = "localhost";
    static final String ESP_FRANKLIN_PORT_KEY = "global/config/v1/worker/esp_web/FRANKLIN_PORT";
    // esp web for account info
    static final String ESP_WEB_HOST_KEY = "global/config/v1/worker/esp_web/ESP_WEB_HOST";
    private static final String ESP_WEB_HOST_DEFAULT = "localhost";
    static final String ESP_WEB_PORT_KEY = "global/config/v1/worker/esp_web/ESP_WEB_PORT";

    static final String ESP_CLIENT_HTTP_MAX_POOL_SIZE_KEY = "global/config/v1/worker/esp_web/MAX_HTTP_POOL_SIZE";
    static final int ESP_CLIENT_HTTP_MAX_POOL_SIZE_DEFAULT = 30;

    private static final Integer ESP_FRANKLIN_PORT_DEFAULT = 80;
    private static final String APPLICATION_JSON_TYPE = "application/json";

    private static long OKHTTP_CACHE_SIZE = 10485760; // 10 MB by default
    private static int OKHTTP_CACHE_TIMEOUT = 60; // 1 min by default
    private static final String CACHE_DIR = "tmp/okhttp/espapi";

    private static final String GCP_ACCOUNT_URL_FORMAT = "https://%s/api/v2/external_accounts/%s/google.json_api";
    private static final String GCP_POST_ALERTS_URL_FORMAT = "https://%s/alerts.json";
    private static final String GCP_SUPPRESS_ALERTS_URL_FORMAT = "https://%s/alerts.json";


    private final ConsulService consulService;
    private final Object LOCK = new Object();
    // keeping account specific cache as in future we could use this worker as a generic worker
    private final Cache<String, OkHttpClient> okHttpClientCache;
    private final Cache<String, String> accountESPWebHostCache;

    @Autowired
    DefaultESPClient(ConsulService consulService) {
        this.consulService = consulService;
        okHttpClientCache = CacheBuilder.<String, OkHttpClient>newBuilder().expireAfterAccess(5, TimeUnit.HOURS).build();
        accountESPWebHostCache = CacheBuilder.<String, String>newBuilder().expireAfterAccess(5, TimeUnit.HOURS).build();
    }

    @VisibleForTesting
    Optional<String> getHostAndPort(@Nonnull String externalAccountId, boolean isEspWeb) {
        String key = Joiner.on("_").join(externalAccountId, isEspWeb);
        if (accountESPWebHostCache.getIfPresent(key) != null) {
            return Optional.ofNullable(accountESPWebHostCache.getIfPresent(key));
        } else {
            synchronized (LOCK) {
                if (accountESPWebHostCache.getIfPresent(key) != null) {
                    return Optional.ofNullable(accountESPWebHostCache.getIfPresent(key));
                } else {

                    com.google.common.base.Optional<String> hostOptional = isEspWeb ? consulService.getValue(ESP_WEB_HOST_KEY)
                            : consulService.getValue(ESP_FRANKLIN_HOST_KEY);
                    val port = isEspWeb ? consulService.getInt(ESP_WEB_PORT_KEY, -1) :
                            consulService.getInt(ESP_FRANKLIN_PORT_KEY, -1);
                    if (!hostOptional.isPresent() || port == -1) {
                        LOGGER.error("Error in ESP Franklin configuration. Host and port information missing from consul");
                        return Optional.empty();
                    }
                    String hostPortFormattedResult = String.format("%s:%s", hostOptional.get(), port);
                    accountESPWebHostCache.put(key, hostPortFormattedResult);
                    return Optional.of(hostPortFormattedResult);
                }
            }
        }
    }

    @VisibleForTesting
    Optional<OkHttpClient> getClient(@Nonnull String externalAccountId, boolean isEspWeb) {
        String key = Joiner.on("_").join(externalAccountId, isEspWeb);
        if (okHttpClientCache.getIfPresent(key) != null) {
            return Optional.ofNullable(okHttpClientCache.getIfPresent(key));
        } else {
            synchronized (LOCK) {
                if (okHttpClientCache.getIfPresent(key) != null) {
                    return Optional.ofNullable(okHttpClientCache.getIfPresent(key));
                } else {
                    int connectionPoolSize = consulService.getInt(ESP_CLIENT_HTTP_MAX_POOL_SIZE_KEY, ESP_CLIENT_HTTP_MAX_POOL_SIZE_DEFAULT);
                    ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                            .tlsVersions(TlsVersion.TLS_1_2, TlsVersion.TLS_1_0, TlsVersion.TLS_1_1)
                            .cipherSuites(
                                    CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256
                            )
                            .build();
                    HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
                    loggingInterceptor.setLevel(LOGGER.isTraceEnabled() ?
                            HttpLoggingInterceptor.Level.BODY :
                            HttpLoggingInterceptor.Level.BASIC);
                    OkHttpClient.Builder builder = new OkHttpClient.Builder()
                            .connectionSpecs(Collections.singletonList(spec))
                            .addNetworkInterceptor(new CachingHeaderInterceptor(OKHTTP_CACHE_TIMEOUT))
                            .addInterceptor(loggingInterceptor)
                            .dispatcher(new Dispatcher(new ThreadPoolExecutor(0, connectionPoolSize, 30, TimeUnit.SECONDS,
                                    new SynchronousQueue<>(), Util.threadFactory("ESP Web Http Dispatcher", false))))
                            .cache(new okhttp3.Cache(new File(CACHE_DIR), OKHTTP_CACHE_SIZE));
                    if (isEspWeb) {
                        com.google.common.base.Optional<String> accessIdOptional = consulService.getValue(ESP_CLIENT_ACCESS_ID_KEY);
                        com.google.common.base.Optional<String> secretKeyOptional = consulService.getValue(ESP_CLIENT_SECRET_KEY_ID_KEY);
                        if (!accessIdOptional.isPresent() || !secretKeyOptional.isPresent()) {
                            LOGGER.error("Required credentials to talk to ESP Web is not configured in consul. Skipping creating client");
                            return Optional.empty();
                        }
                        builder.addNetworkInterceptor(new ESPApiAuth(accessIdOptional.get(), secretKeyOptional.get()));
                    } else {
                        // for franklin we only need the auth token
                        String authToken = consulService.getValue(ESP_FRANKLIN_AUTH_TOKEN_KEY).or("");
                        if (StringUtils.isEmpty(authToken)) {
                            LOGGER.error("Required credentials to talk to Franklin is not configured in consul. Skipping creating client");
                            return Optional.empty();
                        }
                        builder.addNetworkInterceptor(new FranklinAuth(authToken));
                    }
                    OkHttpClient client = builder.build();
                    okHttpClientCache.put(key, client);
                    return Optional.of(client);
                }
            }

        }
    }

    // todo: potentially - add another method that returns the credential information to the caller instead of ESPApiResponse

    /**
     * Fetch the owner_email, service_account Id and the json credential blob from ESP web.
     *
     * @param externalAccountId The external account Id that maps to a gcp customer
     * @return {@link ESPApiResponse} object containing the response from ESP web.
     */
    @Override
    public ESPApiResponse gcpAccount(@Nonnull String externalAccountId) throws IOException {
        Optional<OkHttpClient> clientOptional = getClient(externalAccountId, true);
        Optional<String> hostAndPort = getHostAndPort(externalAccountId, true);

        String url = hostAndPort.map(s -> String.format(GCP_ACCOUNT_URL_FORMAT, s, externalAccountId))
                .orElseThrow(() -> {
                    LOGGER.error("Http client not setup correctly for talking to ESP for account: {}. Will retry", externalAccountId);
                    return new ESPApiException(String.format("Http client not setup correctly for talking to ESP for account: %s. Will retry", externalAccountId));
                });
        Response response = null;
        try {
            response = clientOptional.map(client -> client.newCall(new Request.Builder()
                    .get()
                    .url(url)
                    .build()))
                    .orElseThrow(() -> {
                        LOGGER.error("Http client not setup correctly for talking to ESP for account: {}. Will retry", externalAccountId);
                        return new ESPApiException(String.format("Http client not setup correctly for talking to ESP for account: %s. Will retry", externalAccountId));
                    }).execute();

            if (response.isSuccessful() && response.body() != null) {
                return CustomObjectMapper.decodeValue(response.body().string(), ESPApiResponse.class);
            } else {
                LOGGER.error("Received unsuccessful response from ESP Web for fetching GCP account info. AccountId: {}, code: {}", externalAccountId, response.code());
                throw new ESPApiException(String.format("Received unsuccessful response from ESP Web for fetching GCP account info. AccountId: %s, code: %s",
                        externalAccountId, response.code()));
            }
        } finally {
            if (response != null) {
                response.close();
            }
        }

    }

    /**
     * Post an alert for a specific resource + signature to ESP Franklin
     *
     * @param externalAccountId The external Account Id for which this alert needs to be sent
     * @param alertRequest      {@link AlertRequest} object containing the required information to send to Franklin for a
     *                          resource signature state change
     * @return {@link ESPApiResponse} object containing the response from ESP Franklin.
     */
    @Override
    public ESPApiResponse createAlert(@Nonnull String externalAccountId, @Nonnull AlertRequest alertRequest) throws IOException {
        Optional<OkHttpClient> clientOptional = getClient(externalAccountId, false);
        Optional<String> hostAndPort = getHostAndPort(externalAccountId, false);

        String url = hostAndPort.map(s -> String.format(GCP_POST_ALERTS_URL_FORMAT, s))
                .orElseThrow(() -> {
                    LOGGER.error("Http client not setup correctly for talking to ESP for account: {}. Will retry", externalAccountId);
                    return new ESPApiException(String.format("Http client not setup correctly for talking to ESP for account: %s. Will retry", externalAccountId));
                });

        String alertRequestStr = CustomObjectMapper.encode(alertRequest);
        if (StringUtils.isEmpty(alertRequestStr)) {
            LOGGER.error("Error serializing alert request object to post alerts for account: {}", externalAccountId);
            throw new ESPApiException("Error serializing alert request to post alerts");
        }

        Response response = null;
        try {
            response = clientOptional.map(client -> client.newCall(new Request.Builder()
                    .post(RequestBody.create(MediaType.parse(APPLICATION_JSON_TYPE), alertRequestStr))
                    .url(url)
                    .build()))
                    .orElseThrow(() -> {
                        LOGGER.error("Http client not setup correctly for talking to ESP for account: {}. Will retry", externalAccountId);
                        return new ESPApiException(String.format("Http client not setup correctly for talking to ESP for account: %s. Will retry", externalAccountId));
                    }).execute();

            if (response.isSuccessful() && response.body() != null) {
                return CustomObjectMapper.decodeValue(response.body().string(), ESPApiResponse.class);
            } else {
                LOGGER.error("Received unsuccessful response from ESP Web for alert creation. AccountId: {}, code: {}", externalAccountId, response.code());
                throw new ESPApiException(String.format("Received unsuccessful response from ESP Web for alert creation. AccountId: %s, code: %s, reason: %s",
                        externalAccountId, response.code(), response.body().string()));
            }
        } finally {
            if (response != null) {
                response.close();
            }
        }

    }

    /**
     * Post a request to suppress an alert for a specific resource + signature to ESP Franklin
     *
     * @param externalAccountId The external Account Id for which this alert needs to be sent
     * @param alertRequest      {@link AlertRequest} object containing the required information to send to Franklin for a
     *                          resource signature state change
     * @return {@link ESPApiResponse} object containing the response from ESP Franklin.
     */
    @Override
    public ESPApiResponse suppressAlert(@Nonnull String externalAccountId, @Nonnull AlertRequest alertRequest) throws IOException {
        Optional<OkHttpClient> clientOptional = getClient(externalAccountId, false);
        Optional<String> hostAndPort = getHostAndPort(externalAccountId, false);
        if (!clientOptional.isPresent() || !hostAndPort.isPresent()) {
            LOGGER.error("Http client not setup correctly for talking to ESP for account: {}. Will retry", externalAccountId);
            throw new ESPApiException(String.format("Http client not setup correctly for talking to ESP for account: %s. Will retry", externalAccountId));
        }
        String url = hostAndPort.map(s -> String.format(GCP_SUPPRESS_ALERTS_URL_FORMAT, s))
                .orElseThrow(() -> {
                    LOGGER.error("Http client not setup correctly for talking to ESP for account: {}. Will retry", externalAccountId);
                    return new ESPApiException(String.format("Http client not setup correctly for talking to ESP for account: %s. Will retry", externalAccountId));
                });


        String alertRequestStr = CustomObjectMapper.encode(alertRequest);
        if (StringUtils.isEmpty(alertRequestStr)) {
            LOGGER.error("Error serializing alert request object to suppress alerts for account: {}", externalAccountId);
            throw new ESPApiException("Error serializing alert request to suppress alerts");
        }

        Response response = null;
        try {
            response = clientOptional.map(client -> client.newCall(new Request.Builder()
                    .delete(RequestBody.create(MediaType.parse(APPLICATION_JSON_TYPE), alertRequestStr))
                    .url(url)
                    .build()))
                    .orElseThrow(() -> {
                        LOGGER.error("Http client not setup correctly for talking to ESP for account: {}. Will retry", externalAccountId);
                        return new ESPApiException(String.format("Http client not setup correctly for talking to ESP for account: %s. Will retry", externalAccountId));
                    }).execute();

            if (response.isSuccessful() && response.body() != null) {
                return CustomObjectMapper.decodeValue(response.body().string(), ESPApiResponse.class);
            } else {
                LOGGER.error("Received unsuccessful response from ESP Web for alert suppression. AccountId: {}, code: {}", externalAccountId, response.code());
                throw new ESPApiException(String.format("Received unsuccessful response from ESP Web for alert suppression. AccountId: %s, code: %s, reason: %s",
                        externalAccountId, response.code(), response.body().string()));
            }
        } finally {
            if (response != null) {
                response.close();
            }
        }

    }
}
