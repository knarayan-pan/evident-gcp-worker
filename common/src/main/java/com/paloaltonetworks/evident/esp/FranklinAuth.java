package com.paloaltonetworks.evident.esp;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class FranklinAuth implements Interceptor {
    private final String authToken;

    public FranklinAuth(String authToken) {
        this.authToken = authToken;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        HttpUrl url = request.url().newBuilder().addQueryParameter("auth_token", authToken).build();
        return chain.proceed(request.newBuilder().url(url).build());
    }
}
