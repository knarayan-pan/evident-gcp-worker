package com.paloaltonetworks.evident.esp;

import lombok.val;
import okhttp3.Interceptor;
import okhttp3.Response;

import java.io.IOException;

/**
 * OkHttp network Interceptor to add caching header to response.
 */
public final class CachingHeaderInterceptor implements Interceptor {
    private final int timeout;

    public CachingHeaderInterceptor(int seconds) {
        timeout = seconds;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        val originalResp = chain.proceed(chain.request());
        return originalResp.newBuilder()
                .header("Cache-Control", "public, max-age=" + timeout)
                .build();
    }
}
