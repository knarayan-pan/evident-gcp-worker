package com.paloaltonetworks.evident.esp;

import com.paloaltonetworks.evident.esp.model.AlertRequest;
import com.paloaltonetworks.evident.esp.model.ESPApiResponse;

import javax.annotation.Nonnull;
import java.io.IOException;

public interface ESPClient {
    String ADMIN_EMAIL = "email";
    String SERVICE_ACCOUNT_ID = "service_account_id";
    String JSON_CREDS_KEY = "secret";
    String DOMAIN = "domain";

    /**
     * Fetch the owner_email, service_account Id and the json credential blob from ESP web.
     *
     * @param externalAccountId The external account Id that maps to a gcp customer
     * @return {@link ESPApiResponse} object containing the response from ESP web.
     */
    ESPApiResponse gcpAccount(@Nonnull String externalAccountId) throws IOException;

    /**
     * Post an alert for a specific resource + signature to ESP Franklin
     *
     * @param externalAccountId The external Account Id for which this alert needs to be sent
     * @param alertRequest      {@link AlertRequest} object containing the required information to send to Franklin for a
     *                          resource signature state change
     * @return {@link ESPApiResponse} object containing the response from ESP Franklin.
     */
    ESPApiResponse createAlert(@Nonnull String externalAccountId, @Nonnull AlertRequest alertRequest) throws IOException;

    /**
     * Post a request to suppress an alert for a specific resource + signature to ESP Franklin
     *
     * @param externalAccountId The external Account Id for which this alert needs to be sent
     * @param alertRequest      {@link AlertRequest} object containing the required information to send to Franklin for a
     *                          resource signature state change
     * @return {@link ESPApiResponse} object containing the response from ESP Franklin.
     */
    ESPApiResponse suppressAlert(@Nonnull String externalAccountId, @Nonnull AlertRequest alertRequest) throws IOException;
}
