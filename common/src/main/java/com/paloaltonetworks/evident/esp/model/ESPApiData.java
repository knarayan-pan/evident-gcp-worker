package com.paloaltonetworks.evident.esp.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Value;

/**
 * Model for inner "data" field of ESPApiResponse.
 */
@Value
@Builder
public class ESPApiData {
    @JsonProperty
    String id;

    @JsonProperty
    String type;

    @JsonProperty
    java.util.Map<String, Object> attributes;

    @JsonProperty
    java.util.Map<String, Object> relationships;
}
