package com.paloaltonetworks.evident.esp.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

/**
 * Model for ESP API /alert request.
 */
@Value
@Builder
public class AlertRequest {
    @NonNull
    @JsonProperty
    AlertData data;
}
