package com.paloaltonetworks.evident.data_access.service;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.google.common.collect.ImmutableSet;
import com.paloaltonetworks.evident.cassandra.CassandraTemplateFactory;
import com.paloaltonetworks.evident.data_access.models.AccountMetadata;
import com.paloaltonetworks.evident.data_access.models.AccountStatus;
import com.paloaltonetworks.evident.data_access.models.CloudAppType;
import com.paloaltonetworks.evident.data_access.models.ScanStatus;
import org.apache.commons.lang3.RandomStringUtils;
import org.cassandraunit.CQLDataLoader;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.cassandra.core.CassandraTemplate;

import java.util.Date;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class DefaultAccountStatusServiceTest {

    @Mock
    private CassandraTemplateFactory templateFactory;
    private static CassandraTemplate cassandraTemplate;
    private DefaultAccountStatusService accountStatusService;
    private String externalId;

    @BeforeClass
    public static void startCassandraEmbedded() throws Exception {
        // start with default settings
        EmbeddedCassandraServerHelper.startEmbeddedCassandra("cu-cassandra.yaml");
        Cluster cluster = Cluster.builder()
                .addContactPoints("127.0.0.1").withPort(9142).build();
        Session session = cluster.connect();
        new CQLDataLoader(session).load(new ClassPathCQLDataSet("base_schema.sql"));
        cassandraTemplate = new CassandraTemplate(session);

    }

    @AfterClass
    public static void stopCassandraEmbedded() throws Exception {
        EmbeddedCassandraServerHelper.stopEmbeddedCassandra();
    }

    @Before
    public void setUp() throws Exception {
        externalId = RandomStringUtils.random(4);
        MockitoAnnotations.initMocks(this);
        when(templateFactory.getCassandraTemplate(eq("test"))).thenReturn(cassandraTemplate);
        when(templateFactory.getTenantKeyspace()).thenReturn("test");
        accountStatusService = new DefaultAccountStatusService(templateFactory);
    }

    @Test
    public void upsert() throws Exception {
        AccountStatus acc = AccountStatus.builder()
                .cloudAppType(CloudAppType.GCP)
                .onboardingDate(new Date())
                .scanStatus(ScanStatus.not_started)
                .externalAccountId(externalId)
                .dateCreated(new Date()).build();
        accountStatusService.upsert("test", externalId, acc);
        Optional<AccountStatus> status = accountStatusService.getStatus("test", externalId);
        assertTrue(status.isPresent());
        assertEquals(status.get().getScanStatus(), ScanStatus.not_started);
    }

    @Test
    public void getStatus() throws Exception {
    }

    @Test
    public void updateScanStatus() throws Exception {
        AccountStatus acc = AccountStatus.builder()
                .cloudAppType(CloudAppType.GCP)
                .onboardingDate(new Date())
                .scanStatus(ScanStatus.not_started)
                .externalAccountId(externalId)
                .dateCreated(new Date()).build();
        accountStatusService.upsert("test", externalId, acc);
        accountStatusService.updateScanStatus("test", externalId, ScanStatus.scanning);
        Optional<AccountStatus> status = accountStatusService.getStatus("test", externalId);
        assertTrue(status.isPresent());
        assertEquals(status.get().getScanStatus(), ScanStatus.scanning);
    }

    @Test
    public void updateOnboardingDate() throws Exception {
        AccountStatus acc = AccountStatus.builder()
                .cloudAppType(CloudAppType.GCP)
                .onboardingDate(new Date())
                .scanStatus(ScanStatus.not_started)
                .externalAccountId(externalId)
                .dateCreated(new Date()).build();
        accountStatusService.upsert("test", externalId, acc);
        Date onboarding = new Date();
        accountStatusService.updateOnboardingDate("test", externalId, onboarding);
        Optional<AccountStatus> status = accountStatusService.getStatus("test", externalId);
        assertTrue(status.isPresent());
        assertEquals(status.get().getOnboardingDate(), onboarding);
    }

    @Test
    public void updateMetadata() throws Exception {
        AccountStatus acc = AccountStatus.builder()
                .cloudAppType(CloudAppType.GCP)
                .onboardingDate(new Date())
                .scanStatus(ScanStatus.not_started)
                .externalAccountId(externalId)
                .dateCreated(new Date()).build();
        accountStatusService.upsert("test", externalId, acc);
        AccountMetadata metadata = AccountMetadata.builder()
                .orgId("org12345").build();
        accountStatusService.updateMetadata("test", externalId, metadata);
        Optional<AccountStatus> status = accountStatusService.getStatus("test", externalId);
        assertTrue(status.isPresent());
        assertTrue(status.get().getAccountMetadata().isPresent());
        assertEquals(status.get().getAccountMetadata().get(), metadata);
    }

    @Test
    public void updateLastScanEventTime() throws Exception {
        AccountStatus acc = AccountStatus.builder()
                .cloudAppType(CloudAppType.GCP)
                .onboardingDate(new Date())
                .scanStatus(ScanStatus.not_started)
                .externalAccountId(externalId)
                .dateCreated(new Date()).build();
        accountStatusService.upsert("test", externalId, acc);
        Date lastEventScanTime = new Date();
        accountStatusService.updateLastScanEventTime("test", externalId, lastEventScanTime);
        Optional<AccountStatus> status = accountStatusService.getStatus("test", externalId);
        assertTrue(status.isPresent());
        assertEquals(status.get().getLastEventScanTime(), lastEventScanTime);
    }

}