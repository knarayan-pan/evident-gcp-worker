package com.paloaltonetworks.evident.data_access.models;

import com.paloaltonetworks.evident.config.CustomObjectMapper;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class AccountStatusTest {

    @Test
    public void serialization() throws Exception {
        AccountStatus status = AccountStatus.builder().build();
        assertTrue(status.getScanStatus().equals(ScanStatus.not_started));
        assertNotNull(status.getDateUpdated());
        assertNull(status.getMetadata());
        assertNull(status.getDateCreated());
    }

    @Test
    public void serializeCompleteData() throws Exception {
        AccountMetadata met = AccountMetadata.builder()
                .orgId("org12435")
                .build();
        AccountStatus sta = AccountStatus.builder()
                .cloudAppType(CloudAppType.GCP)
                .dateCreated(new Date())
                .onboardingDate(new Date())
                .scanStatus(ScanStatus.scanning)
                .metadata(CustomObjectMapper.encode(met)).build();
        assertNotNull(sta);
        assertNotNull(CustomObjectMapper.encode(sta));
    }

    @Test
    public void deserialization() throws Exception {
        String json = "{\n" +
                "  \"cloudapp_type\": \"GCP\",\n" +
                "  \"scan_status\": \"scanning\",\n" +
                "  \"onboarding_date\": \"2018-07-09T20:16:35Z\",\n" +
                "  \"date_created\": \"2018-07-09T20:16:35Z\",\n" +
                "  \"date_updated\": \"2018-07-09T20:16:35Z\",\n" +
                "  \"metadata\": \"{\\\"organization_id\\\":\\\"org1234\\\"}\"\n" +
                "}";

        AccountStatus status = CustomObjectMapper.decodeValue(json, AccountStatus.class);
        assert status != null;
        assertTrue(status.getScanStatus().equals(ScanStatus.scanning));
        assertNotNull(status.getMetadata());
        assertTrue(status.getAccountMetadata().isPresent());
        assertTrue(status.getAccountMetadata().get().getOrgId().equals("org1234"));
    }
}