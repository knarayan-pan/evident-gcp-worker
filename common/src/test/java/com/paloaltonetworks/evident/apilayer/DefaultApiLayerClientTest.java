package com.paloaltonetworks.evident.apilayer;

import okhttp3.OkHttpClient;
import okhttp3.mock.MockInterceptor;
import okhttp3.mock.Rule;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DefaultApiLayerClientTest {

    private OkHttpClient okHttpClient;
    private DefaultApiLayerClient apiLayerClient;

    @Before
    public void setUp() throws Exception {
        apiLayerClient = new DefaultApiLayerClient("localhost", 8080, okHttpClient);
        MockInterceptor mockInterceptor = new MockInterceptor();
        mockInterceptor.addRule(new Rule.Builder()
                .post().get().delete()
                .url(String.format(DefaultApiLayerClient.API_LAYER_URL_FORMAT, "localhost", 8080))
                .respond(200));
        okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(mockInterceptor)
                .build();
    }

    @After
    public void tearDown() throws Exception {
        okHttpClient = null;
    }

    @Test
    public void getApiLayerHost() throws Exception {
        assertTrue(apiLayerClient.getApiLayerHost().equals("localhost"));
    }

    @Test
    public void getApiLayerPort() throws Exception {
        assertTrue(apiLayerClient.getApiLayerPort() == 8080);
    }

    @Test(expected = IllegalArgumentException.class)
    public void postUpdate() throws Exception {
        apiLayerClient.postUpdate(ApiLayerTopic.RESOURCES, "test", new JSONObject(), new JSONObject());
    }

    @Test(expected = IllegalArgumentException.class)
    public void postDelete() throws Exception {
        apiLayerClient.postDelete(ApiLayerTopic.RESOURCES, "test", new JSONObject());
    }

    @Test(expected = IllegalArgumentException.class)
    public void postInsert() throws Exception {
        apiLayerClient.postInsert(ApiLayerTopic.RESOURCES, "test", new JSONObject(), new JSONObject());
    }

    @Test(expected = IllegalArgumentException.class)
    public void postUpsert() throws Exception {
        apiLayerClient.postUpsert(ApiLayerTopic.RESOURCES, "test", new JSONObject(), new JSONObject());
    }

    @Test
    public void buildPostString() throws Exception {
        String postString = apiLayerClient.buildPostString(ApiLayerTopic.RESOURCES, "test", ApiLayerTopic.OpType.UPDATE,
                new JSONObject().put("k1", "v1"), new JSONObject());
        String expected = "{\"topic\":\"apilayer-resources\",\"message\":{\"cs_user\":\"service@test.com\",\"commit_databases\":[\"cassandra\"],\"task_type\":\"UPDATE\",\"query\":{\"k1\":\"v1\"},\"document\":{}}}";
        assertEquals(expected, postString);
    }

    @Test(expected = NullPointerException.class)
    public void buildPostStringWithEmptyQueryObject() throws Exception {
        apiLayerClient.buildPostString(ApiLayerTopic.RESOURCES, "tenant", ApiLayerTopic.OpType.DELETE, null, null);
    }
}