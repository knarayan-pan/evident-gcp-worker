package com.paloaltonetworks.evident.esp;

import com.google.common.base.Optional;
import com.paloaltonetworks.aperture.consul.ConsulService;
import com.paloaltonetworks.evident.esp.model.AlertAttributes;
import com.paloaltonetworks.evident.esp.model.AlertData;
import com.paloaltonetworks.evident.esp.model.AlertRequest;
import com.paloaltonetworks.evident.exceptions.ESPApiException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class DefaultESPClientTest {

    @Mock
    private ConsulService consulService;
    private DefaultESPClient espClient;
    private AlertRequest request;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        espClient = new DefaultESPClient(consulService);
        when(consulService.getInt(eq(DefaultESPClient.ESP_CLIENT_HTTP_MAX_POOL_SIZE_KEY), anyInt())).thenReturn(20);
        request = AlertRequest.builder()
                .data(AlertData.builder()
                        .attributes(AlertAttributes.builder()
                                .externalAccount(AlertAttributes.ExternalAccount.builder()
                                        .provider("gcp")
                                        .id("1234")
                                        .build())
                                .metadata(AlertAttributes.Metadata.builder()
                                        .details(Collections.emptyMap())
                                        .build())
                                .signature(AlertAttributes.Signature.builder()
                                        .identifier("1234")
                                        .build())
                                .resource("instance-1")
                                .region(AlertAttributes.Region.builder()
                                        .code("reg")
                                        .name("reg-name")
                                        .build())
                                .status("pass")
                                .build())
                        .build())
                .build();
    }

    @Test
    public void getHostAndPort() throws Exception {
        when(consulService.getValue(eq(DefaultESPClient.ESP_FRANKLIN_HOST_KEY))).thenReturn(Optional.absent());
        assertFalse(espClient.getHostAndPort("1234", false).isPresent());
        when(consulService.getValue(eq(DefaultESPClient.ESP_FRANKLIN_HOST_KEY))).thenReturn(Optional.of("localhost"));
        when(consulService.getInt(eq(DefaultESPClient.ESP_FRANKLIN_PORT_KEY), eq(-1))).thenReturn(80);
        assertTrue(espClient.getHostAndPort("1234", false).isPresent());
        assertEquals(espClient.getHostAndPort("1234", false).get(), "localhost:80");
    }


    @Test
    public void getClient() throws Exception {
        when(consulService.getValue(eq(DefaultESPClient.ESP_CLIENT_ACCESS_ID_KEY))).thenReturn(Optional.absent());
        assertFalse(espClient.getClient("1234", true).isPresent());
        when(consulService.getValue(eq(DefaultESPClient.ESP_CLIENT_ACCESS_ID_KEY))).thenReturn(Optional.of("accessKey"));
        when(consulService.getValue(eq(DefaultESPClient.ESP_CLIENT_SECRET_KEY_ID_KEY))).thenReturn(Optional.of("ENC(kjA2)"));
        when(consulService.getInt(eq(DefaultESPClient.ESP_CLIENT_HTTP_MAX_POOL_SIZE_KEY),
                eq(DefaultESPClient.ESP_CLIENT_HTTP_MAX_POOL_SIZE_DEFAULT))).thenReturn(DefaultESPClient.ESP_CLIENT_HTTP_MAX_POOL_SIZE_DEFAULT);
        assertTrue(espClient.getClient("1234", true).isPresent());
    }

    @Test(expected = ESPApiException.class)
    public void gcpAccount() throws Exception {
        when(consulService.getValue(eq(DefaultESPClient.ESP_WEB_HOST_KEY))).thenReturn(Optional.absent());
        when(consulService.getInt(eq(DefaultESPClient.ESP_WEB_PORT_KEY), eq(-1))).thenReturn(80);
        when(consulService.getValue(eq(DefaultESPClient.ESP_CLIENT_ACCESS_ID_KEY))).thenReturn(Optional.absent());
        when(consulService.getValue(eq(DefaultESPClient.ESP_CLIENT_SECRET_KEY_ID_KEY))).thenReturn(Optional.of("ENC(kjA2)"));
        espClient.gcpAccount("1234");
    }

    @Test(expected = ESPApiException.class)
    public void createAlert() throws Exception {
        when(consulService.getValue(eq(DefaultESPClient.ESP_FRANKLIN_HOST_KEY))).thenReturn(Optional.absent());
        when(consulService.getInt(eq(DefaultESPClient.ESP_FRANKLIN_PORT_KEY), eq(-1))).thenReturn(80);
        when(consulService.getValue(eq(DefaultESPClient.ESP_FRANKLIN_AUTH_TOKEN_KEY))).thenReturn(Optional.absent());
        espClient.createAlert("1234", request);
    }

    @Test(expected = ESPApiException.class)
    public void suppressAlert() throws Exception {
        when(consulService.getValue(eq(DefaultESPClient.ESP_FRANKLIN_HOST_KEY))).thenReturn(Optional.absent());
        when(consulService.getInt(eq(DefaultESPClient.ESP_FRANKLIN_PORT_KEY), eq(-1))).thenReturn(80);
        when(consulService.getValue(eq(DefaultESPClient.ESP_FRANKLIN_AUTH_TOKEN_KEY))).thenReturn(Optional.absent());
        espClient.suppressAlert("1234", request);
    }

}