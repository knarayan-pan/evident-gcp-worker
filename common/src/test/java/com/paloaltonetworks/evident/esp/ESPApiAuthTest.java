package com.paloaltonetworks.evident.esp;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.mock.MockInterceptor;
import okhttp3.mock.Rule;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class ESPApiAuthTest {

    private ESPApiAuth espApiAuth;

    @Before
    public void setUp() throws Exception {
        espApiAuth = new ESPApiAuth("accessKeyId", "secretKeyId");
    }

    @Test
    public void intercept() throws Exception {

        MockInterceptor mockInterceptor = new MockInterceptor();
        mockInterceptor.addRule(new Rule.Builder().url("http://localhost/").respond(200));

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(mockInterceptor)
                .addNetworkInterceptor(espApiAuth)
                .addNetworkInterceptor(new MockHeaderInterceptor())
                .build();

        client.newCall(new Request.Builder()
                .post(RequestBody.create(MediaType.parse("application/json"), "{\"test_key\":\"value\"}"))
                .url("http://localhost/")
                .build()).execute();
    }

    private class MockHeaderInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            assertNotNull(request.header("Authorization"));
            assertNotNull(request.header("Date"));
            assertNotNull(request.header("Content-Type"));
            assertNotNull(request.header("Accept"));
            assertNotNull(request.header("Content-MD5"));
            assertTrue(request.header("Authorization").startsWith("APIAuth accessKeyId:"));
            assertEquals(request.header("Content-MD5"), "axnI2z77QEbBVrg2u33jeA==");
            return chain.proceed(request);
        }
    }
}