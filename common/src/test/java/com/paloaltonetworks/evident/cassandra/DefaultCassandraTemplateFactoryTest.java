package com.paloaltonetworks.evident.cassandra;

import com.google.common.base.Optional;
import com.paloaltonetworks.aperture.consul.ConsulService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.cassandra.core.CassandraTemplate;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class DefaultCassandraTemplateFactoryTest {

    @Mock
    private ConsulService consulService;
    private DefaultCassandraTemplateFactory defaultCassandraTemplateFactory;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(consulService.getValue(eq(DefaultCassandraTemplateFactory.CASSANDRA_CLUSTER_KEYSPACE_KEY)))
                .thenReturn(Optional.of("keyspace"));
        when(consulService.getValue(eq(DefaultCassandraTemplateFactory.CASSANDRA_CLUSTER_GLOBAL_KEY))).thenReturn(Optional.of("LOCALHOST"));
        defaultCassandraTemplateFactory = new DefaultCassandraTemplateFactory(consulService);

    }

    @Test
    public void getKeyspace() throws Exception {
        assertEquals(defaultCassandraTemplateFactory.getTenantKeyspace(), "keyspace");
    }

    @Test
    public void getCassandraTemplate() throws Exception {
        CassandraTemplate test = defaultCassandraTemplateFactory.getCassandraTemplate("test");
        assertNotNull(test);
    }

    @Test
    public void getTemplateWithAuthButNoCreds() throws Exception {
        when(consulService.getBoolean(eq(DefaultCassandraTemplateFactory.CASSANDRA_AUTH_ENABLED), eq(Boolean.FALSE))).thenReturn(Boolean.TRUE);
        CassandraTemplate test = defaultCassandraTemplateFactory.getCassandraTemplate("test");
        assertNull(test);
    }

}