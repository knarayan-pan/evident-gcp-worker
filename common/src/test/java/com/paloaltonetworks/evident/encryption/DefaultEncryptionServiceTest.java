package com.paloaltonetworks.evident.encryption;

import com.paloaltonetworks.aperture.ApertureVaultException;
import com.paloaltonetworks.aperture.VaultService;
import com.paloaltonetworks.evident.config.VaultProperties;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

@Ignore
@RunWith(MockitoJUnitRunner.class)
public class DefaultEncryptionServiceTest {

    @Spy
    private VaultProperties vaultProperties = new VaultProperties("appId", "userId");
    @Mock
    private VaultService vaultService;
    private DefaultEncryptionService encryptionService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        encryptionService = new DefaultEncryptionService(vaultProperties, vaultService);
    }

    @Test
    public void testDecrypt() throws ApertureVaultException {
        given(vaultService.decrypt(anyString(), anyString(), eq("appId"), eq("userId"))).willReturn("test");

        assertThat(encryptionService.decrypt("key", "vault:text")).isEqualTo("test");

        then(vaultService).should(times(1)).decrypt("key", "vault:text", "appId", "userId");
    }

    @Test
    public void testDecryptAlreadyDecrypted() throws ApertureVaultException {
        assertThat(encryptionService.decrypt("key", "text")).isEqualTo("text");

        then(vaultService).should(times(0)).decrypt(anyString(), anyString(), anyString());
    }


    @Test
    public void testEncrypt() throws ApertureVaultException {
        given(vaultService.encrypt(anyString(), anyString(), eq("appId"), eq("userId"))).willReturn("test");

        assertThat(encryptionService.encrypt("key", "text")).isEqualTo("test");

        then(vaultService).should(times(1))
                .encrypt("key", "text", "appId", "userId");
    }
}