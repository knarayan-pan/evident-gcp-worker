package com.paloaltonetworks.evident.redis;

import com.paloaltonetworks.aperture.consul.ConsulService;
import com.paloaltonetworks.evident.apilayer.DefaultApiLayerClient;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.mock.MockInterceptor;
import okhttp3.mock.Rule;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static com.paloaltonetworks.evident.redis.DefaultRedisClient.REDIS_PROXY_HOST_DEFAULT;
import static com.paloaltonetworks.evident.redis.DefaultRedisClient.REDIS_PROXY_HOST_TMP_KEY;
import static com.paloaltonetworks.evident.redis.DefaultRedisClient.REDIS_PROXY_PORT_DEFAULT;
import static com.paloaltonetworks.evident.redis.DefaultRedisClient.REDIS_PROXY_PORT_TMP_KEY;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class DefaultRedisClientTest {

    @Mock
    private ConsulService consulService;
    private DefaultRedisClient redisClient;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        when(consulService.getValue(REDIS_PROXY_HOST_TMP_KEY, REDIS_PROXY_HOST_DEFAULT)).thenReturn(REDIS_PROXY_HOST_DEFAULT);
        when(consulService.getInt(REDIS_PROXY_PORT_TMP_KEY, REDIS_PROXY_PORT_DEFAULT)).thenReturn(REDIS_PROXY_PORT_DEFAULT);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void acquireLock() throws Exception {
        MockInterceptor mockInterceptor = new MockInterceptor();
        mockInterceptor.addRule(new Rule.Builder()
                .get()
                .url("http://localhost:10001/api/v1/lock/someid?ttl=50")
                .respond("{\n" +
                        "    \"acquired\": true,\n" +
                        "    \"token\": \"12345\"\n" +
                        "}"));
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .addInterceptor(mockInterceptor).build();
        redisClient = new DefaultRedisClient(okHttpClient, consulService);
        Optional<String> lockOptional = redisClient.acquireLock("someid", 50);
        assertTrue(lockOptional.isPresent());
        assertEquals(lockOptional.get(), "12345");
    }

    @Test
    public void releaseLock() throws Exception {
        MockInterceptor mockInterceptor = new MockInterceptor();
        mockInterceptor.addRule(new Rule.Builder()
                .delete()
                .url("http://localhost:10001/api/v1/lock/someid?token=12345")
                .respond("{\n" +
                        "    \"released\": true\n" +
                        "}"));
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .addInterceptor(mockInterceptor).build();
        redisClient = new DefaultRedisClient(okHttpClient, consulService);
        assertTrue(redisClient.releaseLock("someid", "12345"));
    }

}