package com.paloaltonetworks.evident.redis;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class DefaultRateLimitingServiceTest {

    @Mock
    DefaultRedisClient redisClient;

    private DefaultRateLimitingService rateLimitingService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        rateLimitingService = new DefaultRateLimitingService(redisClient);
    }

    @Test
    public void generateLockPrefix() throws Exception {
        assertEquals("rate_limit_lock_1234_gcp_instances",
                rateLimitingService.generateLockPrefix("1234", "gcp", "instances"));
    }

    @Test
    public void getRateLimitLock() throws Exception {
        // case where the lock was not retrieved
        when(redisClient.acquireLock(eq("rate_limit_lock_1234_gcp_instance"), anyInt())).thenReturn(Optional.empty());
        assertFalse(rateLimitingService.getRateLimitLock("1234", "instance", "gcp",
                1, TimeUnit.SECONDS, 2, 1, TimeUnit.SECONDS));
    }


}